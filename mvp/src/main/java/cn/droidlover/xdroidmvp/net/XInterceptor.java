package cn.droidlover.xdroidmvp.net;

import android.text.TextUtils;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by wanglei on 2016/12/24.
 */

public class XInterceptor implements Interceptor {

    RequestHandler handler;

    String header;

    public XInterceptor(RequestHandler handler, String header) {
        this.handler = handler;
        this.header = header;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        if (!TextUtils.isEmpty(header)) {
            Request request = chain.request()
                    .newBuilder()
                    .addHeader("header", header)
                    .build();
            return chain.proceed(request);
        } else {
            Request request = chain.request();
            if (handler != null) {
                request = handler.onBeforeRequest(request, chain);
            }
            Response response = chain.proceed(request);
            if (handler != null) {
                Response tmp = handler.onAfterRequest(response, chain);
                if (tmp != null) {
                    return tmp;
                }

            }
            return response;
        }

    }
}
