package cn.droidlover.xdroidmvp;

/**
 * Created by mofangwan
 */
public enum LanguageType {

    CHINESE("zh"),
    ENGLISH("en"),
    THAILAND("th");

    private String language;

    LanguageType(String language) {
        this.language = language;
    }

    public String getLanguage() {
        return language == null ? "" : language;
    }
}
