package cn.km.warehouse.entity;

import com.chad.library.adapter.base.entity.JSectionEntity;

public class RukuDanDetailEntity extends JSectionEntity {

    private boolean isHeader;

    private String applyCode;

    private String code;

    private String name;

    private String xinghao;

    private String unit;

    private String cangwei;

    private String huojiahao;

    private String yishou;

    public void setHeader(boolean header) {
        isHeader = header;
    }

    public String getApplyCode() {
        return applyCode;
    }

    public void setApplyCode(String applyCode) {
        this.applyCode = applyCode;
    }

    public RukuDanDetailEntity(boolean isHeader, String applyCode) {
        this.isHeader = isHeader;
        this.applyCode = applyCode;
    }

    public RukuDanDetailEntity(String code, String name, String xinghao, String unit, String cangwei, String huojiahao, String yishou) {
        this.code = code;
        this.name = name;
        this.xinghao = xinghao;
        this.unit = unit;
        this.cangwei = cangwei;
        this.huojiahao = huojiahao;
        this.yishou = yishou;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getXinghao() {
        return xinghao;
    }

    public void setXinghao(String xinghao) {
        this.xinghao = xinghao;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getCangwei() {
        return cangwei;
    }

    public void setCangwei(String cangwei) {
        this.cangwei = cangwei;
    }

    public String getHuojiahao() {
        return huojiahao;
    }

    public void setHuojiahao(String huojiahao) {
        this.huojiahao = huojiahao;
    }

    public String getYishou() {
        return yishou;
    }

    public void setYishou(String yishou) {
        this.yishou = yishou;
    }

    @Override
    public boolean isHeader() {
        return isHeader;
    }
}
