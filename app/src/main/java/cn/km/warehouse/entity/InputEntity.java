package cn.km.warehouse.entity;

public class InputEntity {

    private String title;

    private String status;

    private String company;

    private String applyMan;

    private String applyTime;

    public InputEntity(String title, String status, String company, String applyMan, String applyTime) {
        this.title = title;
        this.status = status;
        this.company = company;
        this.applyMan = applyMan;
        this.applyTime = applyTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getApplyMan() {
        return applyMan;
    }

    public void setApplyMan(String applyMan) {
        this.applyMan = applyMan;
    }

    public String getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(String applyTime) {
        this.applyTime = applyTime;
    }
}
