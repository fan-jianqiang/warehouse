package cn.km.warehouse.uhf.util;

import android.widget.Toast;

import com.magicrf.uhfreaderlib.reader.Tools;
import com.magicrf.uhfreaderlib.reader.UhfReader;

import java.io.ByteArrayOutputStream;
import java.util.Locale;

import cn.km.warehouse.R;
import cn.km.warehouse.base.WorkOAApplication;


public class LabelReadOrWriteUtils {

    /**
     * @param accessPassword
     * @param reader
     * @param memBank
     * @param addr
     * @param length
     * @return read label
     */
    public static String readLabel(byte[] accessPassword, UhfReader reader, int memBank, int addr, int length) {
        //Whether the password is 4 bytes
        if (passwordIs4Bytes(accessPassword)) {
            return "";
        }
        //Read the data area data
        byte[] data = reader.readFrom6C(memBank, addr, length, accessPassword);
        if (data != null && data.length > 1) {
            // String res=new String()
            String dataStr = Tools.Bytes2HexString(data, data.length);
            return dataStr;
        } else {
//            if (data != null) {
//                String dataStr = Tools.Bytes2HexString(data, data.length);
//                return WorkOAApplication.instance().getString(R.string.read_fail) + dataStr;
//            }
//            return WorkOAApplication.instance().getString(R.string.read_fail_re);
            return "";
        }
    }

    /**
     * @param accessPassword
     * @param reader
     * @param memBank
     * @param addr
     * @param writeData
     * @return write label
     */
    public static String writeLabel(byte[] accessPassword, UhfReader reader, int memBank, int addr, String writeData) {
        String data="";
        if (passwordIs4Bytes(accessPassword)) {
            return "";
        }
        //    Log.d("aa", "writeLabel: " + accessPassword + " " + memBank + "" + addr + "" + writeData);
//        if (writeData.length() % 4 != 0) {
//            Toast.makeText(WorkOAApplication.instance(), WorkOAApplication.instance().getResources().getString(R.string.write_length), Toast.LENGTH_SHORT).show();
//        }
//        if (writeData.length() % 4 == 0) {
//
//        }
        data=writeData;
//        if (writeData.length() % 4 == 1) {
//            data="000"+writeData;
//        }
//        if (writeData.length() % 4 == 2) {
//            data="00"+writeData;
//        }if (writeData.length() % 4 == 3) {
//            data="0"+writeData;
//        }
        //byte[] dataBytes = Tools.HexString2Bytes(data);
        byte[] dataBytes = hexStringToBytes(data);
        //dataLen = dataBytes/2 dataLen
        boolean writeFlag = reader.writeTo6C(accessPassword, memBank, addr, dataBytes.length / 2, dataBytes);
        if (writeFlag) {
            return WorkOAApplication.instance().getString(R.string.write_success);
        } else {
            return WorkOAApplication.instance().getString(R.string.write_fail);
        }
    }

    private final static char[] mChars = "0123456789ABCDEF".toCharArray();
    private final static String mHexStr = "0123456789ABCDEF";

    /**
     * 字符串转换成十六进制字符串
     * @param str String 待转换的ASCII字符串
     * @return String 每个Byte之间空格分隔，如: [61 6C 6B]
     */
    public static String str2HexStr(String str){
        StringBuilder sb = new StringBuilder();
        byte[] bs = str.getBytes();

        for (int i = 0; i < bs.length; i++){
            sb.append(mChars[(bs[i] & 0xFF) >> 4]);
            sb.append(mChars[bs[i] & 0x0F]);
            sb.append(' ');
        }
        return sb.toString().trim();
    }

    /**
     * 十六进制字符串转换成 ASCII字符串
     * @param hexStr String Byte字符串
     * @return String 对应的字符串
     */
    public static String hexStr2Str(String hexStr){
        StringBuilder sb = new StringBuilder();
        StringBuilder temp = new StringBuilder();

        hexStr = hexStr.replace(" ", "");
        // 564e3a322d302e312e34 split into two characters 56, 4e, 3a...
        for (int i = 0; i < hexStr.length() - 1; i += 2) {

            // grab the hex in pairs
            String output = hexStr.substring(i, (i + 2));
            // convert hex to decimal
            int decimal = Integer.parseInt(output, 16);
            // convert the decimal to character
            sb.append((char) decimal);

            temp.append(decimal);
        }
        // System.out.println(sb.toString());
        return sb.toString();
    }
    //3  1个0  1  3个0
    private static String hexString = "0123456789ABCDEF";
    /**
     * 字符串转换为16进制字符串
     *
     * @param s
     * @return
     */
    public static String stringToHexString(String s) {
//      String  s1 = s.toString().trim().replace(" ", "");
////      String data="";
////        String str = "";
////        for (int i = 0; i < s1.length(); i++) {
////            int ch = s1.charAt(i);
////            String s4 = Integer.toHexString(ch);
////            str = str + s4;
////        }
////        if (str.length() % 4 == 0) {
////            data=str;
////        }
////        if (str.length() % 4 == 1) {
////            data="000"+str;
////        }
////        if (str.length() % 4 == 2) {
////            data="00"+str;
////        }if (str.length() % 4 == 3) {
////            data="0"+str;
////        }
////        return "1000"+data;
        byte[] bytes =  s.getBytes();
        String hex = bytesToHexString(bytes);
        return hex;
    }
    public static String complementAWriteData(String s){
        return 3000+"AAAAAAAAAAA"+s;
    }
  public static String complementWriteData(String s,int length){
        int digit=length*8+8;
        String PCStr="";
      switch (length) {
          case 1:
              PCStr = "0800";

              break;
          case 2:
              PCStr = "1000";

              break;
          case 3:
              PCStr = "1800";

              break;
          case 4:
              PCStr = "2000";
              break;
          case 5:
              PCStr = "2800";
              break;
          case 6:
              PCStr = "3000";
              break;
          case 7:
              PCStr = "3800";
              break;
          case 8:
              PCStr = "4000";
              break;
          case 9:
              PCStr = "4800";
              break;
          case 10:
              PCStr = "5000";
              break;
          case 11:
              PCStr = "5800";
              break;
          case 12:
              PCStr = "6000";
              break;
          case 13:
              PCStr = "6800";
              break;
          case 14:
              PCStr = "7000";
              break;
          case 15:
              PCStr = "7800";
              break;
          case 16:
              PCStr = "8000";
              break;
      }
      int i = digit - s.length()-4;
      if (i<0){
           return "ERROR";
       }
      StringBuilder stringBuilder=new StringBuilder();
      for (int j=0;j<i;j++){
          stringBuilder.append("0");
      }
      String s1=PCStr+stringBuilder+s;
      return s1;

  }
    /**
     * 16进制字符串转换为字符串
     *
     * @param s
     * @return
     */
    public static String hexStringToString(String s) {
        if (s == null || s.equals("")) {
            return null;
        }
        s = s.replace(" ", "");
//        byte[] baKeyword = new byte[s.length() / 2];
//        for (int i = 0; i < baKeyword.length; i++) {
//            try {
//                baKeyword[i] = (byte) (0xff & Integer.parseInt(
//                        s.substring(i * 2, i * 2 + 2), 16));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        try {
//            s = new String(baKeyword, "gbk");
//            //s = new String(baKeyword, "UTF-8");
//            s = s.trim().replace(" ", "");
//        } catch (Exception e1) {
//            e1.printStackTrace();
//        }
        byte[] bb = hexStringToBytes(s);
        String rr = new String(bb);
        return rr;
    }
    public static String str2HexStrNew(String origin) {
        byte[] bytes = origin.getBytes();
        String hex = bytesToHexString(bytes);
        return hex;
    }

    public static String hexStr2StrNew(String hex) {
        byte[] bb = hexStringToBytes(hex);
        String rr = new String(bb);
        return rr;
    }

    private static String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0) {
            return null;
        }
        for (int i = 0; i < src.length; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }

    private static byte[] hexStringToBytes(String hexString) {
        if (hexString == null || hexString.equals("")) {
            return null;
        }
        hexString = hexString.toUpperCase();
        int length = hexString.length() / 2;
        char[] hexChars = hexString.toCharArray();
        byte[] d = new byte[length];
        for (int i = 0; i < length; i++) {
            int pos = i * 2;
            d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));
        }
        return d;
    }

    /**
     * Convert char to byte
     *
     * @param c char
     * @return byte
     */
    private static byte charToByte(char c) {
        return (byte) "0123456789ABCDEF".indexOf(c);
    }


//    public static String hexStringToString(String s) {
//        if (s == null || s.equals("")) {
//            return null;
//        }
//        s = s.replace(" ", "");
//        byte[] baKeyword = new byte[s.length() / 2];
//        for (int i = 0; i < baKeyword.length; i++) {
//            try {
//                baKeyword[i] = (byte) (0xff & Integer.parseInt(s.substring(i * 2, i * 2 + 2), 16));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        try {
//            s = new String(baKeyword, "UTF-8");
//            new String();
//        } catch (Exception e1) {
//            e1.printStackTrace();
//        }
//        return s;
//    }

    /**
     * @param accessPassword
     * @param reader
     * @param memBank
     * @param lockType
     * @return lock label
     */
    public static String lockLabel(byte[] accessPassword, UhfReader reader, int memBank, int lockType) {
        if (passwordIs4Bytes(accessPassword)) {
            return "";
        }

        boolean lockFlag = reader.lock6C(accessPassword, memBank, lockType);
        if (lockFlag) {
            return WorkOAApplication.instance().getString(R.string.lock_success);
        } else {
            return WorkOAApplication.instance().getString(R.string.lock_fail);
        }
    }

    /**
     * @param killPassword
     * @param reader
     * @return kill label
     */
    public static String killLabel(byte[] killPassword, UhfReader reader) {
        if (passwordIs4Bytes(killPassword)) {
            return "";
        }
        boolean isKillPasswdAllZero = true;
        for (byte b : killPassword) {
            if (b != 0) {
                isKillPasswdAllZero = false;
            }
        }
        if (isKillPasswdAllZero == true) {
            Toast.makeText(WorkOAApplication.instance(), WorkOAApplication.instance().getResources().getString(R.string.inactivat_ps_fail),
                    Toast.LENGTH_SHORT).show();
        }
        boolean lockFlag = reader.kill6C(killPassword);
        if (lockFlag) {
            return WorkOAApplication.instance().getString(R.string.lock_success);
        } else {
            return WorkOAApplication.instance().getString(R.string.lock_fail);
        }
    }

    private static boolean passwordIs4Bytes(byte[] accessPassword) {
        if (accessPassword.length != 4) {
            Toast.makeText(WorkOAApplication.instance(), WorkOAApplication.instance().getResources().getString(R.string.ps_four), Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }
}
