package cn.km.warehouse.uhf;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import com.magicrf.uhfreaderlib.reader.Tools;
import com.magicrf.uhfreaderlib.reader.UhfReader;

import java.util.ArrayList;
import java.util.List;

import cn.km.warehouse.R;
import cn.km.warehouse.base.BaseActivity;
import cn.km.warehouse.uhf.adapter.ListViewAdapter;
import cn.km.warehouse.uhf.bean.EPC;
import cn.km.warehouse.uhf.util.LogUtils;
import cn.km.warehouse.uhf.util.PowerUtil;
import cn.km.warehouse.uhf.util.SPUtils;
import cn.km.warehouse.uhf.util.SoundPoolHelper;

public class UHFMainActivity extends BaseActivity implements View.OnClickListener, AdapterView.OnItemClickListener, CompoundButton.OnCheckedChangeListener {

//
//    private final String TAG="UHFMainActivity";
//    private Button btn_module, btn_start_save, btn_clear, btn_start_save2;
//    private ListView listView;
//    // Serial Port
//    private String serialPortPath="/dev/ttyS2";
//
//    // run thread flag
//    private boolean runFlag=true;
//    // start thread
//    private boolean startFlag=false;
//
//    private List<EPC> dataStr=null;
//    private ListViewAdapter adapter;
//    private ArrayList<EPC> listEPC;
//
//    private UhfReader reader=null;
//    private TextView text_version;
//
//    private DisplayStatusReceiver mDisplayStatusReceiver;
//    private boolean isStartFlag;
//    private RadioButton radio1, radio2;
//
//    private int radio_tag=1;
//    private Thread thread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_main_uhf);
//
//        dataStr=new ArrayList<>();
//        listEPC=new ArrayList<>();
        initView ();
        initData ();

    }

    @Override
    protected void setStatusBar() {
        super.setStatusBar();
    }

    private void initView() {
//
//        btn_module=findViewById (R.id.btn_module);
//        btn_start_save=findViewById (R.id.btn_start_save);
//        btn_start_save2=findViewById (R.id.btn_start_save2);
//        btn_clear=findViewById (R.id.btn_clear);
//        listView=findViewById (R.id.list_item1);
//
//        text_version=findViewById (R.id.text_version);
//
//        radio1=findViewById (R.id.radio1);
//        radio2=findViewById (R.id.radio2);
//
//        btn_module.setOnClickListener (this);
//        btn_start_save.setOnClickListener (this);
//        btn_start_save2.setOnClickListener (this);
//        btn_clear.setOnClickListener (this);
//        listView.setOnItemClickListener (this);
//        radio1.setOnCheckedChangeListener (this);
//        radio2.setOnCheckedChangeListener (this);
//
//        btn_start_save.setEnabled (false);
//        btn_start_save2.setEnabled (false);
//        btn_clear.setEnabled (false);
    }


    private void initData() {

//        UhfReader.setPortPath (serialPortPath);
//        reader= UhfReader.getInstance ();
//
//        if (reader == null) {
//            text_version.setText ("serialport init fail");
//        }
//
//        int dbm=(int) SPUtils.get (this, "DBM", 0);
//        if (dbm != 0) {
//            LogUtils.d (TAG, "initData: " + dbm);
//            reader.setOutputPower (26);
//        }
//        SoundPoolHelper.getInstance (this);
//
//        adapter=new ListViewAdapter (this);
//        registerDisplayStatusReceiver ();
//        thread=new InventoryThread ();
//        thread.start ();
    }

    @Override
    public void onClick(View view) {
//        switch (view.getId ()) {
//            case R.id.btn_module:
//                byte[] versionBytes=reader.getFirmware ();
//                if (versionBytes != null) {
//                    String version=new String(versionBytes);
//                    Log.e (TAG, "onClick: " + version);
//                }
//                SoundPoolHelper.play (1); // play
//                setButtonClickable (btn_module, false);
//                btn_start_save.setEnabled (true);
//                btn_clear.setEnabled (true);
//                btn_start_save2.setEnabled (true);
//                text_version.setText ("open module success");
//                // UhfUtils.openModule();
//                break;
//            case R.id.btn_start_save:
//                if (!startFlag) {
//                    startFlag=true;
//                    btn_start_save.setText (R.string.start_stop);
//                } else {
//                    startFlag=false;
//                    btn_start_save.setText (R.string.start_save);
//                }
//                break;
//            case R.id.btn_start_save2:
//                readLabel ();
//                break;
//            case R.id.btn_clear:
//                clear ();
//                break;
//            default:
//                break;
  //      }
    }


    // clear
    public void clear() {
//        dataStr.clear ();
//        listEPC.clear (); // clear
//        adapter.notifyDataSetChanged ();
    }


    private void setButtonClickable(Button button, boolean flag) {
        button.setClickable (flag);
        if (flag) {
            button.setTextColor (Color.BLACK);
        } else {
            button.setTextColor (Color.GRAY);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
//        startFlag=false;
//        btn_start_save.setText (R.string.start_save);
//        View view1=adapterView.getAdapter ().getView (position, null, listView);
//        TextView itemEpc=view1.findViewById (R.id.item_epc);
//        Intent intent=new Intent(this, DetailActivity.class);
//        intent.putExtra ("epc", itemEpc.getText ().toString ());
//        startActivity (intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater=getMenuInflater ();
        menuInflater.inflate (R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId ()) {
            case R.id.action_settings:
                Intent intent=new Intent(this, SettingPower.class);
                startActivity (intent);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected (item);
    }

    private void registerDisplayStatusReceiver() {
//        mDisplayStatusReceiver=new DisplayStatusReceiver ();
//        IntentFilter screenStatusIF=new IntentFilter();
//        screenStatusIF.addAction (Intent.ACTION_SCREEN_ON);
//        screenStatusIF.addAction (Intent.ACTION_SCREEN_OFF);
//        registerReceiver (mDisplayStatusReceiver, screenStatusIF);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//        switch (compoundButton.getId ()) {
//            case R.id.radio1:
//                startFlag=false;
//                btn_start_save.setText (R.string.start_save);
//                //      clear();
//                btn_start_save2.setVisibility (View.GONE);
//                btn_start_save.setVisibility (View.VISIBLE);
//                break;
//            case R.id.radio2:
//                //    clear();
//                btn_start_save2.setVisibility (View.VISIBLE);
//                btn_start_save.setVisibility (View.GONE);
//                break;
//        }
    }

    // ========================================================Save operation=======================================================

    private List<byte[]> epcList;

    public void readLabel() {
//        epcList=reader.inventoryRealTime (); //Real-// time inventory
//        if (epcList != null && !epcList.isEmpty ()) {
//            //play volume
//            SoundPoolHelper.play (1);
//            for (byte[] epc : epcList) {
//                String epcStr= Tools.Bytes2HexString (epc, epc.length);
//                // Log.e(TAG, "run: " + epcStr);
//                addToList (listEPC, epcStr);
//            }
//        }
    }

    /**
     * save read Label data thread
     *
     * @author Administrator
     */
    class InventoryThread extends Thread {

        @Override
        public void run() {
            super.run ();
//            LogUtils.d ("radio_tag", "radio_tag：" + radio_tag);
//            while (runFlag) {
//                //  Log.e(TAG, "run: " + "ThreadStart");
//                if (startFlag) {
//                    //	reader.stopInventoryMulti()
//                    readLabel ();
//                    try {
//                        Thread.sleep (80);
//                    } catch (InterruptedException e) {
//                        // TODO Auto-generated catch block
//                        e.printStackTrace ();
//                    }
//                }
//            }

        }
    }

    // Add the read EPC to the list
    private void addToList(final List<EPC> list, final String epc) {
        runOnUiThread (new Runnable() {
            @Override
            public void run() {
//                dataStr.clear ();
//                int idCount=1;
//
//                // read firstTime
//                if (list.isEmpty ()) {
//                    EPC epcTag=new EPC ();
//                    epcTag.setEpc (epc);
//                    list.add (epcTag);
//                } else {
//                    for (int i=0; i < list.size (); i++) {
//                        EPC mEPC=list.get (i);
//                        // list contains EPC
//                        if (epc.equals (mEPC.getEpc ())) {
//                            mEPC.setCount (mEPC.getCount () + 1);
//                            list.set (i, mEPC);
//                            break;
//                        } else if (i == (list.size () - 1)) {
//                            // list not contains epc
//                            EPC newEPC=new EPC ();
//                            newEPC.setEpc (epc);
//                            list.add (newEPC);
//                        }
//                    }
//                }
//                for (EPC epcData : list) {
//                    EPC epc1=new EPC ();
//                    epc1.setId (idCount);
//                    epc1.setEpc (epcData.getEpc ());
//                    epc1.setCount (epcData.getCount () + 1);
//                    idCount++;
//                    dataStr.add (epc1);
//                }
//
//                adapter.setData (dataStr);
//                listView.setAdapter (adapter);
//
          }
        });
    }

    class DisplayStatusReceiver extends BroadcastReceiver {
        String SCREEN_ON="android.intent.action.SCREEN_ON";
        String SCREEN_OFF="android.intent.action.SCREEN_OFF";

        @Override
        public void onReceive(Context context, Intent intent) {
//            if (SCREEN_ON.equals (intent.getAction ())) {
//                isStartFlag=(boolean) SPUtils.get (context, "startFlag", true);
//                startFlag=isStartFlag;
//            } else if (SCREEN_OFF.equals (intent.getAction ())) {
//                SPUtils.remove (context, "startFlag");
//                SPUtils.put (context, "startFlag", startFlag);
//                startFlag=false;
//
//            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume ();
//        PowerUtil.power ("1");
//        int value=(int) SPUtils.get (this, "DBM", 0);
//        if (value != 0) {
//            reader.setOutputPower (value);
//        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy ();
//        PowerUtil.power ("0");
//        runFlag=false;
//        if (reader != null) {
//            reader.close ();
//        }
//        unregisterReceiver (mDisplayStatusReceiver);
    }
}
