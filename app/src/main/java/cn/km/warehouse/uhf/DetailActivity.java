package cn.km.warehouse.uhf;

import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;


import androidx.appcompat.app.AppCompatActivity;

import com.magicrf.uhfreaderlib.reader.Tools;
import com.magicrf.uhfreaderlib.reader.UhfReader;

import cn.km.warehouse.R;
import cn.km.warehouse.uhf.util.LabelReadOrWriteUtils;


public class DetailActivity extends AppCompatActivity {
//
//    private TextView text_epc;
//    private Button btn_write, btn_read, btn_readClear, btn_back, btn_lock_6c, btn_kill_6c;
//    private Spinner spinner_lock_memspace, spinner_lock_type, spinner_membank;
//    private EditText edit_password, edit_addr, edit_length, edit_kill_password, edit_readData, edittext_write;
//
//
//    // Spinner data
//    private final String[] strMemBank={"RESERVE", "EPC", "TID", "USER"};
//    private final String[] strLockMemSpace={"Kill Pwd", "Access Pwd", "EPC", "TID", "USER"};
//
//
//    private final String TAG="DetailActivity";
//    private String ecp=""; // Receives data from the previous activity
//
//
//    // serial port
//    private String uartPort="/dev/ttyS2";
//    // emissivity
//    private int emissivity=26;
//    private UhfReader reader;
//
//    private int memBank;
//    private int lockType;
//    private int lock_memspace;
//
//    private int addr=0;
//    private int length=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_detail);

      //  initView ();
     //   initSpinner ();
    }

//
//    private void initView() {
//
//
//        UhfReader.setPortPath (uartPort);
//        reader= UhfReader.getInstance ();
//        reader.setOutputPower (emissivity);
//        reader.selectEpc (Tools.HexString2Bytes (ecp));
//
//
//        text_epc=findViewById (R.id.text_epc);
//        btn_write=findViewById (R.id.btn_write);
//        btn_read=findViewById (R.id.btn_read);
//        btn_readClear=findViewById (R.id.btn_readClear);
//        btn_back=findViewById (R.id.btn_back);
//        btn_lock_6c=findViewById (R.id.btn_lock_6c);
//        btn_kill_6c=findViewById (R.id.btn_kill_6c);
//
//        spinner_lock_memspace=findViewById (R.id.spinner_lock_memspace);
//        spinner_lock_type=findViewById (R.id.spinner_lock_type);
//        spinner_membank=findViewById (R.id.spinner_membank);
//
//        edit_password=findViewById (R.id.edit_password);
//        edit_addr=findViewById (R.id.edit_addr);
//        edit_length=findViewById (R.id.edit_length);
//        edit_kill_password=findViewById (R.id.edit_kill_password);
//        edit_readData=findViewById (R.id.edit_readData);
//        edittext_write=findViewById (R.id.edittext_write);
//        ecp=getIntent ().getStringExtra ("epc");
//        text_epc.setText (ecp.trim ());
//
//        // listener
//        btn_write.setOnClickListener (this);
//        btn_read.setOnClickListener (this);
//        btn_back.setOnClickListener (this);
//        btn_kill_6c.setOnClickListener (this);
//        btn_lock_6c.setOnClickListener (this);
//        btn_readClear.setOnClickListener (this);
//
//
//    }

//    private void initSpinner() {
//        ArrayAdapter<String> adapter1=new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, strMemBank);
//        ArrayAdapter<String> adapter2=new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, strLockMemSpace);
//        ArrayAdapter<CharSequence> adapter3= ArrayAdapter.createFromResource (this, R.array.arr_lockType, android.R.layout.simple_spinner_item);
//
//        adapter1.setDropDownViewResource (android.R.layout.simple_spinner_dropdown_item);
//        adapter2.setDropDownViewResource (android.R.layout.simple_spinner_dropdown_item);
//        adapter3.setDropDownViewResource (android.R.layout.simple_spinner_dropdown_item);
//
//        spinner_membank.setAdapter (adapter1);
//        spinner_lock_memspace.setAdapter (adapter2);
//        spinner_lock_type.setAdapter (adapter3);
//
//        spinner_membank.setOnItemSelectedListener (this);
//        spinner_lock_memspace.setOnItemSelectedListener (this);
//        spinner_lock_type.setOnItemSelectedListener (this);
//    }
//
//    @Override
//    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
//        switch (adapterView.getId ()) {
//            case R.id.spinner_membank:
//                memBank=position;
//                break;
//            case R.id.spinner_lock_memspace:
//                lock_memspace=position;
//                break;
//            case R.id.spinner_lock_type:
//                lockType=position;
//                break;
//            default:
//                break;
//        }
//    }
//
//    @Override
//    public void onNothingSelected(AdapterView<?> adapterView) {
//
//    }
//
//    @Override
//    public void onClick(View view) {
//        byte[] accessPassword= Tools.HexString2Bytes (edit_password.getText ().toString ());
//        byte[] killPassword= Tools.HexString2Bytes (edit_kill_password.getText ().toString ());
//        addr= Integer.valueOf (edit_addr.getText ().toString ());
//        length= Integer.valueOf (edit_length.getText ().toString ());
//        switch (view.getId ()) {
//            case R.id.btn_read:
//                reader.selectEpc (Tools.HexString2Bytes (ecp));
//                String readState= LabelReadOrWriteUtils.readLabel (accessPassword, reader, memBank, addr, length);
//                if (!"".equals (readState)) {
//                    edit_readData.append (readState + "\n");
//                }
//                break;
//            case R.id.btn_write:
//                reader.selectEpc (Tools.HexString2Bytes (ecp));
//                String writeState=LabelReadOrWriteUtils.writeLabel (accessPassword, reader, memBank, addr, edittext_write.getText ().toString ());
//                if (!"".equals (writeState)) {
//                    edit_readData.append (writeState + "\n");
//                }
//                break;
//            case R.id.btn_lock_6c:
//                reader.selectEpc (Tools.HexString2Bytes (ecp));
//                Log.e (TAG, "lock_memspace= " + lock_memspace + " :lockType= " + lockType);
//                String lockState=LabelReadOrWriteUtils.lockLabel (accessPassword, reader, lock_memspace, lockType);
//                if (!"".equals (lockState)) {
//                    edit_readData.append (lockState + "\n");
//                }
//                break;
//            case R.id.btn_kill_6c:
//                reader.selectEpc (Tools.HexString2Bytes (ecp));
//                String killState=LabelReadOrWriteUtils.killLabel (killPassword, reader);
//                if (!"".equals (killState)) {
//                    edit_readData.append (killState + "\n");
//                }
//                break;
//            case R.id.btn_back:
//                finish ();
//                break;
//            case R.id.btn_readClear:
//                edit_readData.setText ("");
//                break;
//            default:
//                break;
//        }
//    }


}
