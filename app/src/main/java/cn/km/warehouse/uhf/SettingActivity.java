package cn.km.warehouse.uhf;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.magicrf.uhfreaderlib.reader.UhfReader;

import cn.km.warehouse.R;
import cn.km.warehouse.uhf.util.SPUtils;

public class SettingActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {

//    private final String TAG = "SettingActivity";
//
//    private Spinner spinner1, spinner2, spinner3;
//    private EditText editText;
//    private TextView text_tips;
//    private Button btn_setting1, btn_setting2, btn_setting3, btn_setting4, btn_restore;
//
//    // adapter
//    private ArrayAdapter<String> adapterSensitive;
//    private ArrayAdapter<String> adapterPower;
//    private ArrayAdapter<String> adapterArea;
//
//    // datas
//    private String[] powers = null;
//    private String[] sensitives = null;
//    private String[] areas = null;
//
//    // tmp data
//    private int tmp_sensitive = 0;
//    private int tmp_powers = 0;
//    private int tmp_areas = 0;
//    private int tmp_frequency = 0;
//
//    private UhfReader reader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
//        reader = UhfReader.getInstance();
//        initView();
//        initSpinner();
    }

//
//    private void initView() {
//        spinner1 = findViewById(R.id.spinner1);
//        spinner2 = findViewById(R.id.spinner2);
//        spinner3 = findViewById(R.id.spinner3);
//
//        editText = findViewById(R.id.edit_text);
//        text_tips = findViewById(R.id.text_Tips);
//
//        btn_setting1 = findViewById(R.id.btn_setting1);
//        btn_setting2 = findViewById(R.id.btn_setting2);
//        btn_setting3 = findViewById(R.id.btn_setting3);
//        btn_setting4 = findViewById(R.id.btn_setting4);
//        btn_restore = findViewById(R.id.btn_restore);
//        // ========================listener================================
//        btn_setting1.setOnClickListener(this);
//        btn_setting2.setOnClickListener(this);
//        btn_setting3.setOnClickListener(this);
//        btn_setting4.setOnClickListener(this);
//        btn_restore.setOnClickListener(this);
//
//        spinner1.setOnItemSelectedListener(this);
//        spinner2.setOnItemSelectedListener(this);
//        spinner3.setOnItemSelectedListener(this);
//        // get fill spinner res
//        sensitives = getResources().getStringArray(R.array.arr_sensitivity);
//        areas = getResources().getStringArray(R.array.arr_area);
//        powers = getResources().getStringArray(R.array.powers);
//    }

//    private void initSpinner() {
//        adapterSensitive = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, sensitives);
//        adapterPower = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, powers);
//        adapterArea = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, areas);
//
//        spinner1.setAdapter(adapterSensitive);
//        spinner2.setAdapter(adapterPower);
//        spinner3.setAdapter(adapterArea);
//
//        // get SharedPreferences data
//        tmp_powers = (int) SPUtils.get(this, "tmp_powers", 0);
//        tmp_areas = (int) SPUtils.get(this, "tmp_areas", 0);
//        tmp_frequency = (int) SPUtils.get(this, "tmp_frequency", 0);
//        tmp_sensitive = (int) SPUtils.get(this, "tmp_sensitive", 0);
//
//        spinner1.setSelection(tmp_sensitive);
//        spinner2.setSelection(tmp_powers);
//        spinner3.setSelection(tmp_areas);
//        editText.setText(tmp_frequency + "");
//
//    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
//        switch (adapterView.getId()) {
//            case R.id.spinner1:
//                tmp_sensitive = position;
//                text_tips.setText(adapterView.getSelectedItem().toString());
//                break;
//            case R.id.spinner2:
//                tmp_powers = position;
//                text_tips.setText(adapterView.getSelectedItem().toString());
//                break;
//            case R.id.spinner3:
//                tmp_areas = position;
//                text_tips.setText(adapterView.getSelectedItem().toString());
//                break;
//        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onClick(View view) {
//        switch (view.getId()) {
//            case R.id.btn_setting1:
//                reader.setSensitivity(tmp_sensitive);
//                SPUtils.put(this, "tmp_sensitive", tmp_sensitive);
//                break;
//            case R.id.btn_setting2:
//                Boolean outputPower = reader.setOutputPower(tmp_powers);
//                if (outputPower) {
//                    Toast.makeText(this, getString(R.string.setting_ok), Toast.LENGTH_SHORT).show();
//                    SPUtils.put(this, "tmp_powers", tmp_powers);
//                } else {
//                    Toast.makeText(this, getString(R.string.setting_error), Toast.LENGTH_SHORT).show();
//                }
//                break;
//            case R.id.btn_setting3:
//                reader.setWorkArea(tmp_areas);
//               // StateMsg(workArea_state, 0);
//                break;
//            case R.id.btn_setting4:
//                if (editText.getText().toString().isEmpty()) {
//                    Toast.makeText(this, getString(R.string.toast_text), Toast.LENGTH_SHORT).show();
//                } else {
//                    tmp_frequency = Integer.parseInt(editText.getText().toString());
//                    int frequency_state = reader.setFrequency(tmp_frequency, 0, 0);
//                    StateMsg(frequency_state, 1);
//                }
//                break;
//            case R.id.btn_restore:
//                SPUtils.clear(this);
//                editText.setText("");
//                spinner1.setAdapter(adapterSensitive);
//                spinner2.setAdapter(adapterPower);
//                spinner3.setAdapter(adapterArea);
//                break;
//        }
    }

    private void StateMsg(int State, int type) {
//        Log.d(TAG, "onClick: " + State);
//        if (State == 0) {
//            Toast.makeText(this, getString(R.string.setting_ok), Toast.LENGTH_SHORT).show();
//            if (type == 0) {
//                SPUtils.put(this, "tmp_areas", tmp_areas);
//            } else if (type == 1) {
//                SPUtils.put(this, "tmp_frequency", tmp_frequency);
//            }
//        } else {
//            Toast.makeText(this, getString(R.string.setting_error), Toast.LENGTH_SHORT).show();
//        }
    }
}
