package cn.km.warehouse.uhf.util;


import android.content.Context;

import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import cn.km.warehouse.R;


public class TopToolbar extends Toolbar implements View.OnClickListener{

    private ImageButton btn_left;
    private TextView tv_title;
    private ImageButton btn_reght;
    private MenuToolBarListener menuToolBarListener;



    public TopToolbar(Context context) {
        this(context,null);
    }

    public TopToolbar(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public TopToolbar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(context);
    }

    private void init(Context context) {
        LayoutInflater.from(context).inflate(R.layout.layout_top_toolbar, this);
        btn_left = findViewById(R.id.btn_htb_left);
        tv_title = findViewById(R.id.tv_htb_title);
        btn_reght = findViewById(R.id.btn_htb_right);

        btn_left.setOnClickListener(this);
        btn_reght.setOnClickListener(this);
    }


    // Set the content of the middle title
    public void setMainTitle(int text) {
        this.setTitle(" ");
        tv_title.setText(text);
    }

    // Set the content of the middle title
    public void setMainTitle(String text) {
        this.setTitle(" ");
        tv_title.setText(text);
    }

    // Sets the color of the content text of the middle title
    public void setMainTitleColor(int color) {
        tv_title.setTextColor(color);
    }


    // Set the icon to the left of the title
    public void setLeftTitleDrawable(int id) {
        btn_left.setImageResource(id);
    }

    // Set the icon to the left of the title
    public void setLeftTitleVisibility(int visible) {
        btn_left.setVisibility(View.GONE);
    }

    // Set the icon to the right of the title
    public void setRightTitleDrawable(int id) {
        btn_reght.setImageResource(id);
    }

    // Set the icon to the right of the title
    public void setRightTitleVisibility(int visible) {
        btn_reght.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {

        if(menuToolBarListener == null)return;

        switch (v.getId()){
            case R.id.btn_htb_left:
                menuToolBarListener.onToolBarClickLeft(v);
                break;
            case R.id.btn_htb_right:
                menuToolBarListener.onToolBarClickRight(v);
                break;
            default:
                break;
        }
    }

    public void setMenuToolBarListener(MenuToolBarListener listener){
        menuToolBarListener = listener;
    }

    public interface MenuToolBarListener {
        public void onToolBarClickLeft(View v);
        public void onToolBarClickRight(View v);
    }

}
