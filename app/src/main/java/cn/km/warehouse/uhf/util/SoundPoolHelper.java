package cn.km.warehouse.uhf.util;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

import cn.km.warehouse.R;


public class SoundPoolHelper {

  public static SoundPool sp;
  public static Context context;

  //Initialize
  public static SoundPool getInstance(Context context) {
    SoundPoolHelper.context = context;
    if (sp == null) {
      sp = new SoundPool(1, AudioManager.STREAM_MUSIC, 1);
    }
    sp.load(context, R.raw.msg, 1);
    return sp;
  }

  //play
  public static void play(int soundID) {
    sp.play(soundID, 1, 1, 0, 0, 1);
  }
}
