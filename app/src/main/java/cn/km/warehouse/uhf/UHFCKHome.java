package cn.km.warehouse.uhf;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.magicrf.uhfreaderlib.reader.Tools;
import com.magicrf.uhfreaderlib.reader.UhfReader;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import cn.km.warehouse.base.BaseActivity;
import cn.km.warehouse.event.ScanEvent;
import cn.km.warehouse.uhf.bean.EPC;
import cn.km.warehouse.uhf.util.LabelReadOrWriteUtils;
import cn.km.warehouse.uhf.util.LogUtils;
import cn.km.warehouse.uhf.util.PowerUtil;
import cn.km.warehouse.uhf.util.SPUtils;
import cn.km.warehouse.uhf.util.SoundPoolHelper;
import cn.km.warehouse.ui.CKSCANActivity;
import cn.km.warehouse.ui.RlrkActivity;
import cn.km.warehouse.ui.customview.BaseDialog;
import cn.km.warehouse.ui.customview.MessageDialog;
import cn.km.warehouse.utils.TimeUtil;
import cn.km.warehouse.widget.CustomProgressDialog;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/13
 */
public class UHFCKHome  extends BaseActivity {
    private String wirteData;
    private Context context;
    private final String TAG = "UHFMainActivity";
    private String serialPortPath = "/dev/ttyS2";
    private boolean runFlag = true;
    private boolean startFlag = false;
    private List<EPC> dataStr = null;
    private ArrayList<EPC> listEPC;
    private UhfReader reader = null;
    private DisplayStatusReceiver mDisplayStatusReceiver;
    private boolean isStartFlag;
    private int radio_tag = 1;
    private Thread thread;
    private String ecp = "";
    private String Rfid;

    public UHFCKHome(Context context,UhfReader reader) {
        this.context = context;
        startFlag = false;
        this.reader=reader;
        dataStr = new ArrayList<>();
        listEPC = new ArrayList<>();
        initData();

    }

    private boolean isChange = false;

    public Boolean getIsChange() {
        return isChange;
    }

    private void initData() {
        showLoading();
        thread=new InventoryThread();
        thread.start ();
        readmodel();
    }

    public void readmodel() {
        try {
            byte[] versionBytes = reader.getFirmware();
            if (versionBytes != null) {
                String version = new String(versionBytes);
                Log.e(TAG, "onClick: " + version);
            }
            SoundPoolHelper.play(1); // play
            startFlag = true;
        } catch (Exception e) {
            dismissLoading();
            Toast.makeText(context, "请重新扫描", Toast.LENGTH_SHORT).show();
        }

    }

    private void registerDisplayStatusReceiver() {
        mDisplayStatusReceiver = new DisplayStatusReceiver();
        IntentFilter screenStatusIF = new IntentFilter();
        screenStatusIF.addAction(Intent.ACTION_SCREEN_ON);
        screenStatusIF.addAction(Intent.ACTION_SCREEN_OFF);
        context.registerReceiver(mDisplayStatusReceiver, screenStatusIF);
    }

    private List<byte[]> epcList;

    public void readLabel() {
        try {
            epcList=reader.inventoryRealTime (); //Real-// time inventory
            if (epcList != null && !epcList.isEmpty ()) {
                SoundPoolHelper.play (1);
                String epcStr="";
                for (byte[] epc : epcList) {

                    if(epc==null){
                        continue;
                    }
                    // byte[] bytes = epcList.get(epcList.size()-1);
                    epcStr=Tools.Bytes2HexString (epc, epc.length);

                    break;
                }
                if (epcStr==null || "".equals(epcStr)){
                    MessageDialog dialog = new MessageDialog(context, "该标签读取数据失败，是否重新读取？");
                    dialog.setPositiveButton("确定", new BaseDialog.OnDialogButtonClickListener() {
                                @Override
                                public void onClick(Dialog dialog, View v) {
                                    thread.interrupt();;
                                    initData();
                                }

                            }
                    ).show();
                    dialog.setNegativeButton("取消", new BaseDialog.OnDialogButtonClickListener() {
                        @Override
                        public void onClick(Dialog dialog, View v) {
                            dismissLoading();
                        }
                    });

                    return;
                }
                String readData="";

                if (!epcStr.startsWith("AAAAAAAAAA")){
                    readData = LabelReadOrWriteUtils.hexStringToString(epcStr);
                }else {
                    readData=epcStr;
                }

                ecp = epcStr;


                //todo 这是读取的参数是1_物料编码AAAAAAAAAAA1629165168847的判断
                if (!"".equals(readData)) {
                    int b = readData.indexOf("AAAAAAAAAAA");
                    if ( b==-1) {
                        MessageDialog dialog = new MessageDialog(context, "该标签读取数据失败，是否重新读取？");
                        dialog.setPositiveButton("确定", new BaseDialog.OnDialogButtonClickListener() {
                                    @Override
                                    public void onClick(Dialog dialog, View v) {
                                        thread.interrupt();;
                                        initData();
                                    }

                                }
                        ).show();
                        dialog.setNegativeButton("取消", new BaseDialog.OnDialogButtonClickListener() {
                            @Override
                            public void onClick(Dialog dialog, View v) {
                                dismissLoading();
                            }
                        });
                        return;
                    }

                    Rfid=readData.substring(b+11, readData.length());
                    EventBus.getDefault().post(new ScanEvent(Rfid,ecp));

                }
            }else {
                Looper.prepare();
                Toast.makeText(context,"扫描标签失败，请您重新扫描",Toast.LENGTH_SHORT).show();
                dismissLoading();
//            Toast.makeText(context, "请重新扫描", Toast.LENGTH_SHORT).show();
                // Toast.makeText(getApplicationContext(), "登录成功", Toast.LENGTH_SHORT).show();
                Looper.loop();
                // Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();

            }
        }catch (Exception e){
            Looper.prepare();
            dismissLoading();
            Looper.loop();
            Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
        }

    }
//    public void readLabel() {
//        try {
//            epcList = reader.inventoryRealTime(); //Real-// time inventory
//            if (epcList != null && !epcList.isEmpty()) {
//                //play volume
//                SoundPoolHelper.play(1);
//                for (byte[] epc : epcList) {
//                    String epcStr = Tools.Bytes2HexString(epc, epc.length);
//                    addToList(listEPC, epcStr);
//                }
//            }else {
//                Looper.prepare();
//                dismissLoading();
//                Toast.makeText(context, "请重新扫描", Toast.LENGTH_SHORT).show();
//                Looper.loop();
//            }
//        } catch (Exception e) {
//            Looper.prepare();
//
//            dismissLoading();
//            Toast.makeText(context, "请重新扫描", Toast.LENGTH_SHORT).show();
//           // Toast.makeText(getApplicationContext(), "登录成功", Toast.LENGTH_SHORT).show();
//            Looper.loop();
//        }
//
//    }

    /**
     * save read Label data thread
     *
     * @author Administrator
     */
    class InventoryThread extends Thread {

        @Override
        public void run() {
            super.run();
            LogUtils.d("radio_tag", "radio_tag：" + radio_tag);
            while (runFlag) {
                if (startFlag) {
                    runFlag = false;
                    startFlag = false;
                    readLabel();
                    try {
                        Thread.sleep(80);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }

        }
    }

//    // Add the read EPC to the list
//    private void addToList(final List<EPC> list, final String epc) {
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                if (dataStr != null) {
//                    dataStr.clear();
//                }
//
//                int idCount = 1;
//
//
//                if (list.isEmpty()) {
//                    EPC epcTag = new EPC();
//                    epcTag.setEpc(epc);
//                    list.add(epcTag);
//                } else {
//                    for (int i = 0; i < list.size(); i++) {
//                        EPC mEPC = list.get(i);
//                        // list contains EPC
//                        if (epc.equals(mEPC.getEpc())) {
//                            mEPC.setCount(mEPC.getCount() + 1);
//                            list.set(i, mEPC);
//                            break;
//                        } else if (i == (list.size() - 1)) {
//                            // list not contains epc
//                            EPC newEPC = new EPC();
//                            newEPC.setEpc(epc);
//                            list.add(newEPC);
//                        }
//                    }
//                }
//                for (EPC epcData : list) {
//                    EPC epc1 = new EPC();
//                    epc1.setId(idCount);
//                    epc1.setEpc(epcData.getEpc());
//                    epc1.setCount(epcData.getCount() + 1);
//                    idCount++;
//                    dataStr.add(epc1);
//                }
//                ecp = dataStr.get(0).getEpc();
//                readData();
//
//            }
//        });
//    }

//    public String getOldRfid() {
//        return oldRfid;
//    }


//
//    //读数据
//    private void readData() {
//        //   mTextViewREADNumber.setText("");
//
//        if ("".equals(ecp)) {
//            // Toast.makeText(context,"ecp数据为空",Toast.LENGTH_SHORT).show();
//            dismissLoading();
//            return;
//        }
//
//        try {
//            reader.selectEpc(Tools.HexString2Bytes(ecp));
//            byte[] accessPassword = Tools.HexString2Bytes("00000000");
//            int length = Integer.valueOf("4");
//            String readState = LabelReadOrWriteUtils.readLabel(accessPassword, reader, 1, 0, length);
//
//            String read = LabelReadOrWriteUtils.hexStringToString(readState);
//            if (!"".equals(readState)) {
//                int a = read.indexOf("0_");
//                String substring = read.substring(a + 2, read.length());
//                long pretime = Long.parseLong(substring);
//                if (TimeUtil.check(pretime)) {
//                    Log.e("TIme", "超过10fenzhong");
//                    isChange = false;
//                    handler.sendEmptyMessageDelayed(1, 500);
//                } else {
//                    /*
//                     // int a= rfid.indexOf("1_");
//       // String substring = rfid.substring(a+2, rfid.length());
//
//                     */
//                    int i = read.indexOf("1_");
//                    oldRfid = read.substring(i + 2, read.length());
//                    Log.e("TIme", "0fenzhongz之内");
//                    MessageDialog dialog = new MessageDialog(context, "是否重新入库");
//                    dialog.setPositiveButton("确定", new BaseDialog.OnDialogButtonClickListener() {
//                                @Override
//                                public void onClick(Dialog dialog, View v) {
//                                    isChange = true;
//                                    writeData();
//                                }
//
//                            }
//                    ).show();
//                    dialog.setNegativeButton("取消", new BaseDialog.OnDialogButtonClickListener() {
//                        @Override
//                        public void onClick(Dialog dialog, View v) {
//                            dismissLoading();
//                        }
//                    });
//                }
//            }else {
//                isChange = false;
//                handler.sendEmptyMessageDelayed(1, 500);
//            }
//        } catch (Exception e) {
//            dismissLoading();
//            Toast.makeText(context, "请重新扫描", Toast.LENGTH_SHORT).show();
//        }
//    }

    class DisplayStatusReceiver extends BroadcastReceiver {
        String SCREEN_ON = "android.intent.action.SCREEN_ON";
        String SCREEN_OFF = "android.intent.action.SCREEN_OFF";

        @Override
        public void onReceive(Context context, Intent intent) {
            if (SCREEN_ON.equals(intent.getAction())) {
                isStartFlag = (boolean) SPUtils.get(context, "startFlag", true);
                startFlag = isStartFlag;
            } else if (SCREEN_OFF.equals(intent.getAction())) {
                SPUtils.remove(context, "startFlag");
                SPUtils.put(context, "startFlag", startFlag);
                startFlag = false;

            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        PowerUtil.power("1");
        int value = (int) SPUtils.get(context, "DBM", 0);
        if (value != 0) {
            reader.setOutputPower(value);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        PowerUtil.power("0");
        runFlag = false;
        if (reader != null) {
            reader.close();
        }
        unregisterReceiver(mDisplayStatusReceiver);
    }

    private boolean showProgressDiallog = true;
    private CustomProgressDialog mDialog;

    public void showLoading() {
        if (showProgressDiallog && mDialog == null) {
            mDialog = new CustomProgressDialog(context);
            mDialog.setMessage("正在加载数据..");
            mDialog.show();
        } else {
            mDialog.setMessage("正在加载数据..");
            mDialog.show();
        }
    }

    public void dismissLoading() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
            showProgressDiallog = false;
        }
    }
}

