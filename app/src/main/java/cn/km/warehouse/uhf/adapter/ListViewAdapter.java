package cn.km.warehouse.uhf.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;



import java.util.ArrayList;
import java.util.List;

import cn.km.warehouse.R;
import cn.km.warehouse.uhf.bean.EPC;


public class ListViewAdapter extends BaseAdapter {

    private List<EPC> datas;
    private LayoutInflater mInflater;

    private String TAG = "ListViewAdapter";

    public ListViewAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
    }

    public void setData(List<EPC> datas1) {
        datas = new ArrayList<>();
        this.datas = datas1;
        Log.e(TAG, "setData: " + datas.size());
    }

    @Override
    public int getCount() {
        return datas.size();
    }

    @Override
    public Object getItem(int position) {
        return datas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder vh = null;
        if (view == null) {
            vh = new ViewHolder();
            view = mInflater.inflate(R.layout.listview_item, null);
            vh.itemNumber = view.findViewById(R.id.item_number);
            vh.itemEpc = view.findViewById(R.id.item_epc);
            vh.itemCount = view.findViewById(R.id.item_count);
            view.setTag(vh);
            vh.itemCount.setTag(datas.get(position).getId());
        } else {
            vh = (ViewHolder) view.getTag();
        }
        vh.itemNumber.setText(String.valueOf(datas.get(position).getId()));
        Log.d("getEpc", "getEpc" + datas.get(position).getEpc());
        vh.itemEpc.setText(datas.get(position).getEpc());
        vh.itemCount.setText(String.valueOf(datas.get(position).getCount()));

        return view;
    }

    class ViewHolder {
        TextView itemNumber;
        TextView itemEpc;
        TextView itemCount;
    }
}
