package cn.km.warehouse.ui.adapter;

import android.view.View;

import com.chad.library.adapter.base.viewholder.BaseViewHolder;

import org.jetbrains.annotations.NotNull;

import cn.km.warehouse.R;
import cn.km.warehouse.base.BaseAdapter;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/2/7
 */
public class InputListAdapter extends BaseAdapter<InputListAdapter.Item> {
    public InputListAdapter() {
        super(R.layout.item_input_list);
    }



    @Override
    protected void convert(@NotNull BaseViewHolder holder, InputListAdapter.Item item) {
        holder.setText(R.id.tv_title,item.ApplicationCode);
        holder.setText(R.id.tv_status,item.state);
        holder.getView(R.id.input_list_sl).setVisibility(View.VISIBLE);
        holder.setText(R.id.tv_wuliaoname,item.wlmc);
        holder.setText(R.id.tv_ggxh,item.ggxh);
        holder.setText(R.id.tv_dw,item.dw);
        holder.setText(R.id.tv_sl,item.sl);
    }
    public static class Item {
        public String ApplicationCode;
        public String state;
        public String wlmc;
        public String ggxh;
        public String dw;
        public String sl;
        public int billid;
        public int applicationId;
         public int materialId;
         public String orderId;

    }
}
