package cn.km.warehouse.ui;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.magicrf.uhfreaderlib.reader.UhfReader;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import cn.km.warehouse.R;
import cn.km.warehouse.base.BaseActivity;
import cn.km.warehouse.event.ScanEvent;
import cn.km.warehouse.model.CKScanGetDataMdoel;
import cn.km.warehouse.model.CKScanModel;
import cn.km.warehouse.present.CKScanPresenter;
import cn.km.warehouse.present.RlrkPresenter;
import cn.km.warehouse.uhf.UHFCKHome;
import cn.km.warehouse.uhf.UHFHome;
import cn.km.warehouse.uhf.util.LogUtils;
import cn.km.warehouse.uhf.util.PowerUtil;
import cn.km.warehouse.uhf.util.SPUtils;
import cn.km.warehouse.uhf.util.SoundPoolHelper;
import cn.km.warehouse.ui.adapter.CKDetailXQCWAdapter;
import cn.km.warehouse.ui.adapter.RKDetailXQCWAdapter;
import cn.km.warehouse.ui.customview.ContentShowDialog;
import cn.km.warehouse.ui.customview.MyListView;
import cn.km.warehouse.ui.customview.MyToolbar;
import cn.km.warehouse.view.CKScanView;
import cn.km.warehouse.widget.CustomProgressDialog;

public class CKSCANActivity extends BaseActivity implements CKScanView, View.OnClickListener {
    private MyToolbar myToolbar;
    private CKScanPresenter ckScanPresenter;
    private RelativeLayout mRelativelayoutScan;
    private TextView mTextViewRKDH;
    private TextView mTextViewCode;
    private TextView mTextViewName;
    private TextView mTextViewGGXH;
    private TextView mTextViewDW;
    private TextView mTextViewCW;
    private TextView mTextViewSL;
    private TextView mTextViewLYYC;
    private TextView mTextViewYSYS;
    private TextView mTextViewHJ;
    private String applicationId;
    private String materialId;
    private String orderId;
    private MyListView mListView;
    private CKDetailXQCWAdapter ckDetailXQCWAdapter;
    private String serialPortPath = "/dev/ttyS2";
    private UhfReader reader = null;
    private boolean startFlag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ckscan);
        EventBus.getDefault().register(this);
        mRelativelayoutScan = this.findViewById(R.id.realtive_scan);
        mTextViewCode = this.findViewById(R.id.tv_code_info);
        mTextViewRKDH = this.findViewById(R.id.tv_code);
        mTextViewName = this.findViewById(R.id.tv_name_info);
        mTextViewGGXH = this.findViewById(R.id.tv_guige_info);
        mTextViewDW = this.findViewById(R.id.tv_unit_info);
        mTextViewCW = this.findViewById(R.id.textview_cw_content);
        mTextViewHJ = this.findViewById(R.id.textview_hj_content);
        mTextViewYSYS = this.findViewById(R.id.textview_ys);
        mListView = this.findViewById(R.id.listview_ckxqlist);
        mTextViewSL = this.findViewById(R.id.tv_sl);
        mTextViewLYYC = this.findViewById(R.id.textview_ys);
        mRelativelayoutScan.setOnClickListener(this);
        myToolbar = findViewById(R.id.toolBar);
        applicationId = getIntent().getStringExtra("applicationId");
        materialId = getIntent().getStringExtra("materialId");
        myToolbar.setTitleWithNavigation("物料出库", true);
        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CKSCANActivity.this.finish();
            }
        });
        ckScanPresenter = new CKScanPresenter(this);
        ckScanPresenter.getSelectCKQD(Integer.parseInt(applicationId), Integer.parseInt(materialId));
        startFlag = false;
        initData();
    }

    private void initData() {
        UhfReader.setPortPath(serialPortPath);
        reader = UhfReader.getInstance();

        if (reader == null) {
            Toast.makeText(this, "请重新扫描", Toast.LENGTH_SHORT).show();
        }

        int dbm = (int) cn.km.warehouse.uhf.util.SPUtils.get(this, "DBM", 0);
        if (dbm != 0) {
            LogUtils.d(TAG, "initData: " + dbm);
            reader.setOutputPower(26);
        }
        SoundPoolHelper.getInstance(this);
        registerDisplayStatusReceiver();
        startFlag = false;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void ScanSuccessed(ScanEvent event) {
        getCKScanData(event.getRfid());

    }

    private void getCKScanData(String rfid) {
        if (ckScanPresenter == null) {
            ckScanPresenter = new CKScanPresenter(this);
        }
        ckScanPresenter.getCKScanData(rfid);
    }
    UHFHome uhfHome;
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.realtive_scan:
//                UHFCKHome uhfHome = new UHFCKHome(this, reader);
                uhfHome = new UHFHome("", "", this, reader);

                break;
        }
    }

    @Override
    public void submitSuccess(String msg) {
        Toast.makeText(this, "出库成功", Toast.LENGTH_SHORT).show();
        ckScanPresenter.getSelectCKQD(Integer.parseInt(applicationId), Integer.parseInt(materialId));
        dismissLoading();
    }

    @Override
    public void submitFailer(int code, String msg) {
        if (code == 0) {
            Toast.makeText(this, "出库成功", Toast.LENGTH_SHORT).show();
            ckScanPresenter.getSelectCKQD(Integer.parseInt(applicationId), Integer.parseInt(materialId));
        } else {
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        }
        dismissLoading();
    }

    @Override
    public void getSelectCKQDDataSuccess(CKScanModel ckScanModel) {
        if (ckScanModel == null) {
            return;
        }
        dismissLoading();
        mTextViewRKDH.setText(ckScanModel.getDelivery().getApplicationCode());
        mTextViewCode.setText(ckScanModel.getDelivery().getMaterialCode());
        mTextViewName.setText(ckScanModel.getDelivery().getMaterialName());
        mTextViewGGXH.setText(ckScanModel.getDelivery().getMaterialSpecs());
        mTextViewDW.setText(ckScanModel.getDelivery().getMaterialUnitName());
        mTextViewSL.setText(ckScanModel.getDelivery().getActualNum() + "");
        mTextViewLYYC.setText(ckScanModel.getDelivery().getActualNum() + "/" + ckScanModel.getDelivery().getShouldNum());
        orderId = ckScanModel.getDelivery().getOrderId() + "";
//        mTextViewCW.setText(ckScanModel.getPositionName());
//        mTextViewHJ.setText(ckScanModel.getShelfName());
        if (ckDetailXQCWAdapter == null) {
            ckDetailXQCWAdapter = new CKDetailXQCWAdapter(this, ckScanModel.getList());
        }
        mListView.setAdapter(ckDetailXQCWAdapter);
    }

    // 出库扫码
    @Override
    public void getCKScanDataSuccess(CKScanGetDataMdoel ckScanGetDataMdoel) {
        if (ckScanGetDataMdoel == null) {
            return;
        }
        initDialog(ckScanGetDataMdoel);
    }

    private ContentShowDialog contentShowDialog;

    private void initDialog(CKScanGetDataMdoel ckScanGetDataMdoel) {
        if (contentShowDialog == null) {
            int[] ids = {R.id.textview_cancel, R.id.textview_ok_submit, R.id.tv_code_info, R.id.tv_name_info, R.id.tv_guige_info,
                    R.id.tv_unit_info, R.id.tv_sl};

            contentShowDialog = new ContentShowDialog(this, R.layout.scan_ck_dialog, ids, ckScanGetDataMdoel);
        }
        contentShowDialog.show();
        contentShowDialog.setOnCenterItemClickListener(new ContentShowDialog.OnCenterItemClickListener() {
            @Override
            public void OnCenterItemClick(ContentShowDialog dialog, View view) {
                switch (view.getId()) {
                    case R.id.textview_ok_submit:
                        if (ckScanPresenter == null) {
                            ckScanPresenter = new CKScanPresenter(CKSCANActivity.this);
                        }
                        if (orderId == null) {
                            Toast.makeText(CKSCANActivity.this, "数据错误", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        ckScanPresenter.submit(Integer.parseInt(orderId), ckScanGetDataMdoel.getRfid());
                        break;
                    case R.id.textview_cancel:
                        contentShowDialog.dismiss();
                        break;
                }
            }
        });
    }


    @Override
    public void getScanDataFailer(int code, String msg) {
        Toast.makeText(CKSCANActivity.this, msg, Toast.LENGTH_SHORT).show();
        dismissLoading();
    }

    @Override
    public void getCKScanDataFailer(int code, String msg) {
        Toast.makeText(CKSCANActivity.this, msg, Toast.LENGTH_SHORT).show();
        dismissLoading();
    }

    private boolean showProgressDiallog = true;
    private CustomProgressDialog mDialog;

    @Override
    public void showLoading() {
        if (showProgressDiallog && mDialog == null) {
            mDialog = new CustomProgressDialog(this);
            mDialog.setMessage("正在加载数据..");
            mDialog.show();
        } else {
            mDialog.setMessage("正在加载数据..");
            mDialog.show();
        }
    }

    @Override
    public void dismissLoading() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
            showProgressDiallog = false;
        }
    }

    @Override
    public void detach() {

    }

    private boolean isStartFlag;
    private DisplayStatusReceiver mDisplayStatusReceiver;

    private void registerDisplayStatusReceiver() {
        mDisplayStatusReceiver = new DisplayStatusReceiver();
        IntentFilter screenStatusIF = new IntentFilter();
        screenStatusIF.addAction(Intent.ACTION_SCREEN_ON);
        screenStatusIF.addAction(Intent.ACTION_SCREEN_OFF);
        registerReceiver(mDisplayStatusReceiver, screenStatusIF);
    }

    class DisplayStatusReceiver extends BroadcastReceiver {
        String SCREEN_ON = "android.intent.action.SCREEN_ON";
        String SCREEN_OFF = "android.intent.action.SCREEN_OFF";

        @Override
        public void onReceive(Context context, Intent intent) {
            if (SCREEN_ON.equals(intent.getAction())) {
                isStartFlag = (boolean) cn.km.warehouse.uhf.util.SPUtils.get(context, "startFlag", true);
                startFlag = isStartFlag;
            } else if (SCREEN_OFF.equals(intent.getAction())) {
                cn.km.warehouse.uhf.util.SPUtils.remove(context, "startFlag");
                cn.km.warehouse.uhf.util.SPUtils.put(context, "startFlag", startFlag);
                startFlag = false;

            }
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        PowerUtil.power("1");
        int value = (int) cn.km.warehouse.uhf.util.SPUtils.get(this, "DBM", 0);
        if (value != 0) {
            reader.setOutputPower(value);
        }
    }
    @Override
    protected void setStatusBar() {
        super.setStatusBar();
    }
    private boolean runFlag = true;
    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        PowerUtil.power("0");
        runFlag = false;
        if (reader != null) {
            reader.close();
        }
        unregisterReceiver(mDisplayStatusReceiver);
    }
}
