package cn.km.warehouse.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.liaoinstan.springview.container.AliHeader;
import com.liaoinstan.springview.container.DefaultFooter;
import com.liaoinstan.springview.widget.SpringView;

import java.util.ArrayList;
import java.util.List;

import cn.km.warehouse.Contstant;
import cn.km.warehouse.R;
import cn.km.warehouse.base.BaseActivity;
import cn.km.warehouse.model.CKDModel;
import cn.km.warehouse.model.CKListQDModel;
import cn.km.warehouse.model.CKModel;
import cn.km.warehouse.model.CKQDModel;
import cn.km.warehouse.model.CKScanGetDataMdoel;
import cn.km.warehouse.model.RKlistModel;
import cn.km.warehouse.present.RukuListPresenter;
import cn.km.warehouse.present.WLCKPresenter;
import cn.km.warehouse.ui.adapter.InputAdapter;
import cn.km.warehouse.ui.customview.MyToolbar;
import cn.km.warehouse.ui.customview.ProgressLoadingLayout;
import cn.km.warehouse.utils.SPUtils;
import cn.km.warehouse.view.WLCKView;

public class WLCKActivity extends BaseActivity implements WLCKView, SpringView.OnFreshListener {
    private RecyclerView mRecyClerView;
    private MyToolbar myToolbar;
    private WLCKPresenter mWlckPresenter;
    private int pageInex=1;
    private int totalpage;
    private CKModel mCkModel;
    SpringView mSpringView;
    ProgressLoadingLayout progressLoadingLayout;
    private List<InputAdapter.Item> items;
    private List<InputAdapter.Item> itemsAll = new ArrayList<>();
    InputAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wlck);
        mCkModel= SPUtils.getBeanCK(this, Contstant.ck_key);
        progressLoadingLayout=this.findViewById(R.id.progressLoadingLayout);
        mWlckPresenter=new WLCKPresenter(this);
        mSpringView=this.findViewById(R.id.springview_ck);
        myToolbar = findViewById(R.id.toolBar);
        // myToolbar. setNavigationIcon(R.drawable.ic_back);
        myToolbar.setTitleWithNavigation("请选择出库申请单",true);
        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WLCKActivity.this.finish();
            }
        });
        mRecyClerView=this.findViewById(R.id.recyclerView_input);
        initData();
    }
    public void   initData(){
        mSpringView.setHeader(new AliHeader(this, true));
        //mSpringView.setFooter(new AliFooter(this, true));
        mSpringView.setFooter(new DefaultFooter(this));
        mSpringView.setType(SpringView.Type.FOLLOW);
        mSpringView.setListener(this);
        mWlckPresenter.getCKlist(pageInex, Contstant.pageSize,mCkModel.getId(),Contstant.totalCount);
        mAdapter = new InputAdapter();
        mRecyClerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyClerView.setHasFixedSize(true);
        mRecyClerView.setNestedScrollingEnabled(false);
        mRecyClerView.setFocusable(false);
        mRecyClerView.setAdapter(mAdapter);

    };
    @Override
    protected void setStatusBar() {
        super.setStatusBar();
    }

    @Override
    public void getCKListSuccess(CKDModel ckdModel) {
        if (ckdModel == null || ckdModel.getList().size() == 0) {
            if (pageInex == 1) {
                progressLoadingLayout.showEmpty();
            }
            mSpringView.onFinishFreshAndLoad();
            return;
        }
        pageInex=ckdModel.getCurrPage();
        totalpage=ckdModel.getTotalPage();
        progressLoadingLayout.showContent();
        transformData(ckdModel);
        if (items == null) {
            mSpringView.onFinishFreshAndLoad();
            return;
        }
        if (pageInex == 1) {
            mSpringView.onFinishFreshAndLoad();
            mAdapter.setNewData(items);
            itemsAll.clear();
            itemsAll.addAll(items);
        } else {
            if (items != null) {
                itemsAll.addAll(items);
                mAdapter.setNewData(itemsAll);
                mAdapter.notifyDataSetChanged();
            }

            mSpringView.onFinishFreshAndLoad();
        }
        mAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {

                Intent intent=new Intent();
                    intent.setClass(WLCKActivity.this,CKQDFYActiviy.class);
                intent.putExtra("applicationId",ckdModel.getList().get(position).getApplicationId()+"");
//                    startActivity(new Intent(InputActivity.this, RKQDFYActiviy.class));
                WLCKActivity.this.startActivity(intent);
            }
        });
    }
    private void transformData(CKDModel ckdModel) {
        if (ckdModel == null) {
            return;
        }
        items = new ArrayList<>();
        for (CKDModel.ListBean item : ckdModel.getList()) {
            InputAdapter.Item item_ = new InputAdapter.Item();
            item_.ApplicationCode = item.getApplicationCode();
            if ("1".equals(item.getOrderState()+"")){
                item_.state="待出库 ";
            } if ("2".equals(item.getOrderState()+"")){
                item_.state="出库中 ";
            } if ("3".equals(item.getOrderState()+"")){
                item_.state=" 已完成 ";
            }
            //  = item.getEntryTypeName();
            item_.ssgs = item.getOrganizationName();
            item_.sqr = item.getApplicationAccountName().toString();
            item_.sqsj = item.getApplicationDatetime();
           // item_.sl = item.getActualNum()+"";
            items.add(item_);
        }
    }
    @Override
    public void getCKListSuccessFailer(String message) {

    }

    @Override
    public void getCKListQDSuccess(CKListQDModel ckListQDModel) {

    }

    @Override
    public void getCKQDSuccess(CKQDModel ckqdModel) {

    }

//    @Override
//    public void getCKListQDSuccess(CKDModel ckdModel) {
//
//    }
//
//    @Override
//    public void getCKQDSuccess(CKQDModel ckqdModel) {
//
//    }

    @Override
    public void getCKListQDFailer(String message) {

    }

    @Override
    public void getScanDataFailer(int code, String message) {

    }

    @Override
    public void submitFailer(int code, String message) {

    }

    @Override
    public void getCKScanDataSuccess(CKScanGetDataMdoel ckScanGetDataMdoel) {

    }

    @Override
    public void submitSuccess(String message) {

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void dismissLoading() {

    }
    @Override
    public void onRefresh() {
        pageInex = 1;
        if (mWlckPresenter != null) {
            mWlckPresenter.getCKlist(pageInex, Contstant.pageSize,mCkModel.getId(),Contstant.totalCount);
        }
    }

    @Override
    public void onLoadmore() {
        if (pageInex>=totalpage){
            Toast.makeText(WLCKActivity.this, "暂无更多数据", Toast.LENGTH_SHORT).show();
            mSpringView.onFinishFreshAndLoad();
            return;
        }

        pageInex = pageInex+1;
        if (mWlckPresenter== null) {
            mWlckPresenter=new WLCKPresenter(this);

        }
        mWlckPresenter.getCKlist(pageInex, Contstant.pageSize,mCkModel.getId(),Contstant.totalCount);
    }
    @Override
    public void detach() {

    }
}
