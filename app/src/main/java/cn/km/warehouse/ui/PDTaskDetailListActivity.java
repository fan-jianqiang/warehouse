package cn.km.warehouse.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.google.android.material.imageview.ShapeableImageView;
import com.liaoinstan.springview.container.AliHeader;
import com.liaoinstan.springview.container.DefaultFooter;
import com.liaoinstan.springview.widget.SpringView;

import java.util.ArrayList;
import java.util.List;

import cn.km.warehouse.Contstant;
import cn.km.warehouse.R;
import cn.km.warehouse.base.BaseActivity;
import cn.km.warehouse.model.PDDetailModel;
import cn.km.warehouse.model.RKlistModel;
import cn.km.warehouse.present.PDTaskDetailPresenter;
import cn.km.warehouse.present.RlrkPresenter;
import cn.km.warehouse.present.RukuListPresenter;
import cn.km.warehouse.ui.adapter.InputAdapter;
import cn.km.warehouse.ui.adapter.PDDetailAdapter;
import cn.km.warehouse.ui.customview.MyToolbar;
import cn.km.warehouse.ui.customview.ProgressLoadingLayout;
import cn.km.warehouse.view.PDTaskDetailView;
import kotlin.jvm.internal.PrimitiveSpreadBuilder;

public class PDTaskDetailListActivity extends BaseActivity implements SpringView.OnFreshListener, PDTaskDetailView, View.OnClickListener {
    private MyToolbar myToolbar;
    private RecyclerView mRecyClerView;
    SpringView mSpringView;
    ProgressLoadingLayout progressLoadingLayout;
    private PDTaskDetailPresenter pdTaskDetailPresenter;
    private int pageInex=1;
    private int totalpage;
    private String taskId;
    private String taskCode;
    private PDDetailAdapter pdDetailAdapter;
    private List<PDDetailAdapter.Item> items;
    private List<PDDetailAdapter.Item> itemsAll = new ArrayList<>();
    private TextView mTextViewCode;
    private RelativeLayout mRelativeLayoutScan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdtask_detail_list);
        myToolbar = findViewById(R.id.toolBar);
        // myToolbar. setNavigationIcon(R.drawable.ic_back);
        mTextViewCode=findViewById(R.id.tv_code);
        mRelativeLayoutScan=findViewById(R.id.relative_scan);
        taskCode=getIntent().getStringExtra("taskCode");
        taskId=getIntent().getStringExtra("taskId");
        mTextViewCode.setText(taskCode);
        pdTaskDetailPresenter=new PDTaskDetailPresenter(this);
        myToolbar.setTitleWithNavigation("存货盘点",true);
        myToolbar.setRightText("完成");
        SubmitListener submitListener=new SubmitListener();
        myToolbar.setRightTextClickListener(submitListener);
        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PDTaskDetailListActivity.this.finish();
            }
        });
        mRecyClerView=this.findViewById(R.id.recyclerView_pd);
        mSpringView=this.findViewById(R.id.springview_pd);
        progressLoadingLayout=this.findViewById(R.id.progressLoadingLayout);
        mRelativeLayoutScan.setOnClickListener(this);
        initData();
    }
    public void   initData(){
        mSpringView.setHeader(new AliHeader(this, true));
        //mSpringView.setFooter(new AliFooter(this, true));
        mSpringView.setFooter(new DefaultFooter(this));
        mSpringView.setType(SpringView.Type.FOLLOW);
        mSpringView.setListener(this);
        pdTaskDetailPresenter.getPDTaskDetailList(pageInex, Contstant.pageSize,Contstant.totalCount,Integer.parseInt(taskId));
        pdDetailAdapter = new PDDetailAdapter();
        mRecyClerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyClerView.setHasFixedSize(true);
        mRecyClerView.setNestedScrollingEnabled(false);
        mRecyClerView.setFocusable(false);
        mRecyClerView.setAdapter(pdDetailAdapter);

    };
    public  class  SubmitListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            Submit();
        }
    }

    private void Submit() {
        if (pdTaskDetailPresenter==null){
            pdTaskDetailPresenter=new PDTaskDetailPresenter(this);
        }
        pdTaskDetailPresenter.submit(Integer.parseInt(taskId));

    }
    @Override
    public void onRefresh() {
        pageInex = 1;
        if (pdTaskDetailPresenter != null) {
            pdTaskDetailPresenter.getPDTaskDetailList(pageInex, Contstant.pageSize,Contstant.totalCount,Integer.parseInt(taskId));
        }
    }

    @Override
    public void onLoadmore() {
        if (pageInex>=totalpage){
            Toast.makeText(PDTaskDetailListActivity.this, "暂无更多数据", Toast.LENGTH_SHORT).show();
            mSpringView.onFinishFreshAndLoad();
            return;
        }

        pageInex = pageInex+1;
        if (pdTaskDetailPresenter== null) {
            pdTaskDetailPresenter=new PDTaskDetailPresenter(this);

        }
        pdTaskDetailPresenter.getPDTaskDetailList(pageInex, Contstant.pageSize,Contstant.totalCount,Integer.parseInt(taskId));
    }

    @Override
    public void getPDTaskDetialListSuccess( PDDetailModel pdDetailModel) {
        if (pdDetailModel == null || pdDetailModel.getList().size() == 0) {
            if (pageInex == 1) {
                progressLoadingLayout.showEmpty();
            }
            mSpringView.onFinishFreshAndLoad();
            return;
        }
        pageInex=pdDetailModel.getCurrPage();
        totalpage=pdDetailModel.getTotalPage();
        progressLoadingLayout.showContent();
        taskId=pdDetailModel.getList().get(0).getTaskId()+"";
        transformData(pdDetailModel);
        if (items == null) {
            mSpringView.onFinishFreshAndLoad();
            return;
        }
        if (pageInex == 1) {
            mSpringView.onFinishFreshAndLoad();
            pdDetailAdapter.setNewData(items);
            itemsAll.clear();
            itemsAll.addAll(items);
        } else {
            if (items != null) {
                itemsAll.addAll(items);
                pdDetailAdapter.setNewData(itemsAll);
                pdDetailAdapter.notifyDataSetChanged();
            }

            mSpringView.onFinishFreshAndLoad();
        }
//        pdDetailAdapter.setOnItemClickListener(new OnItemClickListener() {
//            @Override
//            public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {
//
//                Intent intent=new Intent();
//                intent.setClass(InputActivity.this,RKQDFYActiviy.class);
//                intent.putExtra("applicationId",rKlistModel.getList().get(position).getApplicationId()+"");
////                    startActivity(new Intent(InputActivity.this, RKQDFYActiviy.class));
//                InputActivity.this.startActivity(intent);
//            }
//        });
    }

    @Override
    public void getPDTaskDetialListFailer(String message) {
      Toast.makeText(this,message,Toast.LENGTH_SHORT).show();

    }

    @Override
    public void getPDTaskCompleteSuccess(String message) {
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getPDTaskCompleteFailure(int code, String message) {

    }

    private void transformData(PDDetailModel pdDetailModel) {
        if (pdDetailModel == null) {
            return;
        }
        items = new ArrayList<>();
        for (PDDetailModel.ListBean item : pdDetailModel.getList()) {
            PDDetailAdapter.Item item_ = new PDDetailAdapter.Item();
            item_.wlmc = item.getMaterialName();
            item_.dw = item.getMaterialUnitName();
            item_.wlbm = item.getMaterialCode().toString();
            item_.wlxh = item.getMaterialSpecs();
            items.add(item_);
        }
    }
    @Override
    public void showLoading() {
        progressLoadingLayout.showLoading();
    }

    @Override
    public void dismissLoading() {
        if (totalpage==0){
            progressLoadingLayout.showEmpty();
        }else {
            progressLoadingLayout.showContent();
        }
    }

    @Override
    public void detach() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.relative_scan:
                Intent intent=new Intent();
                intent.putExtra("taskId",taskId);
                intent.setClass(PDTaskDetailListActivity.this,PDTaskDetailScanActivity.class);
                startActivity(intent);
                break;
        }
    }
}
