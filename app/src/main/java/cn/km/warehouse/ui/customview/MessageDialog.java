package cn.km.warehouse.ui.customview;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.km.warehouse.R;
import cn.km.warehouse.utils.DimensionUtils;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/13
 */
public class MessageDialog  extends BaseDialog {

    /**显示消息内容*/
    TextView txtMessage;
    /**确定与取消按钮*/
    Button btnConfirm, btnCancel;
    /**窗体布局参数*/
    WindowManager.LayoutParams dialogLayoutParams = null;
    /**用于异步处理显示消息内容行数*/
    Handler mHandler = new Handler();

    /**
     * 构造方法
     * @param context
     */
    public MessageDialog(Context context) {
        this(context, "");
    }

    public MessageDialog(Context context,String message){
        super(context);
        this.context = context;
        View contentView = buildContentView();
        setContentView(contentView);
        initWidth();
        btnCancel.setBackgroundResource(R.drawable.viewjar_selector_sky_blue_btn);
        btnCancel.setTextColor(Color.WHITE);
        setNegativeButton("确定", null);
        setMessage(message);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                int lineCount = txtMessage.getLineCount();
                if(lineCount > 1){
                    txtMessage.setGravity(Gravity.LEFT|Gravity.CENTER_VERTICAL);
                }else{
                    txtMessage.setGravity(Gravity.CENTER);
                }
            }
        }, 100);
    }

    /**初始化窗口宽度*/
    private void initWidth(){
        dialogLayoutParams = getWindow().getAttributes();
        dialogLayoutParams.width = DimensionUtils.dip2Px(getContext(), 276);
        getWindow().setAttributes(dialogLayoutParams);
    }

    private View buildContentView(){
        LinearLayout linearRoot = new LinearLayout(getContext());
        LinearLayout.LayoutParams lpRoot = new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        linearRoot.setOrientation(LinearLayout.VERTICAL);
        linearRoot.setGravity(Gravity.CENTER_HORIZONTAL);
        linearRoot.setLayoutParams(lpRoot);
        linearRoot.setBackgroundResource(R.drawable.viewjar_dialog_light_gray_frame);

        addTitleView(linearRoot);

        ViewGroup mainView = addMainView(linearRoot);

        addMessageView(mainView);

        addBottomBlock(mainView);

        return linearRoot;
    }

    /**添加标题*/
    private void addTitleView(ViewGroup linearRoot){
        titleView = new TextView(getContext());
        titleView.setText("温馨提示");
        titleView.setGravity(Gravity.CENTER);
        titleView.setBackgroundResource(R.drawable.viewjar_dialog_title_light_gray_bg);

        titleView.setTextSize(20);
        titleView.setTextColor(Color.parseColor("#666666"));
        titleView.setShadowLayer(1, 1, 1, Color.parseColor("#999999"));
        linearRoot.addView(titleView);
    }

    /**添加主视图*/
    private ViewGroup addMainView(ViewGroup linearRoot){
        LinearLayout linearMain = new LinearLayout(getContext());
        linearMain.setOrientation(LinearLayout.VERTICAL);
        linearMain.setBackgroundResource(R.drawable.viewjar_message_bg);
        LinearLayout.LayoutParams lpMain = new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        linearMain.setLayoutParams(lpMain);
        linearRoot.addView(linearMain);
        return linearMain;
    }

    /**添加主视图*/
    private void addMessageView(ViewGroup linearMain){
        txtMessage = new TextView(getContext());
        txtMessage.setTextSize(18);
        txtMessage.setTextColor(Color.parseColor("#666666"));
        txtMessage.setMaxWidth(DimensionUtils.getScreenWidth(getContext(), 50));
        txtMessage.setGravity(Gravity.CENTER);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        int msgMargin = DimensionUtils.dip2Px(getContext(), 15);
        lp.leftMargin = msgMargin;
        lp.rightMargin = msgMargin;
        txtMessage.setLayoutParams(lp);
        //设置消息内容的最小高度值，否则小于三行时对话框高度与宽度显得不协调
        txtMessage.setMinHeight(DimensionUtils.dip2Px(getContext(), 56));
        linearMain.addView(txtMessage);
    }

    /**添加底部模块*/
    private void addBottomBlock(ViewGroup linearMain){
        LinearLayout linearBtnContainer = new LinearLayout(getContext());
        linearBtnContainer.setOrientation(LinearLayout.HORIZONTAL);
        LinearLayout.LayoutParams lpBtnContainer = new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lpBtnContainer.topMargin = DimensionUtils.dip2Px(getContext(), 15);
        linearBtnContainer.setLayoutParams(lpBtnContainer);
        linearMain.addView(linearBtnContainer);

        btnConfirm = new Button(getContext());
        btnConfirm.setText("确定");
        btnConfirm.setTextSize(18);
        btnConfirm.setTextColor(Color.WHITE);
        btnConfirm.setBackgroundResource(R.drawable.viewjar_selector_sky_blue_btn);
        LinearLayout.LayoutParams lpConfirm = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
        lpConfirm.weight = 1;
        int lrMargin = DimensionUtils.dip2Px(getContext(), 5);
        lpConfirm.leftMargin = lrMargin;
        lpConfirm.rightMargin = lrMargin;
        btnConfirm.setLayoutParams(lpConfirm);
        btnConfirm.setVisibility(View.GONE);
        linearBtnContainer.addView(btnConfirm);

        btnCancel = new Button(getContext());
        btnCancel.setText("取消");
        btnCancel.setTextSize(18);
        btnCancel.setTextColor(Color.parseColor("#666666"));
        btnCancel.setBackgroundResource(R.drawable.viewjar_selector_light_gray_btn);
        LinearLayout.LayoutParams lpCancle = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
        lpCancle.weight = 1;
        lpCancle.leftMargin = lrMargin;
        lpCancle.rightMargin = lrMargin;
        btnCancel.setLayoutParams(lpCancle);
        btnCancel.setVisibility(View.GONE);
        linearBtnContainer.addView(btnCancel);
    }

    public MessageDialog setTitle(String title) {
        if (!TextUtils.isEmpty(title) && null != titleView) {
            titleView.setText(title);
        }
        return this;
    }

    public MessageDialog setMessage(String message) {
        txtMessage.setText(message);
        return this;
    }

    public MessageDialog setPositiveButton(CharSequence text, OnDialogButtonClickListener listener) {
        btnConfirm.setVisibility(View.VISIBLE);
        btnConfirm.setTag(listener);
        btnConfirm.setText(text);
        //默认显示取消按钮，并且设置取消按钮的样式与确定按钮一样(见构造方法)
        //启用确定按钮时，需要还原取消按钮的样式
        btnCancel.setText("取消");
        btnCancel.setTextColor(Color.parseColor("#666666"));
        btnCancel.setBackgroundResource(R.drawable.viewjar_selector_light_gray_btn);
        btnConfirm.setOnClickListener(mListener);
        return this;
    }

    public MessageDialog setNegativeButton(CharSequence text, OnDialogButtonClickListener listener) {
        btnCancel.setVisibility(View.VISIBLE);
        btnCancel.setTag(listener);
        btnCancel.setText(text);
        btnCancel.setOnClickListener(mListener);
        return this;
    }

    public MessageDialog setPositiveButton(int textId, OnDialogButtonClickListener listener) {
        String text = context.getResources().getString(textId);
        setPositiveButton(text, listener);
        return this;
    }

    public MessageDialog setNegativeButton(int textId, OnDialogButtonClickListener listener) {
        String text = context.getResources().getString(textId);
        setNegativeButton(text, listener);
        return this;
    }

}

