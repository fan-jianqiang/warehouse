package cn.km.warehouse.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.liaoinstan.springview.container.AliHeader;
import com.liaoinstan.springview.container.DefaultFooter;
import com.liaoinstan.springview.widget.SpringView;

import java.util.ArrayList;
import java.util.List;

import cn.km.warehouse.Contstant;
import cn.km.warehouse.R;
import cn.km.warehouse.base.BaseActivity;
import cn.km.warehouse.model.CKModel;
import cn.km.warehouse.model.RKDXQModel;
import cn.km.warehouse.model.RKlistModel;
import cn.km.warehouse.present.RKDXQPresenter;
import cn.km.warehouse.present.RukuListPresenter;
import cn.km.warehouse.ui.adapter.InputAdapter;
import cn.km.warehouse.ui.adapter.RKDXQAdapter;
import cn.km.warehouse.ui.customview.MyListView;
import cn.km.warehouse.ui.customview.MyToolbar;
import cn.km.warehouse.ui.customview.ProgressLoadingLayout;
import cn.km.warehouse.utils.SPUtils;
import cn.km.warehouse.view.RKDXQView;

public class RKDXQActivity extends BaseActivity implements RKDXQView {
  private RKDXQPresenter rkdxqPresenter;
  private String orderId;
    private ListView mMyListView;
    private MyToolbar myToolbar;
    ProgressLoadingLayout progressLoadingLayout;
    private RKDXQAdapter rkdxqAdapter;
    private TextView mTextViewCode;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rkdxq);
        progressLoadingLayout=this.findViewById(R.id.progressLoadingLayout);
        mTextViewCode=this.findViewById(R.id.tv_code);
        myToolbar = findViewById(R.id.toolBar);
        myToolbar.setTitleWithNavigation("入库单详情",true);
        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RKDXQActivity.this.finish();
            }
        });
        mMyListView=this.findViewById(R.id.listview_input);
        initData();
    }

    private void initData() {
        orderId=getIntent().getStringExtra("orderId");
        rkdxqPresenter=new RKDXQPresenter(this);
        rkdxqPresenter.getRKDXQ(Integer.parseInt(orderId));
    }


    @Override
    public void getRKXQDataSuccess(RKDXQModel rkdxqModel) {
        if (rkdxqModel == null || rkdxqModel.getBillEntityList().size() == 0) {
            progressLoadingLayout.showEmpty();
            return;
        }

        progressLoadingLayout.showContent();
        mTextViewCode.setText(rkdxqModel.getEntryApplication().getApplicationCode());
       // transformData(rkdxqModel);
        rkdxqAdapter=new RKDXQAdapter(this,rkdxqModel.getBillEntityList(),rkdxqModel.getEntryApplication());
        mMyListView .setAdapter(rkdxqAdapter);
        rkdxqAdapter.setGoToRKClickListener(new RKDXQAdapter.GoToRKClickListener() {
            @Override
            public void goToRK(int position, String billid) {
                Intent intent=new Intent();
                intent.putExtra("billid",billid);
                intent.setClass(RKDXQActivity.this,RlrkActivity.class);
                RKDXQActivity.this. startActivity(intent);
            }
        });
    }

//    private void transformData(RKDXQModel rkdxqModel) {
//        if (rkdxqModel == null) {
//            return;
//        }
//        items = new ArrayList<>();
//        for (RKDXQModel.BillEntityListBean  item : rkdxqModel.getBillEntityList()) {
//            RKDXQAdapter.Item item_ = new RKDXQAdapter.Item();
//            item_.wulbm = item.getMaterialCode();
//            //  = item.getEntryTypeName();
//            item_.wlname = item.getMaterialName();
//            item_.ggxh = item.getMaterialSpecs().toString();
//            item_.hj="暂无";
//            item_.cw = "暂无";
//            item_.dw=item.getMaterialUnitName();
//            items.add(item_);
//        }
//    }

    @Override
    public void getRKXQDataFailer(String message) {
        Toast.makeText(RKDXQActivity.this, message, Toast.LENGTH_SHORT).show();
        progressLoadingLayout.showEmpty();
    }

    @Override
    public void showLoading() {
      progressLoadingLayout.showLoading();
    }

    @Override
    public void dismissLoading() {
        progressLoadingLayout.showEmpty();
    }

    @Override
    public void detach() {

    }
}
