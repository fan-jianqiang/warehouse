package cn.km.warehouse.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.magicrf.uhfreaderlib.reader.Tools;
import com.magicrf.uhfreaderlib.reader.UhfReader;

import java.util.ArrayList;
import java.util.List;

import cn.km.warehouse.R;
import cn.km.warehouse.base.BaseActivity;
import cn.km.warehouse.uhf.DetailActivity;
import cn.km.warehouse.uhf.SettingPower;
import cn.km.warehouse.uhf.UHFMainActivity;
import cn.km.warehouse.uhf.adapter.ListViewAdapter;
import cn.km.warehouse.uhf.bean.EPC;
import cn.km.warehouse.uhf.util.LabelReadOrWriteUtils;
import cn.km.warehouse.uhf.util.LogUtils;
import cn.km.warehouse.uhf.util.PowerUtil;
import cn.km.warehouse.uhf.util.SPUtils;
import cn.km.warehouse.uhf.util.SoundPoolHelper;

public class UHFTestActivity extends BaseActivity implements View.OnClickListener, AdapterView.OnItemClickListener, CompoundButton.OnCheckedChangeListener {

//
//    private final String TAG="UHFMainActivity";
//   // private Button btn_module, btn_start_save, btn_clear, btn_start_save2;
//    private ListView listView;
//    // Serial Port
//    private String serialPortPath="/dev/ttyS2";
//
//    // run thread flag
//    private boolean runFlag=true;
//    // start thread
//    private boolean startFlag=false;
//
//    private List<EPC> dataStr=null;
//    private ListViewAdapter adapter;
//    private ArrayList<EPC> listEPC;
//
//    private UhfReader reader=null;
//    private TextView text_version;
//
//    private UHFTestActivity.DisplayStatusReceiver mDisplayStatusReceiver;
//    private boolean isStartFlag;
//    private RadioButton radio1, radio2;
//
//    private int radio_tag=1;
//    private Thread thread;
//    private TextView mTextViewEPCNumber;
//    private TextView mTextViewWRITENumber;
//    private TextView mTextViewREADNumber;
//    private Button mButtonRead;
//    private Button mButtonWrite;
//    private String ecp="";
//    private EditText editTextwrite;
//
//    private EditText editextRead;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_uhftest);
//        dataStr=new ArrayList<>();
//        listEPC=new ArrayList<>();
//        initView ();
//        initData ();

    }

    @Override
    protected void setStatusBar() {
        super.setStatusBar();
    }

    private void initView() {
//        mTextViewEPCNumber=this.findViewById(R.id.textview_epc_number);
//        mTextViewWRITENumber=this.findViewById(R.id.textview_write_number);
//        mTextViewREADNumber=this.findViewById(R.id.textview_reade);
//        editTextwrite=this.findViewById(R.id.editext_write_number);
//        mButtonRead=this.findViewById(R.id.bt_read);
//        mButtonWrite=this.findViewById(R.id.bt_write);
//        editextRead=this.findViewById(R.id.editext_read_number);
////        btn_module=findViewById (R.id.btn_module);
////        btn_start_save=findViewById (R.id.btn_start_save);
////        btn_start_save2=findViewById (R.id.btn_start_save2);
////        btn_clear=findViewById (R.id.btn_clear);
//      //  listView=findViewById (R.id.list_item1);
//        text_version=findViewById (R.id.text_version);
//        radio1=findViewById (R.id.radio1);
//        radio2=findViewById (R.id.radio2);
//
////        btn_module.setOnClickListener (this);
////        btn_start_save.setOnClickListener (this);
////        btn_start_save2.setOnClickListener (this);
////        btn_clear.setOnClickListener (this);
//       // listView.setOnItemClickListener (this);
//        radio1.setOnCheckedChangeListener (this);
//        radio2.setOnCheckedChangeListener (this);
//
////        btn_start_save.setEnabled (false);
////        btn_start_save2.setEnabled (false);
////        btn_clear.setEnabled (false);
//        startFlag=false;
    }


    private void initData() {

//        UhfReader.setPortPath (serialPortPath);
//        reader= UhfReader.getInstance ();
//
//        if (reader == null) {
//            text_version.setText ("serialport init fail");
//        }
//
//        int dbm=(int) SPUtils.get (this, "DBM", 0);
//        if (dbm != 0) {
//            LogUtils.d (TAG, "initData: " + dbm);
//            reader.setOutputPower (26);
//        }
//        SoundPoolHelper.getInstance (this);
//
//        adapter=new ListViewAdapter (this);
//        registerDisplayStatusReceiver ();
//        thread=new UHFTestActivity.InventoryThread();
//        thread.start ();
//        readmodel();
//        mButtonRead.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mTextViewREADNumber.setText("");
//
//                return;
////                if ("".equals(ecp)){
////                    Toast.makeText(UHFTestActivity.this,"ecp数据为空",Toast.LENGTH_SHORT).show();
////                    return;
////                }
////                try {
////                    reader.selectEpc (Tools.HexString2Bytes (ecp));
////                    byte[] accessPassword= Tools.HexString2Bytes ("00000000");
////               int   length= Integer.valueOf (editextRead.getText().toString().trim());
////                    String readState= LabelReadOrWriteUtils.readLabel (accessPassword, reader, 1, 0, length);
////
////                    String read=LabelReadOrWriteUtils.hexStringToString(readState);
////
////                    String mmq = read.substring(1);
////                    Toast.makeText(UHFTestActivity.this, read+"-----"+readState + "-----" + mmq,Toast.LENGTH_SHORT).show();
////                    if (!"".equals (readState)) {
////                        mTextViewREADNumber.append (read + "\n");
////                    }
//////
////                }catch (Exception e){
////                    Toast.makeText(UHFTestActivity.this,e.toString(),Toast.LENGTH_SHORT).show();
////                }
//
//            }
//        });
//        mButtonWrite.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (dataStr==null && dataStr.size()<=0){
//                    Toast.makeText(UHFTestActivity.this, "写数据？", Toast.LENGTH_SHORT).show();
//
//                    return;
//                }
//                try {
//                    reader.selectEpc (Tools.HexString2Bytes (ecp));
//                    byte[] accessPassword= Tools.HexString2Bytes ("00000000");
////                    String writeStateCSH= LabelReadOrWriteUtils.writeLabel (accessPassword, reader, 1, 1, "0000000000000" +
////                            "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000");
//                    String s = LabelReadOrWriteUtils.stringToHexString(editTextwrite.getText().toString().trim());
//                   // String s = LabelReadOrWriteUtils.str2HexStr(editTextwrite.getText().toString().trim());
//                    String writeState= LabelReadOrWriteUtils.writeLabel (accessPassword, reader, 1, 1,s );
//                    if (!"".equals (writeState)) {
//                        mTextViewWRITENumber.append (writeState + "\n");
//                    }
//                }catch (Exception e){
//                    Toast.makeText(UHFTestActivity.this,e.toString(),Toast.LENGTH_SHORT).show();
//                }
//
//            }
//        });

    }

 public void readmodel(){
//        try {
//            byte[] versionBytes=reader.getFirmware ();
//            if (versionBytes != null) {
//                String version=new String(versionBytes);
//                Log.e (TAG, "onClick: " + version);
//            }
//            SoundPoolHelper.play (1); // play
//            // setButtonClickable (btn_module, false);
////     btn_start_save.setEnabled (true);
////     btn_clear.setEnabled (true);
////     btn_start_save2.setEnabled (true);
//            text_version.setText ("open module success");
//            startFlag=true;
//          //  readLabel ();
//        }catch (Exception e){
//            Toast.makeText(UHFTestActivity.this,e.getMessage(),Toast.LENGTH_SHORT).show();
//        }

 }
    @Override
    public void onClick(View view) {
//        switch (view.getId ()) {
//            case R.id.btn_module:
//
//                break;
//            case R.id.btn_start_save:
//
//                break;
//            case R.id.btn_start_save2:
//              //  readLabel ();
//                break;
//            case R.id.btn_clear:
//             //   clear ();
//                break;
//            default:
//                break;
//        }
    }


    // clear
    public void clear() {
//        dataStr.clear ();
//        listEPC.clear (); // clear
//        adapter.notifyDataSetChanged ();
    }


    private void setButtonClickable(Button button, boolean flag) {
        button.setClickable (flag);
        if (flag) {
            button.setTextColor (Color.BLACK);
        } else {
            button.setTextColor (Color.GRAY);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
//        startFlag=false;
//        btn_start_save.setText (R.string.start_save);
//        View view1=adapterView.getAdapter ().getView (position, null, listView);
//        TextView itemEpc=view1.findViewById (R.id.item_epc);
//        Intent intent=new Intent(this, DetailActivity.class);
//        intent.putExtra ("epc", itemEpc.getText ().toString ());
//        startActivity (intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater=getMenuInflater ();
        menuInflater.inflate (R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId ()) {
            case R.id.action_settings:
                Intent intent=new Intent(this, SettingPower.class);
                startActivity (intent);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected (item);
    }

    private void registerDisplayStatusReceiver() {
//        mDisplayStatusReceiver=new UHFTestActivity.DisplayStatusReceiver();
//        IntentFilter screenStatusIF=new IntentFilter();
//        screenStatusIF.addAction (Intent.ACTION_SCREEN_ON);
//        screenStatusIF.addAction (Intent.ACTION_SCREEN_OFF);
//        registerReceiver (mDisplayStatusReceiver, screenStatusIF);
    }
//
    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//        switch (compoundButton.getId ()) {
//            case R.id.radio1:
//                startFlag=false;
////                btn_start_save.setText (R.string.start_save);
////                //      clear();
////                btn_start_save2.setVisibility (View.GONE);
////                btn_start_save.setVisibility (View.VISIBLE);
//                break;
//            case R.id.radio2:
//                //    clear();
////                btn_start_save2.setVisibility (View.VISIBLE);
////                btn_start_save.setVisibility (View.GONE);
//                break;
//        }
    }

    // ========================================================Save operation=======================================================

    private List<byte[]> epcList;

    public void readLabel() {
//        try {
//            epcList=reader.inventoryRealTime (); //Real-// time inventory
//            if (epcList != null && !epcList.isEmpty ()) {
//                //play volume
//                SoundPoolHelper.play (1);
//                for (byte[] epc : epcList) {
//                    String epcStr= Tools.Bytes2HexString (epc, epc.length);
//                    // Log.e(TAG, "run: " + epcStr);
//                    addToList (listEPC, epcStr);
//                }
//            }
//        }catch (Exception e){
//            Toast.makeText(UHFTestActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
//        }

    }






    /**
     * save read Label data thread
     *
     * @author Administrator
     */
    class InventoryThread extends Thread {

        @Override
        public void run() {
            super.run ();
//            LogUtils.d ("radio_tag", "radio_tag：" + radio_tag);
//            while (runFlag) {
//                //  Log.e(TAG, "run: " + "ThreadStart");
//                if (startFlag) {
//                    //	reader.stopInventoryMulti()
//                    runFlag=false;
//                    startFlag=false;
//                    readLabel ();
//                    try {
//                        Thread.sleep (80);
//                    } catch (InterruptedException e) {
//                        // TODO Auto-generated catch block
//                        e.printStackTrace ();
//                    }
//                }
//            }

        }
    }

    // Add the read EPC to the list
    private void addToList(final List<EPC> list, final String epc) {
       // Toast.makeText(UHFTestActivity.this, "数据加载list？"+list.size(), Toast.LENGTH_SHORT).show();
        runOnUiThread (new Runnable() {
            @Override
            public void run() {
//                if (dataStr!=null){
//                    dataStr.clear ();
//                }
//
//                int idCount=1;
//
//                // read firstTime
//                if (list.isEmpty ()) {
//                    EPC epcTag=new EPC ();
//                    epcTag.setEpc (epc);
//                    list.add (epcTag);
//                } else {
//                    for (int i=0; i < list.size (); i++) {
//                        EPC mEPC=list.get (i);
//                        // list contains EPC
//                        if (epc.equals (mEPC.getEpc ())) {
//                            mEPC.setCount (mEPC.getCount () + 1);
//                            list.set (i, mEPC);
//                            break;
//                        } else if (i == (list.size () - 1)) {
//                            // list not contains epc
//                            EPC newEPC=new EPC ();
//                            newEPC.setEpc (epc);
//                            list.add (newEPC);
//                        }
//                    }
//                }
//                for (EPC epcData : list) {
//                    EPC epc1=new EPC ();
//                    epc1.setId (idCount);
//                    epc1.setEpc (epcData.getEpc ());
//                    epc1.setCount (epcData.getCount () + 1);
//                    idCount++;
//                    dataStr.add (epc1);
//                }
//                ecp=dataStr.get(0).getEpc();
//                mTextViewEPCNumber.setText(dataStr.get(0).getEpc());
//             //   adapter.setData (dataStr);
//              //  listView.setAdapter (adapter);

            }
        });
    }
    class DisplayStatusReceiver extends BroadcastReceiver {
        String SCREEN_ON="android.intent.action.SCREEN_ON";
        String SCREEN_OFF="android.intent.action.SCREEN_OFF";

        @Override
        public void onReceive(Context context, Intent intent) {
//            if (SCREEN_ON.equals (intent.getAction ())) {
//                isStartFlag=(boolean) SPUtils.get (context, "startFlag", true);
//                startFlag=isStartFlag;
//            } else if (SCREEN_OFF.equals (intent.getAction ())) {
//                SPUtils.remove (context, "startFlag");
//                SPUtils.put (context, "startFlag", startFlag);
//                startFlag=false;
//
//            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume ();
//        PowerUtil.power ("1");
//        int value=(int) SPUtils.get (this, "DBM", 0);
//        if (value != 0) {
//            reader.setOutputPower (value);
//        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy ();
//        PowerUtil.power ("0");
//        runFlag=false;
//        if (reader != null) {
//            reader.close ();
//        }
//        unregisterReceiver (mDisplayStatusReceiver);
    }
}

