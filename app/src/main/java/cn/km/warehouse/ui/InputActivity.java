package cn.km.warehouse.ui;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.liaoinstan.springview.container.AliHeader;
import com.liaoinstan.springview.container.DefaultFooter;
import com.liaoinstan.springview.widget.SpringView;

import java.util.ArrayList;
import java.util.List;

import cn.km.warehouse.Contstant;
import cn.km.warehouse.R;
import cn.km.warehouse.base.BaseActivity;
import cn.km.warehouse.model.CKModel;
import cn.km.warehouse.model.RKListQDModel;
import cn.km.warehouse.model.RKlistModel;
import cn.km.warehouse.present.RukuListPresenter;
import cn.km.warehouse.ui.adapter.InputAdapter;
import cn.km.warehouse.ui.customview.MyToolbar;
import cn.km.warehouse.ui.customview.ProgressLoadingLayout;
import cn.km.warehouse.utils.SPUtils;
import cn.km.warehouse.view.RukuListView;

public class InputActivity extends BaseActivity implements RukuListView, SpringView.OnFreshListener {
    private RecyclerView mRecyClerView;
    private MyToolbar myToolbar;
    private RukuListPresenter mRuKuListPresenter;
    private int pageInex=1;
    private int totalpage;
    private CKModel mCkModel;
    SpringView mSpringView;
    ProgressLoadingLayout progressLoadingLayout;
    private List<InputAdapter.Item> items;
    private List<InputAdapter.Item> itemsAll = new ArrayList<>();
    InputAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);
        mCkModel= SPUtils.getBeanCK(this, Contstant.ck_key);
        progressLoadingLayout=this.findViewById(R.id.progressLoadingLayout);
        mRuKuListPresenter=new RukuListPresenter(this);
        mSpringView=this.findViewById(R.id.springview_rk);
        myToolbar = findViewById(R.id.toolBar);
       // myToolbar. setNavigationIcon(R.drawable.ic_back);
        myToolbar.setTitleWithNavigation("请选择入库申请单",true);
        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputActivity.this.finish();
            }
        });
        mRecyClerView=this.findViewById(R.id.recyclerView_input);
        initData();
    }
  public void   initData(){
      mSpringView.setHeader(new AliHeader(this, true));
      //mSpringView.setFooter(new AliFooter(this, true));
      mSpringView.setFooter(new DefaultFooter(this));
      mSpringView.setType(SpringView.Type.FOLLOW);
      mSpringView.setListener(this);
      mRuKuListPresenter.getRKlist(pageInex, Contstant.pageSize,mCkModel.getId(),Contstant.totalCount);
      mAdapter = new InputAdapter();
      mRecyClerView.setLayoutManager(new LinearLayoutManager(this));
      mRecyClerView.setHasFixedSize(true);
      mRecyClerView.setNestedScrollingEnabled(false);
      mRecyClerView.setFocusable(false);
      mRecyClerView.setAdapter(mAdapter);

  };

    @Override
    protected void setStatusBar() {
        super.setStatusBar();
    }

    @Override
    public void getRuKuListSuccess(RKlistModel rKlistModel) {
        if (rKlistModel == null || rKlistModel.getList().size() == 0) {
            if (pageInex == 1) {
                progressLoadingLayout.showEmpty();
            }
            mSpringView.onFinishFreshAndLoad();
            return;
        }
        pageInex=rKlistModel.getCurrPage();
        totalpage=rKlistModel.getTotalPage();
        progressLoadingLayout.showContent();
        transformData(rKlistModel);
        if (items == null) {
            mSpringView.onFinishFreshAndLoad();
            return;
        }
        if (pageInex == 1) {
            mSpringView.onFinishFreshAndLoad();
            mAdapter.setNewData(items);
            itemsAll.clear();
            itemsAll.addAll(items);
        } else {
            if (items != null) {
                itemsAll.addAll(items);
                mAdapter.setNewData(itemsAll);
                mAdapter.notifyDataSetChanged();
            }

            mSpringView.onFinishFreshAndLoad();
        }
        mAdapter.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {

                    Intent intent=new Intent();
                    intent.setClass(InputActivity.this,RKQDFYActiviy.class);
                    intent.putExtra("applicationId",rKlistModel.getList().get(position).getApplicationId()+"");
//                    startActivity(new Intent(InputActivity.this, RKQDFYActiviy.class));
                    InputActivity.this.startActivity(intent);
                }
            });
        }




    private void transformData(RKlistModel rKlistModel) {
        if (rKlistModel == null) {
            return;
        }
        items = new ArrayList<>();
        for (RKlistModel.ListBean item : rKlistModel.getList()) {
            InputAdapter.Item item_ = new InputAdapter.Item();
            item_.ApplicationCode = item.getApplicationCode();
            if ("1".equals(item.getOrderState()+"")){
                item_.state="未入库 ";
            } if ("2".equals(item.getOrderState()+"")){
                item_.state="入库中 ";
            } if ("3".equals(item.getOrderState()+"")){
                item_.state=" 已完成 ";
            }
         //  = item.getEntryTypeName();
            item_.ssgs = item.getOrganizationName();
            item_.sqr = item.getApplicationAccountName().toString();
            item_.sqsj = item.getApplicationDatetime();
            items.add(item_);
        }
    }
    @Override
    public void getRuKuListSuccessFailer(String message) {

    }

    @Override
    public void getRuKuListQDSuccess(RKListQDModel rkListQDModel) {

    }

    @Override
    public void getRuKuListQDSuccessFailer(String message) {

    }

    @Override
    public void showLoading() {
        progressLoadingLayout.showLoading();
    }

    @Override
    public void dismissLoading() {
        if (totalpage==0){
            progressLoadingLayout.showEmpty();
        }else {
            progressLoadingLayout.showContent();
        }

    }

    @Override
    public void detach() {

    }

    @Override
    public void onRefresh() {
        pageInex = 1;
        if (mRuKuListPresenter != null) {
            mRuKuListPresenter.getRKlist(pageInex, Contstant.pageSize,mCkModel.getId(),Contstant.totalCount);
        }
    }

    @Override
    public void onLoadmore() {
            if (pageInex>=totalpage){
                Toast.makeText(InputActivity.this, "暂无更多数据", Toast.LENGTH_SHORT).show();
                mSpringView.onFinishFreshAndLoad();
                return;
            }

          pageInex = pageInex+1;
        if (mRuKuListPresenter== null) {
            mRuKuListPresenter=new RukuListPresenter(this);

        }
        mRuKuListPresenter.getRKlist(pageInex, Contstant.pageSize,mCkModel.getId(),Contstant.totalCount);
    }
}
