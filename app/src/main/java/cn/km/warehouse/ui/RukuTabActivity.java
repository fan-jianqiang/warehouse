package cn.km.warehouse.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import cn.km.warehouse.R;
import cn.km.warehouse.base.BaseActivity;
import cn.km.warehouse.ui.customview.MyToolbar;

public class RukuTabActivity extends BaseActivity implements View.OnClickListener {
    private RKFragmentRKZ mRkFragmentRKZ;
    private RKFragmentSY mRkFragmentSY;
    private RKFragmentYWC mRkFragmentYWC;
    private FragmentManager mFragmentManager;
    private TextView mTextViewSY;
    private TextView mTextViewYWC;
    private TextView mTextViewRKZ;
    private MyToolbar myToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ruku_tab);
        initView();
    }

    private void initView() {
        myToolbar = this.findViewById(R.id.toolBar);
        myToolbar.setTitleWithNavigation("入库单", true);
        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RukuTabActivity.this.finish();
            }
        });
        mTextViewSY = this.findViewById(R.id.textivew_sy);
        mTextViewYWC = this.findViewById(R.id.textivew_ycw);
        mTextViewRKZ = this.findViewById(R.id.textivew_rkz);
        mFragmentManager = this.getSupportFragmentManager();
//        mRecycleView=this.findViewById(R.id.recycleview);
        mTextViewSY.setOnClickListener(this);
        mTextViewYWC.setOnClickListener(this);
        mTextViewRKZ.setOnClickListener(this);
        mRkFragmentRKZ = new RKFragmentRKZ();
        mRkFragmentSY = new RKFragmentSY();
        mRkFragmentYWC = new RKFragmentYWC();
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.relvtive_data_structure_container, mRkFragmentSY);
        fragmentTransaction.add(R.id.relvtive_data_structure_container, mRkFragmentRKZ);
        fragmentTransaction.add(R.id.relvtive_data_structure_container, mRkFragmentYWC);
        fragmentTransaction.hide(mRkFragmentRKZ);
        fragmentTransaction.hide(mRkFragmentYWC);
        fragmentTransaction.commit();
        mTextViewSY.performLongClick();
        mTextViewSY.setBackgroundResource(R.drawable.textview_curve_form);
        mTextViewYWC.setTextColor(Color.parseColor("#999999"));
        mTextViewRKZ.setTextColor(Color.parseColor("#999999"));
        mTextViewSY.setTextColor(Color.parseColor("#FFFFFF"));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.textivew_sy:
                showView(0);
                mTextViewSY.setBackgroundResource(R.drawable.textview_curve_form);
                mTextViewSY.setTextColor(Color.parseColor("#FFFFFF"));
                mTextViewRKZ.setTextColor(Color.parseColor("#999999"));
                mTextViewYWC.setTextColor(Color.parseColor("#999999"));
                // mTextViewForm.setBackgroundResource(null);
                mTextViewRKZ.setBackgroundColor(Color.argb(0, 0, 0, 0));
                mTextViewYWC.setBackgroundColor(Color.argb(0, 0, 0, 0));
                break;
            case R.id.textivew_ycw:
                showView(2);
                mTextViewYWC.setBackgroundResource(R.drawable.textview_curve_form);
                mTextViewYWC.setTextColor(Color.parseColor("#FFFFFF"));
                mTextViewRKZ.setTextColor(Color.parseColor("#999999"));
                mTextViewSY.setTextColor(Color.parseColor("#999999"));
                mTextViewRKZ.setBackgroundColor(Color.argb(0, 0, 0, 0));
                mTextViewSY.setBackgroundColor(Color.argb(0, 0, 0, 0));
                break;
            case R.id.textivew_rkz:
                showView(1);
                mTextViewRKZ.setBackgroundResource(R.drawable.textview_curve_form);
                mTextViewRKZ.setTextColor(Color.parseColor("#FFFFFF"));
                mTextViewYWC.setTextColor(Color.parseColor("#999999"));
                mTextViewSY.setTextColor(Color.parseColor("#999999"));
                mTextViewSY.setBackgroundColor(Color.argb(0, 0, 0, 0));
                mTextViewYWC.setBackgroundColor(Color.argb(0, 0, 0, 0));
                break;
        }

    }

    private void showView(int tag) {
        if (mRkFragmentSY != null) {
            hideFrag(mRkFragmentSY);
        }
        if (mRkFragmentRKZ != null) {
            hideFrag(mRkFragmentRKZ);
        }

        if (mRkFragmentYWC != null) {
            hideFrag(mRkFragmentYWC);
        }
        switch (tag) {
            case 0:
                if (mRkFragmentSY != null) {
                    showFrag(mRkFragmentSY);


                }
                if (mRkFragmentRKZ != null) {
                    hideFrag(mRkFragmentRKZ);
                }
                if (mRkFragmentYWC != null) {
                    hideFrag(mRkFragmentRKZ);
                }


                break;
            case 1:
                if (mRkFragmentRKZ != null) {
                    showFrag(mRkFragmentRKZ);
                }
                if (mRkFragmentSY != null) {
                    hideFrag(mRkFragmentSY);
                }
                if (mRkFragmentYWC != null) {
                    hideFrag(mRkFragmentYWC);
                }
                break;
            case 2:
                if (mRkFragmentYWC != null) {
                    showFrag(mRkFragmentYWC);
                }
                if (mRkFragmentSY != null) {
                    hideFrag(mRkFragmentSY);
                }
                if (mRkFragmentRKZ != null) {
                    hideFrag(mRkFragmentRKZ);
                }
                break;
        }
    }

    public void hideFrag(Fragment frag) {
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.hide(frag);
        fragmentTransaction.commit();
    }

    public void showFrag(Fragment frag) {
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.show(frag);
        fragmentTransaction.commit();
    }
}
