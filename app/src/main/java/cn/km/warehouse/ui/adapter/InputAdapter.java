package cn.km.warehouse.ui.adapter;

import android.view.View;
import android.widget.AdapterView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

import cn.km.warehouse.R;
import cn.km.warehouse.base.BaseAdapter;
import cn.km.warehouse.entity.InputEntity;
import cn.km.warehouse.model.RKlistModel;

public class InputAdapter extends BaseAdapter<InputAdapter.Item> {
    public InputAdapter() {
        super(R.layout.item_input);
    }



    @Override
    protected void convert(@NotNull BaseViewHolder holder, InputAdapter.Item item) {
        holder.setText(R.id.tv_title,item.ApplicationCode);
        holder.setText(R.id.tv_status,item.state);
        holder.setText(R.id.tv_wuliaoname,item.ssgs);
        holder.setText(R.id.tv_ggxh,item.sqr);
        holder.setText(R.id.tv_dw,item.sqsj);
        holder.getView(R.id.input_list_sl).setVisibility(View.GONE);
    }
    public static class Item {
        public String ApplicationCode;
        public String state;
        public String ssgs;
        public String sqr;
        public String sqsj;
       // public String sl;
    }
    public GoToQDListDetailListener goToQDListDetailListener;

    public void setGoToQDListDetailListener(GoToQDListDetailListener goToQDListDetailListener) {
        this.goToQDListDetailListener = goToQDListDetailListener;
    }

    public interface GoToQDListDetailListener{
        void GoToQD(String applicationId);
    }
}
