package cn.km.warehouse.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import cn.km.warehouse.R;
import cn.km.warehouse.model.CKModel;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/2/4
 */
public class CKPopuAdapter extends BaseAdapter {
    private Context mContext;
    private List<CKModel> mList;
    private LayoutInflater layoutInflater;

    public CKPopuAdapter(Context mContext, List<CKModel> mList) {
        this.mContext = mContext;
        this.mList = mList;
        layoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        MyViewHolder myViewHolder = null;
        if (convertView == null) {
            myViewHolder = new MyViewHolder();
            convertView = layoutInflater.inflate(R.layout.item_ck_list, parent, false);
            myViewHolder.mTextView = convertView.findViewById(R.id.textview_item_data_structure_popu);
            myViewHolder.mRelativeLayout=convertView.findViewById(R.id.relatvie_container);
            convertView.setTag(myViewHolder);
        }
        myViewHolder = (MyViewHolder) convertView.getTag();
        myViewHolder.mTextView.setText(mList.get(position).getName());
        myViewHolder.mRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ckMenuClickListener!=null) {
                    ckMenuClickListener.SelectCK(position,mList.get(position));
                }
            }
        });
//        //todo 暂时用指标判断的
//        if (mList.get(position).equals("指标")){
//            myViewHolder.mTextView.setBackgroundResource(R.drawable.positionone);
//        }
        return convertView;
    }

    class MyViewHolder {
        TextView mTextView;
        RelativeLayout mRelativeLayout;
    }
    public CKMenuClickListener ckMenuClickListener;
    public void setCkMenuClickListener(CKMenuClickListener ckMenuClickListener) {
        this.ckMenuClickListener = ckMenuClickListener;


    }

    public interface CKMenuClickListener {
        /*
        table  表名 path string
idFiled  字段名 query string
defaultIds
         */
        void SelectCK(int position,CKModel ckModel);
    }



}
