package cn.km.warehouse.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.chad.library.adapter.base.viewholder.BaseViewHolder;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import cn.km.warehouse.R;
import cn.km.warehouse.model.CKModel;
import cn.km.warehouse.model.RKDXQModel;
import cn.km.warehouse.model.RuKuDetailModel;
import cn.km.warehouse.ui.customview.MyListView;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/4
 */
public class RKDXQAdapter extends BaseAdapter {
    private Context mContext;
    private List<RKDXQModel.BillEntityListBean> mList;
    private RKDXQModel.EntryApplicationBean entryApplicationBean;
    private LayoutInflater layoutInflater;

    public RKDXQAdapter(Context mContext, List<RKDXQModel.BillEntityListBean> mList ,RKDXQModel.EntryApplicationBean entryApplicationBean) {
        this.mContext = mContext;
        this.mList = mList;
        this.entryApplicationBean=entryApplicationBean;
        layoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        MyViewHolder myViewHolder = null;
        if (convertView == null) {
            myViewHolder = new MyViewHolder();
            convertView = layoutInflater.inflate(R.layout.item_rkdxq_list, parent, false);
            myViewHolder.mTextViewCode = convertView.findViewById(R.id.tv_code_info);
            myViewHolder.mTextViewName = convertView.findViewById(R.id.tv_name_info);
            myViewHolder.mTextViewGG = convertView.findViewById(R.id.tv_guige_info);
            myViewHolder.mTextViewDW = convertView.findViewById(R.id.tv_unit_info);
            myViewHolder.mTextViewYS = convertView.findViewById(R.id.textview_ys);
            myViewHolder.mTextViewGoRK = convertView.findViewById(R.id.textview_go_rk);
            myViewHolder.myListViewHJCW = convertView.findViewById(R.id.lisview_cwhj);
            convertView.setTag(myViewHolder);
        }
        myViewHolder = (MyViewHolder) convertView.getTag();
        myViewHolder.mTextViewCode.setText(mList.get(position).getMaterialCode());
        myViewHolder.mTextViewName.setText(mList.get(position).getMaterialName());
        myViewHolder.mTextViewGG.setText(mList.get(position).getMaterialSpecs()+"");
        myViewHolder.mTextViewDW.setText(mList.get(position).getMaterialUnitName()+"");
        myViewHolder.mTextViewYS.setText(mList.get(position).getActualNum()+"/"+mList.get(position).getShouldNum());
        RKDXQCWHJAdapter rkdxqcwhjAdapter=new RKDXQCWHJAdapter(mContext,mList.get(position).getStockList());
        myViewHolder.myListViewHJCW.setAdapter(rkdxqcwhjAdapter);
        if (entryApplicationBean.getOrderState()==1 || entryApplicationBean.getOrderState()==2){
            myViewHolder. mTextViewGoRK.setVisibility(View.VISIBLE);
            myViewHolder.mTextViewGoRK.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (goToRKClickListener!=null) {
                        goToRKClickListener.goToRK(position,mList.get(position).getApplicationBillId()+"");
                    }
                }
            });

        }else {
            myViewHolder. mTextViewGoRK.setVisibility(View.GONE);
        }
        return convertView;
    }

    class MyViewHolder {
        TextView mTextViewCode;
        TextView mTextViewName;
        TextView mTextViewGG;
        TextView mTextViewDW;
        TextView mTextViewYS;
        TextView mTextViewGoRK;
        MyListView myListViewHJCW;

    }
    public GoToRKClickListener goToRKClickListener;
    public void setGoToRKClickListener(GoToRKClickListener goToRKClickListener) {
        this.goToRKClickListener = goToRKClickListener;


    }

    public interface GoToRKClickListener {
        /*
        table  表名 path string
idFiled  字段名 query string
defaultIds
         */
        void goToRK(int position, String billid);
    }
}



