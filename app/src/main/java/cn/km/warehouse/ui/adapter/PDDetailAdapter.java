package cn.km.warehouse.ui.adapter;

import com.chad.library.adapter.base.viewholder.BaseViewHolder;

import org.jetbrains.annotations.NotNull;

import cn.km.warehouse.R;
import cn.km.warehouse.base.BaseAdapter;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/9
 */
public class PDDetailAdapter extends BaseAdapter<PDDetailAdapter.Item> {
    public PDDetailAdapter() {
        super(R.layout.item_pddetail_list);
    }



    @Override
    protected void convert(@NotNull BaseViewHolder holder, PDDetailAdapter.Item item) {
        holder.setText(R.id.tv_wlbm,item.wlbm);
        holder.setText(R.id.tv_wlmc,item.wlmc);
        holder.setText(R.id.tv_wlxh,item.wlxh);
        holder.setText(R.id.tv_dw,item.dw);

//        if (listBean.getOrderState()==3){
//
//        }if (listBean.getOrderState()==2){
//            holder.setText(R.id.tv_status,"入库中");
//        }if (listBean.getOrderState()==1){
//            holder.setText(R.id.tv_status,"未入库");
//        }
//        holder.getView(R.id.relative_content_collection, new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (goToQDListDetailListener!=null){
//                    String code=item.ApplicationCode;
//                    goToQDListDetailListener.GoToQD(code);
//                }
//            }
//        });
//        holder.
    }
    public static class Item {
        public String wlbm;
        public String wlmc;
        public String wlxh;
        public String dw;

    }
    public InputAdapter.GoToQDListDetailListener goToQDListDetailListener;

    public void setGoToQDListDetailListener(InputAdapter.GoToQDListDetailListener goToQDListDetailListener) {
        this.goToQDListDetailListener = goToQDListDetailListener;
    }

    public interface GoToQDListDetailListener{
        void GoToQD(String applicationId);
    }
}

