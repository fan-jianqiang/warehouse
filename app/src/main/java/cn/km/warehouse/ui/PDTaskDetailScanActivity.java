package cn.km.warehouse.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.magicrf.uhfreaderlib.reader.UhfReader;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import cn.km.warehouse.Contstant;
import cn.km.warehouse.R;
import cn.km.warehouse.base.BaseActivity;
import cn.km.warehouse.event.ScanEvent;
import cn.km.warehouse.model.CKModel;
import cn.km.warehouse.model.PDScanModel;
import cn.km.warehouse.present.PDScanPresenter;
import cn.km.warehouse.uhf.UHFCKHome;
import cn.km.warehouse.uhf.UHFHome;
import cn.km.warehouse.uhf.util.LogUtils;
import cn.km.warehouse.uhf.util.PowerUtil;
import cn.km.warehouse.uhf.util.SoundPoolHelper;
import cn.km.warehouse.ui.customview.MyToolbar;
import cn.km.warehouse.utils.SPUtils;
import cn.km.warehouse.view.PDScanView;
import cn.km.warehouse.widget.CustomProgressDialog;

public class PDTaskDetailScanActivity extends BaseActivity implements View.OnClickListener, PDScanView {
    private MyToolbar myToolbar;
    private TextView mTextViewCode;
    private TextView mTextViewName;
    private TextView mTextViewXH;
    private TextView mTextViewDW;
    private TextView mTextViewSL;
    private TextView mTextViewScan;
    private PDScanPresenter pdScanPresenter;
    private String taskId;
    private CKModel mCkModel;
    private String serialPortPath = "/dev/ttyS2";
    private UhfReader reader = null;
    private DisplayStatusReceiver mDisplayStatusReceiver;
    private boolean isStartFlag;
    private boolean startFlag = false;
    private boolean runFlag = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdtask_detail_scan);
        mCkModel= SPUtils.getBeanCK(this, Contstant.ck_key);
        EventBus.getDefault().register(this);
        myToolbar = findViewById(R.id.toolBar);
        myToolbar.setTitleWithNavigation("存货盘点", true);
        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PDTaskDetailScanActivity.this.finish();
            }
        });
        taskId=getIntent().getStringExtra("taskId");
        mTextViewCode=this.findViewById(R.id.tv_code_info);
        mTextViewName=this.findViewById(R.id.tv_name_info);
        mTextViewXH=this.findViewById(R.id.tv_guige_info);
        mTextViewDW=this.findViewById(R.id.tv_unit_info);
        mTextViewSL=this.findViewById(R.id.textview_ys);
        mTextViewScan=this.findViewById(R.id.textview_scan);
        mTextViewScan.setOnClickListener(this);
        pdScanPresenter=new PDScanPresenter(this);
        startFlag = false;
        initData();
    }
    private void initData() {
        UhfReader.setPortPath(serialPortPath);
        reader = UhfReader.getInstance();
        int dbm = (int) cn.km.warehouse.uhf.util.SPUtils.get(this, "DBM", 0);
        if (dbm != 0) {
            LogUtils.d(TAG, "initData: " + dbm);

        }
        reader.setOutputPower(26);
        SoundPoolHelper.getInstance(this);
        registerDisplayStatusReceiver();
    }
    @Override
    protected void setStatusBar() {
        super.setStatusBar();
    }
    UHFHome uhfHome;
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.textview_scan:
               // UHFCKHome uhfckHome=new UHFCKHome(this,reader);
                uhfHome = new UHFHome("", "", this, reader);

                break;
        }
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void ScanSuccessed(ScanEvent event) {
     getCKScanData(event.getRfid());

    }
    @Override
    protected void onResume() {
        super.onResume();
        PowerUtil.power("1");
        int value = (int) cn.km.warehouse.uhf.util.SPUtils.get(this, "DBM", 0);
        if (value != 0) {
            reader.setOutputPower(value);
        }
    }
    private void getCKScanData(String rfid) {
        if (pdScanPresenter==null){
            pdScanPresenter=new PDScanPresenter(this);
        }
        pdScanPresenter.getPDTaskScan(rfid,mCkModel.getId(),Integer.parseInt(taskId));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        PowerUtil.power("0");
        runFlag = false;
        if (reader != null) {
            reader.close();
        }
        unregisterReceiver(mDisplayStatusReceiver);
    }

    @Override
    public void getPDTaskScanSuccess(PDScanModel pdScanModel) {
        if (pdScanModel==null){
            return;
        }
        mTextViewCode.setText(pdScanModel.getInfo().getMaterialCode());
        mTextViewXH.setText(pdScanModel.getInfo().getMaterialSpecs());
        mTextViewName.setText(pdScanModel.getInfo().getMaterialName());
        mTextViewDW.setText(pdScanModel.getInfo().getMaterialUnitName());
        mTextViewSL.setText(pdScanModel.getAlready()+"/"+pdScanModel.getSum());
       // mTextViewDW.setText(pdScanModel.getInfo().getMaterialUnitName());
    }



    @Override
    public void getPDTaskScanFailer(int code, String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

    }
    private boolean showProgressDiallog = true;
    private CustomProgressDialog mDialog;
    @Override
    public void showLoading() {
        if (showProgressDiallog && mDialog == null) {
            mDialog = new CustomProgressDialog(this);
            mDialog.setMessage("正在加载数据..");
            mDialog.show();
        }else {
            mDialog.setMessage("正在加载数据..");
            mDialog.show();
        }
    }

    @Override
    public void dismissLoading() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
            showProgressDiallog = false;
        }
    }

    @Override
    public void detach() {

    }
    private void registerDisplayStatusReceiver() {
        mDisplayStatusReceiver = new DisplayStatusReceiver();
        IntentFilter screenStatusIF = new IntentFilter();
        screenStatusIF.addAction(Intent.ACTION_SCREEN_ON);
        screenStatusIF.addAction(Intent.ACTION_SCREEN_OFF);
        this.registerReceiver(mDisplayStatusReceiver, screenStatusIF);
    }

    class DisplayStatusReceiver extends BroadcastReceiver {
        String SCREEN_ON = "android.intent.action.SCREEN_ON";
        String SCREEN_OFF = "android.intent.action.SCREEN_OFF";

        @Override
        public void onReceive(Context context, Intent intent) {
            if (SCREEN_ON.equals(intent.getAction())) {
                isStartFlag = (boolean) cn.km.warehouse.uhf.util.SPUtils.get(context, "startFlag", true);
                startFlag = isStartFlag;
            } else if (SCREEN_OFF.equals(intent.getAction())) {
                cn.km.warehouse.uhf.util.SPUtils.remove(context, "startFlag");
                cn.km.warehouse.uhf.util.SPUtils.put(context, "startFlag", startFlag);
                startFlag = false;

            }
        }
    }

}
