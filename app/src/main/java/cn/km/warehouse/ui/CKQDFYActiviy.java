package cn.km.warehouse.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.liaoinstan.springview.container.AliHeader;
import com.liaoinstan.springview.container.DefaultFooter;
import com.liaoinstan.springview.widget.SpringView;
import com.magicrf.uhfreaderlib.reader.UhfReader;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import cn.km.warehouse.Contstant;
import cn.km.warehouse.R;
import cn.km.warehouse.base.BaseActivity;
import cn.km.warehouse.event.ScanEvent;
import cn.km.warehouse.model.CKDModel;
import cn.km.warehouse.model.CKListQDModel;
import cn.km.warehouse.model.CKModel;
import cn.km.warehouse.model.CKQDModel;
import cn.km.warehouse.model.CKScanGetDataMdoel;
import cn.km.warehouse.model.RKListQDModel;
import cn.km.warehouse.model.RKlistModel;
import cn.km.warehouse.present.CKScanPresenter;
import cn.km.warehouse.present.RlrkPresenter;
import cn.km.warehouse.present.RukuListPresenter;
import cn.km.warehouse.present.WLCKPresenter;
import cn.km.warehouse.uhf.UHFCKHome;
import cn.km.warehouse.uhf.UHFHome;
import cn.km.warehouse.uhf.util.LogUtils;
import cn.km.warehouse.uhf.util.PowerUtil;
import cn.km.warehouse.uhf.util.SoundPoolHelper;
import cn.km.warehouse.ui.adapter.InputListAdapter;
import cn.km.warehouse.ui.customview.ContentShowDialog;
import cn.km.warehouse.ui.customview.MyToolbar;
import cn.km.warehouse.ui.customview.ProgressLoadingLayout;
import cn.km.warehouse.utils.SPUtils;
import cn.km.warehouse.view.WLCKView;

public class CKQDFYActiviy extends BaseActivity implements WLCKView, SpringView.OnFreshListener {
    private RecyclerView mRecyClerView;
    private MyToolbar myToolbar;
    private WLCKPresenter mWlckPresenter;
    private int pageInex=1;
    private int totalpage;
    private CKModel mCkModel;
    SpringView mSpringView;
    ProgressLoadingLayout progressLoadingLayout;
    private List<InputListAdapter.Item> items;
    private List<InputListAdapter.Item> itemsAll = new ArrayList<>();
    InputListAdapter mAdapter;
    private Integer applicationId=-10086;
    private  int  orderId;
    private String serialPortPath="/dev/ttyS2";
    private UhfReader reader=null;
    private DisplayStatusReceiver mDisplayStatusReceiver;
    private boolean isStartFlag;
    private boolean startFlag=false;
    private boolean runFlag=true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ckqdfy);
        if (getIntent().getStringExtra("applicationId")!=null){
            applicationId=Integer.parseInt(getIntent().getStringExtra("applicationId"));
        }
        EventBus.getDefault().register(this);
        mCkModel= SPUtils.getBeanCK(this, Contstant.ck_key);
        progressLoadingLayout=this.findViewById(R.id.progressLoadingLayout);
        mWlckPresenter=new WLCKPresenter(this);
        mSpringView=this.findViewById(R.id.springview_ck);
        myToolbar = findViewById(R.id.toolBar);
        myToolbar.setTitleWithNavigation("选择出库清单",true);
        myToolbar.setRightIcon(R.mipmap.core_icon_zxing);
        ScanListener scanListener=new ScanListener();
        myToolbar.setRightImageClickListener(scanListener);
        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CKQDFYActiviy.this.finish();
            }
        });
        mRecyClerView=this.findViewById(R.id.recyclerView_input);
        initData();
    }
    public  class  ScanListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            ScanCK();
        }
    }
    UHFHome uhfHome;
    private void ScanCK() {
//        if (rlrkPresenter==null){
//            rlrkPresenter=new RlrkPresenter(this);
//        }
//        rlrkPresenter.submit(mVMaterialStocks,mRuKuDetailModel.getOrderId());
       // UHFCKHome uhfHome = new UHFCKHome( this,reader);
        uhfHome = new UHFHome("", "", this, reader);
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void ScanSuccessed(ScanEvent event) {
        getCKScanData(event.getRfid());

    }

    private void getCKScanData(String rfid) {
        if (mWlckPresenter==null){
            mWlckPresenter=new WLCKPresenter(this);
        }
        mWlckPresenter.getCKScanData(rfid);
    }
    public void   initData(){
        mSpringView.setHeader(new AliHeader(this, true));
        //mSpringView.setFooter(new AliFooter(this, true));
        mSpringView.setFooter(new DefaultFooter(this));
        mSpringView.setType(SpringView.Type.FOLLOW);
        mSpringView.setListener(this);
        if (applicationId==-10086){
            Toast.makeText(this,"数据错误",Toast.LENGTH_SHORT).show();
            this.finish();
            return;
        }
        mWlckPresenter.getCuKuListQD(pageInex, Contstant.pageSize,applicationId,Contstant.totalCount);
        mAdapter = new InputListAdapter();
        mRecyClerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyClerView.setHasFixedSize(true);
        mRecyClerView.setNestedScrollingEnabled(false);
        mRecyClerView.setFocusable(false);
        mRecyClerView.setAdapter(mAdapter);
        UhfReader.setPortPath(serialPortPath);
        reader = UhfReader.getInstance();

        if (reader == null) {
            Toast.makeText(this, "扫描获取实例错误", Toast.LENGTH_SHORT).show();
        }

        int dbm = (int) cn.km.warehouse.uhf.util.SPUtils.get(this, "DBM", 0);
        if (dbm != 0) {
            LogUtils.d(TAG, "initData: " + dbm);
            reader.setOutputPower(26);
        }
        SoundPoolHelper.getInstance(this);
        registerDisplayStatusReceiver();
        startFlag = false;

    };

    @Override
    protected void setStatusBar() {
        super.setStatusBar();
    }

    private void transformData(CKListQDModel ckdModel) {
        if (ckdModel == null) {
            return;
        }
        items = new ArrayList<>();
        /*
        for (RKListQDModel.ListBean item : rKlistModel.getList()) {
            InputListAdapter.Item item_ = new InputListAdapter.Item();
            item_.ApplicationCode = item.getMaterialCode();
            item_.state = "";
            item_.wlmc = item.getMaterialName();
            item_.ggxh = item.getMaterialSpecs().toString();
            item_.dw = item.getMaterialUnitName().toString();
            item_.billid = item.getApplicationBillId();
            items.add(item_);
        }
         */
        for (CKListQDModel.ListBean item : ckdModel.getList()) {
            InputListAdapter.Item item_ = new InputListAdapter.Item();
            item_.ApplicationCode = item.getMaterialCode();
            item_.state = "";
            item_.wlmc = item.getMaterialName();
            item_.ggxh = item.getMaterialSpecs();
            item_.dw = item.getMaterialUnitName();
             item_.billid = item.getApplicationBillId();
            item_.applicationId=item.getApplicationId();
            item_.materialId=  item.getMaterialId();
             item_.sl = item.getActualNum()+"";
             item_.orderId = item.getOrderId()+"";
            orderId=item.getOrderId();
            items.add(item_);
        }
    }
    @Override
    public void getCKListQDSuccess(CKListQDModel ckListQDModel) {
        if (ckListQDModel == null || ckListQDModel.getList().size() == 0) {
            if (pageInex == 1) {
                progressLoadingLayout.showEmpty();
            }
            mSpringView.onFinishFreshAndLoad();
            return;
        }
        pageInex=ckListQDModel.getCurrPage();
        totalpage=ckListQDModel.getTotalPage();
        progressLoadingLayout.showContent();
        transformData(ckListQDModel);
        if (items == null) {
            mSpringView.onFinishFreshAndLoad();
            return;
        }
        if (pageInex == 1) {
            mSpringView.onFinishFreshAndLoad();
            mAdapter.setNewData(items);
            itemsAll.clear();
            itemsAll.addAll(items);
        } else {
            if (items != null) {
                itemsAll.addAll(items);
                mAdapter.setNewData(itemsAll);
                mAdapter.notifyDataSetChanged();
            }

            mSpringView.onFinishFreshAndLoad();
        }
        mAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {
                Intent intent=new Intent();
                intent.putExtra("applicationId",mAdapter.getItem(position).applicationId+"");
                intent.putExtra("materialId",mAdapter.getItem(position).materialId+"");
              //  intent.putExtra("orderId",mAdapter.getItem(position).orderId+"");
               // intent.putExtra("applicationId",mAdapter.getItem(position).+"");
                intent.setClass(CKQDFYActiviy.this,CKSCANActivity.class);
                CKQDFYActiviy.this. startActivity(intent);
            }
        });
    }
    @Override
    public void submitSuccess(String message) {
        Toast.makeText(this,"出库成功",Toast.LENGTH_SHORT).show();
        mWlckPresenter.getCuKuListQD(pageInex, Contstant.pageSize,applicationId,Contstant.totalCount);
    }
    @Override
    public void submitFailer(int code, String message) {
        if (code==0){
            Toast.makeText(this,"出库成功",Toast.LENGTH_SHORT).show();
            mWlckPresenter.getCuKuListQD(pageInex, Contstant.pageSize,applicationId,Contstant.totalCount);
        }else {
            Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void getCKScanDataSuccess(CKScanGetDataMdoel ckScanGetDataMdoel) {
        if (ckScanGetDataMdoel==null){
            return;
        }
        initDialog(ckScanGetDataMdoel);
    }



    private ContentShowDialog contentShowDialog;
    private void initDialog(CKScanGetDataMdoel ckScanGetDataMdoel) {
        if (contentShowDialog == null) {
            int[] ids = {R.id.textview_cancel, R.id.textview_ok_submit,R.id.tv_code_info,R.id.tv_name_info,R.id.tv_guige_info,
                    R.id.tv_unit_info,R.id.tv_sl};

            contentShowDialog = new ContentShowDialog(this, R.layout.scan_ck_dialog, ids,ckScanGetDataMdoel);
        }
        contentShowDialog.show();
        contentShowDialog.setOnCenterItemClickListener(new ContentShowDialog.OnCenterItemClickListener() {
            @Override
            public void OnCenterItemClick(ContentShowDialog dialog, View view) {
                switch (view.getId()) {
                    case R.id.textview_ok_submit:
                        if (mWlckPresenter==null){
                            mWlckPresenter=new WLCKPresenter (CKQDFYActiviy.this);
                        }
//                        if (orderId==null){
//                            Toast.makeText(CKQDFYActiviy.this,"数据错误",Toast.LENGTH_SHORT).show();
//                            return;
//                        }
                        mWlckPresenter.submit(orderId,ckScanGetDataMdoel.getRfid());
                        break;
                    case R.id.textview_cancel:
                        contentShowDialog.dismiss();
                        break;
                }
            }
        });
    }
    @Override
    public void showLoading() {
        progressLoadingLayout.showLoading();
    }

    @Override
    public void dismissLoading() {
        if (totalpage==0){
            progressLoadingLayout.showEmpty();
        }else {
            progressLoadingLayout.showContent();
        }

    }

    @Override
    public void detach() {

    }

    @Override
    public void onRefresh() {
        pageInex = 1;
        if (mWlckPresenter != null) {
            mWlckPresenter.getCuKuListQD(pageInex, Contstant.pageSize,mCkModel.getId(),Contstant.totalCount);
        }
    }

    @Override
    public void onLoadmore() {
        if (pageInex>=totalpage){
            Toast.makeText(CKQDFYActiviy.this, "暂无更多数据", Toast.LENGTH_SHORT).show();
            mSpringView.onFinishFreshAndLoad();
            return;
        }

        pageInex = pageInex+1;
        if (mWlckPresenter== null) {
            mWlckPresenter=new WLCKPresenter(this);

        }
        mWlckPresenter.getCuKuListQD(pageInex, Contstant.pageSize,mCkModel.getId(),Contstant.totalCount);
    }
    @Override
    public void getCKQDSuccess(CKQDModel ckqdModel) {

    }


    @Override
    public void getCKListQDFailer(String message) {

    }

    @Override
    public void getScanDataFailer(int code, String message) {
        Toast.makeText(CKQDFYActiviy.this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getCKListSuccess(CKDModel ckdModel) {

    }

    @Override
    public void getCKListSuccessFailer(String message) {

    }
    private void registerDisplayStatusReceiver() {
        mDisplayStatusReceiver = new DisplayStatusReceiver();
        IntentFilter screenStatusIF = new IntentFilter();
        screenStatusIF.addAction(Intent.ACTION_SCREEN_ON);
        screenStatusIF.addAction(Intent.ACTION_SCREEN_OFF);
        this.registerReceiver(mDisplayStatusReceiver, screenStatusIF);
    }

    class DisplayStatusReceiver extends BroadcastReceiver {
        String SCREEN_ON = "android.intent.action.SCREEN_ON";
        String SCREEN_OFF = "android.intent.action.SCREEN_OFF";

        @Override
        public void onReceive(Context context, Intent intent) {
            if (SCREEN_ON.equals(intent.getAction())) {
                isStartFlag = (boolean) cn.km.warehouse.uhf.util.SPUtils.get(context, "startFlag", true);
                startFlag = isStartFlag;
            } else if (SCREEN_OFF.equals(intent.getAction())) {
                cn.km.warehouse.uhf.util.SPUtils.remove(context, "startFlag");
                cn.km.warehouse.uhf.util.SPUtils.put(context, "startFlag", startFlag);
                startFlag = false;

            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        PowerUtil.power("0");
        runFlag = false;
        if (reader != null) {
            reader.close();
        }
        unregisterReceiver(mDisplayStatusReceiver);
    }
    @Override
    protected void onResume() {
        super.onResume();
        PowerUtil.power("1");
        int value = (int) cn.km.warehouse.uhf.util.SPUtils.get(this, "DBM", 0);
        if (value != 0) {
            reader.setOutputPower(value);
        }
    }
}
