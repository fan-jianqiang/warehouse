package cn.km.warehouse.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import cn.km.warehouse.R;
import cn.km.warehouse.model.RKlistModel;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/1/26
 */
public class WLRKListAdapter extends BaseAdapter {


    private List<RKlistModel> data;
    private LayoutInflater layoutInflater;
    private Context context;

    public WLRKListAdapter(Context context, List<RKlistModel> data) {
        this.context = context;
        this.data = data;
        this.layoutInflater = LayoutInflater.from(context);
    }

    // 组件集合，对应list.xml中的控件
    public final class Zujian {
        TextView mTextViewId;
        TextView mTextViewState;
        TextView mTextViewSSGS;
        TextView mTextViewSQR;
        TextView mTextViewSQSJ;
        public Zujian(View itemView) {
            //logpic = (ImageView) itemView.findViewById(R.id.imgLogTerPic);
            mTextViewId=itemView.findViewById(R.id.textview_number);
            mTextViewState=itemView.findViewById(R.id.textview_state);
            mTextViewSSGS=itemView.findViewById(R.id.textview_ssgs_name);
            mTextViewSQR=itemView.findViewById(R.id.textview_sqr_name);
            mTextViewSQSJ=itemView.findViewById(R.id.textview_sqsj_content);

        }
    }

    public void setData(List<RKlistModel> data) {
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    // 获得某一位置的数据

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    // 获得唯一标识

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Zujian zujian;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.rk_list, null);
            zujian = new Zujian(convertView);
            //获得组件，实例化组件
            convertView.setTag(zujian);
        } else {
//            zujian = (Zujian) convertView.getTag();
//            zujian.mTextViewId.setText(data.get(position).getId());
//            zujian.mTextViewSQR.setText(data.get(position).getSQR());
//            zujian.mTextViewSQSJ.setText(data.get(position).getSQSJ());
//            zujian.mTextViewSSGS.setText(data.get(position).getSSGS());
//            zujian.mTextViewState.setText(data.get(position).getState());
        }

        return convertView;
    }

    public GoToDetailListener goToDetailListener;
    public void setGoToDetailListener(GoToDetailListener goToDetailListener) {
        this.goToDetailListener = goToDetailListener;


    }

    public interface GoToDetailListener{
        void goToDetail(String id);
    }
}
