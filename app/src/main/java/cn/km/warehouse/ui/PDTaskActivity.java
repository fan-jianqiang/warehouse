package cn.km.warehouse.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.View;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.km.warehouse.R;
import cn.km.warehouse.base.BaseActivity;
import cn.km.warehouse.ui.customview.MyToolbar;

public class PDTaskActivity extends BaseActivity {
    TabLayout mTabLayout;
    ViewPager mViewPager;
    MyPagerAdapter myPagerAdapter;
    Map<String, String> map;
    private MyToolbar myToolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdtask);
        mTabLayout = this.findViewById(R.id.tablayout);
        mViewPager = this.findViewById(R.id.viewpager);
        mViewPager.setOffscreenPageLimit(3);
        mTabLayout.setSelectedTabIndicatorHeight(0);
        myToolbar = this.findViewById(R.id.action_bar);
        myToolbar.setTitleWithNavigation("盘点任务", true);
        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PDTaskActivity.this.finish();
            }
        });
        initView();
    }
    public static PDListFragment newInstance(String state, String  title) {
        PDListFragment fragment = new PDListFragment();
        Bundle bundle = new Bundle();
        bundle.putString("state", state);
        bundle.putString("title", title);
        fragment.setArguments(bundle);
        return fragment;
    }

    private void initView() {
        map = new HashMap<>();
        map.put("2", "所有");
        map.put("0", "未完成");
        map.put("1", "已完成");
        myPagerAdapter = new MyPagerAdapter(this.getSupportFragmentManager());
        ArrayList<Fragment> fragments = new ArrayList<>();
//        for (String key : map.keySet()) {//keySet获取map集合key的集合  然后在遍历key即可
//            String value = map.get(key).toString();//
//            System.out.println("key:" + key + " vlaue:" + value);
//
//        }
        PDListFragment fragment1 = PDListFragment.newInstance("2", "所有");
        PDListFragment fragment2 = PDListFragment.newInstance("0", "未完成");
        PDListFragment fragment3 = PDListFragment.newInstance("1", "已完成");
        fragments.add(fragment1);
        fragments.add(fragment2);
        fragments.add(fragment3);
        myPagerAdapter.setFragments(fragments);
        mViewPager.setAdapter(myPagerAdapter);
        for (int i = 0; i <3; i++) {
            mTabLayout.addTab(mTabLayout.newTab());//添加tab选项
        }
        mTabLayout.setupWithViewPager(mViewPager);
        mTabLayout.getTabAt(0).setText(map.get((2)+""));
        mTabLayout.getTabAt(1).setText(map.get((0)+""));
        mTabLayout.getTabAt(2).setText(map.get((1)+""));
//        for (int i = 0; i < mTabLayout.getTabCount(); i++) {
//            mTabLayout.getTabAt(i).setText(map.get((i)+""));
//        }
    }

    public class MyPagerAdapter extends FragmentStatePagerAdapter {

        protected List<Fragment> mFragmentList;

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public void setFragments(ArrayList<Fragment> fragments) {
            mFragmentList = fragments;
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = mFragmentList.get(position);
            return fragment;
        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

    }
}
