package cn.km.warehouse.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.liaoinstan.springview.container.AliHeader;
import com.liaoinstan.springview.container.DefaultFooter;
import com.liaoinstan.springview.widget.SpringView;

import java.util.ArrayList;
import java.util.List;

import cn.km.warehouse.Contstant;
import cn.km.warehouse.R;
import cn.km.warehouse.base.BaseActivity;
import cn.km.warehouse.model.CKModel;
import cn.km.warehouse.model.RKListQDModel;
import cn.km.warehouse.model.RKlistModel;
import cn.km.warehouse.present.RukuListPresenter;
import cn.km.warehouse.ui.adapter.InputListAdapter;
import cn.km.warehouse.ui.customview.MyToolbar;
import cn.km.warehouse.ui.customview.ProgressLoadingLayout;
import cn.km.warehouse.utils.SPUtils;
import cn.km.warehouse.view.RukuListView;

public class RKQDFYActiviy extends BaseActivity implements RukuListView, SpringView.OnFreshListener{
    private RecyclerView mRecyClerView;
    private MyToolbar myToolbar;
    private RukuListPresenter mRuKuListPresenter;
    private int pageInex=1;
    private int totalpage;
    private CKModel mCkModel;
    SpringView mSpringView;
    ProgressLoadingLayout progressLoadingLayout;
    private List<InputListAdapter.Item> items;
    private List<InputListAdapter.Item> itemsAll = new ArrayList<>();
    InputListAdapter mAdapter;
    private Integer applicationId=-10086;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rkqdfyactiviy);
        if (getIntent().getStringExtra("applicationId")!=null){
            applicationId=Integer.parseInt(getIntent().getStringExtra("applicationId"));
        }

        mCkModel= SPUtils.getBeanCK(this, Contstant.ck_key);
        progressLoadingLayout=this.findViewById(R.id.progressLoadingLayout);
        mRuKuListPresenter=new RukuListPresenter(this);
        mSpringView=this.findViewById(R.id.springview_rk);
        myToolbar = findViewById(R.id.toolBar);
        // myToolbar. setNavigationIcon(R.drawable.ic_back);
        myToolbar.setTitleWithNavigation("选择入库清单",true);
        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RKQDFYActiviy.this.finish();
            }
        });
        mRecyClerView=this.findViewById(R.id.recyclerView_input);
        initData();
    }
    public void   initData(){
        mSpringView.setHeader(new AliHeader(this, true));
        //mSpringView.setFooter(new AliFooter(this, true));
        mSpringView.setFooter(new DefaultFooter(this));
        mSpringView.setType(SpringView.Type.FOLLOW);
        mSpringView.setListener(this);
        if (applicationId==-10086){
            Toast.makeText(this,"数据错误",Toast.LENGTH_SHORT).show();
            this.finish();
            return;
        }
        progressLoadingLayout.showLoading();
        mRuKuListPresenter.getRuKuListQD(pageInex, Contstant.pageSize,applicationId,Contstant.totalCount);
        mAdapter = new InputListAdapter();
        mRecyClerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyClerView.setHasFixedSize(true);
        mRecyClerView.setNestedScrollingEnabled(false);
        mRecyClerView.setFocusable(false);
        mRecyClerView.setAdapter(mAdapter);

    };

    @Override
    protected void setStatusBar() {
        super.setStatusBar();
    }

    @Override
    public void getRuKuListSuccess(RKlistModel rKlistModel) {

    }




    private void transformData(RKListQDModel rKlistModel) {
        if (rKlistModel == null) {
            return;
        }
        items = new ArrayList<>();
        for (RKListQDModel.ListBean item : rKlistModel.getList()) {
            InputListAdapter.Item item_ = new InputListAdapter.Item();
            item_.ApplicationCode = item.getMaterialCode();
            item_.state = "";
            item_.wlmc = item.getMaterialName();
            item_.ggxh = item.getMaterialSpecs();
            item_.dw = item.getMaterialUnitName();
            item_.billid = item.getApplicationBillId();
            item_.sl = item.getActualNum()+"";
            items.add(item_);
        }
    }
    @Override
    public void getRuKuListSuccessFailer(String message) {

    }

    @Override
    public void getRuKuListQDSuccess(RKListQDModel rkListQDModel) {
        if (rkListQDModel == null || rkListQDModel.getList().size() == 0) {
            if (pageInex == 1) {
                progressLoadingLayout.showEmpty();
            }
            mSpringView.onFinishFreshAndLoad();
            return;
        }
        mSpringView.onFinishFreshAndLoad();
        pageInex=rkListQDModel.getCurrPage();
        totalpage=rkListQDModel.getTotalPage();
        progressLoadingLayout.showContent();
        transformData(rkListQDModel);
        if (items == null) {
            mSpringView.onFinishFreshAndLoad();
            return;
        }
        if (pageInex == 1) {
            mSpringView.onFinishFreshAndLoad();
            mAdapter.setNewData(items);
            itemsAll.clear();
            itemsAll.addAll(items);
        } else {
            if (items != null) {
                itemsAll.addAll(items);
                mAdapter.setNewData(itemsAll);
                mAdapter.notifyDataSetChanged();
            }

            mSpringView.onFinishFreshAndLoad();
        }
        mAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {
                Intent intent=new Intent();
                intent.putExtra("billid",mAdapter.getItem(position).billid+"");
                intent.setClass(RKQDFYActiviy.this,RlrkActivity.class);
                RKQDFYActiviy.this. startActivity(intent);
            }
        });
    }

    @Override
    public void getRuKuListQDSuccessFailer(String message) {

    }

    @Override
    public void showLoading() {
        progressLoadingLayout.showLoading();
    }

    @Override
    public void dismissLoading() {
        if (totalpage==0){
            progressLoadingLayout.showEmpty();
        }else {
            progressLoadingLayout.showContent();
        }
        mSpringView.onFinishFreshAndLoad();
    }

    @Override
    public void detach() {

    }

    @Override
    public void onRefresh() {
        pageInex = 1;
        if (mRuKuListPresenter != null) {
            mRuKuListPresenter.getRKlist(pageInex, Contstant.pageSize,mCkModel.getId(),Contstant.totalCount);
        }
    }

    @Override
    public void onLoadmore() {
        if (pageInex>=totalpage){
            Toast.makeText(RKQDFYActiviy.this, "暂无更多数据", Toast.LENGTH_SHORT).show();
            mSpringView.onFinishFreshAndLoad();
            return;
        }

        pageInex = pageInex+1;
        if (mRuKuListPresenter== null) {
            mRuKuListPresenter=new RukuListPresenter(this);

        }
        mRuKuListPresenter.getRKlist(pageInex, Contstant.pageSize,mCkModel.getId(),Contstant.totalCount);
    }
}
