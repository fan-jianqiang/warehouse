package cn.km.warehouse.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import cn.km.warehouse.R;
import cn.km.warehouse.model.CKDXQModel;
import cn.km.warehouse.model.RKDXQModel;
import cn.km.warehouse.ui.customview.MyListView;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/16
 */
public class CKDXQAdapter extends BaseAdapter {
    private Context mContext;
    private List<CKDXQModel.BillEntityListBean> mList;
    private CKDXQModel.DeliveryOrderBean deliveryOrderBean;
    private LayoutInflater layoutInflater;

    public CKDXQAdapter(Context mContext, List<CKDXQModel.BillEntityListBean> mList ,CKDXQModel.DeliveryOrderBean deliveryOrderBean) {
        this.mContext = mContext;
        this.mList = mList;
        this.deliveryOrderBean=deliveryOrderBean;
        layoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        MyViewHolder myViewHolder = null;
        if (convertView == null) {
            myViewHolder = new MyViewHolder();
            convertView = layoutInflater.inflate(R.layout.item_ckdxq_list, parent, false);
            myViewHolder.mTextViewCode = convertView.findViewById(R.id.tv_code_info);
            myViewHolder.mTextViewName = convertView.findViewById(R.id.tv_name_info);
            myViewHolder.mTextViewGG = convertView.findViewById(R.id.tv_guige_info);
            myViewHolder.mTextViewDW = convertView.findViewById(R.id.tv_unit_info);
            myViewHolder.mTextViewYS = convertView.findViewById(R.id.textview_ys);
            myViewHolder.mTextViewGoRK = convertView.findViewById(R.id.textview_go_rk);
            myViewHolder.myListViewHJCW = convertView.findViewById(R.id.lisview_cwhj);
            convertView.setTag(myViewHolder);
        }
        myViewHolder = (MyViewHolder) convertView.getTag();
        myViewHolder.mTextViewCode.setText(mList.get(position).getMaterialCode());
        myViewHolder.mTextViewName.setText(mList.get(position).getMaterialName());
        myViewHolder.mTextViewGG.setText(mList.get(position).getMaterialSpecs()+"");
        myViewHolder.mTextViewDW.setText(mList.get(position).getMaterialUnitName()+"");
        myViewHolder.mTextViewYS.setText(mList.get(position).getActualNum()+"/"+mList.get(position).getShouldNum());
        CKDXQCWHJAdapter ckdxqcwhjAdapter=new CKDXQCWHJAdapter(mContext,mList.get(position).getStockList());
        myViewHolder.myListViewHJCW.setAdapter(ckdxqcwhjAdapter);
        if (deliveryOrderBean.getOrderState()==1 || deliveryOrderBean.getOrderState()==2){
            myViewHolder. mTextViewGoRK.setVisibility(View.VISIBLE);
            myViewHolder.mTextViewGoRK.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (goToCKClickListener!=null) {
                        goToCKClickListener.goToCK(position,mList.get(position).getApplicationId()+"",mList.get(position).getMaterialId()+"");
                    }
                }
            });

        }else {
            myViewHolder. mTextViewGoRK.setVisibility(View.GONE);
        }
        return convertView;
    }

    class MyViewHolder {
        TextView mTextViewCode;
        TextView mTextViewName;
        TextView mTextViewGG;
        TextView mTextViewDW;
        TextView mTextViewYS;
        TextView mTextViewGoRK;
        MyListView myListViewHJCW;

    }
    public GoToCKClickListener goToCKClickListener;
    public void setGoToCKClickListener(GoToCKClickListener goToCKClickListener) {
        this.goToCKClickListener = goToCKClickListener;


    }

    public interface GoToCKClickListener {
        /*
        table  表名 path string
idFiled  字段名 query string
defaultIds
         */
        void goToCK(int position, String applicationId,String materialId);
    }
}




