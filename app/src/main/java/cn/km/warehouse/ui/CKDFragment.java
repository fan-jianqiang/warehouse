package cn.km.warehouse.ui;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.liaoinstan.springview.container.AliHeader;
import com.liaoinstan.springview.container.DefaultFooter;
import com.liaoinstan.springview.widget.SpringView;

import java.util.ArrayList;
import java.util.List;

import cn.km.warehouse.Contstant;
import cn.km.warehouse.R;
import cn.km.warehouse.model.CKDModel;
import cn.km.warehouse.model.CKListQDModel;
import cn.km.warehouse.model.CKModel;
import cn.km.warehouse.model.CKQDModel;
import cn.km.warehouse.model.CKScanGetDataMdoel;
import cn.km.warehouse.model.RKListQDModel;
import cn.km.warehouse.model.RKlistModel;
import cn.km.warehouse.present.RukuListPresenter;
import cn.km.warehouse.present.WLCKPresenter;
import cn.km.warehouse.ui.adapter.InputAdapter;
import cn.km.warehouse.ui.customview.ProgressLoadingLayout;
import cn.km.warehouse.utils.SPUtils;
import cn.km.warehouse.view.WLCKView;

/**
 * A simple {@link Fragment} subclass.
 */
public class CKDFragment extends Fragment implements WLCKView, SpringView.OnFreshListener {
    private String state;
    private String title;
    private RecyclerView mRecyClerView;
    private WLCKPresenter mWlckPresenter;
    private int pageInex=1;
    private int totalpage;
    private CKModel mCkModel;
    SpringView mSpringView;
    ProgressLoadingLayout progressLoadingLayout;
    private List<InputAdapter.Item> items;
    private List<InputAdapter.Item> itemsAll = new ArrayList<>();
    InputAdapter mAdapter;
    public static CKDFragment newInstance(String state, String title) {
        CKDFragment fragment = new CKDFragment();
        Bundle bundle = new Bundle();
        bundle.putString("state", state);
        bundle.putString("title", title);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ckd, container, false);
        mCkModel= SPUtils.getBeanCK(getActivity(), Contstant.ck_key);
        progressLoadingLayout=view.findViewById(R.id.progressLoadingLayout);
        mWlckPresenter=new WLCKPresenter(this);
        mSpringView=view.findViewById(R.id.springview_ck);
        mRecyClerView=view.findViewById(R.id.recyclerView_input);
        initData();
        return view;
    }

    private void initData() {
        state = this.getArguments().getString("state");
        title = this.getArguments().get("title").toString();
        mSpringView.setHeader(new AliHeader(getActivity(), true));
        //mSpringView.setFooter(new AliFooter(this, true));
        mSpringView.setFooter(new DefaultFooter(getActivity()));
        mSpringView.setType(SpringView.Type.FOLLOW);
        mSpringView.setListener(this);
        mWlckPresenter.getCKQDlist(pageInex, Contstant.pageSize,mCkModel.getId(),Contstant.totalCount,Integer.parseInt(state));
        mAdapter = new InputAdapter();
        mRecyClerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyClerView.setHasFixedSize(true);
        mRecyClerView.setNestedScrollingEnabled(false);
        mRecyClerView.setFocusable(false);
        mRecyClerView.setAdapter(mAdapter);
    }
    @Override
    public void onRefresh() {
        pageInex = 1;
        if (mWlckPresenter != null) {
            mWlckPresenter.getCKQDlist(pageInex, Contstant.pageSize,mCkModel.getId(),Contstant.totalCount,Integer.parseInt(state));
        }
    }

    @Override
    public void onLoadmore() {
        if (pageInex>=totalpage){
            Toast.makeText(getActivity(), "暂无更多数据", Toast.LENGTH_SHORT).show();
            mSpringView.onFinishFreshAndLoad();
            return;
        }

        pageInex = pageInex+1;
        if (mWlckPresenter== null) {
            mWlckPresenter=new WLCKPresenter(this);

        }
        mWlckPresenter.getCKQDlist(pageInex, Contstant.pageSize,mCkModel.getId(),Contstant.totalCount,Integer.parseInt(state));
    }



    @Override
    public void showLoading() {
        progressLoadingLayout.showLoading();
    }

    @Override
    public void dismissLoading() {
        if (totalpage==0){
            progressLoadingLayout.showEmpty();
        }else {
            progressLoadingLayout.showContent();
        }
    }

    @Override
    public void detach() {

    }

    @Override
    public void getCKListSuccess(CKDModel ckdModel) {

    }

    @Override
    public void getCKListSuccessFailer(String message) {

    }

    @Override
    public void getCKListQDSuccess(CKListQDModel ckListQDModel) {

    }



    @Override
    public void getCKQDSuccess(CKQDModel ckqdModel) {
        if (ckqdModel == null || ckqdModel.getList().size() == 0) {
            if (pageInex == 1) {
                progressLoadingLayout.showEmpty();
            }
            mSpringView.onFinishFreshAndLoad();
            return;
        }
        pageInex=ckqdModel.getCurrPage();
        totalpage=ckqdModel.getTotalPage();
        progressLoadingLayout.showContent();
        transformData(ckqdModel);
        if (items == null) {
            mSpringView.onFinishFreshAndLoad();
            return;
        }
        if (pageInex == 1) {
            mSpringView.onFinishFreshAndLoad();
            mAdapter.setNewData(items);
            itemsAll.clear();
            itemsAll.addAll(items);
        } else {
            if (items != null) {
                itemsAll.addAll(items);
                mAdapter.setNewData(itemsAll);
                mAdapter.notifyDataSetChanged();
            }

            mSpringView.onFinishFreshAndLoad();
        }
        mAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {

                Intent intent=new Intent();
                intent.setClass(getActivity(),CKDXQActivity.class);
                intent.putExtra("orderId",ckqdModel.getList().get(position).getOrderId()+"");
//                    startActivity(new Intent(InputActivity.this, RKQDFYActiviy.class));
                getActivity().startActivity(intent);
            }
        });
    }

    private void transformData(CKQDModel ckqdModel) {
        if (ckqdModel == null) {
            return;
        }
        items = new ArrayList<>();
        for (CKQDModel.ListBean item : ckqdModel.getList()) {
            InputAdapter.Item item_ = new InputAdapter.Item();
            item_.ApplicationCode = item.getOrderCode();
            if ("1".equals(item.getOrderState()+"")){
                item_.state="待出库 ";
            } if ("2".equals(item.getOrderState()+"")){
                item_.state="出库中 ";
            } if ("3".equals(item.getOrderState()+"")){
                item_.state=" 已完成 ";
            }
            //  = item.getEntryTypeName();
            item_.ssgs = item.getOrganizationName();
            item_.sqr = item.getApplicationAccountName().toString();
            item_.sqsj = item.getApplicationDatetime();
            items.add(item_);
        }
    }


    @Override
    public void getCKListQDFailer(String message) {

    }

    @Override
    public void getScanDataFailer(int code, String message) {

    }

    @Override
    public void submitFailer(int code, String message) {

    }

    @Override
    public void getCKScanDataSuccess(CKScanGetDataMdoel ckScanGetDataMdoel) {

    }

    @Override
    public void submitSuccess(String message) {

    }
}
