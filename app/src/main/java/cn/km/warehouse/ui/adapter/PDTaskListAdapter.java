package cn.km.warehouse.ui.adapter;

import com.chad.library.adapter.base.viewholder.BaseViewHolder;

import org.jetbrains.annotations.NotNull;

import cn.km.warehouse.R;
import cn.km.warehouse.base.BaseAdapter;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/7
 */
public class PDTaskListAdapter extends BaseAdapter<PDTaskListAdapter.Item> {
    public PDTaskListAdapter() {
        super(R.layout.item_pdtask_list);
    }



    @Override
    protected void convert(@NotNull BaseViewHolder holder,Item item) {
        holder.setText(R.id.tv_title,item.code);
        holder.setText(R.id.tv_status,item.state);
        holder.setText(R.id.tv_kssj,item.kssj);
        holder.setText(R.id.tv_jssj,item.jssj);
        holder.setText(R.id.tv_fzr,item.fzr);
        holder.setText(R.id.tv_rwsm,item.rwsm);
//        if (listBean.getOrderState()==3){
//
//        }if (listBean.getOrderState()==2){
//            holder.setText(R.id.tv_status,"入库中");
//        }if (listBean.getOrderState()==1){
//            holder.setText(R.id.tv_status,"未入库");
//        }
//        holder.getView(R.id.relative_content_collection, new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (goToQDListDetailListener!=null){
//                    String code=item.ApplicationCode;
//                    goToQDListDetailListener.GoToQD(code);
//                }
//            }
//        });
//        holder.
    }
    public static class Item {
        public String kssj;
        public String jssj;
        public String fzr;
        public String rwsm;
        public String code;
        public String state;
    }
    public InputAdapter.GoToQDListDetailListener goToQDListDetailListener;

    public void setGoToQDListDetailListener(InputAdapter.GoToQDListDetailListener goToQDListDetailListener) {
        this.goToQDListDetailListener = goToQDListDetailListener;
    }

    public interface GoToQDListDetailListener{
        void GoToQD(String applicationId);
    }
}

