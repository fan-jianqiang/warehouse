package cn.km.warehouse.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import cn.km.warehouse.R;
import cn.km.warehouse.uhf.util.LabelReadOrWriteUtils;

public class TestActivity extends AppCompatActivity {
    private EditText mEditextone;
    private EditText mEditextz;
    private EditText mEditextj;
    private Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        mEditextone=this.findViewById(R.id.editext_read_number);
        mEditextz=this.findViewById(R.id.editext_zhuan);
        mEditextj=this.findViewById(R.id.editext_j);
        button=this.findViewById(R.id.bt);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s=mEditextone.getText().toString().trim();
                String s1 = LabelReadOrWriteUtils.stringToHexString(s);
                mEditextz.setText(s1);
                String s2 = LabelReadOrWriteUtils.hexStringToString(s1);
                mEditextj.setText(s2);
            }
        });

    }

}
