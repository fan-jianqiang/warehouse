package cn.km.warehouse.ui.customview;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import cn.km.warehouse.R;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/13
 */
public class BaseDialog  extends Dialog {

    protected Context context;

    protected LayoutInflater inflater;

    protected OnDialogClickListener mListener ;

    public boolean clickWithDismiss = true;

    protected TextView titleView;

    protected OnDialogCancleListener onDialogCancleListener;

    /**
     * 构造方法
     * @param context
     */
    public BaseDialog(Context context) {
        super(context, R.style.BaseDialog);
        this.setCanceledOnTouchOutside(false);

        this.context = context;
        this.inflater = LayoutInflater.from(context);
        mListener = new OnDialogClickListener(this);
        if(context instanceof Activity){
            //添加设置Activity，用于后续弹窗时判断Activity是否结束，未结束才弹窗
            setOwnerActivity((Activity) context);
        }
    }

    private class OnDialogClickListener implements View.OnClickListener{
        private Dialog dialog;
        public OnDialogClickListener(Dialog dialog){
            this.dialog = dialog;
        }
        @Override
        public void onClick(View v) {
            if (null != v.getTag()) {
                OnDialogButtonClickListener odbcl = (OnDialogButtonClickListener)v.getTag();
                odbcl.onClick(dialog, v);
            }
            if (clickWithDismiss) {
                dialog.dismiss();
            }
        }
    }

    @Override
    public void dismiss() {
        try {
            super.dismiss();
        } catch (RuntimeException e) {
            //handle exception
        }
    }

    @Override
    public void show() {
        try {
            super.show();
        } catch (RuntimeException e) {
            //handle exception
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(onDialogCancleListener != null)
            onDialogCancleListener.onDialogCancle();
    }

    public void setOnCancleListener(OnDialogCancleListener listener){
        onDialogCancleListener = listener;
    }

    public interface OnDialogCancleListener{
        public void onDialogCancle();
    }

    public interface OnDialogButtonClickListener{
        public void onClick(Dialog dialog, View v);
    }
}

