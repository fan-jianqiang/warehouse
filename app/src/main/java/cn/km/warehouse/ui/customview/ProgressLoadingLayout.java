package cn.km.warehouse.ui.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import cn.km.warehouse.R;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/2/1
 */
public class ProgressLoadingLayout  extends LoadingLayout {
    public ProgressLoadingLayout(@NonNull Context context) {
        super(context);
    }

    public ProgressLoadingLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ProgressLoadingLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected View getLoadingView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.list_adapter_loading_view,this,true);
        return view;
    }
}

