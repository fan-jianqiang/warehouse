package cn.km.warehouse.ui.adapter

import cn.km.warehouse.R
import cn.km.warehouse.entity.RukuDanDetailEntity
import com.chad.library.adapter.base.BaseSectionQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder

class RukuDanDetailAdapter : BaseSectionQuickAdapter<RukuDanDetailEntity,BaseViewHolder>(R.layout.item_rukudan_code_layout,R.layout.item_rukudan_infor_layout) {
    override fun convert(holder: BaseViewHolder, item: RukuDanDetailEntity) {

    }

    override fun convertHeader(helper: BaseViewHolder, item: RukuDanDetailEntity) {
       helper.setText(R.id.tv_code,item.applyCode)
    }
}