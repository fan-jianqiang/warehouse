package cn.km.warehouse.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.magicrf.uhfreaderlib.reader.UhfReader;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import cn.km.warehouse.Contstant;
import cn.km.warehouse.R;
import cn.km.warehouse.base.BaseActivity;
import cn.km.warehouse.event.ScanEvent;
import cn.km.warehouse.model.CKModel;
import cn.km.warehouse.model.HJModel;
import cn.km.warehouse.model.ZYHJScanModel;
import cn.km.warehouse.present.RlrkPresenter;
import cn.km.warehouse.present.ZYHJPresenter;
import cn.km.warehouse.uhf.UHFCKHome;
import cn.km.warehouse.uhf.UHFHome;
import cn.km.warehouse.uhf.util.LogUtils;
import cn.km.warehouse.uhf.util.PowerUtil;
import cn.km.warehouse.uhf.util.SoundPoolHelper;
import cn.km.warehouse.ui.adapter.HJPopuAdapter;
import cn.km.warehouse.ui.customview.MyToolbar;
import cn.km.warehouse.utils.SPUtils;
import cn.km.warehouse.view.ZYHJView;
import cn.km.warehouse.widget.CustomProgressDialog;

public class ZYHJActivity extends BaseActivity implements View.OnClickListener, ZYHJView {
    private MyToolbar myToolbar;
    private TextView mTextViewCode;
    private TextView mTextViewName;
    private TextView mTextViewGGXH;
    private TextView mTextViewDW;
    private TextView mTextViewCW;
    private TextView mTextViewYCHHJ;
    private TextView mTextViewYRHHJ;
    private TextView mTextViewYDSL;
    private RelativeLayout mRelativeScan;
    private ZYHJPresenter zyhjPresenter;
    private CKModel mCkModel;
    private int CWId=-10086;
    private ArrayList<HJModel> hjModelsdata;
    private int mYRHJId;
    private String mRFId;
    private String serialPortPath = "/dev/ttyS2";
    private UhfReader reader = null;
    private DisplayStatusReceiver mDisplayStatusReceiver;
    private boolean isStartFlag;
    private boolean startFlag = false;
    private boolean runFlag = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zyhj);
        EventBus.getDefault().register(this);
        mCkModel= SPUtils.getBeanCK(this, Contstant.ck_key);
        myToolbar = findViewById(R.id.toolBar);
        mTextViewCode=findViewById(R.id.tv_code_info);
        mTextViewName=findViewById(R.id.tv_name_info);
        mTextViewGGXH=findViewById(R.id.tv_guige_info);
        mTextViewDW=findViewById(R.id.tv_unit_info);
        mTextViewCW=findViewById(R.id.tv_cw);
        mTextViewYCHHJ=findViewById(R.id.tv_ychhj);
        mTextViewYRHHJ=findViewById(R.id.textview_cw_content);
        mTextViewYDSL=findViewById(R.id.tv_ydsl);
        mRelativeScan=findViewById(R.id.relative_scan);
        zyhjPresenter=new ZYHJPresenter(this);
        mTextViewYRHHJ.setOnClickListener(this);
        mRelativeScan.setOnClickListener(this);
        myToolbar.setTitleWithNavigation("转移货架", true);
        myToolbar.setRightText("确定");
        SubmitListener submitListener=new SubmitListener();
        myToolbar.setRightTextClickListener(submitListener);
        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ZYHJActivity.this.finish();
            }
        });
        initData();
    }
    private void initData() {
        UhfReader.setPortPath(serialPortPath);
        reader = UhfReader.getInstance();
        int dbm = (int) cn.km.warehouse.uhf.util.SPUtils.get(this, "DBM", 0);
        if (dbm != 0) {
            LogUtils.d(TAG, "initData: " + dbm);

        }
        reader.setOutputPower(26);
        SoundPoolHelper.getInstance(this);
        registerDisplayStatusReceiver();
    }
    UHFHome uhfHome;
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.textview_cw_content:
                if (CWId==-10086){
                    Toast.makeText(this,"请您先扫描物料",Toast.LENGTH_SHORT).show();
                    return;
                }
                zyhjPresenter.getHJList(mCkModel.getId(),CWId);
                break;
            case R.id.relative_scan:
                mTextViewCode.setText("");
                mTextViewName.setText("");
                mTextViewDW.setText("");
                mTextViewCW.setText("");
                mTextViewYCHHJ.setText("");
                mTextViewGGXH.setText("");
                mTextViewYDSL.setText("");
               // UHFCKHome uhfckHome=new UHFCKHome(this,reader);
                uhfHome = new UHFHome("", "", this, reader);

                break;
        }
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void ScanSuccessed(ScanEvent event) {
        mRFId=event.getRfid();
        getCKScanData(event.getRfid());

    }

    private void getCKScanData(String rfid) {
        if (zyhjPresenter==null){
            zyhjPresenter=new ZYHJPresenter(this);

        }
        zyhjPresenter.getCKScanData(mCkModel.getId(),rfid);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        PowerUtil.power("0");
        runFlag = false;
        if (reader != null) {
            reader.close();
        }
        unregisterReceiver(mDisplayStatusReceiver);
    }

    @Override
    public void getScanDataSuccess(ZYHJScanModel zyhjScanModel) {
        if (zyhjScanModel == null) {
            return;
        }
        CWId=zyhjScanModel.getPositionId();
        mTextViewCode.setText(zyhjScanModel.getMaterialCode());
        mTextViewName.setText(zyhjScanModel.getMaterialName());
        mTextViewDW.setText(zyhjScanModel.getMaterialUnitName());
        mTextViewCW.setText(zyhjScanModel.getPositionName());
        mTextViewYCHHJ.setText(zyhjScanModel.getShelfName());
        mTextViewGGXH.setText(zyhjScanModel.getMaterialSpecs());
        mTextViewYDSL.setText(zyhjScanModel.getStockNum()+"");
    }

    @Override
    public void getScanDataFailure(int code, String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();

    }
    public  class  SubmitListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            Submit();
        }
    }

    private void Submit() {
        if (zyhjPresenter==null){
            zyhjPresenter=new ZYHJPresenter(this);
        }
        if (mRFId==null){
            Toast.makeText(this,"请您重新扫描",Toast.LENGTH_SHORT).show();
            return;
        }
        zyhjPresenter.submit(mYRHJId,mRFId);

    }
    @Override
    public void getHJSuccess(ArrayList<HJModel> hjModels) {
        if (hjModels == null) {
            return;
        }
      hjModelsdata = hjModels;
        showPop();
        showHJ();
    }

    @Override
    public void getZYHJSubmitSuccess(String message) {
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void ggetZYHJSubmitFailure(int code, String message) {
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }

    PopupWindow mPopupWindow;
    private ListView mSelectListView;

    // private RelativeLayout mRelativeLayoutCK;
    private void showPop() {
        mPopupWindow = new PopupWindow();
        // 设置布局文件
        mPopupWindow.setContentView(LayoutInflater.from(this).inflate(R.layout.ck_list_layout, null));
        // 为了避免部分机型不显示，我们需要重新设置一下宽高
        mPopupWindow.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        mPopupWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        mSelectListView = mPopupWindow.getContentView().findViewById(R.id.listview_ck_select);
        // 设置pop透明效果
        mPopupWindow.setBackgroundDrawable(new ColorDrawable(0x0000));
        backgroundAlpha(0.3f);
        // 设置pop出入动画
        mPopupWindow.setAnimationStyle(R.style.pop_add);
        // 设置pop获取焦点，如果为false点击返回按钮会退出当前Activity，如果pop中有Editor的话，focusable必须要为true
        mPopupWindow.setFocusable(true);
        // 设置pop可点击，为false点击事件无效，默认为true
        mPopupWindow.setTouchable(true);
        // 设置点击pop外侧消失，默认为false；在focusable为true时点击外侧始终消失
        mPopupWindow.setOutsideTouchable(true);
        // 相对于 + 号正下面，同时可以设置偏移量
        //  mPopupWindow.showAsDropDown(mLinearLayoutSelect, -150, 30);
        mPopupWindow.showAtLocation(myToolbar, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
        mPopupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {

            @Override
            public void onDismiss() {
                backgroundAlpha(1f);

            }
        });


    }
    public void backgroundAlpha(float bgAlpha) {
        WindowManager.LayoutParams lp = this.getWindow().getAttributes();
        lp.alpha = bgAlpha;
        this.getWindow().setAttributes(lp);
        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
    }
    private void showHJ() {
        HJPopuAdapter hjPopuAdapter = new HJPopuAdapter(this, hjModelsdata);
        mSelectListView.setAdapter(hjPopuAdapter);
        hjPopuAdapter.setCkMenuClickListener(new HJPopuAdapter.CKMenuClickListener() {
            @Override
            public void SelectCK(int position, HJModel hjModel) {
                mYRHJId = hjModel.getId();
                mTextViewYRHHJ.setText(hjModel.getName());
                if (mPopupWindow.isShowing()) {
                    mPopupWindow.dismiss();
                }
                // SPUtils.putBeanCK(RlrkActivity.this, Contstant.ck_key, ckModel);
            }
        });
    }
    private boolean showProgressDiallog = true;
    private CustomProgressDialog mDialog;
    @Override
    public void showLoading() {
        if (showProgressDiallog && mDialog == null) {
            mDialog = new CustomProgressDialog(this);
            mDialog.setMessage("正在加载数据..");
            mDialog.show();
        }else {
            mDialog.setMessage("正在加载数据..");
            mDialog.show();
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        PowerUtil.power("1");
        int value = (int) cn.km.warehouse.uhf.util.SPUtils.get(this, "DBM", 0);
        if (value != 0) {
            reader.setOutputPower(value);
        }
    }
    @Override
    public void dismissLoading() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
            showProgressDiallog = false;
        }
    }
    private void registerDisplayStatusReceiver() {
        mDisplayStatusReceiver = new DisplayStatusReceiver();
        IntentFilter screenStatusIF = new IntentFilter();
        screenStatusIF.addAction(Intent.ACTION_SCREEN_ON);
        screenStatusIF.addAction(Intent.ACTION_SCREEN_OFF);
        this.registerReceiver(mDisplayStatusReceiver, screenStatusIF);
    }

    class DisplayStatusReceiver extends BroadcastReceiver {
        String SCREEN_ON = "android.intent.action.SCREEN_ON";
        String SCREEN_OFF = "android.intent.action.SCREEN_OFF";

        @Override
        public void onReceive(Context context, Intent intent) {
            if (SCREEN_ON.equals(intent.getAction())) {
                isStartFlag = (boolean) cn.km.warehouse.uhf.util.SPUtils.get(context, "startFlag", true);
                startFlag = isStartFlag;
            } else if (SCREEN_OFF.equals(intent.getAction())) {
                cn.km.warehouse.uhf.util.SPUtils.remove(context, "startFlag");
                cn.km.warehouse.uhf.util.SPUtils.put(context, "startFlag", startFlag);
                startFlag = false;

            }
        }
    }

    @Override
    public void detach() {

    }
}
