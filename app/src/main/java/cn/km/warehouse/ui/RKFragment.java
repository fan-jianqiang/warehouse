package cn.km.warehouse.ui;

import cn.km.warehouse.base.BaseFragment;
import cn.km.warehouse.model.RKlistModel;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/4
 */
public abstract class RKFragment  extends BaseFragment {
    public abstract  void showData(RKlistModel rKlistModel);

}
