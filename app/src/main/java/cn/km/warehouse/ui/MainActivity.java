package cn.km.warehouse.ui;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;

import java.util.ArrayList;

import cn.km.warehouse.Contstant;
import cn.km.warehouse.R;
import cn.km.warehouse.base.BaseActivity;
import cn.km.warehouse.entity.HomeItem;

import cn.km.warehouse.model.CKModel;
import cn.km.warehouse.present.CKPresneter;
import cn.km.warehouse.ui.adapter.CKPopuAdapter;
import cn.km.warehouse.ui.adapter.HomeAdapter;
import cn.km.warehouse.utils.SPUtils;
import cn.km.warehouse.view.CKView;
import cn.km.warehouse.widget.CustomProgressDialog;

public class MainActivity extends BaseActivity implements View.OnClickListener, CKView {

    private long exitTimes = 0;
    //    private static final Class<?>[] ACTIVITY = {AnimationUseActivity.class, ChooseMultipleItemUseTypeActivity.class, HeaderAndFooterUseActivity.class, PullToRefreshUseActivity.class, SectionUseActivity.class, EmptyViewUseActivity.class, ItemDragAndSwipeUseActivity.class, ItemClickActivity.class, ExpandableUseActivity.class, DataBindingUseActivity.class,UpFetchUseActivity.class,SectionMultipleItemUseActivity.class, DiffUtilActivity.class};
    private static final String[] TITLE = {"物料入库", "物料出库", "入库单", "出库单",  "盘点任务", "转移货架", "物料状态"};
    private static final int[] IMG = {R.mipmap.icon_intput_apply, R.mipmap.icon_output_apply, R.mipmap.icon_input,  R.mipmap.icon_pandian, R.mipmap.icon_pandian_task, R.mipmap.icon_remove, R.mipmap.icon_wuliao_zhuangtai};
    private static final int[] COLORS = {Color.parseColor("#5100F6"), Color.parseColor("#14A002"), Color.parseColor("#D16E55"),
            Color.parseColor("#990094"), Color.parseColor("#1483F2"),
            Color.parseColor("#136F6A"), Color.parseColor("#C047FF")};
    private ArrayList<HomeItem> mDataList;
    private RecyclerView mRecyclerView;
    private RelativeLayout myToolbar;
    private RelativeLayout mRelativeLayoutCK;
    private TextView mTexViewCKNumber;
    private ImageView mImageViewDownUp;
    private TextView mTextViewActionTitle;
    PopupWindow mPopupWindow;
    ListView mListView;
    private CKPresneter mCKPresenter;
    private CKPopuAdapter ckPopuAdapter;
    private ArrayList<CKModel> ckModelsdata;
    private boolean showProgressDiallog = true;
    private CustomProgressDialog mDialog;
   private CKModel mCKModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindUI();
    }

    public void bindUI() {

        mPopupWindow = new PopupWindow(this);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView_home);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        myToolbar = findViewById(R.id.layout_action_bar);
        mTextViewActionTitle = findViewById(R.id.tv_title);
        mRelativeLayoutCK = findViewById(R.id.realtvie_back_action);
        mRelativeLayoutCK.setVisibility(View.VISIBLE);
        mRelativeLayoutCK.setOnClickListener(this);
        mTexViewCKNumber = findViewById(R.id.textveiw_ck_number);
        mCKModel=SPUtils.getBeanCK(this,Contstant.ck_key);
        if (mCKModel==null){
            mTexViewCKNumber.setText("请选择仓库");
        }else {
            mTexViewCKNumber.setText(mCKModel.getName());
        }
        mImageViewDownUp = findViewById(R.id.iv_down);
        mTextViewActionTitle.setText("仓库管理平台");
        initData();
    }

    public void initData() {
        mCKPresenter = new CKPresneter(this);
        mDataList = new ArrayList<>();
        for (int i = 0; i < TITLE.length; i++) {
            HomeItem item = new HomeItem();
            item.setTitle(TITLE[i]);
//            item.setActivity(ACTIVITY[i]);
            item.setImageResource(IMG[i]);
            item.setBackgroundColor(COLORS[i]);
            mDataList.add(item);
        }
        HomeAdapter homeAdapter = new HomeAdapter(R.layout.item_house_main, mDataList);
        homeAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {
                if (mTexViewCKNumber.getText().equals("请选择仓库")){
                    Toast.makeText(MainActivity.this,"请您选择仓库",Toast.LENGTH_SHORT).show();
                    return;
                }

                switch (position) {
                    case 0:
//
                        startActivity(new Intent(MainActivity.this, InputActivity.class));
                        break;
                    case 1:
                        startActivity(new Intent(MainActivity.this, WLCKActivity.class));
                        break;
                    case 2:
                        startActivity(new Intent(MainActivity.this, RukuTabActivity.class));
                        break;
                    case 3:
                        startActivity(new Intent(MainActivity.this, CukuTabActivity.class));
                        break;
                    case 4:
                        startActivity(new Intent(MainActivity.this, PDTaskActivity.class));
                        break;
                    case 5:
                        startActivity(new Intent(MainActivity.this, ZYHJActivity.class));
                        break;
                    case 6:
                        startActivity(new Intent(MainActivity.this, WLZTActivity.class));
                        break;
//                    case 7:
//                        startActivity(new Intent(MainActivity.this, WLZTActivity.class));
//                        break;
                }
            }
        });

        mRecyclerView.setAdapter(homeAdapter);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.realtvie_back_action:
                mCKPresenter.getCKList();
                break;
        }
    }

    @Override
    public void getCKSuccess(ArrayList<CKModel> ckModels) {
        if (ckModels == null) {
            return;
        }
        ckModelsdata = ckModels;
        showPop();
        showCK();
    }

    @Override
    public void getCKFailer(String message) {

    }

    @Override
    public void showLoading() {
        if (showProgressDiallog && mDialog == null) {
            mDialog = new CustomProgressDialog(this);
            mDialog.setMessage("正在加载数据..");
            mDialog.show();
        }
    }

    @Override
    public void dismissLoading() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
            showProgressDiallog = false;
        }
    }

    @Override
    public void detach() {

    }

    private void showCK() {
        CKPopuAdapter ckPopuAdapter = new CKPopuAdapter(this, ckModelsdata);
        mSelectListView.setAdapter(ckPopuAdapter);
        ckPopuAdapter.setCkMenuClickListener(new CKPopuAdapter.CKMenuClickListener() {
            @Override
            public void SelectCK(int position, CKModel ckModel) {
                mTexViewCKNumber.setText(ckModel.getName());
                if (mPopupWindow.isShowing()){
                    mPopupWindow.dismiss();
                }
                SPUtils.putBeanCK(MainActivity.this, Contstant.ck_key, ckModel);
            }
        });
    }

    private ListView mSelectListView;

    private void showPop() {
        mPopupWindow = new PopupWindow();
        // 设置布局文件
        mPopupWindow.setContentView(LayoutInflater.from(this).inflate(R.layout.ck_list_layout, null));
        // 为了避免部分机型不显示，我们需要重新设置一下宽高
        mPopupWindow.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        mPopupWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        mSelectListView = mPopupWindow.getContentView().findViewById(R.id.listview_ck_select);
        // 设置pop透明效果
        mPopupWindow.setBackgroundDrawable(new ColorDrawable(0x0000));
        backgroundAlpha(0.3f);
        // 设置pop出入动画
        mPopupWindow.setAnimationStyle(R.style.pop_add);
        // 设置pop获取焦点，如果为false点击返回按钮会退出当前Activity，如果pop中有Editor的话，focusable必须要为true
        mPopupWindow.setFocusable(true);
        // 设置pop可点击，为false点击事件无效，默认为true
        mPopupWindow.setTouchable(true);
        // 设置点击pop外侧消失，默认为false；在focusable为true时点击外侧始终消失
        mPopupWindow.setOutsideTouchable(true);
        // 相对于 + 号正下面，同时可以设置偏移量
        //  mPopupWindow.showAsDropDown(mLinearLayoutSelect, -150, 30);
        mPopupWindow.showAtLocation(mRelativeLayoutCK, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
        mPopupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {

            @Override
            public void onDismiss() {
                backgroundAlpha(1f);

            }
        });


    }

    public void backgroundAlpha(float bgAlpha) {
        WindowManager.LayoutParams lp = this.getWindow().getAttributes();
        lp.alpha = bgAlpha;
        this.getWindow().setAttributes(lp);
        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // 判断两次点击的时间间隔（默认设置为2秒）
            if ((System.currentTimeMillis() - exitTimes) > 2000) {// 2S内则退出程序
                //   MyToast.show("再点击一次返回键退出程序");
                Toast.makeText(MainActivity.this, "再点击一次返回键退出程序", Toast.LENGTH_SHORT).show();
                exitTimes = System.currentTimeMillis();
                return true;
            } else {
                finish();
                exitTimes = 0;
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

}