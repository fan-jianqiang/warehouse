package cn.km.warehouse.ui.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import cn.km.warehouse.R;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/2/1
 */
public class LoadingLayout extends FrameLayout {
    private View loadingView_, emptyView_, errorView_;
    private int loadingView,emptyView,errorView;

    public LoadingLayout(@NonNull Context context) {
        this(context, null);
    }

    public LoadingLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, -1);

    }

    public LoadingLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        loadingView_ = getLoadingView();
        emptyView_ = getEmptyView();
        errorView_ = getErrorView();
    }

    protected View getLoadingView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.list_adapter_gif_loading_layout, this, true);
        return view;
    }

    protected View getEmptyView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.list_adapter_empty_view, this, true);
        return view;
    }

    protected View getErrorView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.list_adapter_error_view, this, true);
        return view;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        for (int i = 0; i < getChildCount(); i++) {
            getChildAt(i).setVisibility(GONE);
        }
    }

    public void showLoading() {
        for (int i = 0; i < getChildCount(); i++) {
            View child = this.getChildAt(i);
            child.setVisibility(i == 0 ? VISIBLE : GONE);
        }
    }

    public void showEmpty() {
        for (int i = 0; i < getChildCount(); i++) {
            View child = this.getChildAt(i);
            child.setVisibility(i == 1 ? VISIBLE : GONE);
        }
    }

    public void showError() {
        for (int i = 0; i < getChildCount(); i++) {
            View child = this.getChildAt(i);
            child.setVisibility(i == 2 ? VISIBLE : GONE);
        }
    }

    public void showContent() {
        for (int i = 0; i < getChildCount(); i++) {
            View child = this.getChildAt(i);
            child.setVisibility(i == 3 ? VISIBLE : GONE);
        }
    }
}
