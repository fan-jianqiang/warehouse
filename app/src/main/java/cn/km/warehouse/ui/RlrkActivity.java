package cn.km.warehouse.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.magicrf.uhfreaderlib.reader.UhfReader;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import cn.km.warehouse.Contstant;
import cn.km.warehouse.R;
import cn.km.warehouse.base.BaseActivity;
import cn.km.warehouse.event.ScanEvent;
import cn.km.warehouse.model.CKModel;
import cn.km.warehouse.model.CWModel;
import cn.km.warehouse.model.HJModel;
import cn.km.warehouse.model.RKListQDModel;
import cn.km.warehouse.model.RuKuDetailModel;
import cn.km.warehouse.model.VMaterialStock;
import cn.km.warehouse.present.RlrkPresenter;
import cn.km.warehouse.uhf.UHFHome;
import cn.km.warehouse.uhf.UHFHomeActivity;
import cn.km.warehouse.uhf.UHFMainActivity;
import cn.km.warehouse.uhf.adapter.ListViewAdapter;
import cn.km.warehouse.uhf.util.LogUtils;
import cn.km.warehouse.uhf.util.PowerUtil;
import cn.km.warehouse.uhf.util.SoundPoolHelper;
import cn.km.warehouse.ui.adapter.CKPopuAdapter;
import cn.km.warehouse.ui.adapter.CWPopuAdapter;
import cn.km.warehouse.ui.adapter.HJPopuAdapter;
import cn.km.warehouse.ui.adapter.InputListAdapter;
import cn.km.warehouse.ui.adapter.RKDetailXQCWAdapter;
import cn.km.warehouse.ui.customview.MyListView;
import cn.km.warehouse.ui.customview.MyToolbar;
import cn.km.warehouse.utils.ProgressUtil;
import cn.km.warehouse.utils.SPUtils;
import cn.km.warehouse.utils.uuid.DeviceUuidFactory;
import cn.km.warehouse.view.RukuDetailView;
import cn.km.warehouse.widget.CustomProgressDialog;

public class RlrkActivity extends BaseActivity implements View.OnClickListener, RukuDetailView {
    private MyToolbar myToolbar;
    private ImageView mImgeViewScan;
    private RlrkPresenter rlrkPresenter;
    private String billid;
    private TextView mTextViewRKDH;
    private TextView mTextViewCode;
    private TextView mTextViewName;
    private TextView mTextViewGGXH;
    private TextView mTextViewDW;
    private TextView mTextViewCW;
    private TextView mTextViewYSYS;
    private int mCwId = -10086;
    private int mHjID = -10086;
    private TextView mTextViewHJ;
    private CKModel mCKModel;
    private RuKuDetailModel mRuKuDetailModel;
    String deviceId;
    private RelativeLayout mRelativeLayoutScan;
    private String ystype;
    private ArrayList<VMaterialStock> mVMaterialStocks;
    private boolean showProgressDiallog = true;
    private CustomProgressDialog mDialog;
    private MyListView mListView;
    private RKDetailXQCWAdapter rkDetailXQCWAdapter;
    private String serialPortPath = "/dev/ttyS2";
    private UhfReader reader = null;
    private boolean startFlag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rlrk);

        mVMaterialStocks = new ArrayList<>();
        deviceId = DeviceUuidFactory.getDeviceId(this);
        EventBus.getDefault().register(this);
        mCKModel = SPUtils.getBeanCK(RlrkActivity.this, Contstant.ck_key);
        mRelativeLayoutScan = this.findViewById(R.id.realtive_scan);
        mTextViewCode = this.findViewById(R.id.tv_code_info);
        mTextViewRKDH = this.findViewById(R.id.tv_code);
        mTextViewName = this.findViewById(R.id.tv_name_info);
        mTextViewGGXH = this.findViewById(R.id.tv_guige_info);
        mTextViewDW = this.findViewById(R.id.tv_unit_info);
        mTextViewCW = this.findViewById(R.id.textview_cw_content);
        mTextViewHJ = this.findViewById(R.id.textview_hj_content);
        mTextViewYSYS = this.findViewById(R.id.textview_ys);
        mListView = this.findViewById(R.id.listview_xqlist);
        //  rkDetailXQCWAdapter=new RKDetailXQCWAdapter();
//        mRecyClerView=this.findViewById(R.id.recyclerView_xqlist);
//        mRecyClerView.setLayoutManager(new LinearLayoutManager(this));
//        mRecyClerView.setHasFixedSize(true);
//        mRecyClerView.setNestedScrollingEnabled(false);
//        mRecyClerView.setFocusable(false);
//        mRecyClerView.setAdapter(rkDetailXQCWAdapter);
//        mRecyClerView.setNestedScrollingEnabled(false);
        mTextViewCW.setOnClickListener(this);
        mTextViewHJ.setOnClickListener(this);
        rlrkPresenter = new RlrkPresenter(this);
        billid = getIntent().getStringExtra("billid");
        myToolbar = findViewById(R.id.toolBar);
        myToolbar.setTitleWithNavigation("入库申请单详情", true);
        myToolbar.setRightText("提交");
        SubmitListener submitListener = new SubmitListener();
        myToolbar.setRightTextClickListener(submitListener);
        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RlrkActivity.this.finish();
            }
        });
        mImgeViewScan = this.findViewById(R.id.ivQrCodeSwipe);
        mRelativeLayoutScan.setOnClickListener(this);
        if (billid != null) {
            rlrkPresenter.getRKDetail(Integer.parseInt(billid));
        } else {
            Toast.makeText(this, "数据错误", Toast.LENGTH_SHORT).show();
        }

        initData();

    }

    private void initData() {
        UhfReader.setPortPath(serialPortPath);
        reader = UhfReader.getInstance();

        if (reader == null) {
            Toast.makeText(this, "请重新扫描", Toast.LENGTH_SHORT).show();
        }

        int dbm = (int) cn.km.warehouse.uhf.util.SPUtils.get(this, "DBM", 0);
        if (dbm != 0) {
            LogUtils.d(TAG, "initData: " + dbm);
            reader.setOutputPower(26);
        }
        SoundPoolHelper.getInstance(this);
        registerDisplayStatusReceiver();
        startFlag = false;
    }

    @Override
    protected void setStatusBar() {
        super.setStatusBar();
    }

    private String rfid;
    private String epc;

    public class SubmitListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            Submit();
        }
    }

    private void Submit() {
        if (rlrkPresenter == null) {
            rlrkPresenter = new RlrkPresenter(this);
        }
        if (rfid == null || "".equals(rfid)) {
            Toast.makeText(this, "请您先扫描在提交", Toast.LENGTH_SHORT).show();
            return;
        }
        rlrkPresenter.submit(mVMaterialStocks, mRuKuDetailModel.getOrderId());

    }

    UHFHome uhfHome;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.realtive_scan:
                if (mVMaterialStocks.size() >= 1) {
                    Toast.makeText(RlrkActivity.this, "请您先提交当前数据", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (mCwId == -10086) {
                    Toast.makeText(RlrkActivity.this, "请选择仓位", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (mRuKuDetailModel.getActualNum() >= mRuKuDetailModel.getShouldNum()) {
                    Toast.makeText(this, "已达到最大收货量", Toast.LENGTH_SHORT).show();
                    return;
                }

                //uhfHome = new UHFHome(mRuKuDetailModel.getMaterialCode(),"1_" + mRuKuDetailModel.getMaterialCode() + "0_" +System.currentTimeMillis(), this,reader);
                uhfHome = new UHFHome(mRuKuDetailModel.getMaterialCode(), System.currentTimeMillis() + "", this, reader);
//                Intent  intent=new Intent();
//                intent.setClass(RlrkActivity.this,UHFHomeActivity.class);
//                intent.putExtra("codeId","1_" + mRuKuDetailModel.getMaterialCode() + "0_" +System.currentTimeMillis());
//                startActivity(intent);

                break;
            case R.id.textview_cw_content:
                rlrkPresenter.getCWList(mCKModel.getId());
                break;
            case R.id.textview_hj_content:
                if (mCwId == -10086) {
                    Toast.makeText(RlrkActivity.this, "请选择仓位", Toast.LENGTH_SHORT).show();
                    return;
                }
                rlrkPresenter.getHJList(mCKModel.getId(), mCwId);
                break;
        }
    }

    @Override
    public void getRuKuDetailSuccess(RuKuDetailModel ruKuDetailModel) {
        if (ruKuDetailModel == null) {
            return;
        }
        mRuKuDetailModel = ruKuDetailModel;
        mTextViewRKDH.setText(ruKuDetailModel.getApplicationCode());
        mTextViewCode.setText(ruKuDetailModel.getMaterialCode());
        mTextViewName.setText(ruKuDetailModel.getMaterialName());
        mTextViewGGXH.setText(ruKuDetailModel.getMaterialSpecs());
        mTextViewDW.setText(ruKuDetailModel.getMaterialUnitName());
        mTextViewYSYS.setText(ruKuDetailModel.getActualNum() + "/" + ruKuDetailModel.getShouldNum());
        if (ruKuDetailModel.getSpare()!=null){
            if (ruKuDetailModel.getSpare().equals("1")) {
                //已收加一
                ystype = "1";
            }
            if (ruKuDetailModel.getSpare().equals("2")) {
                //已收加getShouldNum
                ystype = "2";
            }
        }else {
            ystype = "2";
        }

        //  if (rkDetailXQCWAdapter==null){
        rkDetailXQCWAdapter = new RKDetailXQCWAdapter(this, ruKuDetailModel.getStockList());
        //}

        mListView.setAdapter(rkDetailXQCWAdapter);
    }


    @Override
    public void getRuKuDetailFailer(String message) {

    }

    private ArrayList<CWModel> cwModelsdata;
    private ArrayList<HJModel> hjModelsdata;

    @Override
    public void getCWSuccess(ArrayList<CWModel> cwModels) {
        if (cwModels == null) {
            return;
        }
        cwModelsdata = cwModels;
        showPop();
        showCW();
    }

    @Override
    public void getHJSuccess(ArrayList<HJModel> hjModels) {
        if (hjModels == null) {
            return;
        }
        hjModelsdata = hjModels;
        showPop();
        showHJ();
    }

    @Override
    public void getTJSuccess(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        mVMaterialStocks.clear();
        mTextViewCW.setText("");
        mTextViewHJ.setText("");
        mCwId = -10086;
        mHjID = -10086;

        rlrkPresenter.getRKDetail(Integer.parseInt(billid));
    }

    @Override
    public void getTJFailer(int code, String message) {
        if (code == 0) {
            Toast.makeText(this, "入库成功", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
        mVMaterialStocks.clear();
        mTextViewCW.setText("");
        mTextViewHJ.setText("");
        mCwId = -10086;
        mHjID = -10086;
        rlrkPresenter.getRKDetail(Integer.parseInt(billid));
    }

    PopupWindow mPopupWindow;
    private ListView mSelectListView;

    // private RelativeLayout mRelativeLayoutCK;
    private void showPop() {
        mPopupWindow = new PopupWindow();
        // 设置布局文件
        mPopupWindow.setContentView(LayoutInflater.from(this).inflate(R.layout.ck_list_layout, null));
        // 为了避免部分机型不显示，我们需要重新设置一下宽高
        mPopupWindow.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        mPopupWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        mSelectListView = mPopupWindow.getContentView().findViewById(R.id.listview_ck_select);
        // 设置pop透明效果
        mPopupWindow.setBackgroundDrawable(new ColorDrawable(0x0000));
        backgroundAlpha(0.3f);
        // 设置pop出入动画
        mPopupWindow.setAnimationStyle(R.style.pop_add);
        // 设置pop获取焦点，如果为false点击返回按钮会退出当前Activity，如果pop中有Editor的话，focusable必须要为true
        mPopupWindow.setFocusable(true);
        // 设置pop可点击，为false点击事件无效，默认为true
        mPopupWindow.setTouchable(true);
        // 设置点击pop外侧消失，默认为false；在focusable为true时点击外侧始终消失
        mPopupWindow.setOutsideTouchable(true);
        // 相对于 + 号正下面，同时可以设置偏移量
        //  mPopupWindow.showAsDropDown(mLinearLayoutSelect, -150, 30);
        mPopupWindow.showAtLocation(myToolbar, Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
        mPopupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {

            @Override
            public void onDismiss() {
                backgroundAlpha(1f);

            }
        });


    }

    public void backgroundAlpha(float bgAlpha) {
        WindowManager.LayoutParams lp = this.getWindow().getAttributes();
        lp.alpha = bgAlpha;
        this.getWindow().setAttributes(lp);
        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
    }

    private void showCW() {
        CWPopuAdapter cwPopuAdapter = new CWPopuAdapter(this, cwModelsdata);
        mSelectListView.setAdapter(cwPopuAdapter);
        cwPopuAdapter.setCkMenuClickListener(new CWPopuAdapter.CKMenuClickListener() {
            @Override
            public void SelectCK(int position, CWModel cwModel) {
                mCwId = cwModel.getId();
                mTextViewCW.setText(cwModel.getName());
                if (mPopupWindow.isShowing()) {
                    mPopupWindow.dismiss();
                }
                // SPUtils.putBeanCK(RlrkActivity.this, Contstant.ck_key, ckModel);
            }
        });
    }

    private void showHJ() {
        HJPopuAdapter hjPopuAdapter = new HJPopuAdapter(this, hjModelsdata);
        mSelectListView.setAdapter(hjPopuAdapter);
        hjPopuAdapter.setCkMenuClickListener(new HJPopuAdapter.CKMenuClickListener() {
            @Override
            public void SelectCK(int position, HJModel hjModel) {
                mHjID = hjModel.getId();
                mTextViewHJ.setText(hjModel.getName());
                if (mPopupWindow.isShowing()) {
                    mPopupWindow.dismiss();
                }
                // SPUtils.putBeanCK(RlrkActivity.this, Contstant.ck_key, ckModel);
            }
        });
    }

    private Double submitActualNum;

    //获取扫描信息
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void ScanSuccessed(ScanEvent event) {
        rfid = event.getRfid();
        epc = event.getEpc();

        VMaterialStock vMaterialStock = new VMaterialStock();

        vMaterialStock.setApplicationBillId(mRuKuDetailModel.getApplicationBillId());
        vMaterialStock.setEpc(epc);
        if (uhfHome != null) {
            uhfHome.getIsChange();
            if (uhfHome.getIsChange()) {
                String oldRfid = uhfHome.getOldRfid();
                vMaterialStock.setOldRfid(uhfHome.getOldRfid());
                if (ystype.equals("1")) {
                    mTextViewYSYS.setText(mRuKuDetailModel.getActualNum() + "/" + mRuKuDetailModel.getShouldNum());
                    submitActualNum = 1.0;
                }
                if (ystype.equals("2")) {
                    mTextViewYSYS.setText(mRuKuDetailModel.getActualNum() + "/" + mRuKuDetailModel.getShouldNum());
                    submitActualNum = mRuKuDetailModel.getShouldNum();
                }
            } else {
                vMaterialStock.setOldRfid("");
                if (ystype.equals("1")) {
                    mTextViewYSYS.setText(mRuKuDetailModel.getActualNum() + 1 + "/" + mRuKuDetailModel.getShouldNum());
                    submitActualNum = 1.0;
                }
                if (ystype.equals("2")) {
                    mTextViewYSYS.setText(mRuKuDetailModel.getActualNum() + mRuKuDetailModel.getShouldNum() + "/" + mRuKuDetailModel.getShouldNum());
                    submitActualNum = mRuKuDetailModel.getShouldNum();
                }
            }
        } else {
            vMaterialStock.setOldRfid("");
        }
//        10086_222222_8a5853e2287a8b081615536816645
        vMaterialStock.setEntryNum(submitActualNum);
//        int a = rfid.indexOf("1_");
//        String substring = rfid.substring(a + 2, rfid.length());

        vMaterialStock.setRfid(rfid);
        vMaterialStock.setSpare(mRuKuDetailModel.getSpare());
        //  vMaterialStock.setUnitPrice(mRuKuDetailModel.getUnitPrice());

        vMaterialStock.setMaterialId(mRuKuDetailModel.getMaterialId());
        vMaterialStock.setPositionId(mCwId);
        vMaterialStock.setShelfId(mHjID);
        mVMaterialStocks.add(vMaterialStock);
    }

    @Override
    public void showLoading() {
        if (showProgressDiallog && mDialog == null) {
            mDialog = new CustomProgressDialog(this);
            mDialog.setMessage("正在加载数据..");
            mDialog.show();
        } else {
            mDialog.setMessage("正在加载数据..");
            mDialog.show();
        }
    }

    @Override
    public void dismissLoading() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
            showProgressDiallog = false;
        }
    }

    @Override
    public void detach() {

    }

    private boolean isStartFlag;
    private DisplayStatusReceiver mDisplayStatusReceiver;

    private void registerDisplayStatusReceiver() {
        mDisplayStatusReceiver = new DisplayStatusReceiver();
        IntentFilter screenStatusIF = new IntentFilter();
        screenStatusIF.addAction(Intent.ACTION_SCREEN_ON);
        screenStatusIF.addAction(Intent.ACTION_SCREEN_OFF);
        registerReceiver(mDisplayStatusReceiver, screenStatusIF);
    }

    class DisplayStatusReceiver extends BroadcastReceiver {
        String SCREEN_ON = "android.intent.action.SCREEN_ON";
        String SCREEN_OFF = "android.intent.action.SCREEN_OFF";

        @Override
        public void onReceive(Context context, Intent intent) {
            if (SCREEN_ON.equals(intent.getAction())) {
                isStartFlag = (boolean) cn.km.warehouse.uhf.util.SPUtils.get(context, "startFlag", true);
                startFlag = isStartFlag;
            } else if (SCREEN_OFF.equals(intent.getAction())) {
                cn.km.warehouse.uhf.util.SPUtils.remove(context, "startFlag");
                cn.km.warehouse.uhf.util.SPUtils.put(context, "startFlag", startFlag);
                startFlag = false;

            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        PowerUtil.power("1");
        int value = (int) cn.km.warehouse.uhf.util.SPUtils.get(this, "DBM", 0);
        if (value != 0) {
            reader.setOutputPower(value);
        }
    }

    private boolean runFlag = true;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        PowerUtil.power("0");
        runFlag = false;
        if (reader != null) {
            reader.close();
        }
        unregisterReceiver(mDisplayStatusReceiver);
    }

}
