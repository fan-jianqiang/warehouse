package cn.km.warehouse.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.viewholder.BaseViewHolder;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import cn.km.warehouse.R;
import cn.km.warehouse.model.CKModel;
import cn.km.warehouse.model.CWModel;
import cn.km.warehouse.model.RuKuDetailModel;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/15
 */
public class RKDetailXQCWAdapter extends BaseAdapter {
    private Context mContext;
    private List<RuKuDetailModel.StockListBean> mList;
    private LayoutInflater layoutInflater;

    public RKDetailXQCWAdapter(Context mContext, List<RuKuDetailModel.StockListBean> mList) {
        this.mContext = mContext;
        this.mList = mList;
        layoutInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        MyViewHolder myViewHolder = null;
        if (convertView == null) {
            myViewHolder = new MyViewHolder();
            convertView = layoutInflater.inflate(R.layout.item_rkxq_detailxqlist, parent, false);
            myViewHolder.mTextViewCW = convertView.findViewById(R.id.tv_cw_info);
            myViewHolder.mTextViewHJ = convertView.findViewById(R.id.tv_hj_info);
            myViewHolder.mTextViewSL = convertView.findViewById(R.id.tv_number_info);
            convertView.setTag(myViewHolder);
        }
        myViewHolder = (MyViewHolder) convertView.getTag();
        myViewHolder.mTextViewCW.setText(mList.get(position).getPositionName());
        myViewHolder.mTextViewHJ.setText(mList.get(position).getShelfName());
        myViewHolder.mTextViewSL.setText(mList.get(position).getEntryNum()+"");
        return convertView;
    }

    class MyViewHolder {
        TextView mTextViewCW;
        TextView mTextViewHJ;
        TextView mTextViewSL;

    }
}

