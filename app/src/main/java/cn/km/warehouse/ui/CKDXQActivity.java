package cn.km.warehouse.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import cn.km.warehouse.R;
import cn.km.warehouse.base.BaseActivity;
import cn.km.warehouse.model.CKDXQModel;
import cn.km.warehouse.model.RKDXQModel;
import cn.km.warehouse.present.CKDXQPresenter;
import cn.km.warehouse.present.RKDXQPresenter;
import cn.km.warehouse.ui.adapter.CKDXQAdapter;
import cn.km.warehouse.ui.adapter.RKDXQAdapter;
import cn.km.warehouse.ui.customview.MyToolbar;
import cn.km.warehouse.ui.customview.ProgressLoadingLayout;
import cn.km.warehouse.view.CKDXQView;
import cn.km.warehouse.view.RKDXQView;

public class CKDXQActivity extends BaseActivity implements CKDXQView {
    private CKDXQPresenter ckdxqPresenter;
    private String orderId;
    private ListView mMyListView;
    private MyToolbar myToolbar;
    ProgressLoadingLayout progressLoadingLayout;
    private CKDXQAdapter ckdxqAdapter;
    private TextView mTextViewCode;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ckdxq);
        progressLoadingLayout=this.findViewById(R.id.progressLoadingLayout);
        mTextViewCode=this.findViewById(R.id.tv_code);
        myToolbar = findViewById(R.id.toolBar);
        myToolbar.setTitleWithNavigation("出库单详情",true);
        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CKDXQActivity.this.finish();
            }
        });
        mMyListView=this.findViewById(R.id.listview_ck_input);
        initData();
    }
    private void initData() {
        orderId=getIntent().getStringExtra("orderId");
        ckdxqPresenter=new CKDXQPresenter(this);
        ckdxqPresenter.getCKDXQ(Integer.parseInt(orderId));
//        rkdxqAdapter = new RKDXQAdapter();
//        mRecyClerView.setLayoutManager(new LinearLayoutManager(this));
//        mRecyClerView.setHasFixedSize(true);
//        mRecyClerView.setNestedScrollingEnabled(false);
//        mRecyClerView.setFocusable(false);
//        mRecyClerView.setAdapter(rkdxqAdapter);
//        orderId=getIntent().getStringExtra("orderId");
//        ckdxqPresenter=new CKDXQPresenter(this);
//        ckdxqPresenter.getCKDXQ(Integer.parseInt(orderId));
    }


//    private void transformData(CKDXQModel ckdxqModel) {
//        if (ckdxqModel == null) {
//            return;
//        }
//        items = new ArrayList<>();
//        for (CKDXQModel.BillEntityListBean  item : ckdxqModel.getBillEntityList()) {
//            RKDXQAdapter.Item item_ = new RKDXQAdapter.Item();
//            item_.wulbm = item.getMaterialCode();
//            //  = item.getEntryTypeName();
//            item_.wlname = item.getMaterialName();
//            item_.ggxh = item.getMaterialSpecs().toString();
//            item_.hj="暂无";
//            item_.cw = "暂无";
//            item_.dw=item.getMaterialUnitName();
//            items.add(item_);
//        }
//    }

    @Override
    public void showLoading() {
        progressLoadingLayout.showLoading();
    }

    @Override
    public void dismissLoading() {
        progressLoadingLayout.showEmpty();
    }

    @Override
    public void detach() {

    }


    @Override
    public void getCKXQDataSuccess(CKDXQModel ckdxqModel) {
        if (ckdxqModel == null || ckdxqModel.getBillEntityList().size() == 0) {
            progressLoadingLayout.showEmpty();
            return;
        }

        progressLoadingLayout.showContent();
       // mTextViewCode.setText(ckdxqModel.getDeliveryOrder().getApplicationCode());
        mTextViewCode.setText(ckdxqModel.getDeliveryOrder().getApplicationCode());
        // transformData(rkdxqModel);
        ckdxqAdapter=new CKDXQAdapter(this,ckdxqModel.getBillEntityList(),ckdxqModel.getDeliveryOrder());
        mMyListView .setAdapter(ckdxqAdapter);
        ckdxqAdapter.setGoToCKClickListener(new CKDXQAdapter.GoToCKClickListener() {
            @Override
            public void goToCK(int position, String applicationId,String materialId) {
                Intent intent=new Intent();
                intent.putExtra("applicationId",applicationId);
                intent.putExtra("materialId",materialId);
                intent.setClass(CKDXQActivity.this,CKSCANActivity.class);
                CKDXQActivity.this. startActivity(intent);
            }
        });
     //   progressLoadingLayout.showContent();

//        transformData(ckdxqModel);
//        if (items == null) {
//            return;
//        }
//        rkdxqAdapter.setNewData(items);
//        mAdapter.setOnItemClickListener(new OnItemClickListener() {
////            @Override
////            public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {
////
////                Intent intent=new Intent();
////                intent.setClass(InputActivity.this,RKQDFYActiviy.class);
////                intent.putExtra("applicationId",rKlistModel.getList().get(position).getApplicationId()+"");
//////                    startActivity(new Intent(InputActivity.this, RKQDFYActiviy.class));
////                InputActivity.this.startActivity(intent);
////            }
////        });
    }

    @Override
    public void getCKXQDataFailer(String message) {
        Toast.makeText(CKDXQActivity.this, message, Toast.LENGTH_SHORT).show();
        progressLoadingLayout.showEmpty();
    }
}
