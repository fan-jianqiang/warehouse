package cn.km.warehouse.ui;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.liaoinstan.springview.container.AliHeader;
import com.liaoinstan.springview.container.DefaultFooter;
import com.liaoinstan.springview.widget.SpringView;

import java.util.ArrayList;
import java.util.List;

import cn.km.warehouse.Contstant;
import cn.km.warehouse.R;
import cn.km.warehouse.model.CKDModel;
import cn.km.warehouse.model.CKModel;
import cn.km.warehouse.model.CKQDModel;
import cn.km.warehouse.model.PDTaskModel;
import cn.km.warehouse.present.PDTaskPresenter;
import cn.km.warehouse.present.WLCKPresenter;
import cn.km.warehouse.ui.adapter.InputAdapter;
import cn.km.warehouse.ui.adapter.PDTaskListAdapter;
import cn.km.warehouse.ui.customview.ProgressLoadingLayout;
import cn.km.warehouse.utils.SPUtils;
import cn.km.warehouse.view.PDTaskView;

/**
 * A simple {@link Fragment} subclass.
 */
public class PDListFragment extends Fragment implements PDTaskView, SpringView.OnFreshListener {
    private String state;
    private String title;
    private RecyclerView mRecyClerView;
    private PDTaskPresenter mPdTaskPresenter;
    private int pageInex=1;
    private int totalpage;
    private CKModel mCkModel;
    SpringView mSpringView;
    ProgressLoadingLayout progressLoadingLayout;
    private List<PDTaskListAdapter.Item> items;
    private List<PDTaskListAdapter.Item> itemsAll = new ArrayList<>();
    PDTaskListAdapter mAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       View view=inflater.inflate(R.layout.fragment_pdlist, container, false);
        mCkModel= SPUtils.getBeanCK(getActivity(), Contstant.ck_key);
        progressLoadingLayout=view.findViewById(R.id.progressLoadingLayout);
        mPdTaskPresenter=new PDTaskPresenter(this);
        mSpringView=view.findViewById(R.id.springview_pd);
        mRecyClerView=view.findViewById(R.id.recyclerView_input);
        initData();
        return view;
    }
    public static PDListFragment newInstance(String state, String title) {
        PDListFragment fragment = new PDListFragment();
        Bundle bundle = new Bundle();
        bundle.putString("state", state);
        bundle.putString("title", title);
        fragment.setArguments(bundle);
        return fragment;
    }
    private void initData() {
        state = this.getArguments().getString("state");
        title = this.getArguments().get("title").toString();
        mSpringView.setHeader(new AliHeader(getActivity(), true));
        //mSpringView.setFooter(new AliFooter(this, true));
        mSpringView.setFooter(new DefaultFooter(getActivity()));
        mSpringView.setType(SpringView.Type.FOLLOW);
        mSpringView.setListener(this);
        mPdTaskPresenter.getPDTaskList(pageInex, Contstant.pageSize,Integer.parseInt(state),Contstant.totalCount);
        mAdapter = new PDTaskListAdapter();
        mRecyClerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyClerView.setHasFixedSize(true);
        mRecyClerView.setNestedScrollingEnabled(false);
        mRecyClerView.setFocusable(false);
        mRecyClerView.setAdapter(mAdapter);
    }
    @Override
    public void onRefresh() {
        pageInex = 1;
        if (mPdTaskPresenter != null) {
            mPdTaskPresenter.getPDTaskList(pageInex, Contstant.pageSize,Integer.parseInt(state),Contstant.totalCount);
        }
    }

    @Override
    public void onLoadmore() {
        if (pageInex>=totalpage){
            Toast.makeText(getActivity(), "暂无更多数据", Toast.LENGTH_SHORT).show();
            mSpringView.onFinishFreshAndLoad();
            return;
        }

        pageInex = pageInex+1;
        if (mPdTaskPresenter== null) {
            mPdTaskPresenter=new PDTaskPresenter(this);

        }
        mPdTaskPresenter.getPDTaskList(pageInex, Contstant.pageSize,Integer.parseInt(state),Contstant.totalCount);
    }



    @Override
    public void showLoading() {
        progressLoadingLayout.showLoading();
    }

    @Override
    public void dismissLoading() {
        if (totalpage==0){
            progressLoadingLayout.showEmpty();
        }else {
            progressLoadingLayout.showContent();
        }
    }

    @Override
    public void detach() {

    }

    private void transformData(PDTaskModel pdTaskModel) {
        if (pdTaskModel == null) {
            return;
        }
        items = new ArrayList<>();
        for (PDTaskModel.ListBean item : pdTaskModel.getList()) {
            PDTaskListAdapter.Item item_ = new PDTaskListAdapter.Item();
            item_.code = item.getTaskCode();
            if ("0".equals(item.getStatus()+"")){
                item_.state="未完成 ";
            } if ("1".equals(item.getStatus()+"")){
                item_.state="已完成 ";
            }else {
            }
            //  = item.getEntryTypeName();
            item_.kssj = item.getBeginDate();

            item_.jssj = item.getEndDate().toString();
            item_.rwsm = item.getTaskDesc();
            item_.fzr = item.getChargeUserName();
            items.add(item_);
        }
    }



    @Override
    public void getPDTaskListSuccess(PDTaskModel pdTaskModel) {
        if (pdTaskModel == null || pdTaskModel.getList().size() == 0) {
            if (pageInex == 1) {
                progressLoadingLayout.showEmpty();
            }
            mSpringView.onFinishFreshAndLoad();
            return;
        }
        pageInex=pdTaskModel.getCurrPage();
        totalpage=pdTaskModel.getTotalPage();
        progressLoadingLayout.showContent();
        transformData(pdTaskModel);
        if (items == null) {
            mSpringView.onFinishFreshAndLoad();
            return;
        }
        if (pageInex == 1) {
            mSpringView.onFinishFreshAndLoad();
            mAdapter.setNewData(items);
            itemsAll.clear();
            itemsAll.addAll(items);
        } else {
            if (items != null) {
                itemsAll.addAll(items);
                mAdapter.setNewData(itemsAll);
                mAdapter.notifyDataSetChanged();
            }

            mSpringView.onFinishFreshAndLoad();
        }
        mAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {

                Intent intent=new Intent();
                intent.setClass(getActivity(),PDTaskDetailListActivity.class);
                intent.putExtra("taskId",pdTaskModel.getList().get(position).getTaskId()+"");
                intent.putExtra("taskCode",pdTaskModel.getList().get(position).getTaskCode()+"");
//                    startActivity(new Intent(InputActivity.this, RKQDFYActiviy.class));
                getActivity().startActivity(intent);
            }
        });
    }

    @Override
    public void getPDTaskListFailer(String message) {

    }
}
