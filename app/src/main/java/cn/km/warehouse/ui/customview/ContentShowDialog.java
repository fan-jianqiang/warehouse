package cn.km.warehouse.ui.customview;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import cn.km.warehouse.R;
import cn.km.warehouse.model.CKScanGetDataMdoel;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/15
 */
public class ContentShowDialog extends Dialog implements View.OnClickListener {
    private Context context;

    private int layoutResID;

    /**
     * 要监听的控件id
     */
    private int[] listenedItems;

    private OnCenterItemClickListener listener;
    private int type;
    private String tip;
    private TextView mTextViewCode;
    private TextView mTextViewName;
    private TextView mTextViewGGXH;
    private TextView mTextViewDW;
    private TextView mTextViewSL;
    private  CKScanGetDataMdoel ckScanGetDataMdoel;
    public ContentShowDialog(Context context, int layoutResID, int[] listenedItems, CKScanGetDataMdoel ckScanGetDataMdoel) {
        super(context, R.style.MyDialog);
        this.context = context;
        this.layoutResID = layoutResID;
        this.listenedItems = listenedItems;
        this.ckScanGetDataMdoel=ckScanGetDataMdoel;
        //  this.type=type;
    }

    public void setTip(String tip) {
        this.tip = tip;
        if (mTextView != null) {
            mTextView.setText(tip);
        }
    }

    TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        window.setGravity(Gravity.CENTER); // 此处可以设置dialog显示的位置为居中
        setContentView(layoutResID);
        // 宽度全屏
        WindowManager windowManager = ((Activity) context).getWindowManager();
        Display display = windowManager.getDefaultDisplay();
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = display.getWidth() * 4 / 5; // 设置dialog宽度为屏幕的4/5
        getWindow().setAttributes(lp);
        // 点击Dialog外部消失
        setCanceledOnTouchOutside(false);
//        mTextView = findViewById(R.id.textview_ok_or_cancel_waiting);
//        if (mTextView != null) {
//            mTextView.setText(tip);
//        }
        mTextViewCode=findViewById(R.id.tv_code_info);
        mTextViewName=findViewById(R.id.tv_name_info);
        mTextViewGGXH=findViewById(R.id.tv_guige_info);
        mTextViewDW=findViewById(R.id.tv_unit_info);
        mTextViewSL=findViewById(R.id.tv_sl);
        if (ckScanGetDataMdoel.getStockNum()==0){
          findViewById(R.id.textview_ok_submit).setVisibility(View.GONE);
        }else {
            findViewById(R.id.textview_ok_submit).setVisibility(View.VISIBLE);
        }
        mTextViewCode.setText(ckScanGetDataMdoel.getMaterialCode());
        mTextViewName.setText(ckScanGetDataMdoel.getMaterialName());
        mTextViewGGXH.setText(ckScanGetDataMdoel.getMaterialSpecs());
        mTextViewDW.setText(ckScanGetDataMdoel.getMaterialUnitName());
        mTextViewSL.setText(ckScanGetDataMdoel.getStockNum()+"");
        for (int id : listenedItems) {
//        if (type==1){
        if (id==R.id.textview_cancel || id==R.id.textview_ok_submit){
            findViewById(id).setOnClickListener(this);
        }else {

        }

        }
    }

    public interface OnCenterItemClickListener {

        void OnCenterItemClick(ContentShowDialog dialog, View view);

    }

    public void setOnCenterItemClickListener(OnCenterItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.textview_cancel) {
            dismiss();
        } else {
            dismiss();
            listener.OnCenterItemClick(this, view);
        }

    }
}


