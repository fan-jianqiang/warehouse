package cn.km.warehouse.ui;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.liaoinstan.springview.container.AliHeader;
import com.liaoinstan.springview.container.DefaultFooter;
import com.liaoinstan.springview.widget.SpringView;

import java.util.ArrayList;
import java.util.List;

import cn.km.warehouse.Contstant;
import cn.km.warehouse.R;
import cn.km.warehouse.model.CKModel;
import cn.km.warehouse.model.RKListQDModel;
import cn.km.warehouse.model.RKlistModel;
import cn.km.warehouse.present.RukuListPresenter;
import cn.km.warehouse.ui.adapter.InputAdapter;
import cn.km.warehouse.ui.customview.ProgressLoadingLayout;
import cn.km.warehouse.utils.SPUtils;
import cn.km.warehouse.view.RukuListView;


public class RKFragmentYWC extends RKFragment implements RukuListView, SpringView.OnFreshListener {
    private RecyclerView mRecyClerView;
    private RukuListPresenter mRuKuListPresenter;
    private int pageInex=1;
    private int totalpage;
    private CKModel mCkModel;
    SpringView mSpringView;
    ProgressLoadingLayout progressLoadingLayout;
    private List<InputAdapter.Item> items;
    private List<InputAdapter.Item> itemsAll = new ArrayList<>();
    InputAdapter mAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_rkfragment_ywc, container, false);
        mCkModel= SPUtils.getBeanCK(getActivity(), Contstant.ck_key);
        progressLoadingLayout=view.findViewById(R.id.progressLoadingLayout);
        mRuKuListPresenter=new RukuListPresenter(this);
        mSpringView=view.findViewById(R.id.springview_rk);
        mRecyClerView=view.findViewById(R.id.recyclerView_input);
        initData();
        return view;
    }

    public void   initData(){
        mSpringView.setHeader(new AliHeader(getActivity(), true));
        //mSpringView.setFooter(new AliFooter(this, true));
        mSpringView.setFooter(new DefaultFooter(getActivity()));
        mSpringView.setType(SpringView.Type.FOLLOW);
        mSpringView.setListener(this);
        mRuKuListPresenter.getRKDlist(pageInex, Contstant.pageSize,mCkModel.getId(),Contstant.totalCount,3);
        mAdapter = new InputAdapter();
        mRecyClerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyClerView.setHasFixedSize(true);
        mRecyClerView.setNestedScrollingEnabled(false);
        mRecyClerView.setFocusable(false);
        mRecyClerView.setAdapter(mAdapter);

    };
    @Override
    public void showData(RKlistModel rKlistModel) {

    }

    @Override
    public void onRefresh() {
        pageInex = 1;
        if (mRuKuListPresenter != null) {
            mRuKuListPresenter.getRKDlist(pageInex, Contstant.pageSize,mCkModel.getId(),Contstant.totalCount,3);
        }
    }

    @Override
    public void onLoadmore() {
        if (pageInex>=totalpage){
            Toast.makeText(getActivity(), "暂无更多数据", Toast.LENGTH_SHORT).show();
            mSpringView.onFinishFreshAndLoad();
            return;
        }

        pageInex = pageInex+1;
        if (mRuKuListPresenter== null) {
            mRuKuListPresenter=new RukuListPresenter(this);

        }
        mRuKuListPresenter.getRKDlist(pageInex, Contstant.pageSize,mCkModel.getId(),Contstant.totalCount,3);
    }

    @Override
    public void getRuKuListSuccess(RKlistModel rKlistModel) {
        if (rKlistModel == null || rKlistModel.getList().size() == 0) {
            if (pageInex == 1) {
                progressLoadingLayout.showEmpty();
            }
            mSpringView.onFinishFreshAndLoad();
            return;
        }
        pageInex=rKlistModel.getCurrPage();
        totalpage=rKlistModel.getTotalPage();
        progressLoadingLayout.showContent();
        transformData(rKlistModel);
        if (items == null) {
            mSpringView.onFinishFreshAndLoad();
            return;
        }
        if (pageInex == 1) {
            mSpringView.onFinishFreshAndLoad();
            mAdapter.setNewData(items);
            itemsAll.clear();
            itemsAll.addAll(items);
        } else {
            if (items != null) {
                itemsAll.addAll(items);
                mAdapter.setNewData(itemsAll);
                mAdapter.notifyDataSetChanged();
            }

            mSpringView.onFinishFreshAndLoad();
        }
        mAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {

                Intent intent=new Intent();
                intent.setClass(getActivity(),RKDXQActivity.class);
                intent.putExtra("orderId",rKlistModel.getList().get(position).getOrderId()+"");
//                    startActivity(new Intent(InputActivity.this, RKQDFYActiviy.class));
                getActivity().startActivity(intent);
            }
        });
    }
    private void transformData(RKlistModel rKlistModel) {
        if (rKlistModel == null) {
            return;
        }
        items = new ArrayList<>();
        for (RKlistModel.ListBean item : rKlistModel.getList()) {
            InputAdapter.Item item_ = new InputAdapter.Item();
            item_.ApplicationCode = item.getOrderCode();
            if ("1".equals(item.getOrderState()+"")){
                item_.state="未入库 ";
            } if ("2".equals(item.getOrderState()+"")){
                item_.state="入库中 ";
            } if ("3".equals(item.getOrderState()+"")){
                item_.state=" 已完成 ";
            }
            //  = item.getEntryTypeName();
            item_.ssgs = item.getOrganizationName();
            item_.sqr = item.getApplicationAccountName().toString();
            item_.sqsj = item.getApplicationDatetime();
            items.add(item_);
        }
    }
    @Override
    public void getRuKuListSuccessFailer(String message) {

    }

    @Override
    public void getRuKuListQDSuccess(RKListQDModel rkListQDModel) {

    }

    @Override
    public void getRuKuListQDSuccessFailer(String message) {

    }

    @Override
    public void showLoading() {
        progressLoadingLayout.showLoading();
    }

    @Override
    public void dismissLoading() {
        if (totalpage==0){
            progressLoadingLayout.showEmpty();
        }else {
            progressLoadingLayout.showContent();
        }
    }

    @Override
    public void detach() {

    }
}
