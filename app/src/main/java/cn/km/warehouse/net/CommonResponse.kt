package com.drcnet.android.net.base

import com.google.gson.JsonElement

/**
 * 通用响应实体
 * @param code 响应代码
 * @param data 响应实体
 * @param message 提示消息
 * @param version api版本
 * create by halfcup
 * on 2018/10/23
 */
class CommonResponse(val code: Int, val data: JsonElement?, val msg: String?, val version: String?) {
}