package com.drcnet.android.net.base

/**
 * create by halfcup
 * on 2018/10/23
 */
object Headers {
    const val CONTENT_TYPE_JSON = "Content-Type: application/json"
    const val CONTENT_TYPE_FORM="Content-Type: application/x-www-form-urlencoded"
    const val CONTENT_TYPE_MULTI_PART = "Content-Type: multipart/form-data"
}