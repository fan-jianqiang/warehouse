package cn.km.warehouse.net;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/1/31
 */
public class ConnectionChangedEvent {
    private boolean isConnectionChange;

    public ConnectionChangedEvent(boolean isConnectionChange) {
        this.isConnectionChange = isConnectionChange;
    }
}
