package com.drcnet.android.net.base

import android.content.Context
import android.util.Log
import cn.km.warehouse.BuildConfig
import cn.km.warehouse.Configuration
import cn.km.warehouse.Contstant
import cn.km.warehouse.net.AuthInterceptor
import cn.km.warehouse.net.TokenAuthenticator
import cn.km.warehouse.utils.SPUtils


import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonSerializer
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * 整体的api请求封装
 *
 * create by halfcup
 * on 2018/10/22
 */
class ApiUtil private constructor() {
    private val authClient: OkHttpClient
    private val apiClient: OkHttpClient
    private val apiClientLogin: OkHttpClient
    var apiRetrofit: Retrofit
    var apiRetrofitLogin: Retrofit
    val istoken:Boolean = false;
// authRetrofit: Retrofit
  //  val tokenAuthenticator: TokenAuthenticator();
var v= Configuration.getInstance().getServiceUrl();
    companion object {
        val INSTANCE by lazy { ApiUtil() }

    }

    init {
        authClient = OkHttpClient.Builder()
               // .addNetworkInterceptor(StethoInterceptor())
                .build()

        apiClient = OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(45, TimeUnit.SECONDS)
         //    .authenticator(TokenAuthenticator())

              .addInterceptor(TokenAuthenticator())
              //  .addNetworkInterceptor(StethoInterceptor())
                .build()
        apiClientLogin = OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(45, TimeUnit.SECONDS)
         //       .authenticator(TokenAuthenticator())

             // .addInterceptor(TokenAuthenticator())
              // .addNetworkInterceptor(AuthInterceptor())
                .build()

        val gson = GsonBuilder()
//                .registerTypeAdapter(SavedParam::class.java, JsonSerializer<SavedParam> { src, _, _ ->
//                    Gson().toJsonTree(src).asJsonObject.apply {
//                        addProperty("queryParam", get("queryParam").apply { get("lisQuery").asJsonArray.forEach { item -> item.asJsonObject.remove("queryItemName") } }.asString)
//                    }
//                })
                .create()


        apiRetrofit = Retrofit.Builder()
            .client(apiClient)
            .baseUrl(v.toString())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build()
        apiRetrofitLogin = Retrofit.Builder()
            .client(apiClientLogin)
            .baseUrl(v.toString())//"http://panlin.free.idcfengye.com/"
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build()
//        authRetrofit = apiRetrofit.newBuilder()
//                .baseUrl(BuildConfig.AUTH_HOST)
//                .client(authClient)
//                .build()
        Log.e("sjsj-->","fsdfs");
    }

    fun refreshServerConfig() {
            apiRetrofit = Retrofit.Builder()
                .client(apiClient)
                .baseUrl(Configuration.getInstance().getServiceUrl())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
                .build()
        }

        fun refreshLoginServerConfig() {
            apiRetrofitLogin = Retrofit.Builder()
                .client(apiClientLogin)
                .baseUrl(Configuration.getInstance().getServiceUrl())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build()
    }
    fun request(observable: Observable<CommonResponse>, listener: RequestListener): Disposable {
        return observable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ listener.onNext(it) },
                        { listener.onError(it) },
                        { listener.onComplete() },
                        { listener.onSubscribe(it) })
    }

    fun requestDirect(observable: Observable<CommonResponse>, listener: RequestListener): Disposable {
        return observable
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe({ listener.onNext(it) },
                        { listener.onError(it) },
                        { listener.onComplete() },
                        { listener.onSubscribe(it) })
    }



    interface RequestListener {
        fun onSubscribe(disposable: Disposable)
        fun onNext(commonResponse: CommonResponse)
        fun onError(throwable: Throwable)
        fun onComplete()
    }
}