package com.drcnet.android.net.data

import cn.km.warehouse.model.VMaterialStock
import com.drcnet.android.net.base.CommonResponse

import io.reactivex.Observable
import retrofit2.http.*

interface DataApi {

    /**
     *登录
     */
    @POST("appApi/login")
    fun getLogin( @Query("accountName") accountName: String, @Query("accountPassword") accountPassword: String): Observable<CommonResponse>
  /*
     入库清单list
   */
    @GET("appApi/entryPage")
    fun getRuKuList( @Query("pageIndex") pageIndex: Int, @Query("pageSize") pageSize: Int, @Query("houseId") houseId: Int,
                    @Query("totalCount") totalCount: Int): Observable<CommonResponse>/*
     入库清单list
   */
    @GET("appApi/entryPage")
    fun getRuKuDList( @Query("pageIndex") pageIndex: Int, @Query("pageSize") pageSize: Int, @Query("houseId") houseId: Int,
                    @Query("totalCount") totalCount: Int, @Query("state") state: Int): Observable<CommonResponse>

   /*
   获取出库单list
    */
    @GET("appApi/deliveryPage")
    fun getCKDList( @Query("pageIndex") pageIndex: Int, @Query("pageSize") pageSize: Int, @Query("houseId") houseId: Int,
                    @Query("totalCount") totalCount: Int): Observable<CommonResponse>
    /*
   获取出库单清单list
    */
    @GET("appApi/deliveryPage")
    fun getCKDQDList( @Query("pageIndex") pageIndex: Int, @Query("pageSize") pageSize: Int, @Query("houseId") houseId: Int,
                    @Query("totalCount") totalCount: Int,@Query("state") state: Int): Observable<CommonResponse>

    /*
     获取出库单清单list 状态为所有 不传状态
      */
    @GET("appApi/deliveryPage")
    fun getCKDQDListALL( @Query("pageIndex") pageIndex: Int, @Query("pageSize") pageSize: Int, @Query("houseId") houseId: Int,
                      @Query("totalCount") totalCount: Int): Observable<CommonResponse>
    /*
    入库清单list清单
  */
    @GET("appApi/entryBillPage")
    fun getRuKuListQD( @Query("pageIndex") pageIndex: Int, @Query("pageSize") pageSize: Int, @Query("applicationId") applicationId: Int,
                     @Query("totalCount") totalCount: Int): Observable<CommonResponse>
    /*
    入库清单list清单
  */
    @GET("appApi/deliveryBillPage")
    fun getCKListQD( @Query("pageIndex") pageIndex: Int, @Query("pageSize") pageSize: Int, @Query("applicationId") applicationId: Int,
                     @Query("totalCount") totalCount: Int): Observable<CommonResponse>
 /*
     获取仓库
   */
    @GET("warehouseInfo/getAllWarehouse")
    fun getCKList(): Observable<CommonResponse>
    /*
      入库清单详情
    */
    @GET("appApi/selectEntryBill")
    fun getRuKuDetail( @Query("billId") billId: Int): Observable<CommonResponse>
    /*
      获取仓位
    */
    @GET("warehousePositionInfo/getAllPositionByHouseId")
    fun getCW( @Query("houseId") houseId: Int): Observable<CommonResponse>
    /*
      获取仓位
    */
    @GET("warehouseShelfInfo/getAllShelfByPosition")
    fun getHJ( @Query("houseId") houseId: Int,@Query("positionId") positionId: Int): Observable<CommonResponse>
 /*
      获取入库单详情
    */
    @GET("appApi/entryClickView")
    fun getRKDXQ( @Query("orderId") orderId: Int): Observable<CommonResponse>
    /*
      获取出库单详情
    */
    @GET("appApi/deliveryClickView")
    fun getCKDXQ( @Query("orderId") orderId: Int): Observable<CommonResponse>
    /*
      获取盘点任务列表
    */
    @GET("appApi/countTaskPage")
    fun getPDTaskList( @Query("pageIndex") pageIndex: Int, @Query("pageSize") pageSize: Int, @Query("status") status: Int,
                       @Query("totalCount") totalCount: Int): Observable<CommonResponse>
    /*
      获取盘点任务列表
    */
    @GET("appApi/countTaskPage")
    fun getPDTaskListAll( @Query("pageIndex") pageIndex: Int, @Query("pageSize") pageSize: Int,
                       @Query("totalCount") totalCount: Int): Observable<CommonResponse>
    /*
      盘点扫描
    */
    @GET("appApi/countTaskScan")
    fun getPDTaskScan( @Query("rfid") rfid: String, @Query("houseId") houseId: Int,
                          @Query("taskId") taskId: Int): Observable<CommonResponse>
    /*
     盘点完成
   */
    @POST("appApi/inventoryComplete")
    fun getPDComplete(
                       @Query("taskId") taskId: Int): Observable<CommonResponse>
    /*
      获取盘点任务详情列表
    */
    @GET("appApi/countTaskDetialPage")
    fun getPDTaskDetailList( @Query("pageIndex") pageIndex: Int, @Query("pageSize") pageSize: Int,
                       @Query("totalCount") totalCount: Int ,@Query("taskId") taskId: Int): Observable<CommonResponse>


    /*
      入库提交
    */
    @POST("appApi/entryCommit")
    fun submitRK(@Body billList: ArrayList<VMaterialStock>,@Query("orderId") orderId: Int): Observable<CommonResponse>

    /*
       出库提交
     */
    @POST("appApi/deliveryCommit")
    fun submitCk( @Query("orderId") orderId: Int, @Query("rfid") rfid: String): Observable<CommonResponse>

    /*
       出库扫码
     */
    @GET("appApi/deliveryScan")
    fun getCKScanData( @Query("rfid") rfid: String): Observable<CommonResponse>
    /*
       选择一个出库清单
     */
    @GET("appApi/selectDeliveryBill")
    fun getSelectCKQD( @Query("applicationId") applicationId: Int,@Query("materialId") materialId: Int): Observable<CommonResponse>
  /*
      转移货架扫描物料
     */
    @GET("appApi/transScan")
    fun getZYHJScan( @Query("houseId") houseId: Int,@Query("rfid") rfid: String): Observable<CommonResponse>
    /*
    转移货架提交
    */
    @POST("appApi/transScanCommit")
    fun submitZYHJ( @Query("moveInShelfId") moveInShelfId: Int,@Query("rfid") rfid: String): Observable<CommonResponse>
 /*
    物料状态提交
    */
    @POST("appApi/stateScanCommit")
    fun submitWLZT( @Query("state") state: Int,@Query("rfid") rfid: String): Observable<CommonResponse>

}