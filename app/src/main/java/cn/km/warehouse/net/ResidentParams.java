package cn.km.warehouse.net;

import android.content.Context;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/1/30
 */
public class ResidentParams {

    public static String KEY_IMEI 				= "imei";
    public static String KEY_SIM 				= "sim";
    public static String KEY_USER 				= "userid";
    public static String KEY_VERSION 		= "version";
    public static String KEY_SERVICE 		= "service";
    public static String KEY_USER_ID 		= "userid";
    public static String KEY_PASSWORD 	= "password";
    public static String KEY_CLIENT_TYPE = "clientType";
    public static String KEY_ORGID 			= "orgid";
    public static String KEY_OS_VERSION = "osVersion";

    private Context mContext;

    private Map<String, String> mResidentParams = new LinkedHashMap<>();

    public static ResidentParams build(Context context,String type){
        return new ResidentParams(context)
                // .imei()

                .sim(type);
        //.osVersion()
        //  .version()
        //  .service(type);
    }

    private ResidentParams(Context context){
        mContext = context;
    }

    public Map<String, String> getResidentParams(){
        return mResidentParams;
    }
//
//    private ResidentParams imei(){
//        //暂时测试写死
//        mResidentParams.put(KEY_IMEI, "861695033083440");
//        return this;
//    }

    public ResidentParams sim(String clientType){
//        String simNum = MobileInfo.getImsiCode();
//        if(!TextUtils.isEmpty(simNum)){
//            mResidentParams.put(KEY_SIM, simNum);
//        }
        mResidentParams.put(KEY_SIM, clientType);
        return this;
    }
    public ResidentParams userId(String userId){
        mResidentParams.put(KEY_USER, userId);
        return this;
    }

//    private ResidentParams version(){
//        mResidentParams.put(KEY_VERSION, AppInfoHelper.getVersionCode(mContext) + "");
//        return this;
//    }
//
//    private ResidentParams service(String type){
//        if(type != null) {
//            mResidentParams.put(KEY_SERVICE, type.toString());
//        }
//        return this;
//    }
//
//    public ResidentParams userId(String userId){
//        mResidentParams.put(KEY_USER_ID, userId);
//        return this;
//    }
//
//    public ResidentParams password(String password){
//        mResidentParams.put(KEY_PASSWORD, password);
//        return this;
//    }

//    public ResidentParams clientType(String clientType){
//        mResidentParams.put(KEY_CLIENT_TYPE, clientType);
//        return this;
//    }

//    public ResidentParams orgid(String orgid){
//        mResidentParams.put(KEY_ORGID, orgid);
//        return this;
//    }

//    private ResidentParams osVersion(){
//        mResidentParams.put(KEY_OS_VERSION, Build.MODEL);
//        return this;
//    }

}
