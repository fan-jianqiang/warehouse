package cn.km.warehouse.net

import android.text.TextUtils
import okhttp3.Interceptor
import okhttp3.Response

/**
 * 添加认证头的拦截器
 * auth-oauth2的认证
 * userId-登录以后的认证
 *
 * create by halfcup
 * on 2018/10/24
 */
class AuthInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var newRequest = chain.request()
        newRequest = newRequest.newBuilder()
            .header("Connection", "close")
            .build()
        return chain.proceed(newRequest)
    }
}