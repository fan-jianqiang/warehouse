package cn.km.warehouse.net

import android.text.TextUtils
import cn.km.warehouse.Contstant
import cn.km.warehouse.base.WorkOAApplication
import cn.km.warehouse.utils.SPUtils
import com.drcnet.android.net.auth.AuthApi
import com.drcnet.android.net.base.ApiUtil
import com.drcnet.android.net.base.CommonResponse
import okhttp3.*
import retrofit2.Call

/**
 * 加认证头的拦截器
 *
 * create by halfcup
 * on 2018/10/23
 */
class TokenAuthenticator : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var newRequest = chain.request()
        val loginModel1 =
            SPUtils.getBean(WorkOAApplication.instance().applicationContext, Contstant.user_key)
        val token = loginModel1.token
        if (!TextUtils.isEmpty(token)) {
            newRequest = newRequest.newBuilder()
                .header("token", token)
                .build()
            return chain.proceed(newRequest)
        } else {
            return chain.proceed(newRequest)
        }

    }
}
//    override fun authenticate(route: Route, response: Response): Request? {
//        var newRequest = response.request()
//
//        //先检查是否需要重新请求，如果已经请求到新的token了，直接使用，不需要再请求
//
////        val call: Call<CommonResponse> = ApiUtil.INSTANCE.authRetrofit.create(AuthApi::class.java).accessToken()
////        val authResponse = call.execute()
////        if (authResponse.isSuccessful) {
////            if (authResponse.body()?.code == 0) {
////                // 可能需要存一下这个header
////                val token = authResponse.body()?.data?.asJsonObject?.get("text")?.asString
////                SP.putStringImmediately(SP.ACCESS_TOKEN, token)
////
////                newRequest = newRequest.newBuilder()
////                        .header("token", "Bearer $token")
////                        .build()
////            }
////        } else {
////            // clear auth for check
////            newRequest = newRequest.newBuilder()
////                    .header("Authorization", "failed")
////                    .build()
////        }
//        val loginModel1 = SPUtils.getBean(WorkOAApplication.instance().applicationContext, Contstant.user_key)
//        val token = loginModel1.token
//        if (!TextUtils.isEmpty(token)){
//            newRequest = newRequest.newBuilder()
//                .header("token", "Bearer $token")
//                .build()
//        }else{
//            newRequest = newRequest.newBuilder()
//                    .header("token", "failed")
//                    .build()
//        }
//      //  val token = authResponse.body()?.data?.asJsonObject?.get("text")?.asString
//
//        return newRequest
//    }
//}