package com.drcnet.android.net.base

import android.util.Log
import cn.km.warehouse.BuildConfig

import com.google.gson.JsonElement
import io.reactivex.disposables.Disposable

/**
 * create by halfcup
 * on 2018/10/30
 */
abstract class NormalHandle : ApiUtil.RequestListener {
    override fun onSubscribe(disposable: Disposable) = Unit

    override fun onNext(commonResponse: CommonResponse) {

        when (commonResponse.code ) {
            0 -> if (!commonResponse.data.toString().equals("null")) {
                onSuccess(commonResponse.data)
            }else{
                onLogicFailed(commonResponse.code, commonResponse.msg)
            }
            else -> onLogicFailed(commonResponse.code, commonResponse.msg)
        }
    }

    override fun onError(throwable: Throwable) {
       // ToastInstance.INSTANCE.show(App.INSTANCE, "发生异常")50
       // onError(throwable)
        onLogicFailed(1,throwable.message.toString())
        if (BuildConfig.DEBUG)
            throwable.printStackTrace()
    }

    override fun onComplete() = Unit

    /**
     * response with logic success
     */
    abstract fun onSuccess(data: JsonElement?)
    //abstract fun onSuccess1(CommonResponse: JsonElement?)

    /**
     * response with logic error
     */
    open fun onLogicFailed(code: Int, message: String?) {
       // ToastInstance.INSTANCE.show(App.INSTANCE, message)
    }
}