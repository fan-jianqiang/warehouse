package cn.km.warehouse.view;

import com.drcnet.android.mvp.base.BaseView;

import java.util.ArrayList;

import cn.km.warehouse.model.CWModel;
import cn.km.warehouse.model.HJModel;
import cn.km.warehouse.model.RKListQDModel;
import cn.km.warehouse.model.RKlistModel;
import cn.km.warehouse.model.RuKuDetailModel;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/2/25
 */
public interface RukuDetailView extends BaseView {
    public void getRuKuDetailSuccess(RuKuDetailModel ruKuDetailModel);
    public void getRuKuDetailFailer(String message);
    public void getCWSuccess(ArrayList<CWModel>  cwModels);
    public void getHJSuccess(ArrayList<HJModel>  hjModels);
    public void getTJSuccess(String msg);
    public void getTJFailer(int code,String message);

}
