package cn.km.warehouse.view;

import com.drcnet.android.mvp.base.BaseView;

import java.util.ArrayList;

import cn.km.warehouse.model.CKModel;


/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/2/2
 */
public interface CKView  extends BaseView {
    public void getCKSuccess(ArrayList<CKModel> ckModels);
    public void getCKFailer(String message);
}
