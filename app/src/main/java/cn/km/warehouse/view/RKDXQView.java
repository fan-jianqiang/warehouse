package cn.km.warehouse.view;

import com.drcnet.android.mvp.base.BaseView;

import cn.km.warehouse.model.RKDXQModel;
import cn.km.warehouse.model.RKListQDModel;
import cn.km.warehouse.model.RKlistModel;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/4
 */
public interface  RKDXQView extends BaseView {
    public void getRKXQDataSuccess(RKDXQModel rkdxqModel);
    public void getRKXQDataFailer(String message);
}