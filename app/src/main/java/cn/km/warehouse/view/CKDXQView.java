package cn.km.warehouse.view;

import com.drcnet.android.mvp.base.BaseView;

import cn.km.warehouse.model.CKDXQModel;
import cn.km.warehouse.model.RKDXQModel;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/7
 */
public interface  CKDXQView  extends BaseView {
    public void getCKXQDataSuccess(CKDXQModel ckdxqModel);
    public void getCKXQDataFailer(String message);
}
