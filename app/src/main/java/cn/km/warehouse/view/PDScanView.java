package cn.km.warehouse.view;

import com.drcnet.android.mvp.base.BaseView;

import cn.km.warehouse.model.PDScanModel;
import cn.km.warehouse.model.PDTaskModel;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/16
 */
public interface PDScanView extends BaseView {
    public void getPDTaskScanSuccess(   PDScanModel pdScanModel);
    public void getPDTaskScanFailer(int  code ,String message);
}
