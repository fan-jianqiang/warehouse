package cn.km.warehouse.view;



import com.drcnet.android.mvp.base.BaseView;

import cn.km.warehouse.model.PDDetailModel;
import cn.km.warehouse.model.PDTaskModel;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/9
 */
public interface PDTaskDetailView extends BaseView {
    public void getPDTaskDetialListSuccess(PDDetailModel pdDetailModel);
    public void getPDTaskDetialListFailer(String message);
    public void getPDTaskCompleteSuccess(String message);
    public void getPDTaskCompleteFailure(int code,String message);
}
