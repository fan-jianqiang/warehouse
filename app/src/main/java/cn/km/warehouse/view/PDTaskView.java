package cn.km.warehouse.view;

import com.drcnet.android.mvp.base.BaseView;

import java.util.ArrayList;

import cn.km.warehouse.model.CKModel;
import cn.km.warehouse.model.PDTaskModel;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/7
 */
public interface  PDTaskView  extends BaseView {
    public void getPDTaskListSuccess(PDTaskModel pdTaskModel);
    public void getPDTaskListFailer(String message);
}
