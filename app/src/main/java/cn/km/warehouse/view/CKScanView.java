package cn.km.warehouse.view;

import com.drcnet.android.mvp.base.BaseView;

import cn.km.warehouse.model.CKScanGetDataMdoel;
import cn.km.warehouse.model.CKScanModel;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/13
 */
public interface CKScanView extends BaseView {
    public void submitSuccess(String msg);
      public void submitFailer(int code,String msg);
      public void getSelectCKQDDataSuccess(CKScanModel ckScanModel);
      public void getCKScanDataSuccess(CKScanGetDataMdoel ckScanGetDataMdoel);
      public void getScanDataFailer(int code,String msg);
      public void getCKScanDataFailer(int code,String msg);

}
