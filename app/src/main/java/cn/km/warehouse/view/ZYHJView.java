package cn.km.warehouse.view;

import com.drcnet.android.mvp.base.BaseView;

import java.util.ArrayList;

import cn.km.warehouse.model.HJModel;
import cn.km.warehouse.model.ZYHJScanModel;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/17
 */
public interface ZYHJView  extends BaseView {
    public void  getScanDataSuccess(ZYHJScanModel zyhjScanModel);
    public void  getScanDataFailure(int code,String msg);
    public void getHJSuccess(ArrayList<HJModel> hjModels);
    public void getZYHJSubmitSuccess(String message);
    public void ggetZYHJSubmitFailure(int code,String message);
}
