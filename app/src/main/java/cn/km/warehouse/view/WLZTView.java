package cn.km.warehouse.view;

import com.drcnet.android.mvp.base.BaseView;

import cn.km.warehouse.model.ZYHJScanModel;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/17
 */
public interface WLZTView extends BaseView {
    public void  getScanDataSuccess(ZYHJScanModel zyhjScanModel);
    public void  getScanDataFailure(int code,String msg);
    public void getWLZTSubmitSuccess(String message);
    public void getWLZTSubmitFailure(int code,String message);

}
