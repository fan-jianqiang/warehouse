package cn.km.warehouse.view;

import com.drcnet.android.mvp.base.BaseView;

import java.util.ArrayList;

import cn.km.warehouse.model.LoginModel;
import cn.km.warehouse.model.RKListQDModel;
import cn.km.warehouse.model.RKlistModel;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/2/1
 */
public interface RukuListView  extends BaseView {
    public void getRuKuListSuccess(RKlistModel rKlistModel);
    public void getRuKuListSuccessFailer(String message);
    public void getRuKuListQDSuccess(RKListQDModel rkListQDModel);
    public void getRuKuListQDSuccessFailer(String message);
}
