package cn.km.warehouse.view;

import com.drcnet.android.mvp.base.BaseView;

import cn.km.warehouse.model.CKDModel;
import cn.km.warehouse.model.CKListQDModel;
import cn.km.warehouse.model.CKQDModel;
import cn.km.warehouse.model.CKScanGetDataMdoel;
import cn.km.warehouse.model.RKListQDModel;
import cn.km.warehouse.model.RKlistModel;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/4
 */
public interface WLCKView extends BaseView {
    public void getCKListSuccess(CKDModel ckdModel);
    public void getCKListSuccessFailer(String message);
    public void getCKListQDSuccess(CKListQDModel ckListQDModel);
    public void getCKQDSuccess(CKQDModel ckqdModel);
    public void getCKListQDFailer(String message);
    public void getScanDataFailer(int code ,String message);
    public void submitFailer(int code ,String message);
    public void getCKScanDataSuccess(CKScanGetDataMdoel ckScanGetDataMdoel);
    public void submitSuccess(String message);
}
