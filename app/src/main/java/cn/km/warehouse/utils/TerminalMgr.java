package cn.km.warehouse.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class TerminalMgr {

    public TerminalMgr(int gn, int tn) {
        m_mapGroupInfo = new _GroupInfo[gn];
        for (int g = 0; g < gn; g++) {
            _GroupInfo gi = new _GroupInfo();

            gi.gid = -1;
            gi.gname = "";
            gi.tcount = 0;
            gi.guse = 0;
            gi.page = 1;
            gi.ggetstat = Get_NULL;
            gi.TerLink = new LinkedHashMap<>();
            m_mapGroupInfo[g] = gi;
        }

        DbTLink = new LinkedHashMap<>();
        DpTLink = new LinkedHashMap<>();

    }

    public _GroupInfo[] m_mapGroupInfo;
    public final int Page_MAX = 25;
    public final int Get_OK = 0;
    public final int[] Get_PAGE = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
    public final int Get_NULL = -1;
    public final int DATA_VALUE_NULL = 9999999;

    public static class _GroupInfo {
        public String gname;    //组名称
        public int gid;         //组id
        public int tcount;      //改组包含设备数量
        public int tonlinecount;//在线数量
        public int talarmcount; //报警数量
        public int guse;        //是否投入运行

        public int page;        //第几页

        public int ggetstat; //0没有取组列表 1取到完整组列表

        public LinkedHashMap<String, _TerInfo> TerLink;//<tid,terinfo>
        //public _TerInfo[] terinfo;
    }

    public static class _GroupDpDbInfo {
        public String gname;    //组名称
        public int gid;         //组id
        public int tcount;      //改组包含设备数量
        public int tonlinecount;//在线数量
        public int talarmcount; //报警数量
        //public int guse;        //是否投入运行

        //public int page;        //第几页

        //public int ggetstat; //0没有取组列表 1取到完整组列表

        public LinkedHashMap<String, _TerInfo> TerLink;//<tid,terinfo>  设备列表
        //public _TerInfo[] terinfo;
    }

    public static class _TerInfo {

        public int gid;         //属于组的id
        public String tid;      //设备id
        public String tname;    //设备名称
        public int online;      //在线
        public int alarm;       //报警
        public int tcount;      //设备个数

        public String alstarttime;//报警开始时间
        public int alarmcount;  //报警累计次数
        public int alarmtime;  //报警时长
        public int oltime;     //累计在线时长
        public int photos;      //照片数量

        public int dbtype;//设备类型
        public int dptype; //设备所属部门
        public int setuptime; //设备安装时间

        public double jd;//经度
        public double wd;//纬度

        public String cid;         //摄像头编号
        public String purl;         //图片url

        public int weaDtime;//实际值对应的时间
        public boolean hasDataInfo; //天气电池数据规则DataInfo是否有
        public boolean weaHasNewData;//
        public boolean dbHasNewData;//定标数据是否有新数据
        public LinkedHashMap<Integer, _DataInfo> dataLink = new LinkedHashMap<>(); //<num,datainfo> num为从0开始的计数
        public LinkedHashMap<Integer, _ControlDataInfo> controlInfos = new LinkedHashMap<>(); //<num,datainfo> num为从0开始的计数
        public _DbDataInfo dbDataInfo = new _DbDataInfo();
        public String strLogStartDate = ""; //照片开始日期，某个月的1号
        public Map<String, String> useLog = new HashMap<String, String>();
        public List<_DbLogInfo> dbloginfolist = new ArrayList<_DbLogInfo>();
        public List<_BDLogInfo> bdloginfolist = new ArrayList<_BDLogInfo>();

    }

    public static class _DbLogInfo {
        public int ix;
        public int cid;
        public String uid;
        public String optm;
        public String optm1;
        public String optm2;
        public String rm;
        public String bf;
    }

    public static class _BDLogInfo {
        public int ix;
        public int cid;
        public String uid;
        public String optm;
        public int ty;
        public String cang;
        public String sang;
        public int st;
        public String rm;
    }

    public static class _DbType {
        public int id;   //型号类型
        public String name;//名称
        public String size;//尺寸
        public String thecode;//型号

        public int azmin; //方位角最小尺寸
        public int azmax;
        public int elmin; //俯仰角最小尺寸
        public int elmax;
    }

    public LinkedHashMap<Integer, _DbType> DbTLink;

    public static class _DpType {
        public int id;
        public String name;
        public String addr;
        public String admin;
        public String phone;
    }

    public LinkedHashMap<Integer, _DpType> DpTLink;

    public static class _DataInfo {
        public int seq;
        public String name;
        public int type;  //0-AI/VI，1-AO/VO(不可控)，2-DI，3-DO（不可控），4-AO/VO(可控)，5-DO(可控)
        //public int dtime;//实际值对应的时间
        public int dvalue;//实际值
        public int mors; //0缩小1不变2放大
        public int mstimes; //放大缩小倍数
        public int alarmtype;//0范围内有效，1范围外有效
        public int alarmstart; //范围开始值
        public int alarmend; //范围结束值
        public String unit; //单位数值型 km p；开关型   正常^^^^报警  （用^^^隔开）

        //public int duse;
        //查询历史得到的数据
        public double historydata[] = new double[50];
    }

    public static class _DbDataInfo {

        public int datime;//该条数据的时间
        public String lastcaz = "-";//编辑器上次方位角度
        public String caz = "-";//编辑器方位角度
        public String lastcel = "-";//编辑器上次俯仰角度
        public String cel = "-";//编辑器俯仰角度
        public String eaz = "-";//期望的方为角度
        public String eel = "-";//期望的俯仰角度
        public String detime;//设备的当前时间
        public int azs;//方位角状态
        public int els;//俯仰角状态
        public int cs;//控制状态
        public int ios;//限位开关触发状态
        public String tel = "-";//陀螺仪
        //保存最近三次数据
        String[] caza = {"-","-","-"};
        String[] cela = {"-","-","-"};

        //public int duse;
        //查询历史得到的数据
        public double historydata[] = new double[50];
    }

    public static class _ControlDataInfo {
        public int seq;
        public String name;
        public int type;  //0-AI/VI，1-AO/VO(不可控)，2-DI，3-DO（不可控），4-AO/VO(可控)，5-DO(可控)
        public String unit; //单位数值型 km p；开关型   正常^^^^报警  （用^^^隔开）
        public int dvalue;//实际值

        //查询历史得到的数据
        public double historydata[] = new double[50];
    }

    public _HistoryData mHistoryData = new _HistoryData();

    public static class _HistoryData {
        public Boolean bData = false; //是否查询到数据
        public int gid;
        public String gname;
        public String tid;
        public String tname;
        public int starttime; //本次查询的开始时间
        public int nseqlist; //本次查询的数据项数
        public int[] seqlist; //本次查询的数据num列表，不是seq列表，由本地保存的datalink去查询对应的seq
        public int datacount; //本次查询到的数据个数

        public int historytime[] = new int[50];
        public int historyalarm[] = new int[50];
        public int historyonline[] = new int[50];
    }
}
