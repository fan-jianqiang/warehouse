package cn.km.warehouse.utils;

import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2020/9/15
 */
public class JsonUtils {
    /**
     * 生成JSONArray
     * @param jsonStr
     * @return
     */
    public static JSONArray parseJsonArray(String jsonStr){
        JSONArray jsonObject = new JSONArray();
        if(!TextUtils.isEmpty(jsonStr)){
            try{
                int index = jsonStr.indexOf("[");
                int end   = jsonStr.lastIndexOf("]");
                jsonStr =jsonStr.substring(index,end+1);
                jsonObject = new JSONArray(jsonStr);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        return jsonObject;
    }

    /**
     * 生成JSONObject
     * @param jsonStr
     * @return
     */
    public static JSONObject parseJsonObject(String jsonStr){
        JSONObject jsonObject = null;
        if(!TextUtils.isEmpty(jsonStr)){
            try{
                int index = jsonStr.indexOf("{");
                int end   = jsonStr.lastIndexOf("}");
                jsonStr =jsonStr.substring(index,end+1);
                jsonObject = new JSONObject(jsonStr);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        return jsonObject;
    }

//    /**
//     * 生成JSONArray
//     * @param jsonStr
//     * @return
//     */
//    public static JSONArray parseJsonArray(String jsonStr){
//        JSONArray jsonObject = new JSONArray();
//        if(!TextUtils.isEmpty(jsonStr)){
//            try{
//                int index = jsonStr.indexOf("[");
//                int end   = jsonStr.lastIndexOf("]");
//                jsonStr =jsonStr.substring(index,end+1);
//                jsonObject = new JSONArray(jsonStr);
//            }catch(Exception e){
//                e.printStackTrace();
//            }
//        }
//        return jsonObject;
//    }

    /**
     * 判断JsonArray是否为空
     * @param jsonArray
     * @return
     */
    public static boolean emptyJsonArray(JSONArray jsonArray){
        return jsonArray == null || jsonArray.length() < 1;
    }

    /**
     * 获取Json的String属性
     * @param object
     * @param field
     * @param value
     */
    public static void put(JSONObject object,String field, Object value){
        if(field != null){
            try{
                object.put(field, value);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    /**
     * 将一个JSONObject对象转换为一个HashMap
     * @param jsonObj
     * @return
     */
    public static Map<String, Object> parseJsonToMap(JSONObject jsonObj){
        Map<String,Object> params = new HashMap<String, Object>();
        if(jsonObj!=null){
            Iterator<?> keys = jsonObj.keys();
            while(keys.hasNext()){
                String keyt = (String)keys.next();
                params.put(keyt,jsonObj.optString(keyt));
            }
        }
        return params;
    }

    /**
     * 将一个jsonObjStr字符串转换为一个HashMap
     */
    public static Map<String, Object> parseJsonToMap(String jsonObjStr){
        Map<String, Object> map = null;
        try {
            JSONObject jsonObj = parseJsonObject(jsonObjStr);
            map = parseJsonToMap(jsonObj);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    /**
     * 将一个JSONArray对象转换为一个List集合
     * @param jsonArr
     * @return
     */
    public static List<Map<String, Object>> parseJsonArrToMapList(JSONArray jsonArr) {
        List<Map<String, Object>> arrayList = new ArrayList<Map<String, Object>>();
        try {
            for (int i = 0; i < jsonArr.length(); i++) {
                arrayList.add(parseJsonToMap(jsonArr.getJSONObject(i)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return arrayList;
    }

    /**
     *  将一个jsonArrStr字符串转换为一个List集合
     * @param jsonArrStr
     * @return
     */
    public static List<Map<String, Object>> parseJsonArrToMapList(String jsonArrStr) {
        List<Map<String, Object>> arrayList = null;
        try {
            arrayList = parseJsonArrToMapList(JsonUtils.parseJsonArray(jsonArrStr));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return arrayList;
    }
    /**
     * 将Map集合转换成JSONOject
     * @param map
     * @return JSONObject
     */
    public static JSONObject parseMapToJSONObject(Map<String,Object> map){
        JSONObject obj = new JSONObject();
        Iterator<String> iter = map.keySet().iterator();
        while(iter.hasNext()){
            try {
                String key = iter.next();
                obj.put(key, map.get(key)!=null?map.get(key).toString():"");
            } catch (JSONException e) {

                e.printStackTrace();
            }
        }
        return obj;

    }

    /**
     * 将List 转换成JSONArray
     * @param  listMap
     * @return JSONArray
     */
    public static JSONArray parseListToJSONArray(List<Map<String, Object>> listMap) {
        JSONArray array = new JSONArray();
        for(int i = 0; i < listMap.size(); i++){
            Map<String, Object> map = listMap.get(i);
            JSONObject jsonObject = parseMapToJSONObject(map);
            array.put(jsonObject);
        }

        return array;
    }

    /**
     * 迭代JSONArray
     * @param array
     */
    public static void iterateArray(JSONArray array, ArrayIterator iterator){
        for(int i=0; i<array.length(); i++){
            JSONObject item = array.optJSONObject(i);
            iterator.iterate(i, item);
        }
    }

    /**JSONArray的迭代接口*/
    public interface ArrayIterator{
        void iterate(int index, JSONObject item);
    }
    /**
     * 合并JSONObject  第一个参数的数据会完全  覆盖到第二个。（有一样的覆盖，没有则新增）
     * @param newJson 最新json
     * @param oldJson 被合并的json
     * @return 合并之后的 jsonObj，地址是oldJson
     */
    public static JSONObject mergeJsonObj(JSONObject newJson,JSONObject oldJson,String filter){
        try {
            Iterator<String> keys = newJson.keys();
            while (keys.hasNext()) {
                String key = keys.next();
                //需要过滤的字符不参与数据合并
                if(TextUtils.isEmpty(filter) || !key.contains(filter)){
                    oldJson.put(key, newJson.get(key));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return oldJson;
    }

    public static boolean isEmpty(JSONObject json){
        if(json == null)
            return true;
        return json.length() == 0;
    }

    public static boolean isEmpty(JSONArray array){
        if(array == null)
            return true;
        return array.length() == 0;
    }

}
