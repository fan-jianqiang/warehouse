package cn.km.warehouse.utils;

import android.app.Activity;
import android.app.Dialog;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;

import java.util.Timer;

import cn.km.warehouse.R;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/12
 */
public class ProgressUtil {
    Dialog dialog = null;
    private static ProgressUtil instance = null;
    private Timer mTimer = new Timer();

    public static synchronized ProgressUtil getInstance() {

        if (instance == null) {
            instance = new ProgressUtil();
        }
        return instance;
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {

                case 1:

                    dialog.dismiss();
                    break;
            }
        }
    };

    public void ShowProgress(Activity context, boolean b, boolean cancelable) {
        if (b) {
            dialog = createLoadingDialog(context, "正在加载");
            dialog.setCanceledOnTouchOutside(cancelable);
            dialog.show();
//            mTimer.schedule(new TimerTask() {
//                                @Override
//                                public void run() {
//                                    mHandler.sendEmptyMessage(1);
//                                }
//                            }
//
//                    , 3000, 3000);


        } else {
            dialog.cancel();
            dialog.dismiss();
        }
    }


    /**
     * 得到自定义的progressDialog
     *
     * @param context
     * @param msg
     * @return
     */
    private WindowManager m ;
    private Dialog createLoadingDialog(Activity context, String msg) {

        int width = context.getResources().getDisplayMetrics().widthPixels;
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.list_adapter_loading_view, null);// 得到加载view
        Dialog loadingDialog = new Dialog(context, R.style.alert_dialog1);// 创建自定义样式dialog
//        Window window = context.getWindow();
//        m = window.getWindowManager();
//        WindowManager.LayoutParams lp = window.getAttributes();
//        //设置背景透明度 背景透明
//        lp.alpha = 0.9f;//参数为0到1之间。0表示完全透明，1就是不透明。按需求调整参数
//        window.setAttributes(lp);
        loadingDialog.setContentView(v, new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.FILL_PARENT,
                LinearLayout.LayoutParams.FILL_PARENT));// 设置布局
        return loadingDialog;
    }

}
