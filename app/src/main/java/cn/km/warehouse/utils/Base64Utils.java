package cn.km.warehouse.utils;

import android.util.Base64;

public class Base64Utils {

    /**
     * BASE64字符串解码为二进制数据
     *
     * @throws Exception
     */
    public static byte[] decode(String base64) throws Exception {
        return Base64.decode(base64.getBytes(),Base64.DEFAULT);
    }

    /**
     * 二进制数据编码为BASE64字符串
     *
     * @throws Exception
     */
    public static String encode(byte[] bytes) throws Exception {
        return new String(Base64.encode(bytes,Base64.DEFAULT));
    }

    // 加密
    public static String getBase64(String str) {
        return Base64.encodeToString(str.getBytes(), android.util.Base64.DEFAULT);
    }

    // 解密
    public static String getFromBase64(String s) {
        return new String(Base64.decode(s.getBytes(), android.util.Base64.DEFAULT));
    }
}
