package cn.km.warehouse.utils;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2020/10/13
 */
public class DataUtil {
    public static  String GetValueFormBody(String strBody, String strKey) {
        //String strBody = subBytes(recvBuff,41,blen - 41).toString();
        int p1 = -1;
        int p2 = -1;
        p1 = strBody.indexOf(strKey);
        if (p1 >= 0) {
            p2 = strBody.indexOf("&", p1);
            if (p2 <= 0)
                p2 = strBody.indexOf(";", p1);
        }
        if (p2 >= 0 && p2 > p1)
            return strBody.substring(p1 + strKey.length(), p2);
        else
            return null;
    }
    public static String countNum(String str, int addnum) {

        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(str));
            c.add(Calendar.MONTH, addnum);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return sdf.format(c.getTime());
    }
    /**
     * 将整型数字转换为二进制字符串，一共32位，不舍弃前面的0
     *
     * @param strnumber 整型数字
     * @return 二进制字符串
     */
    public static String get32BitBinString(String strnumber) {
        if (strnumber.equals("")) {
            return "00000000000000000000000000000000";
        }
        int number = Integer.parseInt(strnumber);
        StringBuilder sBuilder = new StringBuilder();
        for (int i = 0; i < 32; i++) {
            sBuilder.append(number & 1);
            number = number >>> 1;
        }
        return sBuilder.reverse().toString();
    }
    public static String getCmd(String bf) {
        String cmd = bf.substring(14, 18);
        String retstr;
        switch (cmd) {
            case "1201":
                retstr = "初始化";
                break;
            case "1203":
                if (bf.contains("tmy=0&tmm=0&tmd=0&tmh=0&tmmi=0&tms=0")){
                    retstr = "固定指向";
                }else {
                    retstr = "固定指向(定时)";
                }
                break;
            case "1205":
                retstr = "角度跟踪";
                break;
            case "1207":
                if (bf.contains("tmy=0&tmm=0&tmd=0&tmh=0&tmmi=0&tms=0")){
                    retstr = "固定指向";
                }else {
                    retstr = "固定指向(定时)";
                }
                break;
            case "1209":
                retstr = "角度跟踪";
                break;
            case "1211":
                retstr = "急停";
                break;
            case "1213":
                retstr = "标定";
                break;
            case "1215":
                retstr = "时间校正";
                break;
            case "1217":
                retstr = "删除定时任务";
                break;
            case "1301":
                retstr = "控制指令";
                break;
            default:
                retstr = "默认";
        }

        return retstr;
    }
    public  static  String strToDate(String as) {
        if (as.equals("0")) {
            return "/";
        }
        long l = Long.valueOf(as + "000");
        Date date = new Date(l);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(date);
    }
    public static boolean checkDel(String bf) {
        String temp = bf.substring(14, 18);
        String strTime = "tmy=0&tmm=0&tmd=0&tmh=0&tmmi=0&tms=0";
        try {
            if (temp.equals("1205") || temp.equals("1209")||(temp.equals("1203")&&!bf.contains(strTime))||((temp.equals("1207")&&!bf.contains(strTime)))) {
                StringBuffer sb = new StringBuffer();
                sb.append(GetValueFormBody(bf, "tmy="));
                sb.append(GetValueFormBody(bf, "tmm="));
                sb.append(GetValueFormBody(bf, "tmd="));
                sb.append(GetValueFormBody(bf, "tmh="));
                sb.append(GetValueFormBody(bf, "tmmi="));
                sb.append(GetValueFormBody(bf, "tms="));
                DateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
                Date date = format.parse("20" + sb.toString());
                if (date.getTime() > System.currentTimeMillis()) {
                    return true;
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }
    public static String GetIntToStre(String string) {
        int temp = Integer.parseInt(string);
        DecimalFormat df = new DecimalFormat("0.00");//格式化小数
        String num;
        if(temp>32768){
            num = df.format((float)(temp-65535)*360/65535);//返回的是String类型
        }else {
            num = df.format((float)temp*360/65535);//返回的是String类型
        }
        return num;
    }
    public static String GetIntToStr(String string) {
        String num;
        if(!string.equals("65535")) {
            int temp = Integer.parseInt(string);
            DecimalFormat df = new DecimalFormat("0.00");//格式化小数
            num = df.format((float) temp * 360 / 65535);//返回的是String类型
        }else {
            num = "0";
        }
        return num;
    }
    public static String getTime(String bf) {
        try {
            StringBuffer sb = new StringBuffer();
            sb.append(GetTimeFormBody(bf, "tmy="));
            sb.append(GetTimeFormBody(bf, "tmm="));
            sb.append(GetTimeFormBody(bf, "tmd="));
            sb.append(GetTimeFormBody(bf, "tmh="));
            sb.append(GetTimeFormBody(bf, "tmmi="));
            sb.append(GetTimeFormBody(bf, "tms="));
            DateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
            Date date = format.parse("20" + sb.toString());
            //DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return format1.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static String GetTimeFormBody(String strBody, String strKey) {
        String str = null;
        int p1 = -1;
        int p2 = -1;
        p1 = strBody.indexOf(strKey);
        if (p1 >= 0) {
            p2 = strBody.indexOf("&", p1);
            if (p2 <= 0)
                p2 = strBody.indexOf(";", p1);
        }
        if (p2 >= 0 && p2 > p1) {
            str = strBody.substring(p1 + strKey.length(), p2);
            if (str.length() < 2)
                return "0" + str;
            else
                return str;
        } else {
            return null;
        }
    }
//    public static  String strToDate(String as) {
//        if (as.equals("0")) {
//            return "/";
//        }
//        long l = Long.valueOf(as + "000");
//        Date date = new Date(l);
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        return sdf.format(date);
//    }
}
