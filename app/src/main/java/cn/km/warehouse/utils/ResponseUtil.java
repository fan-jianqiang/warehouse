package cn.km.warehouse.utils;

import android.text.TextUtils;

import java.util.Arrays;
import java.util.List;

public class ResponseUtil {
    public static final int REQUEST_SUCCESS = 10086;
    public static final int REQUEST_FAIL = 10010;

    public static int getResultCode(String responseBody) {
        String resultCode = responseBody.substring(responseBody.indexOf("rt=") + 3, responseBody.indexOf("rt=") + 4);
        if (!TextUtils.isEmpty(resultCode) && TextUtils.equals(resultCode, "0")) {
            return REQUEST_SUCCESS;
        } else {
            return REQUEST_FAIL;
        }
    }

    public static String getRt(String responseBody) {
        if (TextUtils.isEmpty(responseBody) || !responseBody.contains("rt=")) {
            return null;
        }
        return responseBody.substring(responseBody.indexOf("rt=") + 3, responseBody.indexOf("rt=") + 4);
    }

    public static String getParameter(String name, String responseBody) {
        if (TextUtils.isEmpty(responseBody)) {
            return null;
        }
        int beginIndex = responseBody.indexOf(name) + name.length();
        return responseBody.substring(beginIndex, beginIndex + 1);
    }

    public static List<String> getParameterList(String regex, String content) {
        if (TextUtils.isEmpty(content)) {
            return null;
        }
        String[] ids;
        if (content.contains(regex)) {
            ids = content.split(regex);
        } else {
            ids = new String[]{content};
        }
        return Arrays.asList(ids);
    }
}
