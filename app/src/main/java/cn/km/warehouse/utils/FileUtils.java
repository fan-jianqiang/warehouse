package cn.km.warehouse.utils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2020/9/15
 */
public class FileUtils {
    public static String readFile(InputStream fileStream, String charsetName){
        StringBuffer buffer = new StringBuffer();
        BufferedReader reader = null;
        try{
            reader = new BufferedReader(new InputStreamReader(fileStream, charsetName));
            String temp = "";
            while ((temp = reader.readLine()) != null) {
                buffer.append(temp);
            }
        }catch (Exception e) {
            e.printStackTrace();
        }finally{
            try {
                reader.close();
            } catch (Exception fe) {
                fe.printStackTrace();
            }
        }
        return buffer.toString();
    }
}
