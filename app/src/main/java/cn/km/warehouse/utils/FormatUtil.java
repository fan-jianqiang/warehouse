package cn.km.warehouse.utils;

import android.text.TextUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FormatUtil {

    public static boolean isRightEmailAddress(String text) {
        if (TextUtils.isEmpty(text)) return false;
        String regEx1 = "^([a-z0-9A-Z_]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
        Pattern p;
        Matcher m;
        p = Pattern.compile(regEx1);
        m = p.matcher(text);
        if (m.matches()) return true;
        else return false;
    }
}
