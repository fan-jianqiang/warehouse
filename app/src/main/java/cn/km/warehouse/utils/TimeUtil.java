package cn.km.warehouse.utils;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/13
 */
public class TimeUtil {
    private static long curTime=0;
    public static boolean check(long  prevTime){
        curTime=System.currentTimeMillis();
        if (prevTime>0 && ( (curTime-prevTime)>10*60*1000 )){
            prevTime=curTime;
            return true;
        }else{
            prevTime=curTime;
            return false;
        }
    }
}
