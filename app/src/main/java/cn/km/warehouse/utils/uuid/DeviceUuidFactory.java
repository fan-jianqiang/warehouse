package cn.km.warehouse.utils.uuid;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Environment;
import android.provider.Settings;
import android.text.TextUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.UUID;

import cn.km.warehouse.utils.SPUtils;


/**
 * 获取uuid,uuid是根据时间变化，首次获取到的uuid保存在".dev_id.txt"文件以及sp里，之后从sp或者文件里拿
 * @author mo
 */
public class DeviceUuidFactory {

	protected static final String PREFS_FILE = "dev_id.xml";
	protected static final String DEVICE_UUID_FILE_NAME = ".dev_id.txt";
	protected static final String PREFS_DEVICE_ID = "dev_id";
	protected static final String KEY = "cyril'98";
	protected static final String ANDROID_ID = "9774d56d682e549c";
	protected static UUID uuid;
	protected String deviceId;

	public static String getDeviceId(Context context){
        String deviceId = SPUtils.getDeviceId();
		String deviceIdSd = recoverUuidFromSd();
		if (TextUtils.isEmpty(deviceId)) {
			if (context!=null){
				@SuppressLint("HardwareIds") final String androidId = Settings.Secure
						.getString(context.getContentResolver(),
								Settings.Secure.ANDROID_ID);
				if (!ANDROID_ID.equals(androidId) && !TextUtils.isEmpty(androidId)) {
					deviceId = androidId;
				}else {
					if (TextUtils.isEmpty(deviceIdSd)) {
						deviceId = deviceIdSd;
					}else {
						deviceId = UUID.randomUUID().toString();
					}
				}
			}else {
				if (TextUtils.isEmpty(deviceIdSd)) {
					deviceId = deviceIdSd;
				}else {
					deviceId = UUID.randomUUID().toString();
				}
			}
			SPUtils.putDeviceId( deviceId);
			saveUuidToSd(deviceId);
		}
		return deviceId;
    }

	private static String recoverUuidFromSd() {
		try {
			String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath();
			File dir = new File(dirPath);
			File uuidFile = new File(dir, DEVICE_UUID_FILE_NAME);
			if (!dir.exists() || !uuidFile.exists()) {
				return null;
			}
			FileReader fileReader = new FileReader(uuidFile);
			StringBuilder sb = new StringBuilder();
			char[] buffer = new char[100];
			int readCount;
			while ((readCount = fileReader.read(buffer)) > 0) {
				sb.append(buffer, 0, readCount);
			}
			//通过UUID.fromString来检查uuid的格式正确性
//			UUID uuid = UUID.fromString(EncryptUtils.decryptDES(sb.toString(), KEY));
			return sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private static void saveUuidToSd(String uuid) {
		String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath();
		File targetFile = new File(dirPath, DEVICE_UUID_FILE_NAME);
		if (!targetFile.exists()) {
			OutputStreamWriter osw;
			try {
				osw = new OutputStreamWriter(new FileOutputStream(targetFile), "utf-8");
				try {
					osw.write(uuid);
					osw.flush();
					osw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		else {
			targetFile.delete();
		}
	}
}
