package cn.km.warehouse.utils;

import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class UrlUtils {
    /**
     * @param urlStr    url字符串。
     * @param paramName 指定参数名称。
     * @return NA.
     * @desc 获取url中指定参数的值。
     */
    public static String readValueFromUrlStrByParamName(String urlStr, String paramName) {
        if (urlStr != null && urlStr.length() > 0) {
            int index = urlStr.indexOf("?");
            String temp = (index >= 0 ? urlStr.substring(index + 1) : urlStr);
            String[] keyValue = temp.split("&");
            String destPrefix = paramName + "=";
            for (String str : keyValue) {
                if (str.indexOf(destPrefix) == 0) {
                    return str.substring(destPrefix.length());
                }
            }
        }
        return null;
    }


    /**
     * @param urlStr url字符串。
     * @return NA.
     * @desc 获取url中指定参数的值。
     */
    public static String getJsonStrByUrl(String urlStr) {
        if (urlStr != null && urlStr.length() > 0) {
            int index = urlStr.indexOf("?");
            String temp = (index >= 0 ? urlStr.substring(index + 1) : urlStr);
            String[] keyValue = temp.split("&");
            Map<String, String> map = new HashMap<>();
            Map<String, String> mapList = new HashMap<>();
            for (String str : keyValue) {
                String key = str.substring(0, str.indexOf("="));
                if (TextUtils.isEmpty(key)) {
                    continue;
                }
                String value = str.substring(str.indexOf("=") + 1, str.length());
                if (value.contains("~")) {
                    mapList.put(key, value);
                } else {
                    map.put(key, value);
                }
            }


            JSONObject jsonObject = new JSONObject();
            try {
                //遍历map中的键
                for (String key : map.keySet()) {
                    jsonObject.put(key, map.get(key));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                if (mapList.size() > 0) {
                    String firstValue = null;
                    for (String value : mapList.values()) {
                        if (value != null) {
                            firstValue = value;
                        }
                        break;
                    }
                    JSONArray jsonArray = new JSONArray();
                    if (firstValue != null) {
                        String[] values = firstValue.split("~");
                        for (int i = 0; i < values.length; i++) {
                            JSONObject object = new JSONObject();
                            for (String key : mapList.keySet()) {
                                if (mapList.get(key).length()>=2){
                                    String[] valueArray = mapList.get(key).split("~");
                                    object.put(key, valueArray[i]);
                                }else {
                                    object.put(key, "");
                                }
                            }
                            jsonArray.put(object);
                        }
                        jsonObject.put("lisBean", jsonArray);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return jsonObject.toString();
        }
        return null;
    }


}
