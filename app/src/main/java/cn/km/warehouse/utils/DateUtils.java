package cn.km.warehouse.utils;

import android.text.TextUtils;
import android.util.SparseArray;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2020/9/15
 */
public class DateUtils {

    /** yyyy-MM-dd HH:mm*/
    public static final String FORMAT_DATE_TIME_M 	= "yyyy-MM-dd HH:mm";
    /** yyyy-MM-dd HH:mm:ss*/
    public static final String FORMAT_DATE_TIME_S 	= "yyyy-MM-dd HH:mm:ss";
    /** yyyy-MM-dd*/
    public static final String FORMAT_DATE_DASH 		= "yyyy-MM-dd";
    /** yyyy/MM/dd*/
    public static final String FORMAT_DATE_SLASH  	= "yyyy/MM/dd";
    /** yyyyMMdd*/
    public static final String FORMAT_DATE_NONE 		= "yyyyMMdd";
    /** HH:mm*/
    public static final String FORMAT_TIME_HM			= "HH:mm";
    /** HH:mm:ss*/
    public static final String FORMAT_TIME_HMS  		= "HH:mm:ss";

    public static final int SECONDS_OF_DAY = 60 * 60 * 24;

    public static final long MILLIS_OF_DAY = 1000L * SECONDS_OF_DAY;

    /**
     * 判断给定日期是否为周末
     * @param d    日期
     * @return  返回结果
     */
    public static boolean isWeekendDay(Date d){
        if(d == null)
            return false;
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        return isWeekendDay(c);
    }

    /**
     * 判断给定日期是否为周末
     * @param c    日历
     * @return  返回结果
     */
    public static boolean isWeekendDay(Calendar c){
        boolean result = false;
        int dayCode = c.get(Calendar.DAY_OF_WEEK);
        if((dayCode == Calendar.SATURDAY) || (dayCode == Calendar.SUNDAY))
            result = true;
        return result;
    }

    /**根据字符串长度，自动使用与之匹配的格式进行转换*/
    public static Date parseDate(String dateStr){
        if(TextUtils.isEmpty(dateStr)){
            return new Date();
        }

        String dateFormat;
        int strLength = dateStr.trim().length();
        if (FORMAT_DATE_TIME_S.length() == strLength) {
            dateFormat = FORMAT_DATE_TIME_S;
        } else if (FORMAT_DATE_TIME_M.length() == strLength) {
            dateFormat = FORMAT_DATE_TIME_M;
        } else if (FORMAT_DATE_DASH.length() == strLength) {
            dateFormat = FORMAT_DATE_DASH;
        } else {
            dateFormat = FORMAT_DATE_DASH;
        }

        return parseDate(dateStr,dateFormat);
    }

    /**
     * @param dateStr example "2013-01-01 00:00:00"
     * @param format example "yyyy-MM-dd hh:mm:ss"
     * @return
     */
    public static Date parseDate(String dateStr,String format){
        try{
            if(!TextUtils.isEmpty(dateStr)){
                SimpleDateFormat simpleDate = new SimpleDateFormat(format, Locale.getDefault());
                return simpleDate.parse(dateStr);
            }else{
                return new Date();
            }
        }catch(Exception e){
            e.printStackTrace();
            return new Date();
        }
    }

    public static String formatDate(Date date,String format){
        SimpleDateFormat simpleDate = new SimpleDateFormat(format, Locale.getDefault());
        return simpleDate.format(date);
    }

    public static String formatDate(String dateStr,String oldformat,String format){
        Date date = parseDate(dateStr, oldformat);
        return formatDate(date, format);
    }

    public static String formatMilliSeconds(long milliSecondsTime, String format){
        SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.getDefault());
        return dateFormat.format(milliSecondsTime);
    }

    /**
     * 验证前后两个日期的合理性
     * @return
     */
    public static boolean validDateDationality(String start, String end){
        boolean result = false;
        if(!TextUtils.isEmpty(start) && !TextUtils.isEmpty(end)){
            Date startDate 	= parseDate(start + (start.contains(":") ? "":" 00:00"), FORMAT_DATE_TIME_M);
            Date endDate 	= parseDate(end + (end.contains(":") ? "":" 23:59"), FORMAT_DATE_TIME_M);
            if(startDate.before(endDate))
                result = true;
        }else{
            result = true;
        }
        return result;
    }

    /**
     * 比较两个日期之间的天数差
     * @param first
     * @param another
     * @return
     * modify by Siyi Lu, 2014-01-16, 修改比较两个日期时只计算日期之间的差
     */
    public static int getIntervalDays(String first, String another){
        Date firstDate = parseDate(first, FORMAT_DATE_DASH);
        Date anotherDate = parseDate(another, FORMAT_DATE_DASH);
        return getIntervalDays(firstDate, anotherDate);
    }

    /**
     * 比较两个日期之间的天数差
     * @param first
     * @param another
     * @return
     * modify by Siyi Lu,2014-01-16, 修改比较两个日期时只计算日期之间的差
     */
    public static int getIntervalDays(Date first, Date another){
        Calendar c = Calendar.getInstance();
        c.setTime(first);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        long firstM = c.getTimeInMillis();
        c.setTime(another);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        long secondM = c.getTimeInMillis();
        double intervalM = Math.abs(firstM - secondM);
        long dayNum = Math.round(intervalM/(1000*60*60*24));
        return (int)dayNum;
    }

    /**
     * 生成结合日期的UUID
     * @return
     */
    public static String generateSerialNum(){
        String timeStr = DateUtils.formatDate(new Date(), "yyyyMMddHHmm");
        return timeStr + UUID.randomUUID().toString();
    }

    /**
     * 是否闰年
     * @param year 年份
     * @return
     */
    public static boolean isLeapYear(int year){
        boolean isLeapYear = false;
        if(year % 4 == 0 && year % 100 > 0){
            isLeapYear = true;
        }
        if(year % 100 == 0 && year % 400 == 0){
            isLeapYear = true;
        }
        return isLeapYear;
    }

    /**
     * 获取距离指定日期之前的日期
     * @param date 传null，使用当前日期
     * @param year 几年前
     * @param month 几月前
     * @param day 几天前
     * @return
     */
    public static Date getDayBefore(Date date,int year,int month,int day){
        Calendar calendar = Calendar.getInstance();
        if(date != null){
            calendar.setTime(date);
        }
        calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH)-day);
        calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH)-month);
        calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR)-year);
        return calendar.getTime();
    }

    /**
     * 获取距离指定日期之前的日期
     * @param date 传null，使用当前日期
     * @param year 几年前
     * @param month 几月前
     * @param day 几天前
     * @param day 几小时前
     * @param day 几分钟前
     * @return
     */
    public static Date getDateBefore(Date date,int year,int month,int day,int hour,int minute){
        Calendar calendar = Calendar.getInstance();
        if(date != null){
            calendar.setTime(date);
        }
        calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH)-day);
        calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH)-month);
        calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR)-year);
        calendar.set(Calendar.HOUR, calendar.get(Calendar.HOUR)-hour);
        calendar.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE)-minute);
        return calendar.getTime();
    }

    public static String getDayDescription(Date date){
        String result = null;
        String[] descs = {"今天","昨天","#天","3天#","4天#","5天#","6天#"};
        Date curDate = new Date();
        int intervalDays = getIntervalDays(date, curDate);
        if(intervalDays >60){
            result = "两个月#";
        }else if(intervalDays >30){
            result = "一个月#";
        }else if(intervalDays > 14){
            result = "两周#";
        }else if(intervalDays > 7){
            result = "一周#";
        }else{
            descs[1] = curDate.after(date) ? "昨天" : "明天";
            result = descs[intervalDays];
        }
        result = result.replace("#", curDate.after(date) ? "前" : "后");
        return result;
    }

    /**
     * 解析时间的描述语句
     * @param cd
     * @return
     */
    public static String parseTimeDesc(Date cd){
        Calendar c = Calendar.getInstance();
        long curMillSec = c.getTimeInMillis();
        SparseArray<Object[]> descs = new SparseArray<Object[]>();
        descs.put(60, new Object[]{"刚刚",60});
        descs.put(60*60, new Object[]{"%d分钟前", 60});
        descs.put(60*60*24, new Object[]{"%d小时前", 60*60});
        descs.put(60*60*24*4, new Object[]{"%d天前", 60*60*24});
        c.setTime(cd);
        long cc = c.getTimeInMillis();
        long diffSec = (curMillSec - cc)/1000;
        String desc = "";
        int num = -1;
        for(int i=0; i<descs.size(); i++){
            int sec = descs.keyAt(i);
            if(diffSec < sec){
                Object[] value = descs.valueAt(i);
                desc = (String)value[0];
                int dividerFactor = Integer.valueOf(value[1].toString());
                num = (int)(diffSec/(dividerFactor));
                break;
            }
        }
        if(!TextUtils.isEmpty(desc) || desc.contains("%d")){
            desc = String.format(desc, num);
        }else{
            desc = DateUtils.formatDate(cd, DateUtils.FORMAT_DATE_DASH);
        }
        return desc;
    }

    /**
     * 处理时间
     * @param time
     * @return
     */
    public static String handTime(String time) {
        if (time == null || "".equals(time.trim())) {
            return "";
        }
        try {
            Date date = parseDate(time, FORMAT_DATE_TIME_S);
            long tm = System.currentTimeMillis();// 当前时间戳
            long tm2 = date.getTime();// 发表动态的时间戳
            long d = (tm - tm2) / 1000;// 时间差距 单位秒
            if ((d / (60 * 60 * 24)) > 0) {
                return d / (60 * 60 * 24) + "天前";
            } else if ((d / (60 * 60)) > 0) {
                return d / (60 * 60) + "小时前";
            } else if ((d / 60) > 0) {
                return d / 60 + "分钟前";
            } else {
                // return d + "秒前";
                return "刚刚";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 返回一个主观时间（概念）
     * <br>跟当前时间对比，年份不同返回年月日，月份或日期不同返回月日，年月日都相同返回时间。
     * <br>可用于列表等展示时间时，显示一个短日期，而非一长串日期
     * 	年月日（如2015年10月22日）&nbsp;&nbsp;跟当前时间对比，年份不同
     * <br>月日（如10月22日）&nbsp;&nbsp;月份或日期不同
     * <br>时间（如20:48）&nbsp;&nbsp;年月日都相同
     * @param timeInMillis
     * @return
     */
    public static String subjectiveTime(long timeInMillis){
        Calendar now = new GregorianCalendar();
        Calendar cal = new GregorianCalendar();
        cal.setTimeInMillis(timeInMillis);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        if(year != now.get(Calendar.YEAR)){
            return year + "年" + (month + 1) + "月" + day + "日";
        }else{
            if(month == now.get(Calendar.MONTH) && day == now.get(Calendar.DAY_OF_MONTH)){
                int minute = cal.get(Calendar.MINUTE);
                return cal.get(Calendar.HOUR_OF_DAY) + ":" + (minute < 10 ? "0" : "") + minute;
            }else{
                return (month + 1) + "月" + day + "日";
            }
        }
    }

    /**
     * 判断是否同一天
     *
     * 数值计算比文本对比效率更高
     */
    public static boolean isTheSameDay(Date d1, Date d2){
        return Math.abs(d1.getTime() - d2.getTime()) < MILLIS_OF_DAY
                && millis2Day(d1.getTime()) == millis2Day(d2.getTime());
    }

    private static long millis2Day(long millis) {
        // 本地时间(毫秒)=获取到的时间(毫秒)+与UTC对比的时间差(毫秒)
        // 除以MILLIS_OF_DAY得到的是自1900起的天数
        return (millis + TimeZone.getDefault().getOffset(millis)) / MILLIS_OF_DAY;
    }
}
