package cn.km.warehouse.utils;

import java.util.concurrent.CountDownLatch;

public class EndTask implements Runnable{

    private CountDownLatch endTaskLatch;

    public EndTask(CountDownLatch latch) {
        this.endTaskLatch =latch;
    }

    public void run() {
        try {
            endTaskLatch.await();
            System.out.println("开始执行最终任务");
            System.out.println(endTaskLatch.getCount());
        }catch(Exception e) {
            e.printStackTrace();

        }
    }
}
