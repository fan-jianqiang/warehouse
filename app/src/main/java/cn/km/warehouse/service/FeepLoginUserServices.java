package cn.km.warehouse.service;
/**
 * @author ZYP
 * @since 2017-02-25 19:30
 */
public class FeepLoginUserServices implements ILoginUserServices {

	private String mUserId;             // 用户 id
	private String mUserName;           // 用户名称
	private String mImageHref;          // 用户头像 /imageHref/userId/xxx
	private String mServerAddress;      // 服务器地址 http://10.62.1.61:8089

	public void setUserId(String userId) {
		this.mUserId = userId;
	}

	public void setmUserName(String name){
		this.mUserName = name;
	}


	@Override
	public String getUserId() {
		return mUserId;
	}

	@Override
	public String getUserName() {
		return mUserName;
	}
}
