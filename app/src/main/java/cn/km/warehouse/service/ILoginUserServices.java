package cn.km.warehouse.service;


/**
 * @author ZYP
 * @since 2017-02-25 13:32
 * 为子模块准备的获取当前登录用户基本信息的接口，包括服务器地址，基本用户信息。
 */
public interface ILoginUserServices {

	/**
	 * 获取当前用户 id
	 */
	String getUserId();

	/**
	 * 获取当前用户名称
	 */
	String getUserName();


}
