package cn.km.warehouse.service;

/**
 * @author MFW
 * @since 2020-06-07 09:08
 * 项目路径(本地和网络)协议，用于各子模块获取项目路径
 */
public interface IPathServices {

	/**
	 * 根目录目录 /youAlbums
	 */
	String getBasePath();

	/**
	 * 所有相册根目录 /youAlbums/ALBUMS
	 */
	String getAlbumDirPath();

	/**
	 * 某个相册目录 /youAlbums/ALBUMS/name
	 */
	String getAlbumPath(String name);

	/**
	 * 某个相册目录 /youAlbums/autoCamera
	 */
	String getAutoCameraPath();

	/**
	 * 根缓存目录 /youAlbums/FILETEMP
	 */
	String getBaseTempPath();

	/**
	 * 用户目录 /youAlbums/userId
	 */
	String getUserPath();

	/**
	 * 用户日志文件 /youAlbums/userId/log
	 */
	String getLogPath();

	/**
	 * 存放下载文件目录 /youAlbums/userId/FILETEMP
	 */
	String getTempFilePath();

	/**
	 * 获取多媒体文件
	 */
	String getMediaPath();

	/**
	 * crash文件地址
	 */
	String getCrashPath();

	/**
	 * 存放拍照上传的缓存文件目录 /feep/userId/photocache
	 */
	String getImageCachePath();

}
