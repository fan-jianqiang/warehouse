package cn.km.warehouse.service;

import android.os.Environment;
import android.text.TextUtils;

import java.io.File;

/**
 * @author MFW
 * @since 2020-06-07 10:24
 */
public class FeepPathServices implements IPathServices {

	private static final String BASE_DIR = Environment.getExternalStorageDirectory() + File.separator + "youAlbums" + File.separator + ".nomedia";
	private static final String BASE_TEMP_DIR = BASE_DIR + File.separator + "FILETEMP";

	private String mUserId = "nobody";
	private String mAlbumDirPath;
	private String mAlbumPath;
	private String mUserPath;                           // /photoAlbums/userId
	private String mUserTempPath;                       // /photoAlbums/userId/FILETEMP
	private String mUserTempDirPath;                    // /photoAlbums/userId/TEMPDir
	private String mAutoCameraDirPath;                    // /photoAlbums/userId/AUTO_CAMERA

	private String mLogPath;                            // /photoAlbums/userId/log
	private String mUserSafePath;                       // /photoAlbums/userId/SAFEFILE
	private String mUserAddressBookPath;                // /photoAlbums/userId/addressbook
	private String mUserImageCachePath;                 // /photoAlbums/userId/photocache
	private String mUserKnowledgeCachePath;             // /photoAlbums/userId/knowledgeCache

	private String mUserKeyStoreDirPath;                // /photoAlbums/KEYSTORE
	private String mUserkeyStoreFilePath;               // /photoAlbums/KEYSTORE/FEkey.keystore
	private String mUserGroupKeyPath;                   ///photoAlbums/synthesis/groupId.jpg
	private String mUserMediaPath;                      ///photoAlbums/userId/media
	private String mUserCommonPath;                     //photoAlbums/userId/common
	private String mSlateTempPath;                      //photoAlbums/TEMP
	private String mCrashPath;                          //photoAlbums/userId/crash

	public void setUserId(String userId) {
		this.mUserId = userId;
		this.mUserPath = BASE_DIR + File.separator + mUserId;
		this.mAlbumDirPath = mUserPath + File.separator + "ALBUM";
		this.mUserTempPath = mUserPath + File.separator + "FILETEMP";
		this.mUserTempDirPath = mUserPath + File.separator + "TEMPDir";
		this.mAutoCameraDirPath = mUserPath + File.separator + "AUTO_CAMERA";
		this.mCrashPath = mUserPath + File.separator + "crash";

		this.mLogPath = mUserPath + File.separator + "log";
		this.mUserSafePath = mUserPath + File.separator + "SAFEFILE";
		this.mUserAddressBookPath = mUserPath + File.separator + "addressbook";
		this.mUserImageCachePath = mUserPath + File.separator + "photocache";
		this.mUserKnowledgeCachePath = mUserPath + File.separator + "knowledgeCache";

		this.mUserKeyStoreDirPath = BASE_DIR + File.separator + "KEYSTORE";
		this.mUserkeyStoreFilePath = mUserKeyStoreDirPath + File.separator + "FEkey.keystore";
		this.mUserGroupKeyPath = mUserPath + File.separator + "synthesis";
		this.mUserMediaPath = mUserPath + File.separator + "media";
		this.mUserCommonPath = mUserPath + File.separator + "common";
		this.mSlateTempPath = mUserPath + File.separator + "TEMP";

	}

	@Override
    public String getBasePath() {
		return BASE_DIR;
	}

	@Override
	public String getAlbumDirPath() {
		return mAlbumDirPath;
	}

	@Override
	public String getAlbumPath(String name) {
		if (!TextUtils.isEmpty(name)){
			return mAlbumDirPath + File.separator +  name;
		}else {
			return mAlbumDirPath;
		}

	}

	@Override
	public String getAutoCameraPath() {
		return mAutoCameraDirPath;
	}

	@Override
    public String getBaseTempPath() {
		return BASE_TEMP_DIR;
	}

	@Override
    public String getUserPath() {
		return mUserPath;
	}

	@Override
    public String getLogPath() {
		return mLogPath;
	}

	@Override
    public String getTempFilePath() {
		return mUserTempPath;
	}


	@Override
    public String getMediaPath() {
		return mUserMediaPath;
	}

	@Override
	public String getCrashPath() {
		return mCrashPath;
	}

	@Override
	public String getImageCachePath() {
		return mUserImageCachePath;
	}

}
