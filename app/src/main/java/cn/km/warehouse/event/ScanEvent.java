package cn.km.warehouse.event;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/11
 */
public class ScanEvent {
     private String rfid;
     private String epc;

    public ScanEvent(String rfid,String epc) {
        this.rfid = rfid;
        this.epc=epc;
    }

    public String getEpc() {
        return epc;
    }

    public void setEpc(String epc) {
        this.epc = epc;
    }

    public String getRfid() {
        return rfid;
    }

    public void setRfid(String rfid) {
        this.rfid = rfid;
    }
}
