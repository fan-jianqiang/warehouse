package cn.km.warehouse.base;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import androidx.multidex.MultiDexApplication;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/1/31
 */
public class BaseApplication extends MultiDexApplication implements Application.ActivityLifecycleCallbacks{

    private static final String TAG = "SuperviseApplication.java";

    public static final String APP_DB_NAME = "supervise.db";
//	public static final String APP_DB_NAME = "areainfo.db";

    //  private ApplicationLike tinkerApplicationLike;

    private static final String TINKER_TAG = "Tinker.SampleApp";

    private Activity mKeepliveInstance;

    /*当前对象的静态实例*/
    public static BaseApplication instance;

    /*当前显示的Activity*/
    private Activity activity;

    public static BaseApplication instance() {
        return BaseApplication.instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        //  API.setBaseUrl(BuildConfig.API_BASE_URL);
        initLifeCallBack();
        //  x.Ext.init(this);
        System.out.println(TAG + ":onCreate()");
        //   PgyCrashManager.register(this);
        //   mCrashHandler = CrashHandler.getInstance();
//		mCrashHandler.init(this);
        //SystemConfig.getInstance().initWithContext(this);
      //  DbHelper.initContext(this, APP_DB_NAME);
        // NetStateChangeReceiver.registerReceiver(this);

    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        //  NetStateChangeReceiver.unregisterReceiver(this);
    }

    private void initLifeCallBack() {
        registerActivityLifecycleCallbacks(this);
    }

    public void setKeepliveActivity(Activity keepliveActivity){
        mKeepliveInstance = keepliveActivity;
    }

    public  void removeKeepliveActivity(){
        mKeepliveInstance = null;
    }

    public  Activity getKeepliveActivity(){
        return mKeepliveInstance;
    }


    /**
     * 显示View
     *
     * @param view 需要显示到Activity的视图
     */
    public void showView(View view) {
        /*Activity不为空并且没有被释放掉*/
        if (this.activity != null && !this.activity.isFinishing()) {
            /*获取Activity顶层视图,并添加自定义View*/
            ((ViewGroup) this.activity.getWindow().getDecorView()).addView(view);
        }
    }

    /**
     * 隐藏View
     *
     * @param view 需要从Activity中移除的视图
     */
    public void hideView(View view) {
        /*Activity不为空并且没有被释放掉*/
        if (this.activity != null && !this.activity.isFinishing()) {
            /*获取Activity顶层视图*/
            ViewGroup root = ((ViewGroup) this.activity.getWindow().getDecorView());
            /*如果Activity中存在View对象则删除*/
            if (root.indexOfChild(view) != -1) {
                /*从顶层视图中删除*/
                root.removeView(view);
            }
        }
    }


    private static int startCount;

    /**
     *
     * @return  true  处于后台   false  前台
     */
    public static boolean isAppBackground() {
        if (startCount == 0) {
            return true;
        }
        return false;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
    }

    @Override
    public void onActivityStarted(Activity activity) {
        startCount++;
    }

    @Override
    public void onActivityResumed(Activity activity) {
        /*获取当前显示的Activity*/
        this.activity = activity;
        setKeepliveActivity(activity);
    }

    @Override
    public void onActivityPaused(Activity activity) {
    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        startCount--;
    }

}


