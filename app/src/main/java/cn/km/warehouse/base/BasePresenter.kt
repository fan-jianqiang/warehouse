package com.drcnet.android.mvp.base

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * create by halfcup
 * on 2018/10/22
 */
abstract class BasePresenter<V : BaseView>(val v: V) {
    protected val compositeDisposable: CompositeDisposable = CompositeDisposable()

    fun onDetach() {
        compositeDisposable.clear()
        detach()
        v.detach()
    }

    fun addToDisposable(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    abstract fun detach()
//    fun currentUserIsGuest(): Boolean {
//        val bean = SP.getBean(SP.USER_INFO, UserInfo::class.java);
//        return bean == null;
//    }
}