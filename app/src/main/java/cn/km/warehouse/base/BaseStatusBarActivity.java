package cn.km.warehouse.base;

import android.os.Bundle;


import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import cn.km.warehouse.R;
import cn.km.warehouse.utils.StatusBarUtils;


/**
 * Created by 075100 on 2019/11/13 0013.
 */
public class BaseStatusBarActivity  extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBar();
    }

    protected void setStatusBar() {
        StatusBarUtils.setColor(this, getResources().getColor(R.color.colorPrimary),0);
    }
}

