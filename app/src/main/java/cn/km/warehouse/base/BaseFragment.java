package cn.km.warehouse.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
//
//import com.bumptech.glide.load.resource.gif.GifDrawable;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/2/1
 */
public class BaseFragment extends Fragment {

    protected Context mContext;
    protected String TOKEN;

   // private GifDrawable gifDrawable;

    public String mTag = "";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mTag = this.getClass().getSimpleName();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


    /**
     * startActivity
     *
     * @param clazz 需要跳转的activity
     */
    protected void readyGo(Class<?> clazz) {
        Intent intent = new Intent(getActivity(), clazz);
        startActivity(intent);
    }

    /**
     * startActivity with bundle
     *
     * @param clazz  需要跳转的activity
     * @param bundle 携带的bundle
     */
    protected void readyGo(Class<?> clazz, Bundle bundle) {
        Intent intent = new Intent(getActivity(), clazz);
        if (null != bundle) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
    }

    /**
     * startActivityForResult
     *
     * @param clazz       需要跳转的activity
     * @param requestCode 请求码
     */
    protected void readyGoForResult(Class<?> clazz, int requestCode) {
        Intent intent = new Intent(getActivity(), clazz);
        startActivityForResult(intent, requestCode);
    }

    /**
     * startActivityForResult with bundle
     *
     * @param clazz       需要跳转的activity
     * @param requestCode 请求码
     * @param bundle      携带的参数
     */
    protected void readyGoForResult(Class<?> clazz, int requestCode, Bundle bundle) {
        Intent intent = new Intent(getActivity(), clazz);
        if (null != bundle) {
            intent.putExtras(bundle);
        }
        startActivityForResult(intent, requestCode);
    }

    /**
     * 用来跳转页面
     *
     * @param fragment 跳转到指定fragment，带BackStack
     */
    protected void skip2Fragment(int container, Fragment fragment) {
        getFragmentManager().beginTransaction().replace(container, fragment).addToBackStack(null).commit();
    }

    protected void skip2FragmentWithoutBS(int container, Fragment fragment) {
        getFragmentManager().beginTransaction().replace(container, fragment).commit();
    }

    /**
     * 用来跳转页面
     *
     * @param fragment 跳转到指定fragment，带BackStack和tag
     */
    public void skip2FragmentWithTag(int container, Fragment fragment, String tag) {
        getFragmentManager().beginTransaction().replace(container, fragment).addToBackStack(tag).commit();
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
    }

}