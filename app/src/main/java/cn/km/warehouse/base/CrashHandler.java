package cn.km.warehouse.base;

import android.content.Context;
import android.os.Looper;
import android.widget.Toast;

import java.util.Date;

import cn.km.warehouse.utils.DateUtils;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/1/31
 */
public class CrashHandler implements Thread.UncaughtExceptionHandler {

    private static CrashHandler INSTANCE = new CrashHandler();
    private Thread.UncaughtExceptionHandler mDefaultHandler;
    private Context mContext;

    private CrashHandler() {
    }

    public static CrashHandler getInstance() {
        return INSTANCE;
    }

    public void init(Context context) {
        mContext = context;
        mDefaultHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    /**
     * 当UncaughtException发生时会转入该函数来处理
     */
    @Override
    public void uncaughtException(Thread thread, Throwable ex) {
        if (!handleException(ex) && mDefaultHandler != null) {
            // 如果用户没有处理则让系统默认的异常处理器来处理
            mDefaultHandler.uncaughtException(thread, ex);
        } else {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
            }

            // 退出程序

            android.os.Process.killProcess(android.os.Process.myPid());

            System.exit(0);

        }
    }

    /**
     * 自定义错误处理,收集错误信息 发送错误报告等操作均在此完成.
     *
     * @param ex
     * @return true:如果处理了该异常信息;否则返回false.
     */
    private boolean handleException(Throwable ex) {
//              if (ex == null) {
//            return false;
//        }
//        // 使用Toast来显示异常信息
//        new Thread() {
//            @Override
//            public void run() {
//                Looper.prepare();
//                Toast.makeText(mContext, "发现异常，请联系管理员。", Toast.LENGTH_LONG)
//                        .show();
//                Looper.loop();
//            }
//        }.start();
//        StringBuilder builder = new StringBuilder();
//        StackTraceElement[] traces = ex.getStackTrace();
//        if (traces != null) {
//            for (int i = 0; i < traces.length; i++) {
//                if (traces[i] != null)
//                    builder.append(traces[i].toString());
//            }
//        }
//
//        System.out.println(builder.toString());
//        // 保存日志文件
//        String path = SystemConfig.getInstance().getFileRootPath() + "/log/";
//        String name = "log" + DateUtils.formatDate(new Date(), DateUtils.FORMAT_DATE_NONE) + ".txt";
////		FileUtils.createFileByContent(path, name, builder.toString() + "\r\n", true);
//       // FileUtils.saveError(ex);
//        if(ex instanceof Exception){
//            //PgyCrashManager.reportCaughtException(mContext, (Exception) ex);
//        }
//
//
        return true;
  }


}
