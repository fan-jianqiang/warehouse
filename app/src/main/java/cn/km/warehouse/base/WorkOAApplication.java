package cn.km.warehouse.base;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import cn.km.warehouse.BuildConfig;
import cn.km.warehouse.Configuration;
import cn.km.warehouse.Contstant;
import cn.km.warehouse.utils.SPUtils;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/1/31
 */
public class WorkOAApplication extends BaseApplication implements Application.ActivityLifecycleCallbacks{

    private static final String TAG = "SuperviseApplication.java";

    public static final String APP_DB_NAME = "supervise.db";
//	public static final String APP_DB_NAME = "areainfo.db";

    //private CrashHandler mCrashHandler;

    //  private ApplicationLike tinkerApplicationLike;

    private static final String TINKER_TAG = "Tinker.SampleApp";

    private Activity mKeepliveInstance;

    /*当前对象的静态实例*/
    private static WorkOAApplication instance;

    /*当前显示的Activity*/
    private Activity activity;

    public static WorkOAApplication instance() {
        return WorkOAApplication.instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        initLifeCallBack();
        Configuration.getInstance().setmContext(this);
        SPUtils.init(this);
       // SPUtils.setParam(getApplicationContext(), "path", Contstant.PROTOCOL + Contstant.HOST );

        //  x.Ext.init(this);
        System.out.println(TAG + ":onCreate()");
       // PgyCrashManager.register(this);
       // mCrashHandler = CrashHandler.getInstance();
//		mCrashHandler.init(this);
      //  SystemConfig.getInstance().initWithContext(this);
       // DbHelper.initContext(this, APP_DB_NAME);
      //  NetStateChangeReceiver.registerReceiver(this);
        // SDKInitializer.initialize(getApplicationContext());

        //  TraceProxy.getInstance().initialize(getApplicationContext());
//		Intent start = new Intent(this, TraceService.class);
//		startService(start);

//        JPushInterface.setDebugMode(BuildConfig.DEBUG ? true : false);
//        JPushInterface.init(this);

        //初始化tinker热修复
//		initTinkerPatch();

//        try {
//           // copyDatabaseFromAssets();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        // closeAndroidPDialog();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
       // NetStateChangeReceiver.unregisterReceiver(this);
    }

    private void initLifeCallBack() {
        registerActivityLifecycleCallbacks(this);
    }

    /**
     * 从资源目录拷贝初始数据库，必须在第一次调用DbHelper.getDao()之前执行
     *
     * @throws
     */
//    private void copyDatabaseFromAssets() throws IOException {
//        File db = getDatabasePath(APP_DB_NAME);
//        if (!db.getParentFile().exists()) {
//            db.getParentFile().mkdirs();
//        }
//
//        if (!db.exists()) {//如果数据库文件不存在，就从资源里拷贝初始数据库
//            createFile(getAssets().open(APP_DB_NAME), db);
//        }
//
//    }
//
//    private File createFile(InputStream in, File dest) throws IOException {
//        if (!dest.exists())
//            dest.createNewFile();
//
//        FileOutputStream fos = new FileOutputStream(dest);
//        byte[] buf = new byte[2048];
//        int len;
//        while ((len = in.read(buf)) != -1) {
//            fos.write(buf, 0, len);
//        }
//        fos.flush();
//        fos.close();
//        in.close();
//
//        return dest;
//    }

    public void setKeepliveActivity(Activity keepliveActivity){
        mKeepliveInstance = keepliveActivity;
    }

    public  void removeKeepliveActivity(){
        mKeepliveInstance = null;
    }

    public  Activity getKeepliveActivity(){
        return mKeepliveInstance;
    }

//	private void initTinkerPatch() {
//		// 我们可以从这里获得Tinker加载过程的信息
//		if (BuildConfig.TINKER_ENABLE) {
//			tinkerApplicationLike = TinkerPatchApplicationLike.getTinkerPatchApplicationLike();
//			// 初始化TinkerPatch SDK
//			TinkerPatch.init(
//					tinkerApplicationLike
////                new TinkerPatch.Builder(tinkerApplicationLike)
////                    .requestLoader(new OkHttp3Loader())
////                    .build()
//			)
//					.reflectPatchLibrary()
//					.setPatchRollbackOnScreenOff(true)
//					.setPatchRestartOnSrceenOff(true)
//					.setFetchPatchIntervalByHours(1);
//			// 获取当前的补丁版本
//			Log.d(TINKER_TAG, "Current patch version is " + TinkerPatch.with().getPatchVersion());
//
//			// fetchPatchUpdateAndPollWithInterval 与 fetchPatchUpdate(false)
//			// 不同的是，会通过handler的方式去轮询
//			TinkerPatch.with().fetchPatchUpdateAndPollWithInterval();
//		}
//	}

//    private void closeAndroidPDialog(){
//        try {
//            Class aClass = Class.forName("android.content.pm.PackageParser$Package");
//            Constructor declaredConstructor = aClass.getDeclaredConstructor(String.class);
//            declaredConstructor.setAccessible(true);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        try {
//            Class cls = Class.forName("android.app.ActivityThread");
//            Method declaredMethod = cls.getDeclaredMethod("currentActivityThread");
//            declaredMethod.setAccessible(true);
//            Object activityThread = declaredMethod.invoke(null);
//            Field mHiddenApiWarningShown = cls.getDeclaredField("mHiddenApiWarningShown");
//            mHiddenApiWarningShown.setAccessible(true);
//            mHiddenApiWarningShown.setBoolean(activityThread, true);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    /**
     * 显示View
     *
     * @param view 需要显示到Activity的视图
     */
    public void showView(View view) {
        /*Activity不为空并且没有被释放掉*/
        if (this.activity != null && !this.activity.isFinishing()) {
            /*获取Activity顶层视图,并添加自定义View*/
            ((ViewGroup) this.activity.getWindow().getDecorView()).addView(view);
        }
    }

    /**
     * 隐藏View
     *
     * @param view 需要从Activity中移除的视图
     */
    public void hideView(View view) {
        /*Activity不为空并且没有被释放掉*/
        if (this.activity != null && !this.activity.isFinishing()) {
            /*获取Activity顶层视图*/
            ViewGroup root = ((ViewGroup) this.activity.getWindow().getDecorView());
            /*如果Activity中存在View对象则删除*/
            if (root.indexOfChild(view) != -1) {
                /*从顶层视图中删除*/
                root.removeView(view);
            }
        }
    }


    private static int startCount;

    /**
     *
     * @return  true  处于后台   false  前台
     */
    public static boolean isAppBackground() {
        if (startCount == 0) {
            return true;
        }
        return false;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
    }

    @Override
    public void onActivityStarted(Activity activity) {
        startCount++;
    }

    @Override
    public void onActivityResumed(Activity activity) {
        /*获取当前显示的Activity*/
        this.activity = activity;
        setKeepliveActivity(activity);
    }

    @Override
    public void onActivityPaused(Activity activity) {
    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        startCount--;
    }

}


