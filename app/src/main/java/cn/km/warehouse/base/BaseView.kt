package com.drcnet.android.mvp.base

/**
 * create by halfcup
 * on 2018/10/22
 */
interface BaseView{
    fun showLoading()
    fun dismissLoading()

    /**
     * 一般情况下使用的注销
     */
    fun detach()
}