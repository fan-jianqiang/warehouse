package cn.km.warehouse.base;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;

import java.util.ArrayList;
import java.util.List;
/**
 * <p>Description：基类adapter</p>
 *
 * @author FJianQiang
 * @since 2020-09-14
 */
public  abstract  class BaseAdapter<T> extends BaseQuickAdapter<T, BaseViewHolder> {

    public BaseAdapter(int layoutResId) {
        super(layoutResId);
    }

    public void clearData(){
        getData().clear();
        notifyDataSetChanged();
    }

    @Override
    public void setNewData(@Nullable List<T> data) {
        if (data==null){return;}
        ArrayList<T> list = new ArrayList<>();
        for (T t:data){
            list.add(t);
        }
        super.setNewData(list);
    }
}

