package cn.km.warehouse.base;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;

import java.io.File;

import cn.km.warehouse.R;
import cn.km.warehouse.utils.DimensionUtils;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/1/31
 */
public class SystemConfig {
//    private static final String SP_SKIN_INDEX = "skin_index";
//
//    private static Context mContext;
//
//
//   // private static com.szboanda.jghq.meeting.model.LoginUser mLoginedUser;
//
//    private volatile static SystemConfig mInstance;
//
//    private boolean isDebug = false;
//
//    private static Object initLock = new Object();
//
//    private SystemConfig(){}
//
//    public static SystemConfig getInstance(){
//        if(mInstance == null){
//            synchronized (initLock) {
//                if(mInstance == null){
//                    mInstance = new SystemConfig();
//                }
//            }
//        }
//        return mInstance;
//    }
//
//    public void initWithContext(Context context){
//        mContext = context;
//    }
//
////    public void initLoginedUser(com.szboanda.jghq.meeting.model.LoginUser loginedUser){
////        mLoginedUser = loginedUser;
////    }
//
//    public Context getContext(){
//        return mContext;
//    }
//
////    public LoginUser getLoginedUser(){
////        if(mLoginedUser == null){
////            //系统崩溃后，mLoginedUser会被回收，需要重新初始化
////            try{
////                SQLiteDao dao = DbHelper.getDao();
////                if(dao.isTableExist(LoginUser.class)){
////                    mLoginedUser = dao.selector(LoginUser.class).orderBy("LOGIN_TIME", true).findFirst();
////                }
////            }catch(Exception e){
////                e.printStackTrace();
////            }
////        }
////        return mLoginedUser;
////    }
//
//    /**
//     * 获取服务地址
//     * @return
//     */
////    public String getServiceUrl(){
////        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(mContext);
////        return sp.getString("ydzf_service_url", mContext.getString(R.string.supervise_service_url));
////    }
//
//    /**
//     * 获取移动执法系统统一的文件夹根目录
//     * @return
//     */
//    public String getFileRootPath(){
//        String sdcardPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/";
//        String rootpath = mContext.getString(R.string.supervise_file_path);
//        String result = sdcardPath + rootpath;
//        result = result.replace("//", "/");
//        if(!result.endsWith("/")){
//            result += "/";
//        }
//        File dir = new File(result);
//        if(!dir.exists()){
//            dir.mkdirs();
//        }
//        return result;
//    }
//
//    /**
//     * 获取应用文件存储路径
//     * @return
//     */
//    public static String getAppStorePath(){
//        return SystemConfig.getInstance().getFileRootPath();
//    }
//
//    /**
//     * 获取皮肤的索引
//     * @return
//     */
//    public int getSkinIndex(){
//        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(mContext);
//        int skinIndex = sp.getInt(SP_SKIN_INDEX, 0);
//        return skinIndex;
//    }
//
//    public void setDebug(boolean isDebug){
//        this.isDebug = isDebug;
//    }
//
//    public boolean isDebug(){
//        return isDebug;
//    }
//
//    /**
//     * 获取系统屏幕方向类型
//     * @return
//     */
//    public SystemConfig.OrientationType getOrientationType(){
//        String screenPortraitMaxSize = getContext().getString(R.string.screen_portrait_max_size);
//        double threshold = Double.parseDouble(screenPortraitMaxSize);
//        double screePhysicalSize = DimensionUtils.getScreenPhysicalInch(getContext());
//        return screePhysicalSize > threshold ? SystemConfig.OrientationType.LANDSCAPE : SystemConfig.OrientationType.PORTRAIT;
//    }
//
//    public enum OrientationType{
//
//        PORTRAIT,
//
//        LANDSCAPE,
//
//    }
}
