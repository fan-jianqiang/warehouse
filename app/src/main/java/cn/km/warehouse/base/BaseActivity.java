package cn.km.warehouse.base;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;



import org.json.JSONObject;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cn.km.warehouse.R;
import cn.km.warehouse.utils.JsonUtils;


/**
 * Created by 075100 on 2019/11/13 0013.
 */
public class BaseActivity extends BaseStatusBarActivity {

    public final String TAG = getClass().getSimpleName();



    private TextView indicatorLeft;
    private TextView incidatorRight;
    private TextView mTextViewActionTitle;
    private RelativeLayout mRelativeLayoutBack;
    private ImageView mImageViewRight;
    private TextView mTextViewRight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        if(getRequestedOrientation()== ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        //   StatusBarCompat.setStatusBarColor(this, getResources().getColor(R.color.white) , true);
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @SuppressWarnings("unchecked")
    public <E> E findViewAutoConvert(int id) {
        View v = findViewById(id);
        return v == null ? null : (E) v;
    }

    @SuppressWarnings("unchecked")
    public <E> E findViewAutoConvert(View rv, int id) {
        View v = rv.findViewById(id);
        return v == null ? null : (E) v;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //  getLocationPinner().stopRequestLocation();
    }

    protected void initActionBar(String title,boolean isShowLeft,boolean isRight,String titleRight,int imageResource) {
        //   View view = LayoutInflater.from(this).inflate(R.layout.layout_action_bar, null);
        mRelativeLayoutBack = (RelativeLayout) this.findViewById(R.id.realtvie_back_action);
        mImageViewRight = (ImageView) this.findViewById(R.id.iv_title_right);
        mTextViewActionTitle = (TextView) this.findViewById(R.id.tv_title);
        mTextViewRight = (TextView) this.findViewById(R.id.tv_title_right);
        mTextViewActionTitle.setText(title);
        if (isShowLeft) {
            mRelativeLayoutBack.setVisibility(View.VISIBLE);
            mRelativeLayoutBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onLeftClick(v);
                }
            });
        } else {
            mRelativeLayoutBack.setVisibility(View.GONE);
        }
        if (isRight) {
            if (titleRight.equals("")) {
                mImageViewRight.setImageResource(imageResource);
                mImageViewRight.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onRightClick(v);
                    }
                });
            } else {
                mTextViewRight.setText(titleRight);
                mTextViewRight.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onRightClick(v);
                    }
                });
            }
        }
    }

    public void onRightClick(View v) {

    }

    public void onLeftClick(View v) {

    }

    /**
     * startActivity
     *
     * @param clazz 需要跳转的activity
     */
    protected void readyGo(Class<?> clazz) {
        Intent intent = new Intent(this, clazz);
        startActivity(intent);
    }

    /**
     * startActivity with bundle
     *
     * @param clazz  需要跳转的activity
     * @param bundle 携带的bundle
     */
    protected void readyGo(Class<?> clazz, Bundle bundle) {
        Intent intent = new Intent(this, clazz);
        if (null != bundle) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
    }

    /**
     * startActivityForResult
     *
     * @param clazz       需要跳转的activity
     * @param requestCode 请求码
     */
    protected void readyGoForResult(Class<?> clazz, int requestCode) {
        Intent intent = new Intent(this, clazz);
        startActivityForResult(intent, requestCode);
    }

    /**
     * startActivityForResult with bundle
     *
     * @param clazz       需要跳转的activity
     * @param requestCode 请求码
     * @param bundle      携带的参数
     */
    protected void readyGoForResult(Class<?> clazz, int requestCode, Bundle bundle) {
        Intent intent = new Intent(this, clazz);
        if (null != bundle) {
            intent.putExtras(bundle);
        }
        startActivityForResult(intent, requestCode);
    }

    /**
     * startActivity then finish
     *
     * @param clazz 需要跳转的activity
     */
    protected void readyGoThenKill(Class<?> clazz) {
        Intent intent = new Intent(this, clazz);
        startActivity(intent);
        finish();
    }

    /**
     * startActivity with bundle then finish
     *
     * @param clazz  需要跳转的activity
     * @param bundle 携带的参数
     */
    protected void readyGoThenKill(Class<?> clazz, Bundle bundle) {
        Intent intent = new Intent(this, clazz);
        if (null != bundle) {
            intent.putExtras(bundle);
        }
        startActivity(intent);
        finish();
    }

    /**
     * 跳转h5
     */
    protected void GoH5(String url, Class<?> clazz) {
        Bundle bundle = new Bundle();
        bundle.putString("url", url);
        readyGo(clazz, bundle);
    }

    /**
     * 用来跳转页面
     *
     * @param fragment 跳转到指定fragment，带BackStack
     */
    public void skip2Fragment(int container, Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(container, fragment).addToBackStack(null).commit();
    }

    protected void skip2FragmentWithoutBS(int container, Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(container, fragment).commit();
    }
    /**
     * 需要进行检测的权限数组
     */
    protected String[] needPermissions = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.READ_PHONE_STATE,
//            Manifest.permission.CAMERA,
//            Manifest.permission.CALL_PHONE,
    };

    private static final int PERMISSON_REQUESTCODE = 0;

    /**
     * 判断是否需要检测，防止不停的弹框
     */
    private boolean isNeedCheck = true;

    @Override
    protected void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT >= 23
                && getApplicationInfo().targetSdkVersion >= 23) {
            if (isNeedCheck) {
                checkPermissions(needPermissions);
            }
        }
        if (needRegisterNetworkChangeObserver()) {
          //  NetStateChangeReceiver.registerObserver(this);
        }
    }

    /**
     * @param permissions
     * @since 2.5.0
     */
    private void checkPermissions(String... permissions) {
        try {
            if (Build.VERSION.SDK_INT >= 23
                    && getApplicationInfo().targetSdkVersion >= 23) {
                List<String> needRequestPermissonList = findDeniedPermissions(permissions);
                if (null != needRequestPermissonList
                        && needRequestPermissonList.size() > 0) {
                    String[] array = needRequestPermissonList.toArray(new String[needRequestPermissonList.size()]);
                    Method method = getClass().getMethod("requestPermissions", new Class[]{String[].class,
                            int.class});

                    method.invoke(this, array, PERMISSON_REQUESTCODE);
                }
            }
        } catch (Throwable e) {
        }
    }

    /**
     * 获取权限集中需要申请权限的列表
     *
     * @param permissions
     * @return
     * @since 2.5.0
     */
    private List<String> findDeniedPermissions(String[] permissions) {
        List<String> needRequestPermissonList = new ArrayList<String>();
        if (Build.VERSION.SDK_INT >= 23
                && getApplicationInfo().targetSdkVersion >= 23) {
            try {
                for (String perm : permissions) {
                    Method checkSelfMethod = getClass().getMethod("checkSelfPermission", String.class);
                    Method shouldShowRequestPermissionRationaleMethod = getClass().getMethod("shouldShowRequestPermissionRationale",
                            String.class);
                    if ((Integer) checkSelfMethod.invoke(this, perm) != PackageManager.PERMISSION_GRANTED
                            || (Boolean) shouldShowRequestPermissionRationaleMethod.invoke(this, perm)) {
                        needRequestPermissonList.add(perm);
                    }
                }
            } catch (Throwable e) {

            }
        }
        return needRequestPermissonList;
    }

    /**
     * 检测是否所有的权限都已经授权
     *
     * @param grantResults
     * @return
     * @since 2.5.0
     */
    private boolean verifyPermissions(int[] grantResults) {
        for (int result : grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    @TargetApi(23)
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] paramArrayOfInt) {
        super.onRequestPermissionsResult(requestCode, permissions, paramArrayOfInt);
        if (requestCode == PERMISSON_REQUESTCODE) {
            if (!verifyPermissions(paramArrayOfInt)) {
                showMissingPermissionDialog();
                isNeedCheck = false;
            }
        }
    }

    /**
     * 显示提示信息
     *
     * @since 2.5.0
     */
    private void showMissingPermissionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("提示");
        builder.setMessage("当前应用缺少必要权限。\\n\\n请点击\\\"设置\\\"-\\\"权限\\\"-打开所需权限。");

        // 拒绝, 退出应用
        builder.setNegativeButton("取消",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });

        builder.setPositiveButton("设置",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startAppSettings();
                    }
                });

        builder.setCancelable(false);

        builder.show();
    }

    /**
     * 启动应用的设置
     *
     * @since 2.5.0
     */
    private void startAppSettings() {
        Intent intent = new Intent(
                Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setData(Uri.parse("package:" + getPackageName()));
        startActivity(intent);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            this.finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    protected void clearJsonNull(JSONObject jsonObject){
        Iterator<String> keys = jsonObject.keys();
        List<String> resetKeys = new ArrayList<String>();
        while (keys.hasNext()) {
            String k = keys.next();
            String v = jsonObject.optString(k, "");
            if(TextUtils.isEmpty(v) || "null".equalsIgnoreCase(v)){
                resetKeys.add(k);
            }
        }

        for(String k : resetKeys){
            JsonUtils.put(jsonObject, k, " ");
        }
    }

//    @Override
//    public void onNetDisconnected() {
//
//    }
//
//    @Override
//    public void onNetConnected(NetworkType networkType) {
//
//    }
    /**
     * 是否需要注册网络变化的Observer,如果不需要监听网络变化,则返回false;否则返回true.默认返回false
     */
    protected boolean needRegisterNetworkChangeObserver() {
        return true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (needRegisterNetworkChangeObserver()) {
         //   NetStateChangeReceiver.unregisterObserver(this);
        }
    }
}
