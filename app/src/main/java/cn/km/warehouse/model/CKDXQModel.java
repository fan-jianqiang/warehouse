package cn.km.warehouse.model;

import java.util.List;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/7
 */
public class CKDXQModel {


    /**
     * billEntityList : [{"applicationBillId":1,"applicationId":4,"projectId":1,"materialId":1,"warehouseId":1,"positionId":1,"shelfId":1,"shouldNum":2,"actualNum":2,"unitPrice":2,"totalPrice":4,"orderId":1,"positionName":null,"shelfName":null,"materialCode":"111111","materialName":"物料一","materialTypeName":"文具袋","materialUnitName":"台","materialSpecs":"234","projectName":"项目1","projectCode":"1","stockList":[{"stockId":null,"rfid":null,"epc":null,"materialId":null,"projectId":null,"warehouseId":null,"positionId":1,"shelfId":1,"entryNum":3,"stockNum":null,"deliveryNum":null,"entryType":null,"entryTime":null,"deliveryTime":null,"unitPrice":null,"productTime":null,"createrId":null,"createTime":null,"updaterId":null,"updateTime":null,"oldState":null,"applicationBillId":null,"oldRfid":null,"entryOrderId":null,"projectName":null,"orgName":null,"materialCode":null,"materialName":null,"materialTypeName":null,"materialUnitName":null,"materialSpecs":null,"spare":null,"warrantyPeriod":null,"storageAge":null,"stockMonth":null,"shelfName":"库房1仓位1货架1","positionName":"库房1仓位1","warehouseName":null,"updateUserName":null,"state":null}],"operaFlag":null,"stockNum":null}]
     * deliveryOrder : {"orderId":1,"orderCode":"CK1615287725920","applicationId":4,"projectId":1,"warehouseId":1,"orderState":3,"lastDeliveryTime":"2021-03-09 19:06:15","createrId":5,"createTime":"2021-03-09 19:02:06","updaterId":null,"updateTime":null,"applicationCode":"CKSQ1615287603547","applicationDatetime":"2021-03-09 19:00:04","projectName":"项目1","warehouseName":"仓库1","organizationName":"四川电力集团","applicationAccountName":"超级管理员熊大","houseUserName":"库管员张三","totalPrice":null}
     */

    private DeliveryOrderBean deliveryOrder;
    private List<BillEntityListBean> billEntityList;

    public DeliveryOrderBean getDeliveryOrder() {
        return deliveryOrder;
    }

    public void setDeliveryOrder(DeliveryOrderBean deliveryOrder) {
        this.deliveryOrder = deliveryOrder;
    }

    public List<BillEntityListBean> getBillEntityList() {
        return billEntityList;
    }

    public void setBillEntityList(List<BillEntityListBean> billEntityList) {
        this.billEntityList = billEntityList;
    }

    public static class DeliveryOrderBean {
        /**
         * orderId : 1
         * orderCode : CK1615287725920
         * applicationId : 4
         * projectId : 1
         * warehouseId : 1
         * orderState : 3
         * lastDeliveryTime : 2021-03-09 19:06:15
         * createrId : 5
         * createTime : 2021-03-09 19:02:06
         * updaterId : null
         * updateTime : null
         * applicationCode : CKSQ1615287603547
         * applicationDatetime : 2021-03-09 19:00:04
         * projectName : 项目1
         * warehouseName : 仓库1
         * organizationName : 四川电力集团
         * applicationAccountName : 超级管理员熊大
         * houseUserName : 库管员张三
         * totalPrice : null
         */

        private int orderId;
        private String orderCode;
        private int applicationId;
        private int projectId;
        private int warehouseId;
        private int orderState;
        private String lastDeliveryTime;
        private int createrId;
        private String createTime;
        private Object updaterId;
        private Object updateTime;
        private String applicationCode;
        private String applicationDatetime;
        private String projectName;
        private String warehouseName;
        private String organizationName;
        private String applicationAccountName;
        private String houseUserName;
        private Object totalPrice;

        public int getOrderId() {
            return orderId;
        }

        public void setOrderId(int orderId) {
            this.orderId = orderId;
        }

        public String getOrderCode() {
            return orderCode;
        }

        public void setOrderCode(String orderCode) {
            this.orderCode = orderCode;
        }

        public int getApplicationId() {
            return applicationId;
        }

        public void setApplicationId(int applicationId) {
            this.applicationId = applicationId;
        }

        public int getProjectId() {
            return projectId;
        }

        public void setProjectId(int projectId) {
            this.projectId = projectId;
        }

        public int getWarehouseId() {
            return warehouseId;
        }

        public void setWarehouseId(int warehouseId) {
            this.warehouseId = warehouseId;
        }

        public int getOrderState() {
            return orderState;
        }

        public void setOrderState(int orderState) {
            this.orderState = orderState;
        }

        public String getLastDeliveryTime() {
            return lastDeliveryTime;
        }

        public void setLastDeliveryTime(String lastDeliveryTime) {
            this.lastDeliveryTime = lastDeliveryTime;
        }

        public int getCreaterId() {
            return createrId;
        }

        public void setCreaterId(int createrId) {
            this.createrId = createrId;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public Object getUpdaterId() {
            return updaterId;
        }

        public void setUpdaterId(Object updaterId) {
            this.updaterId = updaterId;
        }

        public Object getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Object updateTime) {
            this.updateTime = updateTime;
        }

        public String getApplicationCode() {
            return applicationCode;
        }

        public void setApplicationCode(String applicationCode) {
            this.applicationCode = applicationCode;
        }

        public String getApplicationDatetime() {
            return applicationDatetime;
        }

        public void setApplicationDatetime(String applicationDatetime) {
            this.applicationDatetime = applicationDatetime;
        }

        public String getProjectName() {
            return projectName;
        }

        public void setProjectName(String projectName) {
            this.projectName = projectName;
        }

        public String getWarehouseName() {
            return warehouseName;
        }

        public void setWarehouseName(String warehouseName) {
            this.warehouseName = warehouseName;
        }

        public String getOrganizationName() {
            return organizationName;
        }

        public void setOrganizationName(String organizationName) {
            this.organizationName = organizationName;
        }

        public String getApplicationAccountName() {
            return applicationAccountName;
        }

        public void setApplicationAccountName(String applicationAccountName) {
            this.applicationAccountName = applicationAccountName;
        }

        public String getHouseUserName() {
            return houseUserName;
        }

        public void setHouseUserName(String houseUserName) {
            this.houseUserName = houseUserName;
        }

        public Object getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(Object totalPrice) {
            this.totalPrice = totalPrice;
        }
    }

    public static class BillEntityListBean {
        /**
         * applicationBillId : 1
         * applicationId : 4
         * projectId : 1
         * materialId : 1
         * warehouseId : 1
         * positionId : 1
         * shelfId : 1
         * shouldNum : 2
         * actualNum : 2
         * unitPrice : 2
         * totalPrice : 4
         * orderId : 1
         * positionName : null
         * shelfName : null
         * materialCode : 111111
         * materialName : 物料一
         * materialTypeName : 文具袋
         * materialUnitName : 台
         * materialSpecs : 234
         * projectName : 项目1
         * projectCode : 1
         * stockList : [{"stockId":null,"rfid":null,"epc":null,"materialId":null,"projectId":null,"warehouseId":null,"positionId":1,"shelfId":1,"entryNum":3,"stockNum":null,"deliveryNum":null,"entryType":null,"entryTime":null,"deliveryTime":null,"unitPrice":null,"productTime":null,"createrId":null,"createTime":null,"updaterId":null,"updateTime":null,"oldState":null,"applicationBillId":null,"oldRfid":null,"entryOrderId":null,"projectName":null,"orgName":null,"materialCode":null,"materialName":null,"materialTypeName":null,"materialUnitName":null,"materialSpecs":null,"spare":null,"warrantyPeriod":null,"storageAge":null,"stockMonth":null,"shelfName":"库房1仓位1货架1","positionName":"库房1仓位1","warehouseName":null,"updateUserName":null,"state":null}]
         * operaFlag : null
         * stockNum : null
         */

        private int applicationBillId;
        private int applicationId;
        private int projectId;
        private int materialId;
        private int warehouseId;
        private int positionId;
        private int shelfId;
        private int shouldNum;
        private int actualNum;
        private int unitPrice;
        private int totalPrice;
        private int orderId;
        private Object positionName;
        private Object shelfName;
        private String materialCode;
        private String materialName;
        private String materialTypeName;
        private String materialUnitName;
        private String materialSpecs;
        private String projectName;
        private String projectCode;
        private Object operaFlag;
        private Object stockNum;
        private List<StockListBean> stockList;

        public int getApplicationBillId() {
            return applicationBillId;
        }

        public void setApplicationBillId(int applicationBillId) {
            this.applicationBillId = applicationBillId;
        }

        public int getApplicationId() {
            return applicationId;
        }

        public void setApplicationId(int applicationId) {
            this.applicationId = applicationId;
        }

        public int getProjectId() {
            return projectId;
        }

        public void setProjectId(int projectId) {
            this.projectId = projectId;
        }

        public int getMaterialId() {
            return materialId;
        }

        public void setMaterialId(int materialId) {
            this.materialId = materialId;
        }

        public int getWarehouseId() {
            return warehouseId;
        }

        public void setWarehouseId(int warehouseId) {
            this.warehouseId = warehouseId;
        }

        public int getPositionId() {
            return positionId;
        }

        public void setPositionId(int positionId) {
            this.positionId = positionId;
        }

        public int getShelfId() {
            return shelfId;
        }

        public void setShelfId(int shelfId) {
            this.shelfId = shelfId;
        }

        public int getShouldNum() {
            return shouldNum;
        }

        public void setShouldNum(int shouldNum) {
            this.shouldNum = shouldNum;
        }

        public int getActualNum() {
            return actualNum;
        }

        public void setActualNum(int actualNum) {
            this.actualNum = actualNum;
        }

        public int getUnitPrice() {
            return unitPrice;
        }

        public void setUnitPrice(int unitPrice) {
            this.unitPrice = unitPrice;
        }

        public int getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(int totalPrice) {
            this.totalPrice = totalPrice;
        }

        public int getOrderId() {
            return orderId;
        }

        public void setOrderId(int orderId) {
            this.orderId = orderId;
        }

        public Object getPositionName() {
            return positionName;
        }

        public void setPositionName(Object positionName) {
            this.positionName = positionName;
        }

        public Object getShelfName() {
            return shelfName;
        }

        public void setShelfName(Object shelfName) {
            this.shelfName = shelfName;
        }

        public String getMaterialCode() {
            return materialCode;
        }

        public void setMaterialCode(String materialCode) {
            this.materialCode = materialCode;
        }

        public String getMaterialName() {
            return materialName;
        }

        public void setMaterialName(String materialName) {
            this.materialName = materialName;
        }

        public String getMaterialTypeName() {
            return materialTypeName;
        }

        public void setMaterialTypeName(String materialTypeName) {
            this.materialTypeName = materialTypeName;
        }

        public String getMaterialUnitName() {
            return materialUnitName;
        }

        public void setMaterialUnitName(String materialUnitName) {
            this.materialUnitName = materialUnitName;
        }

        public String getMaterialSpecs() {
            return materialSpecs;
        }

        public void setMaterialSpecs(String materialSpecs) {
            this.materialSpecs = materialSpecs;
        }

        public String getProjectName() {
            return projectName;
        }

        public void setProjectName(String projectName) {
            this.projectName = projectName;
        }

        public String getProjectCode() {
            return projectCode;
        }

        public void setProjectCode(String projectCode) {
            this.projectCode = projectCode;
        }

        public Object getOperaFlag() {
            return operaFlag;
        }

        public void setOperaFlag(Object operaFlag) {
            this.operaFlag = operaFlag;
        }

        public Object getStockNum() {
            return stockNum;
        }

        public void setStockNum(Object stockNum) {
            this.stockNum = stockNum;
        }

        public List<StockListBean> getStockList() {
            return stockList;
        }

        public void setStockList(List<StockListBean> stockList) {
            this.stockList = stockList;
        }

        public static class StockListBean {
            /**
             * stockId : null
             * rfid : null
             * epc : null
             * materialId : null
             * projectId : null
             * warehouseId : null
             * positionId : 1
             * shelfId : 1
             * entryNum : 3
             * stockNum : null
             * deliveryNum : null
             * entryType : null
             * entryTime : null
             * deliveryTime : null
             * unitPrice : null
             * productTime : null
             * createrId : null
             * createTime : null
             * updaterId : null
             * updateTime : null
             * oldState : null
             * applicationBillId : null
             * oldRfid : null
             * entryOrderId : null
             * projectName : null
             * orgName : null
             * materialCode : null
             * materialName : null
             * materialTypeName : null
             * materialUnitName : null
             * materialSpecs : null
             * spare : null
             * warrantyPeriod : null
             * storageAge : null
             * stockMonth : null
             * shelfName : 库房1仓位1货架1
             * positionName : 库房1仓位1
             * warehouseName : null
             * updateUserName : null
             * state : null
             */

            private Object stockId;
            private Object rfid;
            private Object epc;
            private Object materialId;
            private Object projectId;
            private Object warehouseId;
            private int positionId;
            private int shelfId;
            private int entryNum;
            private Object stockNum;
            private Object deliveryNum;
            private Object entryType;
            private Object entryTime;
            private Object deliveryTime;
            private Object unitPrice;
            private Object productTime;
            private Object createrId;
            private Object createTime;
            private Object updaterId;
            private Object updateTime;
            private Object oldState;
            private Object applicationBillId;
            private Object oldRfid;
            private Object entryOrderId;
            private Object projectName;
            private Object orgName;
            private Object materialCode;
            private Object materialName;
            private Object materialTypeName;
            private Object materialUnitName;
            private Object materialSpecs;
            private Object spare;
            private Object warrantyPeriod;
            private Object storageAge;
            private Object stockMonth;
            private String shelfName;
            private String positionName;
            private Object warehouseName;
            private Object updateUserName;
            private Object state;

            public Object getStockId() {
                return stockId;
            }

            public void setStockId(Object stockId) {
                this.stockId = stockId;
            }

            public Object getRfid() {
                return rfid;
            }

            public void setRfid(Object rfid) {
                this.rfid = rfid;
            }

            public Object getEpc() {
                return epc;
            }

            public void setEpc(Object epc) {
                this.epc = epc;
            }

            public Object getMaterialId() {
                return materialId;
            }

            public void setMaterialId(Object materialId) {
                this.materialId = materialId;
            }

            public Object getProjectId() {
                return projectId;
            }

            public void setProjectId(Object projectId) {
                this.projectId = projectId;
            }

            public Object getWarehouseId() {
                return warehouseId;
            }

            public void setWarehouseId(Object warehouseId) {
                this.warehouseId = warehouseId;
            }

            public int getPositionId() {
                return positionId;
            }

            public void setPositionId(int positionId) {
                this.positionId = positionId;
            }

            public int getShelfId() {
                return shelfId;
            }

            public void setShelfId(int shelfId) {
                this.shelfId = shelfId;
            }

            public int getEntryNum() {
                return entryNum;
            }

            public void setEntryNum(int entryNum) {
                this.entryNum = entryNum;
            }

            public Object getStockNum() {
                return stockNum;
            }

            public void setStockNum(Object stockNum) {
                this.stockNum = stockNum;
            }

            public Object getDeliveryNum() {
                return deliveryNum;
            }

            public void setDeliveryNum(Object deliveryNum) {
                this.deliveryNum = deliveryNum;
            }

            public Object getEntryType() {
                return entryType;
            }

            public void setEntryType(Object entryType) {
                this.entryType = entryType;
            }

            public Object getEntryTime() {
                return entryTime;
            }

            public void setEntryTime(Object entryTime) {
                this.entryTime = entryTime;
            }

            public Object getDeliveryTime() {
                return deliveryTime;
            }

            public void setDeliveryTime(Object deliveryTime) {
                this.deliveryTime = deliveryTime;
            }

            public Object getUnitPrice() {
                return unitPrice;
            }

            public void setUnitPrice(Object unitPrice) {
                this.unitPrice = unitPrice;
            }

            public Object getProductTime() {
                return productTime;
            }

            public void setProductTime(Object productTime) {
                this.productTime = productTime;
            }

            public Object getCreaterId() {
                return createrId;
            }

            public void setCreaterId(Object createrId) {
                this.createrId = createrId;
            }

            public Object getCreateTime() {
                return createTime;
            }

            public void setCreateTime(Object createTime) {
                this.createTime = createTime;
            }

            public Object getUpdaterId() {
                return updaterId;
            }

            public void setUpdaterId(Object updaterId) {
                this.updaterId = updaterId;
            }

            public Object getUpdateTime() {
                return updateTime;
            }

            public void setUpdateTime(Object updateTime) {
                this.updateTime = updateTime;
            }

            public Object getOldState() {
                return oldState;
            }

            public void setOldState(Object oldState) {
                this.oldState = oldState;
            }

            public Object getApplicationBillId() {
                return applicationBillId;
            }

            public void setApplicationBillId(Object applicationBillId) {
                this.applicationBillId = applicationBillId;
            }

            public Object getOldRfid() {
                return oldRfid;
            }

            public void setOldRfid(Object oldRfid) {
                this.oldRfid = oldRfid;
            }

            public Object getEntryOrderId() {
                return entryOrderId;
            }

            public void setEntryOrderId(Object entryOrderId) {
                this.entryOrderId = entryOrderId;
            }

            public Object getProjectName() {
                return projectName;
            }

            public void setProjectName(Object projectName) {
                this.projectName = projectName;
            }

            public Object getOrgName() {
                return orgName;
            }

            public void setOrgName(Object orgName) {
                this.orgName = orgName;
            }

            public Object getMaterialCode() {
                return materialCode;
            }

            public void setMaterialCode(Object materialCode) {
                this.materialCode = materialCode;
            }

            public Object getMaterialName() {
                return materialName;
            }

            public void setMaterialName(Object materialName) {
                this.materialName = materialName;
            }

            public Object getMaterialTypeName() {
                return materialTypeName;
            }

            public void setMaterialTypeName(Object materialTypeName) {
                this.materialTypeName = materialTypeName;
            }

            public Object getMaterialUnitName() {
                return materialUnitName;
            }

            public void setMaterialUnitName(Object materialUnitName) {
                this.materialUnitName = materialUnitName;
            }

            public Object getMaterialSpecs() {
                return materialSpecs;
            }

            public void setMaterialSpecs(Object materialSpecs) {
                this.materialSpecs = materialSpecs;
            }

            public Object getSpare() {
                return spare;
            }

            public void setSpare(Object spare) {
                this.spare = spare;
            }

            public Object getWarrantyPeriod() {
                return warrantyPeriod;
            }

            public void setWarrantyPeriod(Object warrantyPeriod) {
                this.warrantyPeriod = warrantyPeriod;
            }

            public Object getStorageAge() {
                return storageAge;
            }

            public void setStorageAge(Object storageAge) {
                this.storageAge = storageAge;
            }

            public Object getStockMonth() {
                return stockMonth;
            }

            public void setStockMonth(Object stockMonth) {
                this.stockMonth = stockMonth;
            }

            public String getShelfName() {
                return shelfName;
            }

            public void setShelfName(String shelfName) {
                this.shelfName = shelfName;
            }

            public String getPositionName() {
                return positionName;
            }

            public void setPositionName(String positionName) {
                this.positionName = positionName;
            }

            public Object getWarehouseName() {
                return warehouseName;
            }

            public void setWarehouseName(Object warehouseName) {
                this.warehouseName = warehouseName;
            }

            public Object getUpdateUserName() {
                return updateUserName;
            }

            public void setUpdateUserName(Object updateUserName) {
                this.updateUserName = updateUserName;
            }

            public Object getState() {
                return state;
            }

            public void setState(Object state) {
                this.state = state;
            }
        }
    }
}
