package cn.km.warehouse.model;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/12
 */
public class VMaterialStock {
    private int applicationBillId;
    private int materialId;
    private int positionId;
    private int shelfId;
    private Double entryNum;
    private String rfid;
    private String epc;
    private String spare;
    private String oldRfid;

    public String getOldRfid() {
        return oldRfid;
    }

    public void setOldRfid(String oldRfid) {
        this.oldRfid = oldRfid;
    }
    //  private BigDecember unitPrice;

    public String getSpare() {
        return spare;
    }

//    public String getUnitPrice() {
//        return unitPrice;
//    }

//    public void setUnitPrice(String unitPrice) {
//        this.unitPrice = unitPrice;
//    }

    public void setSpare(String spare) {
        this.spare = spare;
    }

    public int getApplicationBillId() {
        return applicationBillId;
    }

    public void setApplicationBillId(int applicationBillId) {
        this.applicationBillId = applicationBillId;
    }

    public int getMaterialId() {
        return materialId;
    }

    public void setMaterialId(int materialId) {
        this.materialId = materialId;
    }

    public int getPositionId() {
        return positionId;
    }

    public void setPositionId(int positionId) {
        this.positionId = positionId;
    }

    public int getShelfId() {
        return shelfId;
    }

    public void setShelfId(int shelfId) {
        this.shelfId = shelfId;
    }

    public Double getEntryNum() {
        return entryNum;
    }

    public void setEntryNum(Double entryNum) {
        this.entryNum = entryNum;
    }

    public String getRfid() {
        return rfid;
    }

    public void setRfid(String rfid) {
        this.rfid = rfid;
    }

    public String getEpc() {
        return epc;
    }

    public void setEpc(String epc) {
        this.epc = epc;
    }
}
