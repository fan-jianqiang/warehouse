package cn.km.warehouse.model;

import java.util.List;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/15
 */
public class CKListQDModel {

    /**
     * totalCount : 1
     * pageSize : 10
     * totalPage : 1
     * currPage : 1
     * list : [{"applicationBillId":null,"applicationId":4,"projectId":null,"materialId":1,"warehouseId":null,"positionId":null,"shelfId":null,"shouldNum":2,"actualNum":2,"unitPrice":null,"totalPrice":null,"orderId":null,"positionName":null,"shelfName":null,"materialCode":"111111","materialName":"物料一","materialTypeName":null,"materialUnitName":"台","materialSpecs":"234","projectName":null,"projectCode":null,"stockList":null,"operaFlag":null,"stockNum":null}]
     */

    private int totalCount;
    private int pageSize;
    private int totalPage;
    private int currPage;
    private List<ListBean> list;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getCurrPage() {
        return currPage;
    }

    public void setCurrPage(int currPage) {
        this.currPage = currPage;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * applicationBillId : null
         * applicationId : 4
         * projectId : null
         * materialId : 1
         * warehouseId : null
         * positionId : null
         * shelfId : null
         * shouldNum : 2.0
         * actualNum : 2.0
         * unitPrice : null
         * totalPrice : null
         * orderId : null
         * positionName : null
         * shelfName : null
         * materialCode : 111111
         * materialName : 物料一
         * materialTypeName : null
         * materialUnitName : 台
         * materialSpecs : 234
         * projectName : null
         * projectCode : null
         * stockList : null
         * operaFlag : null
         * stockNum : null
         */

        private int applicationBillId;
        private int applicationId;
        private Object projectId;
        private int materialId;
        private Object warehouseId;
        private Object positionId;
        private Object shelfId;
        private double shouldNum;
        private double actualNum;
        private Object unitPrice;
        private Object totalPrice;
        private int  orderId;
        private Object positionName;
        private Object shelfName;
        private String materialCode;
        private String materialName;
        private Object materialTypeName;
        private String materialUnitName;
        private String materialSpecs;
        private Object projectName;
        private Object projectCode;
        private Object stockList;
        private Object operaFlag;
        private Object stockNum;



        public int  getApplicationBillId() {
            return applicationBillId;
        }

        public void setApplicationBillId(int applicationBillId) {
            this.applicationBillId = applicationBillId;
        }

        public int getApplicationId() {
            return applicationId;
        }

        public void setApplicationId(int applicationId) {
            this.applicationId = applicationId;
        }

        public Object getProjectId() {
            return projectId;
        }

        public void setProjectId(Object projectId) {
            this.projectId = projectId;
        }

        public int getMaterialId() {
            return materialId;
        }

        public void setMaterialId(int materialId) {
            this.materialId = materialId;
        }

        public Object getWarehouseId() {
            return warehouseId;
        }

        public void setWarehouseId(Object warehouseId) {
            this.warehouseId = warehouseId;
        }

        public Object getPositionId() {
            return positionId;
        }

        public void setPositionId(Object positionId) {
            this.positionId = positionId;
        }

        public Object getShelfId() {
            return shelfId;
        }

        public void setShelfId(Object shelfId) {
            this.shelfId = shelfId;
        }

        public double getShouldNum() {
            return shouldNum;
        }

        public void setShouldNum(double shouldNum) {
            this.shouldNum = shouldNum;
        }

        public double getActualNum() {
            return actualNum;
        }

        public void setActualNum(double actualNum) {
            this.actualNum = actualNum;
        }

        public Object getUnitPrice() {
            return unitPrice;
        }

        public void setUnitPrice(Object unitPrice) {
            this.unitPrice = unitPrice;
        }

        public Object getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(Object totalPrice) {
            this.totalPrice = totalPrice;
        }

        public int getOrderId() {
            return orderId;
        }

        public void setOrderId(int orderId) {
            this.orderId = orderId;
        }

        public Object getPositionName() {
            return positionName;
        }

        public void setPositionName(Object positionName) {
            this.positionName = positionName;
        }

        public Object getShelfName() {
            return shelfName;
        }

        public void setShelfName(Object shelfName) {
            this.shelfName = shelfName;
        }

        public String getMaterialCode() {
            return materialCode;
        }

        public void setMaterialCode(String materialCode) {
            this.materialCode = materialCode;
        }

        public String getMaterialName() {
            return materialName;
        }

        public void setMaterialName(String materialName) {
            this.materialName = materialName;
        }

        public Object getMaterialTypeName() {
            return materialTypeName;
        }

        public void setMaterialTypeName(Object materialTypeName) {
            this.materialTypeName = materialTypeName;
        }

        public String getMaterialUnitName() {
            return materialUnitName;
        }

        public void setMaterialUnitName(String materialUnitName) {
            this.materialUnitName = materialUnitName;
        }

        public String getMaterialSpecs() {
            return materialSpecs;
        }

        public void setMaterialSpecs(String materialSpecs) {
            this.materialSpecs = materialSpecs;
        }

        public Object getProjectName() {
            return projectName;
        }

        public void setProjectName(Object projectName) {
            this.projectName = projectName;
        }

        public Object getProjectCode() {
            return projectCode;
        }

        public void setProjectCode(Object projectCode) {
            this.projectCode = projectCode;
        }

        public Object getStockList() {
            return stockList;
        }

        public void setStockList(Object stockList) {
            this.stockList = stockList;
        }

        public Object getOperaFlag() {
            return operaFlag;
        }

        public void setOperaFlag(Object operaFlag) {
            this.operaFlag = operaFlag;
        }

        public Object getStockNum() {
            return stockNum;
        }

        public void setStockNum(Object stockNum) {
            this.stockNum = stockNum;
        }
    }
}
