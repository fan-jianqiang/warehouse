package cn.km.warehouse.model;

import java.util.List;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/2/7
 */
public class RKListQDModel {

    /**
     * totalCount : 2
     * pageSize : 10
     * totalPage : 1
     * currPage : 1
     * list : [{"applicationBillId":1,"applicationId":1,"materialId":1,"shouldNum":5,"actualNum":5,"unitPrice":3,"totalPrice":15,"planArrtivalTime":"2021-03-30 12:00:00","productTime":null,"materialCode":"111111","materialName":"物料一","materialTypeName":null,"materialUnitName":"台","materialSpecs":"234","spare":"1","projectId":1,"projectName":"项目1","projectCode":"1","operaFlag":null,"hasMater":false},{"applicationBillId":2,"applicationId":1,"materialId":2,"shouldNum":8,"actualNum":8,"unitPrice":2.5,"totalPrice":20,"planArrtivalTime":"2021-03-30 12:00:00","productTime":null,"materialCode":"222222","materialName":"物料二","materialTypeName":null,"materialUnitName":"台","materialSpecs":"234","spare":"1","projectId":1,"projectName":"项目1","projectCode":"1","operaFlag":null,"hasMater":false}]
     */

    private int totalCount;
    private int pageSize;
    private int totalPage;
    private int currPage;
    private List<ListBean> list;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getCurrPage() {
        return currPage;
    }

    public void setCurrPage(int currPage) {
        this.currPage = currPage;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * applicationBillId : 1
         * applicationId : 1
         * materialId : 1
         * shouldNum : 5
         * actualNum : 5
         * unitPrice : 3
         * totalPrice : 15
         * planArrtivalTime : 2021-03-30 12:00:00
         * productTime : null
         * materialCode : 111111
         * materialName : 物料一
         * materialTypeName : null
         * materialUnitName : 台
         * materialSpecs : 234
         * spare : 1
         * projectId : 1
         * projectName : 项目1
         * projectCode : 1
         * operaFlag : null
         * hasMater : false
         */

        private int applicationBillId;
        private int applicationId;
        private int materialId;
        private int shouldNum;
        private int actualNum;
        private int unitPrice;
        private int totalPrice;
        private String planArrtivalTime;
        private Object productTime;
        private String materialCode;
        private String materialName;
        private Object materialTypeName;
        private String materialUnitName;
        private String materialSpecs;
        private String spare;
        private int projectId;
        private String projectName;
        private String projectCode;
        private Object operaFlag;
        private boolean hasMater;

        public int getApplicationBillId() {
            return applicationBillId;
        }

        public void setApplicationBillId(int applicationBillId) {
            this.applicationBillId = applicationBillId;
        }

        public int getApplicationId() {
            return applicationId;
        }

        public void setApplicationId(int applicationId) {
            this.applicationId = applicationId;
        }

        public int getMaterialId() {
            return materialId;
        }

        public void setMaterialId(int materialId) {
            this.materialId = materialId;
        }

        public int getShouldNum() {
            return shouldNum;
        }

        public void setShouldNum(int shouldNum) {
            this.shouldNum = shouldNum;
        }

        public int getActualNum() {
            return actualNum;
        }

        public void setActualNum(int actualNum) {
            this.actualNum = actualNum;
        }

        public int getUnitPrice() {
            return unitPrice;
        }

        public void setUnitPrice(int unitPrice) {
            this.unitPrice = unitPrice;
        }

        public int getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(int totalPrice) {
            this.totalPrice = totalPrice;
        }

        public String getPlanArrtivalTime() {
            return planArrtivalTime;
        }

        public void setPlanArrtivalTime(String planArrtivalTime) {
            this.planArrtivalTime = planArrtivalTime;
        }

        public Object getProductTime() {
            return productTime;
        }

        public void setProductTime(Object productTime) {
            this.productTime = productTime;
        }

        public String getMaterialCode() {
            return materialCode;
        }

        public void setMaterialCode(String materialCode) {
            this.materialCode = materialCode;
        }

        public String getMaterialName() {
            return materialName;
        }

        public void setMaterialName(String materialName) {
            this.materialName = materialName;
        }

        public Object getMaterialTypeName() {
            return materialTypeName;
        }

        public void setMaterialTypeName(Object materialTypeName) {
            this.materialTypeName = materialTypeName;
        }

        public String getMaterialUnitName() {
            return materialUnitName;
        }

        public void setMaterialUnitName(String materialUnitName) {
            this.materialUnitName = materialUnitName;
        }

        public String getMaterialSpecs() {
            return materialSpecs;
        }

        public void setMaterialSpecs(String materialSpecs) {
            this.materialSpecs = materialSpecs;
        }

        public String getSpare() {
            return spare;
        }

        public void setSpare(String spare) {
            this.spare = spare;
        }

        public int getProjectId() {
            return projectId;
        }

        public void setProjectId(int projectId) {
            this.projectId = projectId;
        }

        public String getProjectName() {
            return projectName;
        }

        public void setProjectName(String projectName) {
            this.projectName = projectName;
        }

        public String getProjectCode() {
            return projectCode;
        }

        public void setProjectCode(String projectCode) {
            this.projectCode = projectCode;
        }

        public Object getOperaFlag() {
            return operaFlag;
        }

        public void setOperaFlag(Object operaFlag) {
            this.operaFlag = operaFlag;
        }

        public boolean isHasMater() {
            return hasMater;
        }

        public void setHasMater(boolean hasMater) {
            this.hasMater = hasMater;
        }
    }
}
