package cn.km.warehouse.model;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/1/31
 */
public class LoginModel {

    /**
     * accountId : 10
     * source : okhttp/3.6.0
     * fromIp : 192.168.1.70
     * token : c865728dc4ef4661bb37c7d774575dfd
     * expireTime : 2021-02-02 14:51:57
     * updateTime : 2021-02-01 14:51:57
     * name : 张三
     */

    private int accountId;
    private String source;
    private String fromIp;
    private String token;
    private String expireTime;
    private String updateTime;
    private String name;

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getFromIp() {
        return fromIp;
    }

    public void setFromIp(String fromIp) {
        this.fromIp = fromIp;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(String expireTime) {
        this.expireTime = expireTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "LoginModel{" +
                "accountId=" + accountId +
                ", source='" + source + '\'' +
                ", fromIp='" + fromIp + '\'' +
                ", token='" + token + '\'' +
                ", expireTime='" + expireTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
