package cn.km.warehouse.model;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/16
 */
public class PDScanModel {

    /**
     * already : 12.0
     * sum : 16.0
     * info : {"stockId":598,"rfid":"1111110_1615628713347","epc":null,"materialId":1,"projectId":null,"warehouseId":1,"positionId":1,"shelfId":2,"entryNum":null,"stockNum":0,"deliveryNum":null,"entryType":null,"entryTime":null,"deliveryTime":null,"unitPrice":null,"productTime":null,"createrId":null,"createTime":null,"updaterId":null,"updateTime":null,"oldState":null,"applicationBillId":null,"oldRfid":null,"entryOrderId":null,"projectName":null,"orgName":null,"materialCode":"111111","materialName":"物料一","materialTypeName":null,"materialUnitName":"台","materialSpecs":"234","spare":null,"warrantyPeriod":null,"storageAge":null,"stockMonth":null,"shelfName":"库房1仓位1货架2","positionName":"库房1仓位1","warehouseName":"仓库1","updateUserName":null,"state":8}
     */

    private double already;
    private double sum;
    private InfoBean info;

    public double getAlready() {
        return already;
    }

    public void setAlready(double already) {
        this.already = already;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum = sum;
    }

    public InfoBean getInfo() {
        return info;
    }

    public void setInfo(InfoBean info) {
        this.info = info;
    }

    public static class InfoBean {
        /**
         * stockId : 598
         * rfid : 1111110_1615628713347
         * epc : null
         * materialId : 1
         * projectId : null
         * warehouseId : 1
         * positionId : 1
         * shelfId : 2
         * entryNum : null
         * stockNum : 0.0
         * deliveryNum : null
         * entryType : null
         * entryTime : null
         * deliveryTime : null
         * unitPrice : null
         * productTime : null
         * createrId : null
         * createTime : null
         * updaterId : null
         * updateTime : null
         * oldState : null
         * applicationBillId : null
         * oldRfid : null
         * entryOrderId : null
         * projectName : null
         * orgName : null
         * materialCode : 111111
         * materialName : 物料一
         * materialTypeName : null
         * materialUnitName : 台
         * materialSpecs : 234
         * spare : null
         * warrantyPeriod : null
         * storageAge : null
         * stockMonth : null
         * shelfName : 库房1仓位1货架2
         * positionName : 库房1仓位1
         * warehouseName : 仓库1
         * updateUserName : null
         * state : 8
         */

        private int stockId;
        private String rfid;
        private Object epc;
        private int materialId;
        private Object projectId;
        private int warehouseId;
        private int positionId;
        private int shelfId;
        private Object entryNum;
        private double stockNum;
        private Object deliveryNum;
        private Object entryType;
        private Object entryTime;
        private Object deliveryTime;
        private Object unitPrice;
        private Object productTime;
        private Object createrId;
        private Object createTime;
        private Object updaterId;
        private Object updateTime;
        private Object oldState;
        private Object applicationBillId;
        private Object oldRfid;
        private Object entryOrderId;
        private Object projectName;
        private Object orgName;
        private String materialCode;
        private String materialName;
        private Object materialTypeName;
        private String materialUnitName;
        private String materialSpecs;
        private Object spare;
        private Object warrantyPeriod;
        private Object storageAge;
        private Object stockMonth;
        private String shelfName;
        private String positionName;
        private String warehouseName;
        private Object updateUserName;
        private int state;

        public int getStockId() {
            return stockId;
        }

        public void setStockId(int stockId) {
            this.stockId = stockId;
        }

        public String getRfid() {
            return rfid;
        }

        public void setRfid(String rfid) {
            this.rfid = rfid;
        }

        public Object getEpc() {
            return epc;
        }

        public void setEpc(Object epc) {
            this.epc = epc;
        }

        public int getMaterialId() {
            return materialId;
        }

        public void setMaterialId(int materialId) {
            this.materialId = materialId;
        }

        public Object getProjectId() {
            return projectId;
        }

        public void setProjectId(Object projectId) {
            this.projectId = projectId;
        }

        public int getWarehouseId() {
            return warehouseId;
        }

        public void setWarehouseId(int warehouseId) {
            this.warehouseId = warehouseId;
        }

        public int getPositionId() {
            return positionId;
        }

        public void setPositionId(int positionId) {
            this.positionId = positionId;
        }

        public int getShelfId() {
            return shelfId;
        }

        public void setShelfId(int shelfId) {
            this.shelfId = shelfId;
        }

        public Object getEntryNum() {
            return entryNum;
        }

        public void setEntryNum(Object entryNum) {
            this.entryNum = entryNum;
        }

        public double getStockNum() {
            return stockNum;
        }

        public void setStockNum(double stockNum) {
            this.stockNum = stockNum;
        }

        public Object getDeliveryNum() {
            return deliveryNum;
        }

        public void setDeliveryNum(Object deliveryNum) {
            this.deliveryNum = deliveryNum;
        }

        public Object getEntryType() {
            return entryType;
        }

        public void setEntryType(Object entryType) {
            this.entryType = entryType;
        }

        public Object getEntryTime() {
            return entryTime;
        }

        public void setEntryTime(Object entryTime) {
            this.entryTime = entryTime;
        }

        public Object getDeliveryTime() {
            return deliveryTime;
        }

        public void setDeliveryTime(Object deliveryTime) {
            this.deliveryTime = deliveryTime;
        }

        public Object getUnitPrice() {
            return unitPrice;
        }

        public void setUnitPrice(Object unitPrice) {
            this.unitPrice = unitPrice;
        }

        public Object getProductTime() {
            return productTime;
        }

        public void setProductTime(Object productTime) {
            this.productTime = productTime;
        }

        public Object getCreaterId() {
            return createrId;
        }

        public void setCreaterId(Object createrId) {
            this.createrId = createrId;
        }

        public Object getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Object createTime) {
            this.createTime = createTime;
        }

        public Object getUpdaterId() {
            return updaterId;
        }

        public void setUpdaterId(Object updaterId) {
            this.updaterId = updaterId;
        }

        public Object getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Object updateTime) {
            this.updateTime = updateTime;
        }

        public Object getOldState() {
            return oldState;
        }

        public void setOldState(Object oldState) {
            this.oldState = oldState;
        }

        public Object getApplicationBillId() {
            return applicationBillId;
        }

        public void setApplicationBillId(Object applicationBillId) {
            this.applicationBillId = applicationBillId;
        }

        public Object getOldRfid() {
            return oldRfid;
        }

        public void setOldRfid(Object oldRfid) {
            this.oldRfid = oldRfid;
        }

        public Object getEntryOrderId() {
            return entryOrderId;
        }

        public void setEntryOrderId(Object entryOrderId) {
            this.entryOrderId = entryOrderId;
        }

        public Object getProjectName() {
            return projectName;
        }

        public void setProjectName(Object projectName) {
            this.projectName = projectName;
        }

        public Object getOrgName() {
            return orgName;
        }

        public void setOrgName(Object orgName) {
            this.orgName = orgName;
        }

        public String getMaterialCode() {
            return materialCode;
        }

        public void setMaterialCode(String materialCode) {
            this.materialCode = materialCode;
        }

        public String getMaterialName() {
            return materialName;
        }

        public void setMaterialName(String materialName) {
            this.materialName = materialName;
        }

        public Object getMaterialTypeName() {
            return materialTypeName;
        }

        public void setMaterialTypeName(Object materialTypeName) {
            this.materialTypeName = materialTypeName;
        }

        public String getMaterialUnitName() {
            return materialUnitName;
        }

        public void setMaterialUnitName(String materialUnitName) {
            this.materialUnitName = materialUnitName;
        }

        public String getMaterialSpecs() {
            return materialSpecs;
        }

        public void setMaterialSpecs(String materialSpecs) {
            this.materialSpecs = materialSpecs;
        }

        public Object getSpare() {
            return spare;
        }

        public void setSpare(Object spare) {
            this.spare = spare;
        }

        public Object getWarrantyPeriod() {
            return warrantyPeriod;
        }

        public void setWarrantyPeriod(Object warrantyPeriod) {
            this.warrantyPeriod = warrantyPeriod;
        }

        public Object getStorageAge() {
            return storageAge;
        }

        public void setStorageAge(Object storageAge) {
            this.storageAge = storageAge;
        }

        public Object getStockMonth() {
            return stockMonth;
        }

        public void setStockMonth(Object stockMonth) {
            this.stockMonth = stockMonth;
        }

        public String getShelfName() {
            return shelfName;
        }

        public void setShelfName(String shelfName) {
            this.shelfName = shelfName;
        }

        public String getPositionName() {
            return positionName;
        }

        public void setPositionName(String positionName) {
            this.positionName = positionName;
        }

        public String getWarehouseName() {
            return warehouseName;
        }

        public void setWarehouseName(String warehouseName) {
            this.warehouseName = warehouseName;
        }

        public Object getUpdateUserName() {
            return updateUserName;
        }

        public void setUpdateUserName(Object updateUserName) {
            this.updateUserName = updateUserName;
        }

        public int getState() {
            return state;
        }

        public void setState(int state) {
            this.state = state;
        }
    }
}
