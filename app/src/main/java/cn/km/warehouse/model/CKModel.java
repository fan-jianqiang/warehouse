package cn.km.warehouse.model;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/2/2
 */
public class CKModel {

    /**
     * id : 1
     * code : 11111
     * name : 宏业中低压变电站
     * orgId : 4
     * managerId : 10
     * status : 1
     * longLat : 116.329778,39.962995
     * buildingPlanPath :
     * createrId : 10
     * createTime : 2020-11-01 09:49:24
     * updaterId : 2
     * updateTime : 2021-01-26 14:21:47
     * imgPath : uploadMap/1_201fcdadd99f345745a774f29b9b514.png
     * orgName : null
     * managerName : null
     */

    private int id;
    private String code;
    private String name;
    private int orgId;
    private int managerId;
    private int status;
    private String longLat;
    private String buildingPlanPath;
    private int createrId;
    private String createTime;
    private int updaterId;
    private String updateTime;
    private String imgPath;
    private Object orgName;
    private Object managerName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOrgId() {
        return orgId;
    }

    public void setOrgId(int orgId) {
        this.orgId = orgId;
    }

    public int getManagerId() {
        return managerId;
    }

    public void setManagerId(int managerId) {
        this.managerId = managerId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getLongLat() {
        return longLat;
    }

    public void setLongLat(String longLat) {
        this.longLat = longLat;
    }

    public String getBuildingPlanPath() {
        return buildingPlanPath;
    }

    public void setBuildingPlanPath(String buildingPlanPath) {
        this.buildingPlanPath = buildingPlanPath;
    }

    public int getCreaterId() {
        return createrId;
    }

    public void setCreaterId(int createrId) {
        this.createrId = createrId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public int getUpdaterId() {
        return updaterId;
    }

    public void setUpdaterId(int updaterId) {
        this.updaterId = updaterId;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public Object getOrgName() {
        return orgName;
    }

    public void setOrgName(Object orgName) {
        this.orgName = orgName;
    }

    public Object getManagerName() {
        return managerName;
    }

    public void setManagerName(Object managerName) {
        this.managerName = managerName;
    }
}
