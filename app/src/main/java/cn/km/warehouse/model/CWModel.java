package cn.km.warehouse.model;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/2/25
 */
public class CWModel {

    /**
     * id : 1
     * code : 1
     * name : 库房1仓位1
     * warehouseId : 1
     * createrId : 1
     * createTime : 2021-01-28 15:29:01
     * updaterId : null
     * updateTime : null
     */

    private int id;
    private String code;
    private String name;
    private int warehouseId;
    private int createrId;
    private String createTime;
    private Object updaterId;
    private Object updateTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(int warehouseId) {
        this.warehouseId = warehouseId;
    }

    public int getCreaterId() {
        return createrId;
    }

    public void setCreaterId(int createrId) {
        this.createrId = createrId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Object getUpdaterId() {
        return updaterId;
    }

    public void setUpdaterId(Object updaterId) {
        this.updaterId = updaterId;
    }

    public Object getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Object updateTime) {
        this.updateTime = updateTime;
    }
}
