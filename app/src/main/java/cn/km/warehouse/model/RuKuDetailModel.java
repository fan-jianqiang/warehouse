package cn.km.warehouse.model;

import java.util.List;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/2/25
 */
public class RuKuDetailModel {

    /**
     * applicationBillId : 18
     * applicationId : 26
     * materialId : 1
     * shouldNum : 5.0
     * actualNum : 2.0
     * unitPrice : 2.0
     * totalPrice : 10.0
     * planArrtivalTime : null
     * productTime : 2021-03-11 16:52:31
     * orderId : 16
     * materialCode : 111111
     * materialName : 物料一
     * materialTypeName : null
     * materialUnitName : 台
     * materialSpecs : 234
     * spare : 1
     * projectId : 1
     * projectName : 项目1
     * projectCode : 1
     * stockList : [{"stockId":null,"rfid":null,"epc":null,"materialId":null,"projectId":null,"warehouseId":null,"positionId":1,"shelfId":2,"entryNum":2,"stockNum":null,"deliveryNum":null,"entryType":null,"entryTime":null,"deliveryTime":null,"unitPrice":null,"productTime":null,"createrId":null,"createTime":null,"updaterId":null,"updateTime":null,"oldState":null,"applicationBillId":null,"oldRfid":null,"entryOrderId":null,"projectName":null,"orgName":null,"materialCode":null,"materialName":null,"materialTypeName":null,"materialUnitName":null,"materialSpecs":null,"spare":null,"warrantyPeriod":null,"storageAge":null,"stockMonth":null,"shelfName":"库房1仓位1货架2","positionName":"库房1仓位1","warehouseName":null,"updateUserName":null,"state":null}]
     * operaFlag : null
     * hasMater : false
     */

    private int applicationBillId;
    private int applicationId;

    public String getApplicationCode() {
        return applicationCode;
    }

    public void setApplicationCode(String applicationCode) {
        this.applicationCode = applicationCode;
    }

    private String applicationCode;
    private int materialId;
    private double shouldNum;
    private double actualNum;
    private double unitPrice;
    private double totalPrice;
    private Object planArrtivalTime;
    private String productTime;
    private int orderId;
    private String materialCode;
    private String materialName;
    private Object materialTypeName;
    private String materialUnitName;
    private String materialSpecs;
    private String spare;
    private int projectId;
    private String projectName;
    private String projectCode;
    private Object operaFlag;
    private boolean hasMater;
    private List<StockListBean> stockList;

    public int getApplicationBillId() {
        return applicationBillId;
    }

    public void setApplicationBillId(int applicationBillId) {
        this.applicationBillId = applicationBillId;
    }

    public int getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(int applicationId) {
        this.applicationId = applicationId;
    }

    public int getMaterialId() {
        return materialId;
    }

    public void setMaterialId(int materialId) {
        this.materialId = materialId;
    }

    public double getShouldNum() {
        return shouldNum;
    }

    public void setShouldNum(double shouldNum) {
        this.shouldNum = shouldNum;
    }

    public double getActualNum() {
        return actualNum;
    }

    public void setActualNum(double actualNum) {
        this.actualNum = actualNum;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Object getPlanArrtivalTime() {
        return planArrtivalTime;
    }

    public void setPlanArrtivalTime(Object planArrtivalTime) {
        this.planArrtivalTime = planArrtivalTime;
    }

    public String getProductTime() {
        return productTime;
    }

    public void setProductTime(String productTime) {
        this.productTime = productTime;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getMaterialCode() {
        return materialCode;
    }

    public void setMaterialCode(String materialCode) {
        this.materialCode = materialCode;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public Object getMaterialTypeName() {
        return materialTypeName;
    }

    public void setMaterialTypeName(Object materialTypeName) {
        this.materialTypeName = materialTypeName;
    }

    public String getMaterialUnitName() {
        return materialUnitName;
    }

    public void setMaterialUnitName(String materialUnitName) {
        this.materialUnitName = materialUnitName;
    }

    public String getMaterialSpecs() {
        return materialSpecs;
    }

    public void setMaterialSpecs(String materialSpecs) {
        this.materialSpecs = materialSpecs;
    }

    public String getSpare() {
        return spare;
    }

    public void setSpare(String spare) {
        this.spare = spare;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public Object getOperaFlag() {
        return operaFlag;
    }

    public void setOperaFlag(Object operaFlag) {
        this.operaFlag = operaFlag;
    }

    public boolean isHasMater() {
        return hasMater;
    }

    public void setHasMater(boolean hasMater) {
        this.hasMater = hasMater;
    }

    public List<StockListBean> getStockList() {
        return stockList;
    }

    public void setStockList(List<StockListBean> stockList) {
        this.stockList = stockList;
    }

    public static class StockListBean {
        /**
         * stockId : null
         * rfid : null
         * epc : null
         * materialId : null
         * projectId : null
         * warehouseId : null
         * positionId : 1
         * shelfId : 2
         * entryNum : 2.0
         * stockNum : null
         * deliveryNum : null
         * entryType : null
         * entryTime : null
         * deliveryTime : null
         * unitPrice : null
         * productTime : null
         * createrId : null
         * createTime : null
         * updaterId : null
         * updateTime : null
         * oldState : null
         * applicationBillId : null
         * oldRfid : null
         * entryOrderId : null
         * projectName : null
         * orgName : null
         * materialCode : null
         * materialName : null
         * materialTypeName : null
         * materialUnitName : null
         * materialSpecs : null
         * spare : null
         * warrantyPeriod : null
         * storageAge : null
         * stockMonth : null
         * shelfName : 库房1仓位1货架2
         * positionName : 库房1仓位1
         * warehouseName : null
         * updateUserName : null
         * state : null
         */

        private Object stockId;
        private Object rfid;
        private Object epc;
        private Object materialId;
        private Object projectId;
        private Object warehouseId;
        private int positionId;
        private int shelfId;
        private double entryNum;
        private Object stockNum;
        private Object deliveryNum;
        private Object entryType;
        private Object entryTime;
        private Object deliveryTime;
        private Object unitPrice;
        private Object productTime;
        private Object createrId;
        private Object createTime;
        private Object updaterId;
        private Object updateTime;
        private Object oldState;
        private Object applicationBillId;
        private Object oldRfid;
        private Object entryOrderId;
        private Object projectName;
        private Object orgName;
        private Object materialCode;
        private Object materialName;
        private Object materialTypeName;
        private Object materialUnitName;
        private Object materialSpecs;
        private Object spare;
        private Object warrantyPeriod;
        private Object storageAge;
        private Object stockMonth;
        private String shelfName;
        private String positionName;
        private Object warehouseName;
        private Object updateUserName;
        private Object state;

        public Object getStockId() {
            return stockId;
        }

        public void setStockId(Object stockId) {
            this.stockId = stockId;
        }

        public Object getRfid() {
            return rfid;
        }

        public void setRfid(Object rfid) {
            this.rfid = rfid;
        }

        public Object getEpc() {
            return epc;
        }

        public void setEpc(Object epc) {
            this.epc = epc;
        }

        public Object getMaterialId() {
            return materialId;
        }

        public void setMaterialId(Object materialId) {
            this.materialId = materialId;
        }

        public Object getProjectId() {
            return projectId;
        }

        public void setProjectId(Object projectId) {
            this.projectId = projectId;
        }

        public Object getWarehouseId() {
            return warehouseId;
        }

        public void setWarehouseId(Object warehouseId) {
            this.warehouseId = warehouseId;
        }

        public int getPositionId() {
            return positionId;
        }

        public void setPositionId(int positionId) {
            this.positionId = positionId;
        }

        public int getShelfId() {
            return shelfId;
        }

        public void setShelfId(int shelfId) {
            this.shelfId = shelfId;
        }

        public double getEntryNum() {
            return entryNum;
        }

        public void setEntryNum(double entryNum) {
            this.entryNum = entryNum;
        }

        public Object getStockNum() {
            return stockNum;
        }

        public void setStockNum(Object stockNum) {
            this.stockNum = stockNum;
        }

        public Object getDeliveryNum() {
            return deliveryNum;
        }

        public void setDeliveryNum(Object deliveryNum) {
            this.deliveryNum = deliveryNum;
        }

        public Object getEntryType() {
            return entryType;
        }

        public void setEntryType(Object entryType) {
            this.entryType = entryType;
        }

        public Object getEntryTime() {
            return entryTime;
        }

        public void setEntryTime(Object entryTime) {
            this.entryTime = entryTime;
        }

        public Object getDeliveryTime() {
            return deliveryTime;
        }

        public void setDeliveryTime(Object deliveryTime) {
            this.deliveryTime = deliveryTime;
        }

        public Object getUnitPrice() {
            return unitPrice;
        }

        public void setUnitPrice(Object unitPrice) {
            this.unitPrice = unitPrice;
        }

        public Object getProductTime() {
            return productTime;
        }

        public void setProductTime(Object productTime) {
            this.productTime = productTime;
        }

        public Object getCreaterId() {
            return createrId;
        }

        public void setCreaterId(Object createrId) {
            this.createrId = createrId;
        }

        public Object getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Object createTime) {
            this.createTime = createTime;
        }

        public Object getUpdaterId() {
            return updaterId;
        }

        public void setUpdaterId(Object updaterId) {
            this.updaterId = updaterId;
        }

        public Object getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Object updateTime) {
            this.updateTime = updateTime;
        }

        public Object getOldState() {
            return oldState;
        }

        public void setOldState(Object oldState) {
            this.oldState = oldState;
        }

        public Object getApplicationBillId() {
            return applicationBillId;
        }

        public void setApplicationBillId(Object applicationBillId) {
            this.applicationBillId = applicationBillId;
        }

        public Object getOldRfid() {
            return oldRfid;
        }

        public void setOldRfid(Object oldRfid) {
            this.oldRfid = oldRfid;
        }

        public Object getEntryOrderId() {
            return entryOrderId;
        }

        public void setEntryOrderId(Object entryOrderId) {
            this.entryOrderId = entryOrderId;
        }

        public Object getProjectName() {
            return projectName;
        }

        public void setProjectName(Object projectName) {
            this.projectName = projectName;
        }

        public Object getOrgName() {
            return orgName;
        }

        public void setOrgName(Object orgName) {
            this.orgName = orgName;
        }

        public Object getMaterialCode() {
            return materialCode;
        }

        public void setMaterialCode(Object materialCode) {
            this.materialCode = materialCode;
        }

        public Object getMaterialName() {
            return materialName;
        }

        public void setMaterialName(Object materialName) {
            this.materialName = materialName;
        }

        public Object getMaterialTypeName() {
            return materialTypeName;
        }

        public void setMaterialTypeName(Object materialTypeName) {
            this.materialTypeName = materialTypeName;
        }

        public Object getMaterialUnitName() {
            return materialUnitName;
        }

        public void setMaterialUnitName(Object materialUnitName) {
            this.materialUnitName = materialUnitName;
        }

        public Object getMaterialSpecs() {
            return materialSpecs;
        }

        public void setMaterialSpecs(Object materialSpecs) {
            this.materialSpecs = materialSpecs;
        }

        public Object getSpare() {
            return spare;
        }

        public void setSpare(Object spare) {
            this.spare = spare;
        }

        public Object getWarrantyPeriod() {
            return warrantyPeriod;
        }

        public void setWarrantyPeriod(Object warrantyPeriod) {
            this.warrantyPeriod = warrantyPeriod;
        }

        public Object getStorageAge() {
            return storageAge;
        }

        public void setStorageAge(Object storageAge) {
            this.storageAge = storageAge;
        }

        public Object getStockMonth() {
            return stockMonth;
        }

        public void setStockMonth(Object stockMonth) {
            this.stockMonth = stockMonth;
        }

        public String getShelfName() {
            return shelfName;
        }

        public void setShelfName(String shelfName) {
            this.shelfName = shelfName;
        }

        public String getPositionName() {
            return positionName;
        }

        public void setPositionName(String positionName) {
            this.positionName = positionName;
        }

        public Object getWarehouseName() {
            return warehouseName;
        }

        public void setWarehouseName(Object warehouseName) {
            this.warehouseName = warehouseName;
        }

        public Object getUpdateUserName() {
            return updateUserName;
        }

        public void setUpdateUserName(Object updateUserName) {
            this.updateUserName = updateUserName;
        }

        public Object getState() {
            return state;
        }

        public void setState(Object state) {
            this.state = state;
        }
    }
}
