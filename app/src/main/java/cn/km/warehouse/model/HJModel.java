package cn.km.warehouse.model;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/2/25
 */
public class HJModel {

    /**
     * id : 1
     * code : 111
     * name : 库房1仓位1货架1
     * warehousePositionId : 1
     * createrId : 1
     * createTime : 2020-11-10 18:26:13
     * updaterId : 1
     * updateTime : 2020-11-11 15:28:41
     * positionName : 库房1仓位1
     * warehouseName : 宏业中低压变电站
     * organizationName : 四川宏业电力集团有限公司
     */

    private int id;
    private String code;
    private String name;
    private int warehousePositionId;
    private int createrId;
    private String createTime;
    private int updaterId;
    private String updateTime;
    private String positionName;
    private String warehouseName;
    private String organizationName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWarehousePositionId() {
        return warehousePositionId;
    }

    public void setWarehousePositionId(int warehousePositionId) {
        this.warehousePositionId = warehousePositionId;
    }

    public int getCreaterId() {
        return createrId;
    }

    public void setCreaterId(int createrId) {
        this.createrId = createrId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public int getUpdaterId() {
        return updaterId;
    }

    public void setUpdaterId(int updaterId) {
        this.updaterId = updaterId;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }
}
