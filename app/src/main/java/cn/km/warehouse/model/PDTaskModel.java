package cn.km.warehouse.model;

import java.util.List;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/7
 */
public class PDTaskModel {

    /**
     * totalCount : 2
     * pageSize : 10
     * totalPage : 1
     * currPage : 1
     * list : [{"taskId":7,"taskCode":"PD23234","beginDate":"2020-11-20 20:20:20","endDate":"2020-12-20 20:00:00","countMonth":"2020-12","accountId":2,"taskDesc":"盘点任务3","status":0,"createrId":3,"createTime":"2020-11-20 10:50:56","updaterId":null,"updateTime":null,"chargeUserName":"管理员"},{"taskId":8,"taskCode":"","beginDate":"2020-11-02 20:12:12","endDate":"2020-11-12 20:12:12","countMonth":"2020-11","accountId":0,"taskDesc":"","status":0,"createrId":3,"createTime":"2020-11-20 10:51:58","updaterId":0,"updateTime":null,"chargeUserName":null}]
     */

    private int totalCount;
    private int pageSize;
    private int totalPage;
    private int currPage;
    private List<ListBean> list;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getCurrPage() {
        return currPage;
    }

    public void setCurrPage(int currPage) {
        this.currPage = currPage;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * taskId : 7
         * taskCode : PD23234
         * beginDate : 2020-11-20 20:20:20
         * endDate : 2020-12-20 20:00:00
         * countMonth : 2020-12
         * accountId : 2
         * taskDesc : 盘点任务3
         * status : 0
         * createrId : 3
         * createTime : 2020-11-20 10:50:56
         * updaterId : null
         * updateTime : null
         * chargeUserName : 管理员
         */

        private int taskId;
        private String taskCode;
        private String beginDate;
        private String endDate;
        private String countMonth;
        private int accountId;
        private String taskDesc;
        private int status;
        private int createrId;
        private String createTime;
        private Object updaterId;
        private Object updateTime;
        private String chargeUserName;

        public int getTaskId() {
            return taskId;
        }

        public void setTaskId(int taskId) {
            this.taskId = taskId;
        }

        public String getTaskCode() {
            return taskCode;
        }

        public void setTaskCode(String taskCode) {
            this.taskCode = taskCode;
        }

        public String getBeginDate() {
            return beginDate;
        }

        public void setBeginDate(String beginDate) {
            this.beginDate = beginDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public String getCountMonth() {
            return countMonth;
        }

        public void setCountMonth(String countMonth) {
            this.countMonth = countMonth;
        }

        public int getAccountId() {
            return accountId;
        }

        public void setAccountId(int accountId) {
            this.accountId = accountId;
        }

        public String getTaskDesc() {
            return taskDesc;
        }

        public void setTaskDesc(String taskDesc) {
            this.taskDesc = taskDesc;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getCreaterId() {
            return createrId;
        }

        public void setCreaterId(int createrId) {
            this.createrId = createrId;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public Object getUpdaterId() {
            return updaterId;
        }

        public void setUpdaterId(Object updaterId) {
            this.updaterId = updaterId;
        }

        public Object getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Object updateTime) {
            this.updateTime = updateTime;
        }

        public String getChargeUserName() {
            return chargeUserName;
        }

        public void setChargeUserName(String chargeUserName) {
            this.chargeUserName = chargeUserName;
        }
    }
}
