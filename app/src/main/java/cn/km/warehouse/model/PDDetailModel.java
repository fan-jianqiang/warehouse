package cn.km.warehouse.model;

import java.util.List;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/9
 */
public class PDDetailModel {

    /**
     * totalCount : 1
     * pageSize : 10
     * totalPage : 1
     * currPage : 1
     * list : [{"detailId":1,"taskId":7,"materialId":3,"rfid":null,"projectId":2,"warehouseId":2,"inventoryNum":2,"remark":"qewqe","projectCode":"2","projectName":"项目2","materialCode":"aassdd123","materialName":"1","materialSpecs":"1","materialUnitName":"台","orgId":1,"orgName":"四川电力集团","warehouseName":"仓库2"}]
     */

    private int totalCount;
    private int pageSize;
    private int totalPage;
    private int currPage;
    private List<ListBean> list;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getCurrPage() {
        return currPage;
    }

    public void setCurrPage(int currPage) {
        this.currPage = currPage;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * detailId : 1
         * taskId : 7
         * materialId : 3
         * rfid : null
         * projectId : 2
         * warehouseId : 2
         * inventoryNum : 2.0
         * remark : qewqe
         * projectCode : 2
         * projectName : 项目2
         * materialCode : aassdd123
         * materialName : 1
         * materialSpecs : 1
         * materialUnitName : 台
         * orgId : 1
         * orgName : 四川电力集团
         * warehouseName : 仓库2
         */

        private int detailId;
        private int taskId;
        private int materialId;
        private Object rfid;
        private int projectId;
        private int warehouseId;
        private double inventoryNum;
        private String remark;
        private String projectCode;
        private String projectName;
        private String materialCode;
        private String materialName;
        private String materialSpecs;
        private String materialUnitName;
        private int orgId;
        private String orgName;
        private String warehouseName;

        public int getDetailId() {
            return detailId;
        }

        public void setDetailId(int detailId) {
            this.detailId = detailId;
        }

        public int getTaskId() {
            return taskId;
        }

        public void setTaskId(int taskId) {
            this.taskId = taskId;
        }

        public int getMaterialId() {
            return materialId;
        }

        public void setMaterialId(int materialId) {
            this.materialId = materialId;
        }

        public Object getRfid() {
            return rfid;
        }

        public void setRfid(Object rfid) {
            this.rfid = rfid;
        }

        public int getProjectId() {
            return projectId;
        }

        public void setProjectId(int projectId) {
            this.projectId = projectId;
        }

        public int getWarehouseId() {
            return warehouseId;
        }

        public void setWarehouseId(int warehouseId) {
            this.warehouseId = warehouseId;
        }

        public double getInventoryNum() {
            return inventoryNum;
        }

        public void setInventoryNum(double inventoryNum) {
            this.inventoryNum = inventoryNum;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getProjectCode() {
            return projectCode;
        }

        public void setProjectCode(String projectCode) {
            this.projectCode = projectCode;
        }

        public String getProjectName() {
            return projectName;
        }

        public void setProjectName(String projectName) {
            this.projectName = projectName;
        }

        public String getMaterialCode() {
            return materialCode;
        }

        public void setMaterialCode(String materialCode) {
            this.materialCode = materialCode;
        }

        public String getMaterialName() {
            return materialName;
        }

        public void setMaterialName(String materialName) {
            this.materialName = materialName;
        }

        public String getMaterialSpecs() {
            return materialSpecs;
        }

        public void setMaterialSpecs(String materialSpecs) {
            this.materialSpecs = materialSpecs;
        }

        public String getMaterialUnitName() {
            return materialUnitName;
        }

        public void setMaterialUnitName(String materialUnitName) {
            this.materialUnitName = materialUnitName;
        }

        public int getOrgId() {
            return orgId;
        }

        public void setOrgId(int orgId) {
            this.orgId = orgId;
        }

        public String getOrgName() {
            return orgName;
        }

        public void setOrgName(String orgName) {
            this.orgName = orgName;
        }

        public String getWarehouseName() {
            return warehouseName;
        }

        public void setWarehouseName(String warehouseName) {
            this.warehouseName = warehouseName;
        }
    }
}
