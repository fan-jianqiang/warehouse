package cn.km.warehouse.model;

import java.util.List;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/7
 */
public class CKQDModel {

    /**
     * totalCount : 1
     * pageSize : 10
     * totalPage : 1
     * currPage : 1
     * list : [{"orderId":2,"orderCode":"CK4574764567","applicationId":10,"projectId":0,"warehouseId":1,"orderState":2,"lastDeliveryTime":null,"createrId":3,"createTime":"2020-11-21 19:12:30","updaterId":null,"updateTime":null,"applicationCode":"CKSQ1605955176856","applicationDatetime":"2020-11-21 18:39:37","projectName":"项目3","warehouseName":"仓库1","organizationName":"四川电力集团","applicationAccountName":"张三","houseUserName":null,"totalPrice":null}]
     */

    private int totalCount;
    private int pageSize;
    private int totalPage;
    private int currPage;
    private List<ListBean> list;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getCurrPage() {
        return currPage;
    }

    public void setCurrPage(int currPage) {
        this.currPage = currPage;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * orderId : 2
         * orderCode : CK4574764567
         * applicationId : 10
         * projectId : 0
         * warehouseId : 1
         * orderState : 2
         * lastDeliveryTime : null
         * createrId : 3
         * createTime : 2020-11-21 19:12:30
         * updaterId : null
         * updateTime : null
         * applicationCode : CKSQ1605955176856
         * applicationDatetime : 2020-11-21 18:39:37
         * projectName : 项目3
         * warehouseName : 仓库1
         * organizationName : 四川电力集团
         * applicationAccountName : 张三
         * houseUserName : null
         * totalPrice : null
         */

        private int orderId;
        private String orderCode;
        private int applicationId;
        private int projectId;
        private int warehouseId;
        private int orderState;
        private Object lastDeliveryTime;
        private int createrId;
        private String createTime;
        private Object updaterId;
        private Object updateTime;
        private String applicationCode;
        private String applicationDatetime;
        private String projectName;
        private String warehouseName;
        private String organizationName;
        private String applicationAccountName;
        private Object houseUserName;
        private Object totalPrice;

        public int getOrderId() {
            return orderId;
        }

        public void setOrderId(int orderId) {
            this.orderId = orderId;
        }

        public String getOrderCode() {
            return orderCode;
        }

        public void setOrderCode(String orderCode) {
            this.orderCode = orderCode;
        }

        public int getApplicationId() {
            return applicationId;
        }

        public void setApplicationId(int applicationId) {
            this.applicationId = applicationId;
        }

        public int getProjectId() {
            return projectId;
        }

        public void setProjectId(int projectId) {
            this.projectId = projectId;
        }

        public int getWarehouseId() {
            return warehouseId;
        }

        public void setWarehouseId(int warehouseId) {
            this.warehouseId = warehouseId;
        }

        public int getOrderState() {
            return orderState;
        }

        public void setOrderState(int orderState) {
            this.orderState = orderState;
        }

        public Object getLastDeliveryTime() {
            return lastDeliveryTime;
        }

        public void setLastDeliveryTime(Object lastDeliveryTime) {
            this.lastDeliveryTime = lastDeliveryTime;
        }

        public int getCreaterId() {
            return createrId;
        }

        public void setCreaterId(int createrId) {
            this.createrId = createrId;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public Object getUpdaterId() {
            return updaterId;
        }

        public void setUpdaterId(Object updaterId) {
            this.updaterId = updaterId;
        }

        public Object getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Object updateTime) {
            this.updateTime = updateTime;
        }

        public String getApplicationCode() {
            return applicationCode;
        }

        public void setApplicationCode(String applicationCode) {
            this.applicationCode = applicationCode;
        }

        public String getApplicationDatetime() {
            return applicationDatetime;
        }

        public void setApplicationDatetime(String applicationDatetime) {
            this.applicationDatetime = applicationDatetime;
        }

        public String getProjectName() {
            return projectName;
        }

        public void setProjectName(String projectName) {
            this.projectName = projectName;
        }

        public String getWarehouseName() {
            return warehouseName;
        }

        public void setWarehouseName(String warehouseName) {
            this.warehouseName = warehouseName;
        }

        public String getOrganizationName() {
            return organizationName;
        }

        public void setOrganizationName(String organizationName) {
            this.organizationName = organizationName;
        }

        public String getApplicationAccountName() {
            return applicationAccountName;
        }

        public void setApplicationAccountName(String applicationAccountName) {
            this.applicationAccountName = applicationAccountName;
        }

        public Object getHouseUserName() {
            return houseUserName;
        }

        public void setHouseUserName(Object houseUserName) {
            this.houseUserName = houseUserName;
        }

        public Object getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(Object totalPrice) {
            this.totalPrice = totalPrice;
        }
    }
}
