package cn.km.warehouse.model;

import java.util.List;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/13
 */
public class CKScanModel {


    /**
     * delivery : {"applicationBillId":null,"applicationId":4,"projectId":null,"materialId":null,"warehouseId":null,"positionId":null,"shelfId":null,"shouldNum":2,"actualNum":2,"unitPrice":null,"totalPrice":null,"orderId":1,"positionName":null,"shelfName":null,"materialCode":"111111","materialName":"物料一","materialTypeName":null,"materialUnitName":"台","materialSpecs":"234","projectName":null,"projectCode":null,"stockList":null,"operaFlag":null,"stockNum":null}
     * list : [{"applicationBillId":null,"applicationId":4,"projectId":null,"materialId":1,"warehouseId":null,"positionId":1,"shelfId":1,"shouldNum":2,"actualNum":2,"unitPrice":null,"totalPrice":null,"orderId":1,"positionName":"库房1仓位1","shelfName":"库房1仓位1货架1","materialCode":"111111","materialName":"物料一","materialTypeName":null,"materialUnitName":"台","materialSpecs":"234","projectName":null,"projectCode":null,"stockList":null,"operaFlag":null,"stockNum":null}]
     */

    private DeliveryBean delivery;
    private List<ListBean> list;

    public DeliveryBean getDelivery() {
        return delivery;
    }

    public void setDelivery(DeliveryBean delivery) {
        this.delivery = delivery;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class DeliveryBean {
        /**
         * applicationBillId : null
         * applicationId : 4
         * projectId : null
         * materialId : null
         * warehouseId : null
         * positionId : null
         * shelfId : null
         * shouldNum : 2.0
         * actualNum : 2.0
         * unitPrice : null
         * totalPrice : null
         * orderId : 1
         * positionName : null
         * shelfName : null
         * materialCode : 111111
         * materialName : 物料一
         * materialTypeName : null
         * materialUnitName : 台
         * materialSpecs : 234
         * projectName : null
         * projectCode : null
         * stockList : null
         * operaFlag : null
         * stockNum : null
         */

        private Object applicationBillId;
        private int applicationId;
        private Object projectId;
        private Object materialId;
        private Object warehouseId;
        private Object positionId;
        private Object shelfId;
        private double shouldNum;
        private double actualNum;
        private Object unitPrice;
        private Object totalPrice;
        private int orderId;
        private Object positionName;
        private Object shelfName;
        private String materialCode;

        public String getApplicationCode() {
            return applicationCode;
        }

        public void setApplicationCode(String applicationCode) {
            this.applicationCode = applicationCode;
        }

        private String applicationCode;
        private String materialName;
        private Object materialTypeName;
        private String materialUnitName;
        private String materialSpecs;
        private Object projectName;
        private Object projectCode;
        private Object stockList;
        private Object operaFlag;
        private Object stockNum;

        public Object getApplicationBillId() {
            return applicationBillId;
        }

        public void setApplicationBillId(Object applicationBillId) {
            this.applicationBillId = applicationBillId;
        }

        public int getApplicationId() {
            return applicationId;
        }

        public void setApplicationId(int applicationId) {
            this.applicationId = applicationId;
        }

        public Object getProjectId() {
            return projectId;
        }

        public void setProjectId(Object projectId) {
            this.projectId = projectId;
        }

        public Object getMaterialId() {
            return materialId;
        }

        public void setMaterialId(Object materialId) {
            this.materialId = materialId;
        }

        public Object getWarehouseId() {
            return warehouseId;
        }

        public void setWarehouseId(Object warehouseId) {
            this.warehouseId = warehouseId;
        }

        public Object getPositionId() {
            return positionId;
        }

        public void setPositionId(Object positionId) {
            this.positionId = positionId;
        }

        public Object getShelfId() {
            return shelfId;
        }

        public void setShelfId(Object shelfId) {
            this.shelfId = shelfId;
        }

        public double getShouldNum() {
            return shouldNum;
        }

        public void setShouldNum(double shouldNum) {
            this.shouldNum = shouldNum;
        }

        public double getActualNum() {
            return actualNum;
        }

        public void setActualNum(double actualNum) {
            this.actualNum = actualNum;
        }

        public Object getUnitPrice() {
            return unitPrice;
        }

        public void setUnitPrice(Object unitPrice) {
            this.unitPrice = unitPrice;
        }

        public Object getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(Object totalPrice) {
            this.totalPrice = totalPrice;
        }

        public int getOrderId() {
            return orderId;
        }

        public void setOrderId(int orderId) {
            this.orderId = orderId;
        }

        public Object getPositionName() {
            return positionName;
        }

        public void setPositionName(Object positionName) {
            this.positionName = positionName;
        }

        public Object getShelfName() {
            return shelfName;
        }

        public void setShelfName(Object shelfName) {
            this.shelfName = shelfName;
        }

        public String getMaterialCode() {
            return materialCode;
        }

        public void setMaterialCode(String materialCode) {
            this.materialCode = materialCode;
        }

        public String getMaterialName() {
            return materialName;
        }

        public void setMaterialName(String materialName) {
            this.materialName = materialName;
        }

        public Object getMaterialTypeName() {
            return materialTypeName;
        }

        public void setMaterialTypeName(Object materialTypeName) {
            this.materialTypeName = materialTypeName;
        }

        public String getMaterialUnitName() {
            return materialUnitName;
        }

        public void setMaterialUnitName(String materialUnitName) {
            this.materialUnitName = materialUnitName;
        }

        public String getMaterialSpecs() {
            return materialSpecs;
        }

        public void setMaterialSpecs(String materialSpecs) {
            this.materialSpecs = materialSpecs;
        }

        public Object getProjectName() {
            return projectName;
        }

        public void setProjectName(Object projectName) {
            this.projectName = projectName;
        }

        public Object getProjectCode() {
            return projectCode;
        }

        public void setProjectCode(Object projectCode) {
            this.projectCode = projectCode;
        }

        public Object getStockList() {
            return stockList;
        }

        public void setStockList(Object stockList) {
            this.stockList = stockList;
        }

        public Object getOperaFlag() {
            return operaFlag;
        }

        public void setOperaFlag(Object operaFlag) {
            this.operaFlag = operaFlag;
        }

        public Object getStockNum() {
            return stockNum;
        }

        public void setStockNum(Object stockNum) {
            this.stockNum = stockNum;
        }
    }

    public static class ListBean {
        /**
         * applicationBillId : null
         * applicationId : 4
         * projectId : null
         * materialId : 1
         * warehouseId : null
         * positionId : 1
         * shelfId : 1
         * shouldNum : 2.0
         * actualNum : 2.0
         * unitPrice : null
         * totalPrice : null
         * orderId : 1
         * positionName : 库房1仓位1
         * shelfName : 库房1仓位1货架1
         * materialCode : 111111
         * materialName : 物料一
         * materialTypeName : null
         * materialUnitName : 台
         * materialSpecs : 234
         * projectName : null
         * projectCode : null
         * stockList : null
         * operaFlag : null
         * stockNum : null
         */

        private Object applicationBillId;
        private int applicationId;
        private Object projectId;
        private int materialId;
        private Object warehouseId;
        private int positionId;
        private int shelfId;
        private double shouldNum;
        private double actualNum;
        private Object unitPrice;
        private Object totalPrice;
        private int orderId;
        private String positionName;
        private String shelfName;
        private String materialCode;
        private String materialName;
        private Object materialTypeName;
        private String materialUnitName;
        private String materialSpecs;
        private Object projectName;
        private Object projectCode;
        private Object stockList;
        private Object operaFlag;
        private Object stockNum;

        public Object getApplicationBillId() {
            return applicationBillId;
        }

        public void setApplicationBillId(Object applicationBillId) {
            this.applicationBillId = applicationBillId;
        }

        public int getApplicationId() {
            return applicationId;
        }

        public void setApplicationId(int applicationId) {
            this.applicationId = applicationId;
        }

        public Object getProjectId() {
            return projectId;
        }

        public void setProjectId(Object projectId) {
            this.projectId = projectId;
        }

        public int getMaterialId() {
            return materialId;
        }

        public void setMaterialId(int materialId) {
            this.materialId = materialId;
        }

        public Object getWarehouseId() {
            return warehouseId;
        }

        public void setWarehouseId(Object warehouseId) {
            this.warehouseId = warehouseId;
        }

        public int getPositionId() {
            return positionId;
        }

        public void setPositionId(int positionId) {
            this.positionId = positionId;
        }

        public int getShelfId() {
            return shelfId;
        }

        public void setShelfId(int shelfId) {
            this.shelfId = shelfId;
        }

        public double getShouldNum() {
            return shouldNum;
        }

        public void setShouldNum(double shouldNum) {
            this.shouldNum = shouldNum;
        }

        public double getActualNum() {
            return actualNum;
        }

        public void setActualNum(double actualNum) {
            this.actualNum = actualNum;
        }

        public Object getUnitPrice() {
            return unitPrice;
        }

        public void setUnitPrice(Object unitPrice) {
            this.unitPrice = unitPrice;
        }

        public Object getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(Object totalPrice) {
            this.totalPrice = totalPrice;
        }

        public int getOrderId() {
            return orderId;
        }

        public void setOrderId(int orderId) {
            this.orderId = orderId;
        }

        public String getPositionName() {
            return positionName;
        }

        public void setPositionName(String positionName) {
            this.positionName = positionName;
        }

        public String getShelfName() {
            return shelfName;
        }

        public void setShelfName(String shelfName) {
            this.shelfName = shelfName;
        }

        public String getMaterialCode() {
            return materialCode;
        }

        public void setMaterialCode(String materialCode) {
            this.materialCode = materialCode;
        }

        public String getMaterialName() {
            return materialName;
        }

        public void setMaterialName(String materialName) {
            this.materialName = materialName;
        }

        public Object getMaterialTypeName() {
            return materialTypeName;
        }

        public void setMaterialTypeName(Object materialTypeName) {
            this.materialTypeName = materialTypeName;
        }

        public String getMaterialUnitName() {
            return materialUnitName;
        }

        public void setMaterialUnitName(String materialUnitName) {
            this.materialUnitName = materialUnitName;
        }

        public String getMaterialSpecs() {
            return materialSpecs;
        }

        public void setMaterialSpecs(String materialSpecs) {
            this.materialSpecs = materialSpecs;
        }

        public Object getProjectName() {
            return projectName;
        }

        public void setProjectName(Object projectName) {
            this.projectName = projectName;
        }

        public Object getProjectCode() {
            return projectCode;
        }

        public void setProjectCode(Object projectCode) {
            this.projectCode = projectCode;
        }

        public Object getStockList() {
            return stockList;
        }

        public void setStockList(Object stockList) {
            this.stockList = stockList;
        }

        public Object getOperaFlag() {
            return operaFlag;
        }

        public void setOperaFlag(Object operaFlag) {
            this.operaFlag = operaFlag;
        }

        public Object getStockNum() {
            return stockNum;
        }

        public void setStockNum(Object stockNum) {
            this.stockNum = stockNum;
        }
    }
}
