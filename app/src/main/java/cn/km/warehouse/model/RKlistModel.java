package cn.km.warehouse.model;

import java.util.List;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/1/26
 */
public class RKlistModel {

    /**
     * totalCount : 1
     * pageSize : 10
     * totalPage : 1
     * currPage : 1
     * list : [{"orderId":1,"orderCode":"RK1243456456","applicationId":1,"entryTime":null,"houseUserId":null,"orderState":2,"createrId":11,"createTime":"2021-02-23 15:04:12","updaterId":null,"updateTime":null,"orderNum":null,"actualNum":null,"applicationCode":"RKSQ1614062451034","applicationDatetime":"2021-02-23 14:40:51","projectName":"项目1","warehouseName":"宏业中低压变电站","organizationName":"四川电力集团","applicationAccountName":"李四","houseUserName":null,"entryTypeName":"物料状态"}]
     */

    private int totalCount;
    private int pageSize;
    private int totalPage;
    private int currPage;
    private List<ListBean> list;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getCurrPage() {
        return currPage;
    }

    public void setCurrPage(int currPage) {
        this.currPage = currPage;
    }

    public List<ListBean> getList() {
        return list;
    }

    public void setList(List<ListBean> list) {
        this.list = list;
    }

    public static class ListBean {
        /**
         * orderId : 1
         * orderCode : RK1243456456
         * applicationId : 1
         * entryTime : null
         * houseUserId : null
         * orderState : 2
         * createrId : 11
         * createTime : 2021-02-23 15:04:12
         * updaterId : null
         * updateTime : null
         * orderNum : null
         * actualNum : null
         * applicationCode : RKSQ1614062451034
         * applicationDatetime : 2021-02-23 14:40:51
         * projectName : 项目1
         * warehouseName : 宏业中低压变电站
         * organizationName : 四川电力集团
         * applicationAccountName : 李四
         * houseUserName : null
         * entryTypeName : 物料状态
         */

        private int orderId;
        private String orderCode;
        private int applicationId;
        private Object entryTime;
        private Object houseUserId;
        private int orderState;
        private int createrId;
        private String createTime;
        private Object updaterId;
        private Object updateTime;
        private Object orderNum;
        private Object actualNum;
        private String applicationCode;
        private String applicationDatetime;
        private String projectName;
        private String warehouseName;
        private String organizationName;
        private String applicationAccountName;
        private Object houseUserName;
        private String entryTypeName;

        public int getOrderId() {
            return orderId;
        }

        public void setOrderId(int orderId) {
            this.orderId = orderId;
        }

        public String getOrderCode() {
            return orderCode;
        }

        public void setOrderCode(String orderCode) {
            this.orderCode = orderCode;
        }

        public int getApplicationId() {
            return applicationId;
        }

        public void setApplicationId(int applicationId) {
            this.applicationId = applicationId;
        }

        public Object getEntryTime() {
            return entryTime;
        }

        public void setEntryTime(Object entryTime) {
            this.entryTime = entryTime;
        }

        public Object getHouseUserId() {
            return houseUserId;
        }

        public void setHouseUserId(Object houseUserId) {
            this.houseUserId = houseUserId;
        }

        public int getOrderState() {
            return orderState;
        }

        public void setOrderState(int orderState) {
            this.orderState = orderState;
        }

        public int getCreaterId() {
            return createrId;
        }

        public void setCreaterId(int createrId) {
            this.createrId = createrId;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public Object getUpdaterId() {
            return updaterId;
        }

        public void setUpdaterId(Object updaterId) {
            this.updaterId = updaterId;
        }

        public Object getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Object updateTime) {
            this.updateTime = updateTime;
        }

        public Object getOrderNum() {
            return orderNum;
        }

        public void setOrderNum(Object orderNum) {
            this.orderNum = orderNum;
        }

        public Object getActualNum() {
            return actualNum;
        }

        public void setActualNum(Object actualNum) {
            this.actualNum = actualNum;
        }

        public String getApplicationCode() {
            return applicationCode;
        }

        public void setApplicationCode(String applicationCode) {
            this.applicationCode = applicationCode;
        }

        public String getApplicationDatetime() {
            return applicationDatetime;
        }

        public void setApplicationDatetime(String applicationDatetime) {
            this.applicationDatetime = applicationDatetime;
        }

        public String getProjectName() {
            return projectName;
        }

        public void setProjectName(String projectName) {
            this.projectName = projectName;
        }

        public String getWarehouseName() {
            return warehouseName;
        }

        public void setWarehouseName(String warehouseName) {
            this.warehouseName = warehouseName;
        }

        public String getOrganizationName() {
            return organizationName;
        }

        public void setOrganizationName(String organizationName) {
            this.organizationName = organizationName;
        }

        public String getApplicationAccountName() {
            return applicationAccountName;
        }

        public void setApplicationAccountName(String applicationAccountName) {
            this.applicationAccountName = applicationAccountName;
        }

        public Object getHouseUserName() {
            return houseUserName;
        }

        public void setHouseUserName(Object houseUserName) {
            this.houseUserName = houseUserName;
        }

        public String getEntryTypeName() {
            return entryTypeName;
        }

        public void setEntryTypeName(String entryTypeName) {
            this.entryTypeName = entryTypeName;
        }
    }
}
