package cn.km.warehouse.widget;

import android.animation.FloatEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.ActionBar;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import cn.km.warehouse.R;
import cn.km.warehouse.utils.DimensionUtils;

/**
 * Created by 075100 on 2019/8/2 0002.
 */

public class CustomProgressDialog extends Dialog
        implements IProgressChangeView{

    /**显示提示内容*/
    TextView txtMsg ;

    String tipMsg;

    public CustomProgressDialog(Context context, String tipMsg){
        super(context, R.style.BaseDialog);
        this.tipMsg = tipMsg;
        LinearLayout rootLinear = new LinearLayout(context);

        rootLinear.setBackgroundResource(R.drawable.viewjar_wait_bg);
        rootLinear.setOrientation(LinearLayout.VERTICAL);
        rootLinear.setGravity(Gravity.CENTER);
        //内边距值
        int paddingValue = 10;
        float scale = context.getResources().getDisplayMetrics().density;
        int paddingCaculated = (int) (paddingValue * scale + 0.5f * (paddingValue >= 0 ? 1 : -1));
        rootLinear.setPadding(paddingCaculated, paddingCaculated, paddingCaculated, paddingCaculated);

        ImageView imgv = new ImageView(context);
        imgv.setLayoutParams(new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT));
        imgv.setBackgroundResource(R.drawable.dash_circle);
        ValueAnimator rotation = ObjectAnimator.ofFloat(imgv,"Rotation", 0f, 360f);
        rotation.setDuration(1000);
        rotation.setEvaluator(new FloatEvaluator());
        rotation.setRepeatMode(ValueAnimator.RESTART);
        rotation.setRepeatCount(ValueAnimator.INFINITE);
        rotation.setInterpolator(new LinearInterpolator());
        rotation.setStartDelay(0L);
        rotation.start();

        txtMsg = new TextView(context);
        txtMsg.setText(tipMsg);
        txtMsg.setLayoutParams( new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT));
        txtMsg.setTextColor(Color.WHITE);
        txtMsg.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        txtMsg.setPadding(0, DimensionUtils.dip2Px(context,20), 0, 0);

        rootLinear.addView(imgv);
        rootLinear.addView(txtMsg);

        int width = DimensionUtils.dip2Px(context,210);
        setContentView(rootLinear,new ActionBar.LayoutParams(width, width));

        setCancelable(true);
        setCanceledOnTouchOutside(true);
    }

    public CustomProgressDialog(Context context) {
        this(context, "请稍候...");
    }

    public void setMessage(String msg) {
        txtMsg.setText(msg);
    }

    @Override
    public void initProgressMax(long progressMax) {}

    @Override
    public void onProgressChanged(long total, long current, int ratio) {}

    @Override
    public void onFinish() {
        dismiss();
    }

    @Override
    public void dismiss() {
        try {
            super.dismiss();
        } catch (RuntimeException e) {
            //handle exception
        }
    }

    @Override
    public void show() {
        try {
            super.show();
        } catch (RuntimeException e) {
            //handle exception
        }
    }

}
