package cn.km.warehouse.widget;

/**
 * Created by 075100 on 2019/8/2 0002.
 */

public interface IProgressChangeView {
    public void initProgressMax(long progressMax);

    public void onProgressChanged(long total, long current, int ratio);

    public void onFinish();
}
