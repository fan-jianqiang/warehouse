package cn.km.warehouse.login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import cn.km.warehouse.Configuration;
import cn.km.warehouse.Contstant;
import cn.km.warehouse.R;
import cn.km.warehouse.base.BaseActivity;
import cn.km.warehouse.model.LoginModel;
import cn.km.warehouse.uhf.UHFMainActivity;
import cn.km.warehouse.uhf.util.LabelReadOrWriteUtils;
import cn.km.warehouse.ui.MainActivity;
import cn.km.warehouse.ui.TestActivity;
import cn.km.warehouse.ui.UHFTestActivity;
import cn.km.warehouse.ui.customview.ProgressLoadingLayout;
import cn.km.warehouse.utils.SPUtils;
import cn.km.warehouse.widget.CustomProgressDialog;

public class LoginActivity extends BaseActivity implements LoginView ,View.OnClickListener  {
   private LinearLayout mLinearLayout;
   private LoginPresenter loginPresenter;
   private EditText mEditextName;
   private EditText mEditPassword;
    private boolean showProgressDiallog=true;
    private CustomProgressDialog mDialog;
    private LinearLayout mLinearLayoutSM;
    private LinearLayout mLinearLayoutce;
    private TextView mTextViewSetIp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loginPresenter=new LoginPresenter(this);
        mLinearLayout=this.findViewById(R.id.layoutLogin);
        mEditextName=this.findViewById(R.id.etAccount);
        mEditPassword=this.findViewById(R.id.etPassword);
        mLinearLayout.setOnClickListener(this);
        mLinearLayoutSM=this.findViewById(R.id.layoutLoginsm);
        mLinearLayoutce=this.findViewById(R.id.layoutLogince);
        mTextViewSetIp=this.findViewById(R.id.textview_set_ip);
        mLinearLayoutSM.setOnClickListener(this);
        mTextViewSetIp.setOnClickListener(this);
        mLinearLayoutce.setOnClickListener(this);
        SPUtils.putBean(this, Contstant.user_key,null);
    }



    @Override
    protected void setStatusBar() {
        super.setStatusBar();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.layoutLogin:
                if (TextUtils.isEmpty(mEditextName.getText().toString().trim())){
                    Toast.makeText(this,"请输入账号",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(mEditPassword.getText().toString().trim())){
                    Toast.makeText(this,"请输入密码",Toast.LENGTH_SHORT).show();
                    return;
                }
               // if (Configuration.getInstance().getServiceUrl()==null || "")
                loginPresenter.getLogin(mEditextName.getText().toString().trim(),mEditPassword.getText().toString().trim());
                break;
            case R.id.textview_set_ip:
                Intent intent = new Intent(LoginActivity.this, ServerConfigActivtiy.class);
                startActivity(intent);
                break;
                case R.id.layoutLoginsm:
                    String read1= LabelReadOrWriteUtils.stringToHexString("1_S1234567890_1629009390131");
                    Log.e("read1-->",read1);
                    String substring = read1.substring(4, read1.length());
                    String readexport=LabelReadOrWriteUtils.hexStringToString(read1);
                    Log.e("readexport-->",readexport);
                    Log.e("time-->",System.currentTimeMillis()+"");
                    //startActivity(new Intent(LoginActivity.this, UHFTestActivity.class));
//                    Intent intent=new Intent();
//                    intent.setClass(this, UHFMainActivity.class);
//                    startActivity(intent);
                break;
                case R.id.layoutLogince:
                    startActivity(new Intent(LoginActivity.this, TestActivity.class));
//                    Intent intent=new Intent();
//                    intent.setClass(this, UHFMainActivity.class);
//                    startActivity(intent);
                break;

        }
    }


    @Override
    public void getLoginSuccess(LoginModel loginModel) {
         if (loginModel==null){
             Toast.makeText(this,"获取数据为空",Toast.LENGTH_SHORT).show();
             return;
         }
          SPUtils.putBean(this, Contstant.user_key,loginModel);

          startActivity(new Intent(LoginActivity.this,MainActivity.class));
          this.finish();
    }

    @Override
    public void getLoginFailer(String message) {
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading() {
        if(showProgressDiallog && mDialog == null){
            mDialog = new CustomProgressDialog(this);
            mDialog.setMessage("正在加载数据..");
            mDialog.show();
        }
    }

    @Override
    public void dismissLoading() {
        if(mDialog != null && mDialog.isShowing()){
            mDialog.dismiss();
            showProgressDiallog=false;
        }
    }

    @Override
    public void detach() {

    }
}
