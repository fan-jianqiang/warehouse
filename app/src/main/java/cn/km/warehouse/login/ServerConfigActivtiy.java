package cn.km.warehouse.login;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.drcnet.android.net.base.ApiUtil;

import cn.km.warehouse.Configuration;
import cn.km.warehouse.Contstant;
import cn.km.warehouse.R;
import cn.km.warehouse.base.BaseActivity;
import cn.km.warehouse.ui.customview.BaseDialog;
import cn.km.warehouse.ui.customview.MessageDialog;
import cn.km.warehouse.utils.SPUtils;

public class ServerConfigActivtiy extends BaseActivity implements View.OnClickListener {
    private TextView view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server_config_activtiy);
        view = (TextView) findViewById(R.id.server_url);
        view.setText(Configuration.getInstance().getServiceUrl());
        findViewById(R.id.config_confirm).setOnClickListener(this);

    }

    @Override
    protected void setStatusBar() {
        super.setStatusBar();
    }

    @Override
    public void onClick(View v) {
      switch (v.getId()){
          case R.id.config_confirm:
              final String url = view.getText().toString();

              if(TextUtils.isEmpty(url)) {
                  Toast.makeText(ServerConfigActivtiy.this, "地址为空，设置不生效", Toast.LENGTH_LONG).show();
                  return;
              }


//              if(!SPUtils.getParam(this,"path", Contstant.PROTOCOL + Contstant.HOST).equals(url)){
//
//              }
              MessageDialog dialog = new MessageDialog(ServerConfigActivtiy.this,"服务地址设置成功，点击确定返回！");
              dialog.setNegativeButton("确定", new BaseDialog.OnDialogButtonClickListener() {
                  @Override
                  public void onClick(Dialog dialog, View v) {
                      SPUtils.setParam(getApplicationContext(), "path", url);
//                      ApiUtil.Companion.getINSTANCE().refreshLoginServerConfig();
//                      ApiUtil.Companion.getINSTANCE().refreshServerConfig();

                      dialog.dismiss();
                      finish();
                  }
              });
              dialog.show();
              break;
      }
    }
}
