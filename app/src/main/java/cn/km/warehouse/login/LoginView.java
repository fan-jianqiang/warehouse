package cn.km.warehouse.login;

import com.drcnet.android.mvp.base.BaseView;

import cn.km.warehouse.model.LoginModel;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/2/1
 */
public interface LoginView extends BaseView {
    public void getLoginSuccess(LoginModel loginModel);
    public void getLoginFailer(String message);
}
