package cn.km.warehouse.login;

import android.content.Context;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

//import com.hkst.dblib.lib.myAppLib;
//import com.hkst.dblib.lib.myAppLibListener;

import com.drcnet.android.mvp.base.BasePresenter;
import com.drcnet.android.net.base.ApiUtil;
import com.drcnet.android.net.base.NormalHandle;
import com.drcnet.android.net.data.DataApi;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import cn.km.warehouse.Contstant;
import cn.km.warehouse.model.LoginModel;
import cn.km.warehouse.utils.MD5Utils;


public class LoginPresenter extends BasePresenter<LoginView> {


    public LoginPresenter(@NotNull LoginView loginView) {
        super(loginView);
    }
    public void getLogin(String accountName,String accountPassword) {
       String word= MD5Utils.stringToMD5(accountPassword);
        getV().showLoading();
        getCompositeDisposable().add(ApiUtil.Companion.getINSTANCE()
                .request(ApiUtil.Companion.getINSTANCE().getApiRetrofitLogin().create(DataApi.class)
                        .getLogin(accountName, MD5Utils.stringToMD5(accountPassword)), new NormalHandle() {
                    @Override
                    public void onSuccess(@Nullable JsonElement data) {
                      LoginModel loginModel=new Gson().fromJson(data,new TypeToken<LoginModel>(){}.getType());
                        Log.e("LOIGN--->",loginModel.toString());
                        getV().getLoginSuccess(loginModel);
                        getV().dismissLoading();
                    }

                    @Override
                    public void onLogicFailed(int code, @Nullable String message) {
                        super.onLogicFailed(code, message);
                        getV().getLoginFailer(message);
                        getV().dismissLoading();
                    }

                    @Override
                    public void onComplete() {
                        super.onComplete();
                    }
                }));
    }
    @Override
    public void detach() {

    }
}
