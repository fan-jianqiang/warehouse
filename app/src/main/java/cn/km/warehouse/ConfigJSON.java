package cn.km.warehouse;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2020/9/15
 */
public class ConfigJSON {
    public static final String MachineMangerJSON="[\n" +
            "  {\n" +
            "    \"ELEMENT_ID\": \"00001\",\n" +
            "    \"ELEMENT_TYPE\": \"TEXT_TEXTVIEW\",\n" +
            "    \"ElEMENT_MC\":\"设备名称\",\n" +
            "     \"ElEMENT_HINT\":\"2米远控1号\",\n" +
            "    \"ElEMENT_VALUE\": \"\"\n" +
            "  },\n" +
            "  {\n" +
            "     \"ELEMENT_ID\": \"00002\",\n" +
            "     \"ELEMENT_TYPE\": \"TEXT_TEXTVIEW\",\n" +
            "     \"ElEMENT_MC\":\"设备类型\",\n" +
            "     \"ElEMENT_HINT\":\"2米标定\",\n" +
            "     \"ElEMENT_VALUE\": \"\"\n" +
            "   },\n" +
            "  {\n" +
            "     \"ELEMENT_ID\": \"00003\",\n" +
            "     \"ELEMENT_TYPE\": \"TEXT_TEXTVIEW\",\n" +
            "     \"ElEMENT_MC\":\"设备尺寸\",\n" +
            "     \"ElEMENT_HINT\":\"2米\",\n" +
            "     \"ElEMENT_VALUE\": \"\"\n" +
            "   },\n" +
            "  {\n" +
            "     \"ELEMENT_ID\": \"00004\",\n" +
            "     \"ELEMENT_TYPE\": \"TEXT_TEXTVIEW\",\n" +
            "     \"ElEMENT_MC\":\"所属部门\",\n" +
            "      \"ElEMENT_HINT\":\"国务院\",\n" +
            "     \"ElEMENT_VALUE\": \"\"\n" +
            "   },\n" +
            "    {\n" +
            "       \"ELEMENT_ID\": \"00005\",\n" +
            "       \"ELEMENT_TYPE\": \"TEXT_TEXTVIEW\",\n" +
            "       \"ElEMENT_MC\":\"布置经度\",\n" +
            "        \"ElEMENT_HINT\":\"经度\",\n" +
            "       \"ElEMENT_VALUE\": \"\"\n" +
            "     },\n" +
            "  {\n" +
            "         \"ELEMENT_ID\": \"00006\",\n" +
            "         \"ELEMENT_TYPE\": \"TEXT_TEXTVIEW\",\n" +
            "         \"ElEMENT_MC\":\"布置纬度\",\n" +
            "          \"ElEMENT_HINT\":\"纬度\",\n" +
            "         \"ElEMENT_VALUE\": \"\"\n" +
            "       },\n" +
            "   {\n" +
            "      \"ELEMENT_ID\": \"00007\",\n" +
            "      \"ELEMENT_TYPE\": \"TEXT_EDITEXT\",\n" +
            "      \"ElEMENT_MC\":\"俯仰角上限\",\n" +
            "      \"ElEMENT_VALUE\": \"\"\n" +
            "    },\n" +
            "   {\n" +
            "      \"ELEMENT_ID\": \"00008\",\n" +
            "      \"ELEMENT_TYPE\": \"TEXT_EDITEXT\",\n" +
            "      \"ElEMENT_MC\":\"俯仰角下限\",\n" +
            "      \"ElEMENT_VALUE\": \"\"\n" +
            "    },\n" +
            "  {\n" +
            "     \"ELEMENT_ID\": \"00001\",\n" +
            "     \"ELEMENT_TYPE\": \"TEXT_EDITEXT\",\n" +
            "     \"ElEMENT_MC\":\"设备名称\",\n" +
            "     \"ElEMENT_VALUE\": \"\"\n" +
            "   }\n" +
            "\n" +
            "]";
}
