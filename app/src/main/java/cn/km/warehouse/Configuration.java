package cn.km.warehouse;

import android.content.Context;

import cn.km.warehouse.utils.SPUtils;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/6/27
 */
public class Configuration {
    private Context mContext;

    public Context getmContext() {

        return mContext;
    }

    public void setmContext(Context mContext) {
        this.mContext = mContext;
    }

    /**
     * 获取移动端服务地址
     * @return
     */
    public String getServiceUrl(){
       String a= SPUtils.getParam(mContext, "path", Contstant.PROTOCOL + Contstant.HOST).toString();

        return SPUtils.getParam(mContext, "path", Contstant.PROTOCOL + Contstant.HOST).toString();
    }

//    /**
//     * 获取基础服务地址
//     * @return
//     */
//    public String getBaseServiceUrl(){
//        String url=SPUtils.getParam(mContext, "path", Contstant.PROTOCOL + Contstant.HOST).toString();
//        return SPUtils.getParam(mContext, "path", Contstant.PROTOCOL + Contstant.HOST).toString();
//    }


    private volatile static Configuration mInstance;
    //	private static LoginUser mLoginedUser;
    private Configuration( ){

    }

    public static Configuration getInstance(){
        if(mInstance == null){
            synchronized (Configuration.class) {
                if(mInstance == null){
                    mInstance = new Configuration();
                }
            }
        }
        return mInstance;
    }
    //
    //	public void initWithContext(Context context){
    //		mContext = context;
    //	}
    //
    //	/**
    //	 * 获取服务地址
    //	 * @return
    //	 */
    //	public String getServiceUrl(){
    //		SPUtils.getParam(mContext, "path", GlobalConstant.BASE_SERVER_URL);
    //		SharedPreferences preferences=mContext.getSharedPreferences("Path", Context.MODE_PRIVATE);
    //		String path=preferences.getString("path", null);
    //		if(WeekViewUtils.isEmpty(path)){
    //			return mContext.getString(R.string.oa_service_url);
    //		}
    //		return path+"/invoke";
    //	}
    //	/**
    //	 * 获取数据库类型判断
    //	 * @return
    //	 */
    //	public String getDatabaseEd(){
    //		return mContext.getString(R.string.choose_database);
    //	}
    //	/**
    //	 * 获取消息提示间隔时间
    //	 * @return
    //	 */
    //	public long getUpdataTime(){
    //		long i=Integer.parseInt(mContext.getString(R.string.updata_time));
    //		return i;
    //	}
    //
    //	public Context getContext(){
    //		return mContext;
    //	}
    //
    //	/**
    //	 * 获取移动执法系统统一的文件夹根目录
    //	 * @return
    //	 */
    //	public String getFileRootPath(){
    //		String sdcardPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/";
    //		String rootpath = mContext.getString(R.string.ydbg_file_rootpath);
    //		String result = sdcardPath + rootpath;
    //		result = result.replace("//", "/");
    //		if(!result.endsWith("/")){
    //			result += "/";
    //		}
    //		File dir = new File(result);
    //		if(!dir.exists()){
    //			dir.mkdirs();
    //		}
    //		return result;
    //	}
    //
    //
    //	public void initLoginedUser(LoginUser loginedUser){
    //		mLoginedUser = loginedUser;
    //	}
    //
    //
    //	public LoginUser getLoginedUser(){
    //		if(mLoginedUser == null){
    //			//系统崩溃后，mLoginedUser会被回收，需要重新初始化
    //			mLoginedUser = LoginManager.getLastLoginedUser();
    //		}
    //		return mLoginedUser;
    //	}
    //
    //
    //	public void setRememb(Boolean isRememb)
    //	{
    //		SharedPreferences.Editor editor = WorkOAApplication.mPreferences.edit();
    //		editor.putBoolean("isRememb", isRememb);
    //		editor.commit();
    //	}
    //
    //	public void setAutoLogin(Boolean isAutoLogin)
    //	{
    //		SharedPreferences.Editor editor = WorkOAApplication.mPreferences.edit();
    //		editor.putBoolean("isAutoLogin", isAutoLogin);
    //		editor.commit();
    //	}
}
