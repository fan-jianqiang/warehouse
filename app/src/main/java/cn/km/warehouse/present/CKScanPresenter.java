package cn.km.warehouse.present;

import android.util.Log;

import com.drcnet.android.mvp.base.BasePresenter;
import com.drcnet.android.net.base.ApiUtil;
import com.drcnet.android.net.base.NormalHandle;
import com.drcnet.android.net.data.DataApi;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;

import cn.km.warehouse.model.CKScanGetDataMdoel;
import cn.km.warehouse.model.CKScanModel;
import cn.km.warehouse.model.CWModel;
import cn.km.warehouse.model.HJModel;
import cn.km.warehouse.model.RuKuDetailModel;
import cn.km.warehouse.model.VMaterialStock;
import cn.km.warehouse.view.CKScanView;
import cn.km.warehouse.view.RukuDetailView;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/13
 */
public class CKScanPresenter extends BasePresenter<CKScanView> {


    public CKScanPresenter(@NotNull CKScanView ckScanView) {
        super(ckScanView);
    }

    public void submit(int orderId, String rfid) {
        getV().showLoading();
        getCompositeDisposable().add(ApiUtil.Companion.getINSTANCE()
                .request(ApiUtil.Companion.getINSTANCE().getApiRetrofit().create(DataApi.class)
                        .submitCk(orderId, rfid), new NormalHandle() {
                    @Override
                    public void onSuccess(@Nullable JsonElement data) {
                        Log.e("tijiao-->", data.toString());
                        getV().submitSuccess("出库成功");

//                        RuKuDetailModel ruKuDetailModel = new Gson().fromJson(data, new TypeToken<RuKuDetailModel>() {
//                        }.getType());
//
//                        getV().getRuKuDetailSuccess(ruKuDetailModel);
//                        getV().dismissLoading();
                    }

                    @Override
                    public void onLogicFailed(int code, @Nullable String message) {
                        super.onLogicFailed(code, message);
                        getV().submitFailer(code, message);
                        getV().dismissLoading();
                    }
                }));
    }

    public void getSelectCKQD(int applicationId, int materialId) {
        getV().showLoading();
        getCompositeDisposable().add(ApiUtil.Companion.getINSTANCE()
                .request(ApiUtil.Companion.getINSTANCE().getApiRetrofit().create(DataApi.class)
                        .getSelectCKQD(applicationId, materialId), new NormalHandle() {
                    @Override
                    public void onSuccess(@Nullable JsonElement data) {
                        Log.e("tijiao-->", data.toString());
                        // getV().submitSuccess("出库成功");

                        CKScanModel ckScanModel = new Gson().fromJson(data, new TypeToken<CKScanModel>() {
                        }.getType());
                        getV().getSelectCKQDDataSuccess(ckScanModel);
                        getV().dismissLoading();
                    }

                    @Override
                    public void onLogicFailed(int code, @Nullable String message) {
                        super.onLogicFailed(code, message);
                        getV().getScanDataFailer(code, message);
                        getV().dismissLoading();
                    }
                }));
    }

    public void getCKScanData(String rfid) {
        getV().showLoading();
        getCompositeDisposable().add(ApiUtil.Companion.getINSTANCE()
                .request(ApiUtil.Companion.getINSTANCE().getApiRetrofit().create(DataApi.class)
                        .getCKScanData(rfid), new NormalHandle() {
                    @Override
                    public void onSuccess(@Nullable JsonElement data) {
                        Log.e("tijiao-->", data.toString());
                        //  getV().submitSuccess("出库成功");
                        getV().dismissLoading();
                        CKScanGetDataMdoel ckScanGetDataMdoel = new Gson().fromJson(data, new TypeToken<CKScanGetDataMdoel>() {
                        }.getType());

                        getV().getCKScanDataSuccess(ckScanGetDataMdoel);

                    }

                    @Override
                    public void onLogicFailed(int code, @Nullable String message) {
                        getV().dismissLoading();
                        super.onLogicFailed(code, message);

                        getV().getScanDataFailer(code, message);

                    }
                }));
    }

    @Override
    public void detach() {

    }
}
