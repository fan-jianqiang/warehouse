package cn.km.warehouse.present;

import com.drcnet.android.mvp.base.BasePresenter;
import com.drcnet.android.mvp.base.BaseView;

import org.jetbrains.annotations.NotNull;

import cn.km.warehouse.ui.InputActivity;

public class InputPresenter extends BasePresenter{


    public InputPresenter(@NotNull BaseView baseView) {
        super(baseView);
    }

    @Override
    public void detach() {

    }
}
