package cn.km.warehouse.present;

import android.util.Log;

import com.drcnet.android.mvp.base.BasePresenter;
import com.drcnet.android.net.base.ApiUtil;
import com.drcnet.android.net.base.NormalHandle;
import com.drcnet.android.net.data.DataApi;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;

import cn.km.warehouse.login.LoginView;
import cn.km.warehouse.model.CKModel;
import cn.km.warehouse.model.LoginModel;
import cn.km.warehouse.view.CKView;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/2/2
 */
public class CKPresneter extends BasePresenter<CKView> {


    public CKPresneter(@NotNull CKView ckView) {
        super(ckView);
    }
    public void getCKList() {
        getV().showLoading();
        getCompositeDisposable().add(ApiUtil.Companion.getINSTANCE()
                .request(ApiUtil.Companion.getINSTANCE().getApiRetrofit().create(DataApi.class)
                        .getCKList(), new NormalHandle() {
                    @Override
                    public void onSuccess(@Nullable JsonElement data) {
                        Log.e("测试数据---》",data.toString());
                        ArrayList<CKModel> ckModels = new Gson().fromJson(data, new TypeToken<ArrayList<CKModel>>() {
                        }.getType());
                        getV().getCKSuccess(ckModels);
                        getV().dismissLoading();
                    }

                    @Override
                    public void onLogicFailed(int code, @Nullable String message) {
                        super.onLogicFailed(code, message);
                        getV().getCKFailer(message);
                        getV().dismissLoading();
                    }

                }));
    }
    @Override
    public void detach() {

    }
}

