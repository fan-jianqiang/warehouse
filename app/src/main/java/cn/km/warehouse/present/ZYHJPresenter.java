package cn.km.warehouse.present;

import android.util.Log;

import com.drcnet.android.mvp.base.BasePresenter;
import com.drcnet.android.net.base.ApiUtil;
import com.drcnet.android.net.base.NormalHandle;
import com.drcnet.android.net.data.DataApi;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;

import cn.km.warehouse.model.CKDModel;
import cn.km.warehouse.model.CKListQDModel;
import cn.km.warehouse.model.CKQDModel;
import cn.km.warehouse.model.CKScanGetDataMdoel;
import cn.km.warehouse.model.HJModel;
import cn.km.warehouse.model.VMaterialStock;
import cn.km.warehouse.model.ZYHJScanModel;
import cn.km.warehouse.view.WLCKView;
import cn.km.warehouse.view.ZYHJView;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/17
 */
public class ZYHJPresenter extends BasePresenter<ZYHJView> {


    public ZYHJPresenter(@NotNull ZYHJView zyhjView) {
        super(zyhjView);
    }
    public void getCKScanData(int houseId,String rfid) {
        getV().showLoading();
        getCompositeDisposable().add(ApiUtil.Companion.getINSTANCE()
                .request(ApiUtil.Companion.getINSTANCE().getApiRetrofit().create(DataApi.class)
                        .getZYHJScan(houseId,rfid), new NormalHandle() {
                    @Override
                    public void onSuccess(@Nullable JsonElement data) {
                        Log.e("tijiao-->", data.toString());
                        //  getV().submitSuccess("出库成功");

                        ZYHJScanModel zyhjScanModel = new Gson().fromJson(data, new TypeToken<ZYHJScanModel>() {
                        }.getType());

                        getV().getScanDataSuccess(zyhjScanModel);
                        getV().dismissLoading();
                    }

                    @Override
                    public void onLogicFailed(int code, @Nullable String message) {
                        super.onLogicFailed(code, message);
                        getV().getScanDataFailure(code, message);
                        getV().dismissLoading();
                    }
                }));
    }
    public void getHJList(int houseId,int cwID) {
        getV().showLoading();
        getCompositeDisposable().add(ApiUtil.Companion.getINSTANCE()
                .request(ApiUtil.Companion.getINSTANCE().getApiRetrofit().create(DataApi.class)
                        .getHJ(houseId,cwID), new NormalHandle() {
                    @Override
                    public void onSuccess(@Nullable JsonElement data) {

                        ArrayList<HJModel> hjModels = new Gson().fromJson(data, new TypeToken<ArrayList<HJModel>>() {
                        }.getType());
                        getV().getHJSuccess(hjModels);
                        getV().dismissLoading();
                    }

                    @Override
                    public void onLogicFailed(int code, @Nullable String message) {
                        super.onLogicFailed(code, message);
//                        getV().getCKFailer(message);
//                        getV().dismissLoading();
                    }

                }));
    }
    public void submit(int moveInShelfId, String rfid) {
        getV().showLoading();
        getCompositeDisposable().add(ApiUtil.Companion.getINSTANCE()
                .request(ApiUtil.Companion.getINSTANCE().getApiRetrofit().create(DataApi.class)
                        .submitZYHJ(moveInShelfId,rfid), new NormalHandle() {
                    @Override
                    public void onSuccess(@Nullable JsonElement data) {
                        Log.e("tijiao-->",data.toString());
                        getV().getZYHJSubmitSuccess("转移成功");
                        getV().dismissLoading();
                    }

                    @Override
                    public void onLogicFailed(int code, @Nullable String message) {
                        super.onLogicFailed(code, message);
                        getV().ggetZYHJSubmitFailure(code,message);
                        getV().dismissLoading();
                    }
                }));
    }
    @Override
    public void detach() {

    }

}
