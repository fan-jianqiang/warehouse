package cn.km.warehouse.present;

import android.util.Log;

import com.drcnet.android.mvp.base.BasePresenter;
import com.drcnet.android.net.base.ApiUtil;
import com.drcnet.android.net.base.NormalHandle;
import com.drcnet.android.net.data.DataApi;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;

import cn.km.warehouse.model.CKModel;
import cn.km.warehouse.model.PDTaskModel;
import cn.km.warehouse.model.RKDXQModel;
import cn.km.warehouse.view.CKView;
import cn.km.warehouse.view.PDTaskView;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/7
 */
public class PDTaskPresenter extends BasePresenter<PDTaskView> {


    public PDTaskPresenter(@NotNull PDTaskView pdTaskView) {
        super(pdTaskView);
    }
    public void getPDTaskList(int pageIndex,int pageSize,int status,int totalCount) {
        getV().showLoading();
        if (status==2){
            getCompositeDisposable().add(ApiUtil.Companion.getINSTANCE()
                    .request(ApiUtil.Companion.getINSTANCE().getApiRetrofit().create(DataApi.class)
                            .getPDTaskListAll(pageIndex,pageSize,totalCount), new NormalHandle() {
                        @Override
                        public void onSuccess(@Nullable JsonElement data) {
                            Log.e("测试数据---》",data.toString());
                            PDTaskModel pdTaskModel=new Gson().fromJson(data,new TypeToken<PDTaskModel>(){}.getType());
//
                        getV().getPDTaskListSuccess(pdTaskModel);
                        getV().dismissLoading();
                        }

                        @Override
                        public void onLogicFailed(int code, @Nullable String message) {
                            super.onLogicFailed(code, message);
                            getV().getPDTaskListFailer(message);
                            getV().dismissLoading();
                        }

                    }));
        }else {
            getCompositeDisposable().add(ApiUtil.Companion.getINSTANCE()
                    .request(ApiUtil.Companion.getINSTANCE().getApiRetrofit().create(DataApi.class)
                            .getPDTaskList(pageIndex,pageSize,status,totalCount), new NormalHandle() {
                        @Override
                        public void onSuccess(@Nullable JsonElement data) {
                            Log.e("测试数据---》",data.toString());
                            PDTaskModel pdTaskModel=new Gson().fromJson(data,new TypeToken<PDTaskModel>(){}.getType());
//
                            getV().getPDTaskListSuccess(pdTaskModel);
                            getV().dismissLoading();
                        }

                        @Override
                        public void onLogicFailed(int code, @Nullable String message) {
                            super.onLogicFailed(code, message);
                            getV().getPDTaskListFailer(message);
                            getV().dismissLoading();
                        }

                    }));
        }

    }
    @Override
    public void detach() {

    }
}

