package cn.km.warehouse.present;

import android.util.Log;

import com.drcnet.android.mvp.base.BasePresenter;
import com.drcnet.android.net.base.ApiUtil;
import com.drcnet.android.net.base.NormalHandle;
import com.drcnet.android.net.data.DataApi;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import cn.km.warehouse.model.PDScanModel;
import cn.km.warehouse.model.PDTaskModel;
import cn.km.warehouse.view.PDScanView;
import cn.km.warehouse.view.PDTaskView;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/16
 */
public class PDScanPresenter extends BasePresenter<PDScanView> {


    public PDScanPresenter(@NotNull PDScanView pdScanView) {
        super(pdScanView);
    }
    public void getPDTaskScan(String rfid,int houseId,int taskId) {
        getV().showLoading();
            getCompositeDisposable().add(ApiUtil.Companion.getINSTANCE()
                    .request(ApiUtil.Companion.getINSTANCE().getApiRetrofit().create(DataApi.class)
                            .getPDTaskScan(rfid,houseId,taskId), new NormalHandle() {
                        @Override
                        public void onSuccess(@Nullable JsonElement data) {
                            Log.e("测试数据---》",data.toString());
                          //  PDTaskModel pdTaskModel=new Gson().fromJson(data,new TypeToken<PDTaskModel>(){}.getType());
//
                            PDScanModel pdScanModel=new Gson().fromJson(data,new TypeToken<PDScanModel>(){}.getType());
                            getV().getPDTaskScanSuccess( pdScanModel);
                            getV().dismissLoading();
                        }

                        @Override
                        public void onLogicFailed(int code, @Nullable String message) {
                            super.onLogicFailed(code, message);
                            getV().getPDTaskScanFailer(code,message);
                            getV().dismissLoading();
                        }

                    }));


    }

    @Override
    public void detach() {

    }
}


