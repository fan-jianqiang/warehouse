package cn.km.warehouse.present;

import android.util.Log;

import com.drcnet.android.mvp.base.BasePresenter;
import com.drcnet.android.net.base.ApiUtil;
import com.drcnet.android.net.base.NormalHandle;
import com.drcnet.android.net.data.DataApi;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import cn.km.warehouse.model.CKDXQModel;
import cn.km.warehouse.model.RKDXQModel;
import cn.km.warehouse.view.CKDXQView;
import cn.km.warehouse.view.RKDXQView;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/7
 */
public class CKDXQPresenter extends BasePresenter<CKDXQView> {


    public CKDXQPresenter(@NotNull CKDXQView ckdxqView) {
        super(ckdxqView);
    }

    public void getCKDXQ(int orderId) {
        getV().showLoading();
        getCompositeDisposable().add(ApiUtil.Companion.getINSTANCE()
                .request(ApiUtil.Companion.getINSTANCE().getApiRetrofit().create(DataApi.class)
                        .getCKDXQ(orderId), new NormalHandle() {
                    @Override
                    public void onSuccess(@Nullable JsonElement data) {
                        Log.e("data--->", data.toString());
                        CKDXQModel ckdxqModel=new Gson().fromJson(data,new TypeToken<CKDXQModel>(){}.getType());
//
                          getV().getCKXQDataSuccess(ckdxqModel);
                         // getV().dismissLoading();
                    }

                    @Override
                    public void onLogicFailed(int code, @Nullable String message) {
                        super.onLogicFailed(code, message);
                        getV().dismissLoading();
                    }
                }));
    }

    @Override
    public void detach() {

    }
}
