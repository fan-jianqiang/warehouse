package cn.km.warehouse.present;

import android.util.Log;

import androidx.fragment.app.Fragment;

import com.drcnet.android.mvp.base.BasePresenter;
import com.drcnet.android.net.base.ApiUtil;
import com.drcnet.android.net.base.NormalHandle;
import com.drcnet.android.net.data.DataApi;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;

import cn.km.warehouse.login.LoginView;
import cn.km.warehouse.model.LoginModel;
import cn.km.warehouse.model.RKListQDModel;
import cn.km.warehouse.model.RKlistModel;
//import cn.km.warehouse.ui.RukuListFragment;
import cn.km.warehouse.view.RukuListView;

public class RukuListPresenter extends BasePresenter<RukuListView> {


    public RukuListPresenter(@NotNull RukuListView rukuListView) {
        super(rukuListView);
    }
    public void getRKlist(int pageIndex,int pageSize,int houseId,int totalCount) {
        getV().showLoading();
        getCompositeDisposable().add(ApiUtil.Companion.getINSTANCE()
                .request(ApiUtil.Companion.getINSTANCE().getApiRetrofit().create(DataApi.class)
                        .getRuKuList(pageIndex,pageSize,houseId,totalCount), new NormalHandle() {
                    @Override
                    public void onSuccess(@Nullable JsonElement data) {
                        Log.e("data--->",data.toString());
                       RKlistModel rKlistModel=new Gson().fromJson(data,new TypeToken<RKlistModel>(){}.getType());

                        getV().getRuKuListSuccess(rKlistModel);
                        getV().dismissLoading();
                    }

                    @Override
                    public void onLogicFailed(int code, @Nullable String message) {
                        super.onLogicFailed(code, message);
                        getV().dismissLoading();
                    }
                }));
    }
    public void getRKDlist(int pageIndex,int pageSize,int houseId,int totalCount,int state) {
        getV().showLoading();
        getCompositeDisposable().add(ApiUtil.Companion.getINSTANCE()
                .request(ApiUtil.Companion.getINSTANCE().getApiRetrofit().create(DataApi.class)
                        .getRuKuDList(pageIndex,pageSize,houseId,totalCount,state), new NormalHandle() {
                    @Override
                    public void onSuccess(@Nullable JsonElement data) {
                        Log.e("data--->",data.toString());
                       RKlistModel rKlistModel=new Gson().fromJson(data,new TypeToken<RKlistModel>(){}.getType());

                        getV().getRuKuListSuccess(rKlistModel);
                        getV().dismissLoading();
                    }

                    @Override
                    public void onLogicFailed(int code, @Nullable String message) {
                        super.onLogicFailed(code, message);
                        getV().dismissLoading();
                    }
                }));
    }
    public void getRuKuListQD(int pageIndex,int pageSize,int applicationId,int totalCount) {
        getV().showLoading();
        getCompositeDisposable().add(ApiUtil.Companion.getINSTANCE()
                .request(ApiUtil.Companion.getINSTANCE().getApiRetrofit().create(DataApi.class)
                        .getRuKuListQD(pageIndex,pageSize,applicationId,totalCount), new NormalHandle() {
                    @Override
                    public void onSuccess(@Nullable JsonElement data) {
                        Log.e("data--qingdan->",data.toString());
                       RKListQDModel rkListQDModel=new Gson().fromJson(data,new TypeToken<RKListQDModel>(){}.getType());

                        getV().getRuKuListQDSuccess(rkListQDModel);
                        getV().dismissLoading();
                    }

                    @Override
                    public void onLogicFailed(int code, @Nullable String message) {
                        super.onLogicFailed(code, message);
                        getV().dismissLoading();
                    }
                }));
    }
    @Override
    public void detach() {

    }
}