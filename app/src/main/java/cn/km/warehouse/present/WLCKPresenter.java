package cn.km.warehouse.present;

import android.util.Log;

import com.drcnet.android.mvp.base.BasePresenter;
import com.drcnet.android.net.base.ApiUtil;
import com.drcnet.android.net.base.NormalHandle;
import com.drcnet.android.net.data.DataApi;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import cn.km.warehouse.model.CKDModel;
import cn.km.warehouse.model.CKListQDModel;
import cn.km.warehouse.model.CKQDModel;
import cn.km.warehouse.model.CKScanGetDataMdoel;
import cn.km.warehouse.model.RKListQDModel;
import cn.km.warehouse.model.RKlistModel;
import cn.km.warehouse.view.RukuListView;
import cn.km.warehouse.view.WLCKView;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/4
 */
public class WLCKPresenter extends BasePresenter<WLCKView> {


    public WLCKPresenter(@NotNull WLCKView wlckView) {
        super(wlckView);
    }
    public void getCKScanData(String rfid) {
        getV().showLoading();
        getCompositeDisposable().add(ApiUtil.Companion.getINSTANCE()
                .request(ApiUtil.Companion.getINSTANCE().getApiRetrofit().create(DataApi.class)
                        .getCKScanData(rfid), new NormalHandle() {
                    @Override
                    public void onSuccess(@Nullable JsonElement data) {
                        Log.e("tijiao-->", data.toString());
                        //  getV().submitSuccess("出库成功");

                        CKScanGetDataMdoel ckScanGetDataMdoel = new Gson().fromJson(data, new TypeToken<CKScanGetDataMdoel>() {
                        }.getType());

                        getV().getCKScanDataSuccess(ckScanGetDataMdoel);
                        getV().dismissLoading();
                    }

                    @Override
                    public void onLogicFailed(int code, @Nullable String message) {
                        super.onLogicFailed(code, message);
                        getV().getScanDataFailer(code, message);
                        getV().dismissLoading();
                    }
                }));
    }
    public void submit(int orderId, String rfid) {
        getV().showLoading();
        getCompositeDisposable().add(ApiUtil.Companion.getINSTANCE()
                .request(ApiUtil.Companion.getINSTANCE().getApiRetrofit().create(DataApi.class)
                        .submitCk(orderId, rfid), new NormalHandle() {
                    @Override
                    public void onSuccess(@Nullable JsonElement data) {
                        Log.e("tijiao-->", data.toString());
                        getV().submitSuccess("出库成功");
                        getV().dismissLoading();
//                        RuKuDetailModel ruKuDetailModel = new Gson().fromJson(data, new TypeToken<RuKuDetailModel>() {
//                        }.getType());
//
//                        getV().getRuKuDetailSuccess(ruKuDetailModel);
//                        getV().dismissLoading();
                    }

                    @Override
                    public void onLogicFailed(int code, @Nullable String message) {
                        super.onLogicFailed(code, message);
                        getV().submitFailer(code, message);
                        getV().dismissLoading();
                    }
                }));
    }

    public void getCKlist(int pageIndex, int pageSize, int houseId, int totalCount) {
        getV().showLoading();
        getCompositeDisposable().add(ApiUtil.Companion.getINSTANCE()
                .request(ApiUtil.Companion.getINSTANCE().getApiRetrofit().create(DataApi.class)
                        .getCKDList(pageIndex, pageSize, houseId, totalCount), new NormalHandle() {
                    @Override
                    public void onSuccess(@Nullable JsonElement data) {
                        Log.e("data--->", data.toString());
                        CKDModel ckdModel = new Gson().fromJson(data, new TypeToken<CKDModel>() {
                        }.getType());

                        getV().getCKListSuccess(ckdModel);
                        getV().dismissLoading();
                    }

                    @Override
                    public void onLogicFailed(int code, @Nullable String message) {
                        super.onLogicFailed(code, message);
                        getV().dismissLoading();
                    }
                }));
    }
    public void getCKQDlist(int pageIndex, int pageSize, int houseId, int totalCount,int state) {
        getV().showLoading();
        if (state==1){
            getCompositeDisposable().add(ApiUtil.Companion.getINSTANCE()
                    .request(ApiUtil.Companion.getINSTANCE().getApiRetrofit().create(DataApi.class)
                            .getCKDQDListALL(pageIndex, pageSize, houseId, totalCount), new NormalHandle() {
                        @Override
                        public void onSuccess(@Nullable JsonElement data) {
                            Log.e("data--->", data.toString());
                            CKQDModel ckqdModel = new Gson().fromJson(data, new TypeToken<CKQDModel>() {
                            }.getType());

                            getV().getCKQDSuccess(ckqdModel);
                            getV().dismissLoading();
                        }

                        @Override
                        public void onLogicFailed(int code, @Nullable String message) {
                            super.onLogicFailed(code, message);
                            getV().dismissLoading();
                        }
                    }));
        }else {
            getCompositeDisposable().add(ApiUtil.Companion.getINSTANCE()
                    .request(ApiUtil.Companion.getINSTANCE().getApiRetrofit().create(DataApi.class)
                            .getCKDQDList(pageIndex, pageSize, houseId, totalCount,state), new NormalHandle() {
                        @Override
                        public void onSuccess(@Nullable JsonElement data) {
                            Log.e("data--->", data.toString());
                            CKQDModel ckqdModel = new Gson().fromJson(data, new TypeToken<CKQDModel>() {
                            }.getType());

                            getV().getCKQDSuccess(ckqdModel);
                            getV().dismissLoading();
                        }

                        @Override
                        public void onLogicFailed(int code, @Nullable String message) {
                            super.onLogicFailed(code, message);
                            getV().dismissLoading();
                        }
                    }));
        }

    }

    public void getCuKuListQD(int pageIndex,int pageSize,int applicationId,int totalCount) {
        getV().showLoading();
        getCompositeDisposable().add(ApiUtil.Companion.getINSTANCE()
                .request(ApiUtil.Companion.getINSTANCE().getApiRetrofit().create(DataApi.class)
                        .getCKListQD(pageIndex,pageSize,applicationId,totalCount), new NormalHandle() {
                    @Override
                    public void onSuccess(@Nullable JsonElement data) {
                        CKListQDModel ckListQDModel=new Gson().fromJson(data,new TypeToken<CKListQDModel>(){}.getType());
                       getV().getCKListQDSuccess(ckListQDModel);
                        getV().dismissLoading();
                    }

                    @Override
                    public void onLogicFailed(int code, @Nullable String message) {
                        super.onLogicFailed(code, message);
                        getV().getCKListQDFailer(message);
                        getV().dismissLoading();
                    }
                }));
    }
    @Override
    public void detach() {

    }

}