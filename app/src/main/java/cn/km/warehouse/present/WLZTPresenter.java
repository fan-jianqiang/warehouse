package cn.km.warehouse.present;

import android.util.Log;

import com.drcnet.android.mvp.base.BasePresenter;
import com.drcnet.android.net.base.ApiUtil;
import com.drcnet.android.net.base.NormalHandle;
import com.drcnet.android.net.data.DataApi;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;

import cn.km.warehouse.model.HJModel;
import cn.km.warehouse.model.ZYHJScanModel;
import cn.km.warehouse.view.WLZTView;
import cn.km.warehouse.view.ZYHJView;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/17
 */
public class WLZTPresenter extends BasePresenter<WLZTView> {


    public WLZTPresenter(@NotNull WLZTView wlztView) {
        super(wlztView);
    }
    public void getCKScanData(int houseId,String rfid) {
        getV().showLoading();
        getCompositeDisposable().add(ApiUtil.Companion.getINSTANCE()
                .request(ApiUtil.Companion.getINSTANCE().getApiRetrofit().create(DataApi.class)
                        .getZYHJScan(houseId,rfid), new NormalHandle() {
                    @Override
                    public void onSuccess(@Nullable JsonElement data) {
                        Log.e("tijiao-->", data.toString());
                        //  getV().submitSuccess("出库成功");

                        ZYHJScanModel zyhjScanModel = new Gson().fromJson(data, new TypeToken<ZYHJScanModel>() {
                        }.getType());

                        getV().getScanDataSuccess(zyhjScanModel);
                        getV().dismissLoading();
                    }

                    @Override
                    public void onLogicFailed(int code, @Nullable String message) {
                        super.onLogicFailed(code, message);
                        getV().getScanDataFailure(code, message);
                        getV().dismissLoading();
                    }
                }));
    }

    public void submit(int state, String rfid) {
        getV().showLoading();
        getCompositeDisposable().add(ApiUtil.Companion.getINSTANCE()
                .request(ApiUtil.Companion.getINSTANCE().getApiRetrofit().create(DataApi.class)
                        .submitWLZT(state,rfid), new NormalHandle() {
                    @Override
                    public void onSuccess(@Nullable JsonElement data) {
                        Log.e("tijiao-->",data.toString());
                        getV().getWLZTSubmitSuccess("提交成功");
                        getV().dismissLoading();
                    }

                    @Override
                    public void onLogicFailed(int code, @Nullable String message) {
                        super.onLogicFailed(code, message);
                        getV().getWLZTSubmitFailure(code,message);
                        getV().dismissLoading();
                    }
                }));
    }
    @Override
    public void detach() {

    }

}

