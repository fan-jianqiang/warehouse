package cn.km.warehouse.present;

import android.util.Log;

import com.drcnet.android.mvp.base.BasePresenter;
import com.drcnet.android.net.base.ApiUtil;
import com.drcnet.android.net.base.NormalHandle;
import com.drcnet.android.net.data.DataApi;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import cn.km.warehouse.model.PDDetailModel;
import cn.km.warehouse.model.PDScanModel;
import cn.km.warehouse.model.PDTaskModel;
import cn.km.warehouse.view.PDTaskDetailView;
import cn.km.warehouse.view.PDTaskView;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/9
 */
public class PDTaskDetailPresenter extends BasePresenter<PDTaskDetailView> {


    public PDTaskDetailPresenter(@NotNull PDTaskDetailView pdTaskDetailView) {
        super(pdTaskDetailView);
    }
    public void submit(int taskId) {
        getV().showLoading();
        getCompositeDisposable().add(ApiUtil.Companion.getINSTANCE()
                .request(ApiUtil.Companion.getINSTANCE().getApiRetrofit().create(DataApi.class)
                        .getPDComplete(taskId), new NormalHandle() {
                    @Override
                    public void onSuccess(@Nullable JsonElement data) {
                        Log.e("测试数据---》",data.toString());
                        getV().getPDTaskCompleteSuccess("完成任务");
                   //     PDScanModel pdScanModel=new Gson().fromJson(data,new TypeToken<PDScanModel>(){}.getType());
                        getV().dismissLoading();
                    }

                    @Override
                    public void onLogicFailed(int code, @Nullable String message) {
                        super.onLogicFailed(code, message);
                        getV().getPDTaskCompleteFailure(code,message);
                        getV().dismissLoading();
                    }

                }));


    }
    public void getPDTaskDetailList(int pageIndex, int pageSize, int totalCount ,int taskId) {
        getV().showLoading();
        getCompositeDisposable().add(ApiUtil.Companion.getINSTANCE()
                .request(ApiUtil.Companion.getINSTANCE().getApiRetrofit().create(DataApi.class)
                        .getPDTaskDetailList(pageIndex, pageSize, totalCount,taskId), new NormalHandle() {
                    @Override
                    public void onSuccess(@Nullable JsonElement data) {
                        Log.e("测试数据---》", data.toString());
                        PDDetailModel pdDetailModel = new Gson().fromJson(data, new TypeToken<PDDetailModel>() {
                        }.getType());
//
                        getV().getPDTaskDetialListSuccess(pdDetailModel);
                        getV().dismissLoading();
                    }

                    @Override
                    public void onLogicFailed(int code, @Nullable String message) {
                        super.onLogicFailed(code, message);
                        getV().getPDTaskDetialListFailer(message);
                        getV().dismissLoading();
                    }

                }));

    }

    @Override
    public void detach() {

    }
}

