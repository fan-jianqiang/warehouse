package cn.km.warehouse.present;

import android.util.Log;

import com.drcnet.android.mvp.base.BasePresenter;
import com.drcnet.android.net.base.ApiUtil;
import com.drcnet.android.net.base.NormalHandle;
import com.drcnet.android.net.data.DataApi;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import cn.km.warehouse.model.RKDXQModel;
import cn.km.warehouse.model.RKListQDModel;
import cn.km.warehouse.model.RKlistModel;
import cn.km.warehouse.view.RKDXQView;
import cn.km.warehouse.view.RukuListView;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/3/4
 */
public class RKDXQPresenter extends BasePresenter<RKDXQView> {


    public RKDXQPresenter(@NotNull RKDXQView rkdxqView) {
        super(rkdxqView);
    }

    public void getRKDXQ(int orderId) {
        getV().showLoading();
        getCompositeDisposable().add(ApiUtil.Companion.getINSTANCE()
                .request(ApiUtil.Companion.getINSTANCE().getApiRetrofit().create(DataApi.class)
                        .getRKDXQ(orderId), new NormalHandle() {
                    @Override
                    public void onSuccess(@Nullable JsonElement data) {
                        Log.e("data--->", data.toString());
                      RKDXQModel rkdxqModel=new Gson().fromJson(data,new TypeToken<RKDXQModel>(){}.getType());
//
                        getV().getRKXQDataSuccess(rkdxqModel);
                      //  getV().dismissLoading();
                    }

                    @Override
                    public void onLogicFailed(int code, @Nullable String message) {
                        super.onLogicFailed(code, message);
                        getV().dismissLoading();
                    }
                }));
    }

    @Override
    public void detach() {

    }
}
