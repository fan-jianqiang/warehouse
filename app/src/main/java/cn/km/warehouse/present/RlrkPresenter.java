package cn.km.warehouse.present;

import android.util.Log;

import com.drcnet.android.mvp.base.BasePresenter;
import com.drcnet.android.net.base.ApiUtil;
import com.drcnet.android.net.base.NormalHandle;
import com.drcnet.android.net.data.DataApi;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;

import cn.km.warehouse.model.CKModel;
import cn.km.warehouse.model.CWModel;
import cn.km.warehouse.model.HJModel;
import cn.km.warehouse.model.RKListQDModel;
import cn.km.warehouse.model.RKlistModel;
import cn.km.warehouse.model.RuKuDetailModel;
import cn.km.warehouse.model.VMaterialStock;
import cn.km.warehouse.view.RukuDetailView;
import cn.km.warehouse.view.RukuListView;

/**
 * <p>Description：</p>
 *
 * @author FJianQiang
 * @since 2021/2/25
 */
public class RlrkPresenter extends BasePresenter<RukuDetailView> {


    public RlrkPresenter(@NotNull RukuDetailView rukuDetailView) {
        super(rukuDetailView);
    }

    public void getRKDetail(int billid) {
        getV().showLoading();
        getCompositeDisposable().add(ApiUtil.Companion.getINSTANCE()
                .request(ApiUtil.Companion.getINSTANCE().getApiRetrofit().create(DataApi.class)
                        .getRuKuDetail(billid), new NormalHandle() {
                    @Override
                    public void onSuccess(@Nullable JsonElement data) {
                        RuKuDetailModel ruKuDetailModel = new Gson().fromJson(data, new TypeToken<RuKuDetailModel>() {
                        }.getType());

                        getV().getRuKuDetailSuccess(ruKuDetailModel);
                        getV().dismissLoading();
                    }

                    @Override
                    public void onLogicFailed(int code, @Nullable String message) {
                        super.onLogicFailed(code, message);
                        getV().dismissLoading();
                    }
                }));
    }
    public void getCWList(int houseId) {
        getV().showLoading();
        getCompositeDisposable().add(ApiUtil.Companion.getINSTANCE()
                .request(ApiUtil.Companion.getINSTANCE().getApiRetrofit().create(DataApi.class)
                        .getCW(houseId), new NormalHandle() {
                    @Override
                    public void onSuccess(@Nullable JsonElement data) {
                        Log.e("cangwei---》",data.toString());
                        ArrayList<CWModel> cwModels = new Gson().fromJson(data, new TypeToken<ArrayList<CWModel>>() {
                        }.getType());
                        getV().getCWSuccess(cwModels);
                        getV().dismissLoading();
                    }

                    @Override
                    public void onLogicFailed(int code, @Nullable String message) {
                        super.onLogicFailed(code, message);
//                        getV().getCKFailer(message);
//                        getV().dismissLoading();
                    }

                }));
    }
    public void getHJList(int houseId,int cwID) {
        getV().showLoading();
        getCompositeDisposable().add(ApiUtil.Companion.getINSTANCE()
                .request(ApiUtil.Companion.getINSTANCE().getApiRetrofit().create(DataApi.class)
                        .getHJ(houseId,cwID), new NormalHandle() {
                    @Override
                    public void onSuccess(@Nullable JsonElement data) {

                        ArrayList<HJModel> hjModels = new Gson().fromJson(data, new TypeToken<ArrayList<HJModel>>() {
                        }.getType());
                        getV().getHJSuccess(hjModels);
                        getV().dismissLoading();
                    }

                    @Override
                    public void onLogicFailed(int code, @Nullable String message) {
                        super.onLogicFailed(code, message);
//                        getV().getCKFailer(message);
//                        getV().dismissLoading();
                    }

                }));
    }
    public void submit(ArrayList<VMaterialStock> billlist,int orderId) {
        getV().showLoading();
        getCompositeDisposable().add(ApiUtil.Companion.getINSTANCE()
                .request(ApiUtil.Companion.getINSTANCE().getApiRetrofit().create(DataApi.class)
                        .submitRK(billlist,orderId), new NormalHandle() {
                    @Override
                    public void onSuccess(@Nullable JsonElement data) {
                        Log.e("tijiao-->",data.toString());
                          getV().getTJSuccess("入库成功");

//                        RuKuDetailModel ruKuDetailModel = new Gson().fromJson(data, new TypeToken<RuKuDetailModel>() {
//                        }.getType());
//
//                        getV().getRuKuDetailSuccess(ruKuDetailModel);
//                        getV().dismissLoading();
                    }

                    @Override
                    public void onLogicFailed(int code, @Nullable String message) {
                        super.onLogicFailed(code, message);
                        getV().getTJFailer(code,message);
                        getV().dismissLoading();
                    }
                }));
    }
    @Override
    public void detach() {

    }
}